#!/bin/bash

set -e

if [[ ${1} == 'docker' ]]; then
  :
else
  . ./.env
fi

echo "endpoint: '${PRISMA_SERVER_HOSTNAME}:${PRISMA_SERVER_PORT}'" >prisma.yml

echo "datamodel:" >>prisma.yml
for x in $(tree -faiR datamodels/models/base | egrep '.+\.graphql$'); do
  echo "  -" ${x} >>prisma.yml
done

echo "generate:" >>prisma.yml
echo "  - generator: typescript-client" >>prisma.yml
echo "    output: ./src/generated/prisma-client/" >>prisma.yml

echo "hooks:" >>prisma.yml
echo "  post-deploy:" >>prisma.yml
echo "    - prisma generate" >>prisma.yml
echo "    - npx nexus-prisma-generate --client ./src/generated/prisma-client --output ./src/generated/nexus-prisma" >>prisma.yml

echo "primsa.yml generated"
