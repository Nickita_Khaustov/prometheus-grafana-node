#!/bin/sh

set -e

npm run prisma:generate-yml docker
npm run prisma:deploy

npm run start
