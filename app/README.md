# Intech Applications
Intech Applications

![Version](https://img.shields.io/badge/Version-v0.0.1-red.svg)
![Language](https://img.shields.io/badge/Language-TypeScript-blue.svg)
![Build](https://img.shields.io/badge/Build-Pass-green.svg)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Here are the major libraries to get this project running. Some of these libraries come from [JFrog Artifactory](https://intechww.jfrog.io/intechww/). We assume you already had JFrog setup on your local machine.

```
* graphql
* prisma
* prisma-client-lib
* nexus
* nexus-prisma
* graphql-yoga
```

For details please check `package.json`.

### Installation and Running

You can clone this repository and get it running on your local system by following below steps.

Please run below commands to clone this repository.

```sh
$ git clone https://bitbucket.org/intechww/intech-apps.git
$ cd intech-apps
```

After cloning the repository, you need to clone `datamodels` repository too. Use below command to initialize `datamodels` repository.

```sh
$ npm run datamodels:init
```

First of all you need to start prisma server. Set your database configurations in `docker-compose.yml` file and run below command.

```sh
$ docker-compose up -d
```

At this point prisma server should be up and running on port specified in `docker-compose.yml` and is connected with your configured database.
Now you should set environment variable to make your prisma application communicate with the prisma server. 
Make a copy of `.env.sample` and rename it to `.env` using below command and set the values for environment variables in it.

```sh
$ cp .env.sample .env
```

Install project dependencies (use below command for this).

```sh
$ npm install
```

After setting environment variables and installing dependencies data models can be deployed to prisma server. Please execute below command.

> Please note you should have ```tree``` utility installed on your machine before running this step.

```sh
$ npm run prisma
``` 

This will not only deploy data models in the database but it also generate required code in the `generated` folder.
The basic setup is now completed and if you check your prisma server URL you may see all the generated queries, mutations and subscriptions there.


To run the application server use below commands.

For production server:
```sh
$ npm start
```

For development server:
```sh
$ npm run dev
```

#### Building and Running Docker Image

In order to run the project via building docker image, you can use following commands.

To build docker image:

```sh
$ npm run docker:build
```

To run docker image:

```sh
$ docker run -ti -p [port]:[port] [repo-name]:[tag]
```

## Folder Structure
    
```
.
├── .gitignore                  # gitignore file
├── .dockerignore               # Files ignore while building docker image
├── .eslintignore               # Files ignore while running ESLint
├── .eslintrc.js                # ESLint configurations
├── .env.sample                 # Sample file for environment variables
├── .gitmodules                 # Git Submodule configurations
├── src                         # This folder contains all the application tier code/logic
├── datamodels                  # This folder contains all datamodel files
├── scripts                     # Shell scripts
├── Dockerfile                  # Dockerfile for building docker images
├── docker-compose.yml          # Docker compose file for running prisma server
├── package.json                # NPM Project configuration file containing basic project info and dependencies
├── package-lock.json           # NPM Lock file
├── tsconfig.json               # TypeScript configuration file
├── bitbucket-pipelines.yml     # Pipelines for Bitbucket
└── README.md                   # Documentation
```

## Running the tests

In order to run the test cases please use below command.

```sh
$ npm run test
```

## Deployment

Whenever a commit is made on **staging** branch a new docker image is build via Bitbucket pipelines and published on [JFrog Artifactory](https://intechww.jfrog.io/intechww/).
Please check `bitbucket-pipelines.yml` file for more details regarding packaging and deployment.

## Coding Standard

Not specified.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](/#). 

## Authors

**IIS - Intech Process Automation**
