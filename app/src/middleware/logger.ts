export const logInput = async (resolve: any, root: any, args: any, context: any, info: any) => {
  //  we can update create our own args/info objects containing some required metadata
  const startTime = Date.now();
  const result = await resolve(root, args, context, info);
  const endTime = Date.now();
  console.log(`Total query execution time: ${endTime - startTime}ms`);
  return result;
};

export const logResult = async (
  resolve: Function, root: any,
  args: any, context: any, info: any,
) => {
// we can check some metadata here from previous middleware and/or some auth data, versions, etc.
// middleware, worth taking a look: https://github.com/prisma-labs/graphql-middleware#awesome-middlewares-
  const result = await resolve(root, args, context, info);
  return result;
};
