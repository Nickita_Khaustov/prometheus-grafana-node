import { createLogger, format, transports } from 'winston';
import EnvService, { ENV_VARIABLES } from '../app-constants/env-service';

const logger = createLogger({
  level: 'info',
  format: format.json(),
  defaultMeta: { loggerType: 'main' },
  transports: [
    new transports.Console({
      format: format.simple(),
    }),
  ],
});

const logFilePath = EnvService.getEnvVarByKey(ENV_VARIABLES.LOG_FILE_PATH);
const fileLogs = EnvService.getEnvVarByKey(ENV_VARIABLES.FILE_LOGS);

if (fileLogs === 'true' && logFilePath) {
  logger.add(new transports.File({ filename: logFilePath, level: 'error' }));
}

export default logger;
