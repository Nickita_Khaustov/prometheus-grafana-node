import { Express } from 'express';
import { collectDefaultMetrics, register } from 'prom-client';
import * as gcStats from 'prometheus-gc-stats';

export default function runPrometheusMetricsCollecting(expressInstance: Express): void {
  collectDefaultMetrics();
  const startGcStats = gcStats(register, {
    prefix: 'nodejs_app_',
  });
  startGcStats();
  expressInstance
    .get('/metrics', (req, res) => {
      res.end(register.metrics());
    });
}
