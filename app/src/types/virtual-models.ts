import { objectType } from 'nexus';

export const pump = objectType({
  name: 'Pump',
  definition: t => {
    t.string('code');
    t.string('description');
    t.string('isDeleted');
    t.string('name');
    t.id('id');
    t.int('sortOrder');
    // t.list.field('children', { type: 'Equipment' });
    // t.list.field('equipmentClasses', { type: 'EquipmentClass' });
    // t.list.field('equipmentProperties', { type: 'EquipmentProperty' });
    // t.field('equipmentLevel', { type: 'Lookup' });
    // extra fields
    t.int('flow');
  },
});

export const IGNORE = 'default';
