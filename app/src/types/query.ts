import { prismaObjectType } from 'nexus-prisma';
import { Equipment } from 'generated/prisma-client';

export default prismaObjectType({
  name: 'Query',
  definition(t) {
    t.prismaFields(['*']);
    // custom query
    t.list.field('bearings', {
      type: 'Equipment',
      resolve: (_, args, ctx) => ctx.prisma.equipments(
        { where: { equipmentClasses_some: { code: 'bearing' } } },
      ),
    });

    // custom fetchEquipment Query
    t.list.field('pumps', {
      type: 'Pump',
      resolve: async (_, args, ctx) => {
        const resp = await ctx.prisma.equipments({ where: { equipmentClasses_some: { code: 'pump' } } });
        return resp.map((eq: Equipment) => ({
          ...eq,
          flow: 2,
        }));
      },
    });

    // In order to Override Default Equipment Type uncomment below code
    t.list.field('equipments', {
      type: 'Equipment',
      resolve: async (_, args, ctx) => {
        const result = await ctx.prisma.equipments();
        return result.map((eq: Equipment) => ({
          ...eq,
          name: `${eq.name} UPDATED`,
        }));
      },
    });
  },
});
