import { config as loadEnv } from 'dotenv';

loadEnv();

export enum ENV_VARIABLES {
  APPLICATION_PORT = 'APPLICATION_PORT',
  CORS = 'CORS',
  FILE_LOGS = 'FILE_LOGS',
  ENV = 'ENV',
  LOG_FILE_PATH = 'LOG_FILE_PATH'
}

export default class EnvService {
  public static getEnvVarByKey(key: ENV_VARIABLES): string {
    if (!process.env[key]) {
      throw new TypeError(`Expected ${key} to be present, got ${process.env[key]}`);
    } else {
      return process.env[key]!;
    }
  }
}
