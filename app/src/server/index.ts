import { GraphQLServer } from 'graphql-yoga';
import schema from 'schema';
import serverOptions from 'server/options';
import { prisma } from 'generated/prisma-client';
import { logInput, logResult } from '../middleware/logger';



const server = new GraphQLServer({
  schema,
  context: { prisma },
  middlewares: [logInput, logResult],
});

export default server;
export {
  serverOptions,
};