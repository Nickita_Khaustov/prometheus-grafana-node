import { Options } from 'graphql-yoga';
import EnvService, { ENV_VARIABLES } from '../app-constants/env-service';

const serverOptions: Options = {
  port: EnvService.getEnvVarByKey(ENV_VARIABLES.APPLICATION_PORT),
  cors: EnvService.getEnvVarByKey(ENV_VARIABLES.CORS) === 'true' ? { origin: '*' } : false,
};

export default serverOptions;
