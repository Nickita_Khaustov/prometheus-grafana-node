import { makePrismaSchema } from 'nexus-prisma';
import * as path from 'path';
import Mutation from 'types/mutation';
import { pump } from 'types/virtual-models';
import dataModelInfo from 'generated/nexus-prisma/datamodel-info';
import { prisma } from 'generated/prisma-client';
import Query from 'types/query';

export default makePrismaSchema({
  types: [Query, Mutation, pump],
  prisma: {
    datamodelInfo: dataModelInfo,
    client: prisma,
  },
  outputs: {
    schema: path.join(__dirname, '../generated/schema.graphql'),
    typegen: path.join(__dirname, '../generated/nexus.ts'),
  },
});
