import server, { serverOptions } from 'server';
import logger from 'logger';
import { Express } from 'express';
import EnvService, { ENV_VARIABLES } from './app-constants/env-service';
import runPrometheusMetricsCollecting from './run-prometheus-metrics-collecting';


if (require.main === module) {
  runPrometheusMetricsCollecting(server.express as Express);
  server
    .start(serverOptions)
    .then(() => logger.info(`
      Server is running on port ${EnvService.getEnvVarByKey(ENV_VARIABLES.APPLICATION_PORT)}
     `))
    .catch((error: Error) => logger.error('Unable to start server', error.toString()));
}
