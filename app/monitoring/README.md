## Prerequisites:
1. Any linux-based OS
2. Docker must be installed
3. PostgreSQL must be install
4. PostgreSQL user `grafanareader` must be created and must have
required permissions: 
```
/Applications/Postgres.app/Contents/Versions/12/bin/psql -p5432 "postgres";
CREATE USER grafanareader WITH PASSWORD 'password';
GRANT USAGE ON SCHEMA schema TO grafanareader;
GRANT SELECT ON schema.table TO grafanareader;
```
## Installation:
1. Install and run Prometheus in docker container: `docker run --name [prometheus] --net=host --restart=always -p 9090:9090 prom/prometheus  -d`;
2. Install and run Grafana in docker container: ` docker run -d --name grafana --net=host --restart=always -p 3000:3000 grafana/grafana`;
3. Create folder containing config files for Grafana and Prometheus (e.g. mkdir configs)
4. [Connect to DB from docker container](https://gist.github.com/MauricioMoraes/87d76577babd4e084cba70f63c04b07d)
5. 

### Plugins
1. Connect to PostgreSQL via exporter to get PostgreSQL metrics: `CREATE USER intech_user WITH SUPERUSER PASSWORD 'password'`;
