# --------- clean up docker imgs ---------
docker stop $(docker ps -a -q);
docker rm $(docker ps -a -q);

# --------- variables ---------
ID=$(id -u) # saves your user id in the ID variable
CONFIGS_PATH="/Users/user/configs"


# --------- grant read permissions db -----------
#/Applications/Postgres.app/Contents/Versions/12/bin/psql -p5432 "postgres";
#CREATE USER grafanareader WITH PASSWORD 'password';
#GRANT USAGE ON SCHEMA schema TO grafanareader;
#GRANT SELECT ON schema.table TO grafanareader;


# --------- run prometheus ---------
docker run --name prometheus_container -d --restart=always -p 9090:9090 prom/prometheus;
#  ---------create a persistent volume for your data in /var/lib/grafana (database and plugins) ---------
docker volume create grafana-storage;

# create config folder (test purpose below)
# cd ~;
# rm -rf configs && mkdir configs;

# --------- grant permissions ---------
#chown -R root:root /etc/grafana && \
#  chmod -R a+r /etc/grafana && \
#  chown -R grafana:grafana /var/lib/grafana && \
#  chown -R grafana:grafana /usr/share/grafana

# --------- starts grafana with your user id and using the data folder ---------
#docker run --name grafana_container -d \
# --user $ID --volume "$CONFIGS_PATH:$CONFIGS_PATH" -p 3000:3000 grafana/grafana \
#  --config "/Users/user/configs/config.ini";
docker run \
  --user $ID \
  --name grafana_container -d \
  --volume "/Users/user/configs:/etc/grafana" \
  --restart=always \
  -p 3000:3000 \
  -e "GF_PATHS_CONFIG=/etc/grafana/config.ini" grafana/grafana;

