import { cloneDeep } from 'lodash';
import { refineObject, getFragment } from './utils/helper';

const sample = {
  code: 'code',
  name: 'name',
  description: 'description',
  lookup: {
    create: {
      code: 'lkp_code',
      name: 'lkp_name',
      category: {
        connect: {
          code: 'lkp_cat_code',
          name: 'lkp_cat_name',
        },
      },
    },
  },
  properties: {
    create: [
      {
        code: 'prop_1_code',
        name: 'prop_1_name',
        lookup: {
          create: {
            code: 'lkp_code',
            name: 'lkp_name',
          },
        },
      },
      {
        code: 'prop_2_code',
        name: 'prop_2_name',
        description: 'prop_2_description',
      },
    ],
  },
};

const refinedSample = {
  code: 'code',
  name: 'name',
  description: 'description',
  lookup: {
    code: 'lkp_code',
    name: 'lkp_name',
    category: {
      code: 'lkp_cat_code',
      name: 'lkp_cat_name',
    },
  },
  properties: [
    {
      code: 'prop_1_code',
      name: 'prop_1_name',
      lookup: {
        code: 'lkp_code',
        name: 'lkp_name',
      },
    },
    {
      code: 'prop_2_code',
      name: 'prop_2_name',
      description: 'prop_2_description',
    },
  ],
};

const sampleFragment = 'fragment SampleFragment on Sample { code name description lookup { code name category { code name } } properties { code name lookup { code name } description } }';

test('Refine Object Method Test', () => {
  const object = cloneDeep(sample);
  const a = refineObject(object);
  expect(a).toStrictEqual(refinedSample);
});

test('Construct Fragment Method Test', () => {
  const result = getFragment(cloneDeep(sample), 'Sample');
  expect(result).toStrictEqual(sampleFragment);
});
