import { cloneDeep } from 'lodash';
import { segmentResponse } from '../../utils/data-provider/operations-performance-data';
import { SegmentResponseCreateInput, SegmentResponse, prisma } from '../../../generated/prisma-client/index';
import { getFragment, refineObject } from '../../utils/helper';


const resourceName = 'SegmentResponse';
const data: SegmentResponseCreateInput = {
  ...segmentResponse(),
};

test(`${resourceName} Model Test`, async () => {
  const result: SegmentResponse = await prisma
    .createSegmentResponse(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
