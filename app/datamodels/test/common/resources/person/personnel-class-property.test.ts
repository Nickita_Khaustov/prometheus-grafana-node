import { cloneDeep } from 'lodash';
import { PersonnelClassProperty, prisma } from '../../../../generated/prisma-client';
import { personnelClassProperty } from '../../../utils/data-provider/common-data';
import { getFragment, refineObject, getArray } from '../../../utils/helper';

const resourceName = 'PersonnelClassProperty';
const data = {
  ...personnelClassProperty(),
  parent: { create: personnelClassProperty() },
  children: { create: getArray(personnelClassProperty(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: PersonnelClassProperty = await prisma
    .createPersonnelClassProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
