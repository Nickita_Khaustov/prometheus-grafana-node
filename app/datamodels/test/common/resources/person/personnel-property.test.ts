import { cloneDeep } from 'lodash';
import { PersonProperty, prisma } from '../../../../generated/prisma-client';
import { personProperty } from '../../../utils/data-provider/common-data';
import { getFragment, refineObject, getArray } from '../../../utils/helper';

const resourceName = 'PersonProperty';
const data = {
  ...personProperty(),
  parent: { create: personProperty() },
  children: { create: getArray(personProperty(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: PersonProperty = await prisma
    .createPersonProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
