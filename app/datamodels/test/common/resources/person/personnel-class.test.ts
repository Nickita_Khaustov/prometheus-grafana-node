import { cloneDeep } from 'lodash';
import { PersonnelClass, prisma } from '../../../../generated/prisma-client';
import {
  basicPropertyFields,
  personnelClass,
  person,
} from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';

const resourceName = 'PersonnelClass';
const data = {
  ...personnelClass(),
  parent: { create: personnelClass() },
  children: { create: getArray(personnelClass(), 3) },
  personnelClassProperties: { create: getArray(basicPropertyFields(), 3) },
  persons: { create: getArray(person(), 2) },
};

test(`${resourceName} Model Test`, async () => {
  const result: PersonnelClass = await prisma
    .createPersonnelClass(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
