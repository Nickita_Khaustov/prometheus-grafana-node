import { cloneDeep } from 'lodash';
import { Person, prisma } from '../../../../generated/prisma-client';
import {
  basicPropertyFields,
  person,
  personnelClass,
} from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';

const resourceName = 'Person';
const data = {
  ...person(),
  personnelProperties: { create: getArray(basicPropertyFields(), 3) },
  personnelClasses: { create: getArray(personnelClass(), 2) },
};

test(`${resourceName} Model Test`, async () => {
  const result: Person = await prisma
    .createPerson(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
