import { cloneDeep } from 'lodash';
import { PhysicalAssetClassProperty, prisma } from '../../../../generated/prisma-client';
import { physicalAssetClassProperty } from '../../../utils/data-provider/common-data';
import { getFragment, refineObject, getArray } from '../../../utils/helper';

const resourceName = 'PhysicalAssetClassProperty';
const data = {
  ...physicalAssetClassProperty(),
  parent: { create: physicalAssetClassProperty() },
  children: { create: getArray(physicalAssetClassProperty(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAssetClassProperty = await prisma
    .createPhysicalAssetClassProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
