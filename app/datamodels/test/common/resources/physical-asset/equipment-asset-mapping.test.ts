import { cloneDeep } from 'lodash';
import { EquipmentAssetMapping, prisma } from '../../../../generated/prisma-client';
import { equipment, equipmentAssetMapping, physicalAsset } from '../../../utils/data-provider/common-data';
import { getFragment, refineObject } from '../../../utils/helper';

const resourceName = 'EquipmentAssetMapping';
const data = {
  ...equipmentAssetMapping(),
  equipment: { create: equipment() },
  physicalAsset: { create: physicalAsset() },
};

test(`${resourceName} Model Test`, async () => {
  const result: EquipmentAssetMapping = await prisma
    .createEquipmentAssetMapping(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
