import { cloneDeep } from 'lodash';
import { PhysicalAsset, prisma } from '../../../../generated/prisma-client';
import {
  basicPropertyFields,
  physicalAsset,
  physicalAssetClass,
  equipmentAssetMappingForPhysicalAsset,
} from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';

const resourceName = 'PhysicalAsset';
const data = {
  ...physicalAsset(),
  parent: { create: physicalAsset() },
  children: { create: getArray(physicalAsset(), 3) },
  physicalAssetProperties: { create: getArray(basicPropertyFields(), 3) },
  physicalAssetClasses: { create: getArray(physicalAssetClass(), 2) },
  equipments: { create: getArray(equipmentAssetMappingForPhysicalAsset(), 2) },
};

test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAsset = await prisma
    .createPhysicalAsset(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
