import { cloneDeep } from 'lodash';
import { PhysicalAssetProperty, prisma } from '../../../../generated/prisma-client';
import { physicalAssetProperty } from '../../../utils/data-provider/common-data';
import { getFragment, refineObject, getArray } from '../../../utils/helper';

const resourceName = 'PhysicalAssetProperty';
const data = {
  ...physicalAssetProperty(),
  parent: { create: physicalAssetProperty() },
  children: { create: getArray(physicalAssetProperty(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAssetProperty = await prisma
    .createPhysicalAssetProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
