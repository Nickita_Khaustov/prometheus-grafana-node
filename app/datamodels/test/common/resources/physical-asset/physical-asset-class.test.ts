import { cloneDeep } from 'lodash';
import { PhysicalAssetClass, prisma } from '../../../../generated/prisma-client';
import {
  basicPropertyFields,
  physicalAsset,
  physicalAssetClass,
} from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';

const resourceName = 'PhysicalAssetClass';
const data = {
  ...physicalAssetClass(),
  parent: { create: physicalAssetClass() },
  children: { create: getArray(physicalAssetClass(), 3) },
  physicalAssets: { create: getArray(physicalAsset(), 2) },
  physicalAssetClassProperties: { create: getArray(basicPropertyFields(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAssetClass = await prisma
    .createPhysicalAssetClass(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
