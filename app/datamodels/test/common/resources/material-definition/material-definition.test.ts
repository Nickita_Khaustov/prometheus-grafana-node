import { cloneDeep } from 'lodash';
import { MaterialDefinition, prisma } from '../../../../generated/prisma-client';
import {
  basicPropertyFields,
  materialDefinition,
  lookup,
  materialClass,
  materialAssemblyDefinition,
} from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';

const resourceName = 'MaterialDefinition';
const data = {
  ...materialDefinition(),
  assemblyRelationship: { create: lookup() },
  assemblyType: { create: lookup() },
  parent: { create: materialDefinition() },
  children: { create: getArray(materialDefinition(), 3) },
  materialDefinitionProperties: { create: getArray(basicPropertyFields(), 3) },
  materialClasses: { create: getArray(materialClass(), 2) },
  materialAssemblyDefinitions: { create: getArray(materialAssemblyDefinition(), 2) },
};

test(`${resourceName} Model Test`, async () => {
  const result: MaterialDefinition = await prisma
    .createMaterialDefinition(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
