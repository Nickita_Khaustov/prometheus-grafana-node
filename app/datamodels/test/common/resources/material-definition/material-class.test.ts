import { cloneDeep } from 'lodash';
import { MaterialClass, prisma } from '../../../../generated/prisma-client';
import {
  basicPropertyFields,
  materialClass,
  lookup,
  materialDefinition,
} from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';

const resourceName = 'MaterialClass';
const data = {
  ...materialClass(),
  assemblyRelationship: { create: lookup() },
  assemblyType: { create: lookup() },
  parent: { create: materialClass() },
  children: { create: getArray(materialClass(), 3) },
  materialClassProperties: { create: getArray(basicPropertyFields(), 3) },
  materialDefinitions: { create: getArray(materialDefinition(), 2) },
};

test(`${resourceName} Model Test`, async () => {
  const result: MaterialClass = await prisma
    .createMaterialClass(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
