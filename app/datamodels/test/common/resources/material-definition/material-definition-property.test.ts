import { cloneDeep } from 'lodash';
import { MaterialDefinitionProperty, prisma } from '../../../../generated/prisma-client';
import { materialDefinitionProperty } from '../../../utils/data-provider/common-data';
import { getFragment, refineObject, getArray } from '../../../utils/helper';

const resourceName = 'MaterialDefinitionProperty';
const data = {
  ...materialDefinitionProperty(),
  parent: { create: materialDefinitionProperty() },
  children: { create: getArray(materialDefinitionProperty(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: MaterialDefinitionProperty = await prisma
    .createMaterialDefinitionProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
