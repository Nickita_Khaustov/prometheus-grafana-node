import { cloneDeep } from 'lodash';
import { MaterialClassProperty, prisma } from '../../../../generated/prisma-client';
import { materialClassProperty } from '../../../utils/data-provider/common-data';
import { getFragment, refineObject, getArray } from '../../../utils/helper';

const resourceName = 'MaterialClassProperty';
const data = {
  ...materialClassProperty(),
  parent: { create: materialClassProperty() },
  children: { create: getArray(materialClassProperty(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: MaterialClassProperty = await prisma
    .createMaterialClassProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
