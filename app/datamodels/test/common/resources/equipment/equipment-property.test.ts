import { cloneDeep } from 'lodash';
import { EquipmentProperty, prisma } from '../../../../generated/prisma-client';
import { equipmentProperty } from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';


const resourceName = 'EquipmentProperty';
const data = {
  ...equipmentProperty(),
  parent: { create: equipmentProperty() },
  children: { create: getArray(equipmentProperty(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: EquipmentProperty = await prisma
    .createEquipmentProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
