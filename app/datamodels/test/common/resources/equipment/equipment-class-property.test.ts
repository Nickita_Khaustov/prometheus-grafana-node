import { cloneDeep } from 'lodash';
import { EquipmentClassProperty, prisma } from '../../../../generated/prisma-client';
import { equipmentClassProperty } from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';


const resourceName = 'EquipmentClassProperty';
const data = {
  ...equipmentClassProperty(),
  parent: { create: equipmentClassProperty() },
  children: { create: getArray(equipmentClassProperty(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: EquipmentClassProperty = await prisma
    .createEquipmentClassProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
