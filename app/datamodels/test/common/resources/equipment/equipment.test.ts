import { cloneDeep } from 'lodash';
import { Equipment, prisma } from '../../../../generated/prisma-client';
import {
  basicPropertyFields,
  equipment,
  equipmentClass,
  lookup,
  equipmentAssetMappingForEquipment,
} from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';


const resourceName = 'Equipment';
const data = {
  ...equipment(),
  equipmentLevel: { create: lookup() },
  parent: { create: equipment() },
  children: { create: getArray(equipment(), 3) },
  equipmentProperties: { create: getArray(basicPropertyFields(), 3) },
  equipmentClasses: { create: getArray(equipmentClass(), 2) },
  physicalAssets: { create: getArray(equipmentAssetMappingForEquipment(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: Equipment = await prisma
    .createEquipment(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
