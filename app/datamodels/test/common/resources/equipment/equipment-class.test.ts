import { cloneDeep } from 'lodash';
import { EquipmentClass, prisma } from '../../../../generated/prisma-client';
import {
  basicPropertyFields,
  equipment,
  equipmentClass,
  lookup,
} from '../../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';


const resourceName = 'EquipmentClass';
const data = {
  ...equipmentClass(),
  equipmentLevel: { create: lookup() },
  parent: { create: equipmentClass() },
  children: { create: getArray(equipmentClass(), 3) },
  equipmentClassProperties: { create: getArray(basicPropertyFields(), 3) },
  equipments: { create: getArray(equipment(), 2) },
};

test(`${resourceName} Model Test`, async () => {
  const result: EquipmentClass = await prisma
    .createEquipmentClass(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
