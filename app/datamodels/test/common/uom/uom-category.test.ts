import { cloneDeep } from 'lodash';
import { UomCategory, prisma } from '../../../generated/prisma-client';
import { uomCategory } from '../../utils/data-provider/common-data';
import { getFragment, refineObject } from '../../utils/helper';


const resourceName = 'UOMCategory';
const data = uomCategory();

test(`${resourceName} Model Test`, async () => {
  const result: UomCategory = await prisma
    .createUomCategory(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
