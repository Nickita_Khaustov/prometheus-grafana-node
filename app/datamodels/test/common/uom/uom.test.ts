import { cloneDeep } from 'lodash';
import { Uom, prisma } from '../../../generated/prisma-client';
import { uom } from '../../utils/data-provider/common-data';
import { getFragment, refineObject } from '../../utils/helper';


const resourceName = 'UOM';
const data = uom();

test(`${resourceName} Model Test`, async () => {
  const result: Uom = await prisma
    .createUom(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
