import { cloneDeep } from 'lodash';
import { workMaster } from '../../utils/data-provider/common-data';
import { WorkMasterCreateInput, WorkMaster, prisma } from '../../../generated/prisma-client/index';
import { getFragment, refineObject } from '../../utils/helper';


const resourceName = 'WorkMaster';
const data: WorkMasterCreateInput = workMaster();

test(`${resourceName} Model Test`, async () => {
  const result: WorkMaster = await prisma
    .createWorkMaster(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
