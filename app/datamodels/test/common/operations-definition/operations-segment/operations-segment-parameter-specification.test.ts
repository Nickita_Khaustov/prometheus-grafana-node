import { cloneDeep } from 'lodash';
import { getArray, getFragment, refineObject } from '../../../utils/helper';
import {
  OperationsSegmentParameterSpecification,
  OperationsSegmentParameterSpecificationCreateInput,
  prisma,
} from '../../../../generated/prisma-client';
import {
  operationsSegmentParameterSpecification,
} from '../../../utils/data-provider/operations-definition-data';

const resourceName = 'OperationsSegmentParameterSpecification';
const data: OperationsSegmentParameterSpecificationCreateInput = {
  ...operationsSegmentParameterSpecification(),
  children: { create: getArray(operationsSegmentParameterSpecification(), 3) },
  parent: { create: operationsSegmentParameterSpecification() },
};
test(`${resourceName} Model Test`, async () => {
  const result: OperationsSegmentParameterSpecification = await prisma
    .createOperationsSegmentParameterSpecification(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
