import { cloneDeep } from 'lodash';
import {
  OperationsSegment,
  OperationsSegmentCreateInput,
  prisma,
} from '../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../utils/helper';
import {
  equipmentSpecification, materialSpecification,
  operationsSegment, operationsSegmentDependency,
  operationsSegmentParameterSpecification,
  personnelSpecification, physicalAssetSpecification,
} from '../../../utils/data-provider/operations-definition-data';
import { processSegment } from '../../../utils/data-provider/process-segment-data';

const resourceName = 'OperationsSegment';
const data: OperationsSegmentCreateInput = {
  ...operationsSegment(),
  parent: { create: operationsSegment() },
  children: { create: getArray(operationsSegment(), 3) },
  parameterSpecifications: {
    create: getArray(operationsSegmentParameterSpecification(false), 2),
  },
  dependencies: { create: getArray(operationsSegmentDependency(), 3) },
  personnelSpecifications: {
    create: getArray(personnelSpecification(false), 2),
  },
  equipmentSpecifications: {
    create: getArray(equipmentSpecification(false), 2),
  },
  physicalAssetSpecifications: {
    create: getArray(physicalAssetSpecification(false), 2),
  },
  materialSpecifications: {
    create: getArray(materialSpecification(false), 2),
  },
  eligibleProcessSegments: {
    create: getArray(processSegment(), 3),
  },
};
test(`${resourceName} Model Test`, async () => {
  const result: OperationsSegment = await prisma
    .createOperationsSegment(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
