import { cloneDeep } from 'lodash';
import {
  OperationsSegmentDependency,
  OperationsSegmentDependencyCreateInput,
  prisma,
} from '../../../generated/prisma-client';
import { getFragment, refineObject } from '../../utils/helper';
import {
  operationsSegment,
  operationsSegmentDependency,
} from '../../utils/data-provider/operations-definition-data';

const resourceName = 'OperationsSegmentParameter';
const data: OperationsSegmentDependencyCreateInput = {
  ...operationsSegmentDependency(),
  segment: { create: operationsSegment() },
};
test(`${resourceName} Model Test`, async () => {
  const result: OperationsSegmentDependency = await prisma
    .createOperationsSegmentDependency(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
