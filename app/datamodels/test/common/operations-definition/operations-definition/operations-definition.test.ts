import { cloneDeep } from 'lodash';
import {
  OperationsDefinition,
  OperationsDefinitionCreateInput,
  prisma,
} from '../../../../generated/prisma-client';
import {
  operationsDefinition, operationsDefinitionProperty, operationsMaterialBill, operationsSegment,
} from '../../../utils/data-provider/operations-definition-data';
import { getArray, getFragment, refineObject } from '../../../utils/helper';

const resourceName = 'OperationsDefinition';
const data: OperationsDefinitionCreateInput = {
  ...operationsDefinition(),
  operationsSegments: {
    create: getArray(operationsSegment(false), 2),
  },
  operationsMaterialBills: {
    create: getArray(operationsMaterialBill(false), 2),
  },
  operationsDefinitionProperties: {
    create: getArray(operationsDefinitionProperty(false), 2),
  },
};

test(`${resourceName} Model Test`, async () => {
  const result: OperationsDefinition = await prisma
    .createOperationsDefinition(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
