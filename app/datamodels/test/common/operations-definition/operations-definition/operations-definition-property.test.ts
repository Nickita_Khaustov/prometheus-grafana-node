import { cloneDeep } from 'lodash';
import { getArray, getFragment, refineObject } from '../../../utils/helper';
import {
  OperationsDefinitionProperty,
  OperationsDefinitionPropertyCreateInput,
  prisma,
} from '../../../../generated/prisma-client';
import {
  operationsDefinitionProperty,
} from '../../../utils/data-provider/operations-definition-data';

const resourceName = 'OperationsDefinitionProperty';
const data: OperationsDefinitionPropertyCreateInput = {
  ...operationsDefinitionProperty(),
  children: { create: getArray(operationsDefinitionProperty(), 3) },
  parent: { create: operationsDefinitionProperty() },
};

test(`${resourceName} Model Test`, async () => {
  const result: OperationsDefinitionProperty = await prisma
    .createOperationsDefinitionProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
