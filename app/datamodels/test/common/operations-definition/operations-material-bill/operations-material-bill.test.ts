import { cloneDeep } from 'lodash';
import {
  OperationsMaterialBill,
  OperationsMaterialBillCreateInput,
  prisma,
} from '../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../utils/helper';
import {
  operationsMaterialBill,
  operationsMaterialBillItem,
} from '../../../utils/data-provider/operations-definition-data';

const resourceName = 'OperationsMaterialBill';
const data: OperationsMaterialBillCreateInput = {
  ...operationsMaterialBill(),
  operationsMaterialBillItems: {
    create: getArray(operationsMaterialBillItem(false), 2),
  },
};

test(`${resourceName} Model Test`, async () => {
  const result: OperationsMaterialBill = await prisma
    .createOperationsMaterialBill(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
