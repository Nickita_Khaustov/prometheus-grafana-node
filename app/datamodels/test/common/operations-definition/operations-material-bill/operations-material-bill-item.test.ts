import { cloneDeep } from 'lodash';
import {
  OperationsMaterialBillItem,
  OperationsMaterialBillItemCreateInput,
  prisma,
} from '../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../utils/helper';
import {
  operationsMaterialBillItem,
} from '../../../utils/data-provider/operations-definition-data';

const resourceName = 'OperationsMaterialBillItem';
const data: OperationsMaterialBillItemCreateInput = {
  ...operationsMaterialBillItem(),
  parent: { create: operationsMaterialBillItem() },
  children: { create: getArray(operationsMaterialBillItem(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: OperationsMaterialBillItem = await prisma
    .createOperationsMaterialBillItem(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
