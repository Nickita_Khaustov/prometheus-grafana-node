import { cloneDeep } from 'lodash';
import {
  PersonnelSpecificationProperty,
  PersonnelSpecificationPropertyCreateInput,
  prisma,
} from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  personnelSpecificationProperty,
} from '../../../../utils/data-provider/operations-definition-data';

const resourceName = 'PersonnelSpecificationProperty';
const data: PersonnelSpecificationPropertyCreateInput = {
  ...personnelSpecificationProperty(),
  children: { create: getArray(personnelSpecificationProperty(), 3) },
  parent: { create: personnelSpecificationProperty() },
};
test(`${resourceName} Model Test`, async () => {
  const result: PersonnelSpecificationProperty = await prisma
    .createPersonnelSpecificationProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
