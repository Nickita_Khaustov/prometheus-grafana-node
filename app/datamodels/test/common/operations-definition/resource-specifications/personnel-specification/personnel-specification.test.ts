import { cloneDeep } from 'lodash';
import {
  PersonnelSpecification,
  PersonnelSpecificationCreateInput,
  prisma,
} from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  personnelSpecification,
  personnelSpecificationProperty,
} from '../../../../utils/data-provider/operations-definition-data';

const resourceName = 'PersonnelSpecification';
const data: PersonnelSpecificationCreateInput = {
  ...personnelSpecification(),
  personnelSpecificationProperties: {
    create: getArray(personnelSpecificationProperty(false), 3),
  },
};
test(`${resourceName} Model Test`, async () => {
  const result: PersonnelSpecification = await prisma
    .createPersonnelSpecification(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
