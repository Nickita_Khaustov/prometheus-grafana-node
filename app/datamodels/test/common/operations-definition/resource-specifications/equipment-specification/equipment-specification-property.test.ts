import { cloneDeep } from 'lodash';
import {
  EquipmentSpecificationProperty,
  EquipmentSpecificationPropertyCreateInput,
  prisma,
} from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  equipmentSpecificationProperty,
} from '../../../../utils/data-provider/operations-definition-data';

const resourceName = 'EquipmentSpecificationProperty';
const data: EquipmentSpecificationPropertyCreateInput = {
  ...equipmentSpecificationProperty(),
  children: { create: getArray(equipmentSpecificationProperty(), 3) },
  parent: { create: equipmentSpecificationProperty() },
};
test(`${resourceName} Model Test`, async () => {
  const result: EquipmentSpecificationProperty = await prisma
    .createEquipmentSpecificationProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
