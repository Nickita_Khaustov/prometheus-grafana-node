import { cloneDeep } from 'lodash';
import {
  EquipmentSpecification,
  EquipmentSpecificationCreateInput,
  prisma,
} from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  equipmentSpecification,
  equipmentSpecificationProperty,
} from '../../../../utils/data-provider/operations-definition-data';

const resourceName = 'EquipmentSpecification';
const data: EquipmentSpecificationCreateInput = {
  ...equipmentSpecification(),
  equipmentSpecificationProperties: {
    create: getArray(equipmentSpecificationProperty(false), 3),
  },
};

test(`${resourceName} Model Test`, async () => {
  const result: EquipmentSpecification = await prisma
    .createEquipmentSpecification(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
