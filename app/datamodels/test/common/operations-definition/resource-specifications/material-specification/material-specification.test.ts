import { cloneDeep } from 'lodash';
import { MaterialSpecification, prisma } from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  materialSpecification,
  materialSpecificationProperty,
} from '../../../../utils/data-provider/operations-definition-data';

const resourceName = 'MaterialSpecification';
const data = {
  ...materialSpecification(),
  parent: { create: materialSpecification() },
  children: { create: getArray(materialSpecification(), 3) },
  materialSpecificationProperties: {
    create: getArray(materialSpecificationProperty(false), 3),
  },
};
test(`${resourceName} Model Test`, async () => {
  const result: MaterialSpecification = await prisma
    .createMaterialSpecification(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
