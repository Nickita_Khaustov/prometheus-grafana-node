import { cloneDeep } from 'lodash';
import {
  MaterialSpecificationProperty,
  MaterialSpecificationPropertyCreateInput,
  prisma,
} from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  materialSpecificationProperty,
} from '../../../../utils/data-provider/operations-definition-data';

const resourceName = 'MaterialSpecificationProperty';
const data: MaterialSpecificationPropertyCreateInput = {
  ...materialSpecificationProperty(),
  children: { create: getArray(materialSpecificationProperty(), 3) },
  parent: { create: materialSpecificationProperty() },
};
test(`${resourceName} Model Test`, async () => {
  const result: MaterialSpecificationProperty = await prisma
    .createMaterialSpecificationProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
