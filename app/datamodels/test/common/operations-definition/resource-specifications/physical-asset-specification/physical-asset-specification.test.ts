import { cloneDeep } from 'lodash';
import {
  PhysicalAssetSpecification,
  PhysicalAssetSpecificationCreateInput,
  prisma,
} from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  physicalAssetSpecification,
  physicalAssetSpecificationProperty,
} from '../../../../utils/data-provider/operations-definition-data';

const resourceName = 'PhysicalAssetSpecification';
const data: PhysicalAssetSpecificationCreateInput = {
  ...physicalAssetSpecification(),
  physicalAssetSpecificationProperties: {
    create: getArray(physicalAssetSpecificationProperty(false), 3),
  },
};
test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAssetSpecification = await prisma
    .createPhysicalAssetSpecification(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
