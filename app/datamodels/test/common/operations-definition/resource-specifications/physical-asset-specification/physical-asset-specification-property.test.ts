import { cloneDeep } from 'lodash';
import {
  PhysicalAssetSpecificationProperty,
  PhysicalAssetSpecificationPropertyCreateInput,
  prisma,
} from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  physicalAssetSpecificationProperty,
} from '../../../../utils/data-provider/operations-definition-data';

const resourceName = 'PhysicalAssetSpecificationProperty';
const data: PhysicalAssetSpecificationPropertyCreateInput = {
  ...physicalAssetSpecificationProperty(),
  children: { create: getArray(physicalAssetSpecificationProperty(), 3) },
  parent: { create: physicalAssetSpecificationProperty() },
};
test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAssetSpecificationProperty = await prisma
    .createPhysicalAssetSpecificationProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
