import { cloneDeep } from 'lodash';
import { LookupCategory, prisma } from '../../../generated/prisma-client';
import { lookupCategory } from '../../utils/data-provider/common-data';
import { getFragment, refineObject } from '../../utils/helper';


const resourceName = 'LookupCategory';
const data = lookupCategory();

test(`${resourceName} Model Test`, async () => {
  const result: LookupCategory = await prisma
    .createLookupCategory(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
