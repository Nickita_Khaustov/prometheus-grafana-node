import { cloneDeep } from 'lodash';
import { Lookup, prisma } from '../../../generated/prisma-client';
import { lookup } from '../../utils/data-provider/common-data';
import { getFragment, refineObject } from '../../utils/helper';


const resourceName = 'Lookup';
const data = lookup();

test(`${resourceName} Model Test`, async () => {
  const result: Lookup = await prisma
    .createLookup(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
