import { cloneDeep } from 'lodash';
import { ProcessSegment, prisma } from '../../../generated/prisma-client';
import { basicPropertyFields } from '../../utils/data-provider/common-data';
import {
  processSegment,
  processSegmentDependency,
  personnelSegmentSpecification,
  equipmentSegmentSpecification,
  materialSegmentSpecification,
  physicalAssetSegmentSpecification,
} from '../../utils/data-provider/process-segment-data';
import { operationsSegment } from '../../utils/data-provider/operations-definition-data';
import { getArray, getFragment, refineObject } from '../../utils/helper';


const resourceName = 'ProcessSegment';
const data = {
  ...processSegment(),
  parent: { create: processSegment() },
  children: { create: getArray(processSegment(), 3) },
  dependencies: { create: getArray(processSegmentDependency(), 3) },
  processSegmentParameters: { create: getArray(basicPropertyFields(), 2) },
  personnelSegmentSpecifications: {
    create: getArray(personnelSegmentSpecification(false), 2),
  },
  equipmentSegmentSpecifications: {
    create: getArray(equipmentSegmentSpecification(false), 2),
  },
  physicalAssetSegmentSpecifications: {
    create: getArray(physicalAssetSegmentSpecification(false), 2),
  },
  materialSegmentSpecifications: {
    create: getArray(materialSegmentSpecification(false), 2),
  },
  operationsSegments: {
    create: getArray(operationsSegment(), 3),
  },
};

test(`${resourceName} Model Test`, async () => {
  const result: ProcessSegment = await prisma
    .createProcessSegment(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
