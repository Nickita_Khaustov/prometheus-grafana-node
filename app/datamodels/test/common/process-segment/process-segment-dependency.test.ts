import { cloneDeep } from 'lodash';
import { ProcessSegmentDependency, prisma } from '../../../generated/prisma-client';
import {
  processSegmentDependency,
  processSegment,
} from '../../utils/data-provider/process-segment-data';
import { getFragment, refineObject } from '../../utils/helper';


const resourceName = 'ProcessSegmentParameter';
const data = {
  ...processSegmentDependency(),
  segment: { create: processSegment() },
};

test(`${resourceName} Model Test`, async () => {
  const result: ProcessSegmentDependency = await prisma
    .createProcessSegmentDependency(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
