import { cloneDeep } from 'lodash';
import { PersonnelSegmentSpecification, prisma } from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  personnelSegmentSpecification,
  resourceSegmentSpecificationProperty,
} from '../../../../utils/data-provider/process-segment-data';


const resourceName = 'PersonnelSegmentSpecification';
const data = {
  ...personnelSegmentSpecification(),
  personnelSegmentSpecificationProperties: {
    create: getArray(resourceSegmentSpecificationProperty(), 3),
  },
};

test(`${resourceName} Model Test`, async () => {
  const result: PersonnelSegmentSpecification = await prisma
    .createPersonnelSegmentSpecification(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
