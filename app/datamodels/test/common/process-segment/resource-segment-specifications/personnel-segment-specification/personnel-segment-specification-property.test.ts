import { cloneDeep } from 'lodash';
import { PersonnelSegmentSpecificationProperty, prisma } from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  personnelSegmentSpecificationProperty,
} from '../../../../utils/data-provider/process-segment-data';


const resourceName = 'PersonnelSegmentSpecificationProperty';
const data = {
  ...personnelSegmentSpecificationProperty(),
  children: { create: getArray(personnelSegmentSpecificationProperty(), 3) },
  parent: { create: personnelSegmentSpecificationProperty() },
};

test(`${resourceName} Model Test`, async () => {
  const result: PersonnelSegmentSpecificationProperty = await prisma
    .createPersonnelSegmentSpecificationProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
