import { cloneDeep } from 'lodash';
import { EquipmentSegmentSpecificationProperty, prisma } from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  equipmentSegmentSpecificationProperty,
} from '../../../../utils/data-provider/process-segment-data';


const resourceName = 'EquipmentSegmentSpecificationProperty';
const data = {
  ...equipmentSegmentSpecificationProperty(),
  children: { create: getArray(equipmentSegmentSpecificationProperty(), 3) },
  parent: { create: equipmentSegmentSpecificationProperty() },
};

test(`${resourceName} Model Test`, async () => {
  const result: EquipmentSegmentSpecificationProperty = await prisma
    .createEquipmentSegmentSpecificationProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
