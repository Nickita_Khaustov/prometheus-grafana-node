import { cloneDeep } from 'lodash';
import { EquipmentSegmentSpecification, prisma } from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  equipmentSegmentSpecification,
  resourceSegmentSpecificationProperty,
} from '../../../../utils/data-provider/process-segment-data';


const resourceName = 'EquipmentSegmentSpecification';
const data = {
  ...equipmentSegmentSpecification(),
  equipmentSegmentSpecificationProperties: {
    create: getArray(resourceSegmentSpecificationProperty(), 3),
  },
};

test(`${resourceName} Model Test`, async () => {
  const result: EquipmentSegmentSpecification = await prisma
    .createEquipmentSegmentSpecification(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
