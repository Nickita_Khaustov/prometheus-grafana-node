import { cloneDeep } from 'lodash';
import { PhysicalAssetSegmentSpecificationProperty, prisma } from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  physicalAssetSegmentSpecificationProperty,
} from '../../../../utils/data-provider/process-segment-data';


const resourceName = 'PhysicalAssetSegmentSpecificationProperty';
const data = {
  ...physicalAssetSegmentSpecificationProperty(),
  children: { create: getArray(physicalAssetSegmentSpecificationProperty(), 3) },
  parent: { create: physicalAssetSegmentSpecificationProperty() },
};

test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAssetSegmentSpecificationProperty = await prisma
    .createPhysicalAssetSegmentSpecificationProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
