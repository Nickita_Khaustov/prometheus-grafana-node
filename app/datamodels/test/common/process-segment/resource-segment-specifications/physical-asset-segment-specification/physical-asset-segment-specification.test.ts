import { cloneDeep } from 'lodash';
import { PhysicalAssetSegmentSpecification, prisma } from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  physicalAssetSegmentSpecification,
  resourceSegmentSpecificationProperty,
} from '../../../../utils/data-provider/process-segment-data';


const resourceName = 'PhysicalAssetSegmentSpecification';
const data = {
  ...physicalAssetSegmentSpecification(),
  physicalAssetSegmentSpecificationProperties: {
    create: getArray(resourceSegmentSpecificationProperty(), 3),
  },
};

test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAssetSegmentSpecification = await prisma
    .createPhysicalAssetSegmentSpecification(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
