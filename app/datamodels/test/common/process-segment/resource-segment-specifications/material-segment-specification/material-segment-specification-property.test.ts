import { cloneDeep } from 'lodash';
import { MaterialSegmentSpecificationProperty, prisma } from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  materialSegmentSpecificationProperty,
} from '../../../../utils/data-provider/process-segment-data';


const resourceName = 'MaterialSegmentSpecificationProperty';
const data = {
  ...materialSegmentSpecificationProperty(),
  children: { create: getArray(materialSegmentSpecificationProperty(), 3) },
  parent: { create: materialSegmentSpecificationProperty() },
};

test(`${resourceName} Model Test`, async () => {
  const result: MaterialSegmentSpecificationProperty = await prisma
    .createMaterialSegmentSpecificationProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
