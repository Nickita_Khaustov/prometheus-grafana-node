import { cloneDeep } from 'lodash';
import { MaterialSegmentSpecification, prisma } from '../../../../../generated/prisma-client';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';
import {
  materialSegmentSpecification,
  resourceSegmentSpecificationProperty,
} from '../../../../utils/data-provider/process-segment-data';


const resourceName = 'MaterialSegmentSpecification';
const data = {
  ...materialSegmentSpecification(),
  parent: { create: materialSegmentSpecification() },
  children: { create: getArray(materialSegmentSpecification(), 3) },
  materialSegmentSpecificationProperties: {
    create: getArray(resourceSegmentSpecificationProperty(), 3),
  },
};

test(`${resourceName} Model Test`, async () => {
  const result: MaterialSegmentSpecification = await prisma
    .createMaterialSegmentSpecification(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
