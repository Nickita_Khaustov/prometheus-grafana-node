import { cloneDeep } from 'lodash';
import { ProcessSegmentParameter, prisma } from '../../../generated/prisma-client';
import { processSegmentParameter } from '../../utils/data-provider/process-segment-data';
import { getArray, getFragment, refineObject } from '../../utils/helper';


const resourceName = 'ProcessSegmentParameter';
const data = {
  ...processSegmentParameter(),
  parent: { create: processSegmentParameter() },
  children: { create: getArray(processSegmentParameter(), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: ProcessSegmentParameter = await prisma
    .createProcessSegmentParameter(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
