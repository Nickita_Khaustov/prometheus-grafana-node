import { cloneDeep } from 'lodash';
import {
  MaterialLotPropertyCreateInput, MaterialLotProperty, prisma,
} from '../../../generated/prisma-client/index';

import { materialSubLot, materialLotProperty } from '../../utils/data-provider/common-data';
import { getArray, getFragment, refineObject } from '../../utils/helper';

const resourceName = 'MaterialLotProperty';
const data: MaterialLotPropertyCreateInput = {
  ...materialLotProperty(),
  materialSubLot: { create: materialSubLot() },
  parent: { create: materialLotProperty() },
  children: { create: getArray(materialLotProperty(), 3) },
};
test(`${resourceName} Model Test`, async () => {
  const result: MaterialLotProperty = await prisma
    .createMaterialLotProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
