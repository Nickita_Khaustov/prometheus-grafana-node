import { cloneDeep } from 'lodash';
import {
  materialSubLot,
  materialLot,
  materialLotProperty,
} from '../../utils/data-provider/common-data';
import { MaterialLotCreateInput, MaterialLot, prisma } from '../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../utils/helper';

const resourceName = 'MaterialLot';
const data: MaterialLotCreateInput = {
  ...materialLot(),
  parent: { create: materialLot() },
  children: { create: getArray(materialLot(), 3) },
  materialLotProperties: { create: getArray(materialLotProperty(false), 3) },
  materialSubLots: { create: getArray(materialSubLot(false), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: MaterialLot = await prisma
    .createMaterialLot(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
