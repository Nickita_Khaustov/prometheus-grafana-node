import { cloneDeep } from 'lodash';
import {
  materialSubLot,
  materialLotProperty,
} from '../../utils/data-provider/common-data';
import { prisma, MaterialSubLotCreateInput, MaterialSubLot } from '../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../utils/helper';

const resourceName = 'MaterialLot';
const data: MaterialSubLotCreateInput = {
  ...materialSubLot(),
  materialLotProperties: { create: getArray(materialLotProperty(true), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: MaterialSubLot = await prisma
    .createMaterialSubLot(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
