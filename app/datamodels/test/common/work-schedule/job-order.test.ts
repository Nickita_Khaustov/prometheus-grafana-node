import { cloneDeep } from 'lodash';
import {
  materialRequirement,
  physicalAssetRequirement,
  equipmentRequirement,
  personnelRequirement,
} from '../../utils/data-provider/operations-schedule-data';

import { JobOrderCreateInput, JobOrder, prisma } from '../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../utils/helper';
import { jobOrder } from '../../utils/data-provider/work-schedule-data';

const resourceName = 'JobOrder';
const data: JobOrderCreateInput = {
  ...jobOrder(),
  personnelRequirements: { create: getArray(personnelRequirement(true, false), 3) },
  equipmentRequirements: { create: getArray(equipmentRequirement(true, false), 3) },
  physicalAssetRequirements: { create: getArray(physicalAssetRequirement(true, false), 3) },
  materialRequirements: { create: getArray(materialRequirement(true, false), 3) },
};

test(`${resourceName} Model Test`, async () => {
  const result: JobOrder = await prisma
    .createJobOrder(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
