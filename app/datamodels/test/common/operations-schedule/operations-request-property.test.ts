import { cloneDeep } from 'lodash';
import { operationsRequestProperty } from '../../utils/data-provider/operations-schedule-data';
import { OperationsRequestPropertyCreateInput, OperationsRequestProperty, prisma } from '../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../utils/helper';

const resourceName = 'OperationsRequestProperty';
const data: OperationsRequestPropertyCreateInput = {
  ...operationsRequestProperty(),
  parent: { create: operationsRequestProperty() },
  children: { create: getArray(operationsRequestProperty(), 4) },
};
test(`${resourceName} Model Test`, async () => {
  const result: OperationsRequestProperty = await prisma
    .createOperationsRequestProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
