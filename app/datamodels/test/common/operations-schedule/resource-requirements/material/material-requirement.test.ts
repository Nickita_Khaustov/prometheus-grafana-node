import { cloneDeep } from 'lodash';
import { materialRequirement, materialRequirementProperty } from '../../../../utils/data-provider/operations-schedule-data';
import { MaterialRequirementCreateInput, MaterialRequirement, prisma } from '../../../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';

const resourceName = 'MaterialRequirement';
const data: MaterialRequirementCreateInput = {
  ...materialRequirement(),
  parent: { create: materialRequirement() },
  children: { create: getArray(materialRequirement(), 3) },
  materialRequirementProperties: { create: getArray(materialRequirementProperty(false), 3) },
};
test(`${resourceName} Model Test`, async () => {
  const result: MaterialRequirement = await prisma
    .createMaterialRequirement(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
