import { cloneDeep } from 'lodash';
import { materialRequirementProperty } from '../../../../utils/data-provider/operations-schedule-data';
import { MaterialRequirementPropertyCreateInput, MaterialRequirementProperty, prisma } from '../../../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';

const resourceName = 'MaterialRequirementProperty';
const data: MaterialRequirementPropertyCreateInput = {
  ...materialRequirementProperty(),
  parent: { create: materialRequirementProperty() },
  children: { create: getArray(materialRequirementProperty(), 4) },
};
test(`${resourceName} Model Test`, async () => {
  const result: MaterialRequirementProperty = await prisma
    .createMaterialRequirementProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
