import { cloneDeep } from 'lodash';
import { personnelRequirementProperty, personnelRequirement } from '../../../../utils/data-provider/operations-schedule-data';
import { PersonnelRequirementCreateInput, PersonnelRequirement, prisma } from '../../../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';

const resourceName = 'PersonnelRequirement';
const data: PersonnelRequirementCreateInput = {
  ...personnelRequirement(),
  personnelRequirementProperties: { create: getArray(personnelRequirementProperty(false), 3) },
};
test(`${resourceName} Model Test`, async () => {
  const result: PersonnelRequirement = await prisma
    .createPersonnelRequirement(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
