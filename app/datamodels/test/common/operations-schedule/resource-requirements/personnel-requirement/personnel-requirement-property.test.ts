import { cloneDeep } from 'lodash';
import { personnelRequirementProperty } from '../../../../utils/data-provider/operations-schedule-data';
import { PersonnelRequirementPropertyCreateInput, PersonnelRequirementProperty, prisma } from '../../../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';

const resourceName = 'PersonnelRequirementProperty';
const data: PersonnelRequirementPropertyCreateInput = {
  ...personnelRequirementProperty(),
  parent: { create: personnelRequirementProperty() },
  children: { create: getArray(personnelRequirementProperty(), 4) },
};
test(`${resourceName} Model Test`, async () => {
  const result: PersonnelRequirementProperty = await prisma
    .createPersonnelRequirementProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
