import { cloneDeep } from 'lodash';
import { physicalAssetRequirementProperty, physicalAssetRequirement } from '../../../../utils/data-provider/operations-schedule-data';
import { PhysicalAssetRequirementCreateInput, PhysicalAssetRequirement, prisma } from '../../../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';

const resourceName = 'PhysicalAssetRequirement';
const data: PhysicalAssetRequirementCreateInput = {
  ...physicalAssetRequirement(),
  physicalAssetRequirementProperties: {
    create: getArray(physicalAssetRequirementProperty(false), 3),
  },
};
test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAssetRequirement = await prisma
    .createPhysicalAssetRequirement(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
