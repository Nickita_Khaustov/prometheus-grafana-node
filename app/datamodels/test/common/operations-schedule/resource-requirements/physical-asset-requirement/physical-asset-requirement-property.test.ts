import { cloneDeep } from 'lodash';
import { physicalAssetRequirementProperty } from '../../../../utils/data-provider/operations-schedule-data';
import { PhysicalAssetRequirementPropertyCreateInput, PhysicalAssetRequirementProperty, prisma } from '../../../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';

const resourceName = 'PhysicalAssetRequirementProperty';
const data: PhysicalAssetRequirementPropertyCreateInput = {
  ...physicalAssetRequirementProperty(),
  parent: { create: physicalAssetRequirementProperty() },
  children: { create: getArray(physicalAssetRequirementProperty(), 4) },
};
test(`${resourceName} Model Test`, async () => {
  const result: PhysicalAssetRequirementProperty = await prisma
    .createPhysicalAssetRequirementProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
