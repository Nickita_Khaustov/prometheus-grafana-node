import { cloneDeep } from 'lodash';
import { equipmentRequirementProperty } from '../../../../utils/data-provider/operations-schedule-data';
import { EquipmentRequirementPropertyCreateInput, EquipmentRequirementProperty, prisma } from '../../../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';

const resourceName = 'EquipmentRequirementProperty';
const data: EquipmentRequirementPropertyCreateInput = {
  ...equipmentRequirementProperty(),
  parent: { create: equipmentRequirementProperty() },
  children: { create: getArray(equipmentRequirementProperty(), 4) },
};
test(`${resourceName} Model Test`, async () => {
  const result: EquipmentRequirementProperty = await prisma
    .createEquipmentRequirementProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
