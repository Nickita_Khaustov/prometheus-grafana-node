import { cloneDeep } from 'lodash';
import { equipmentRequirementProperty, equipmentRequirement } from '../../../../utils/data-provider/operations-schedule-data';
import { EquipmentRequirementCreateInput, EquipmentRequirement, prisma } from '../../../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../../../utils/helper';

const resourceName = 'EquipmentRequirement';
const data: EquipmentRequirementCreateInput = {
  ...equipmentRequirement(),
  equipmentRequirementProperties: { create: getArray(equipmentRequirementProperty(false), 3) },
};
test(`${resourceName} Model Test`, async () => {
  const result: EquipmentRequirement = await prisma
    .createEquipmentRequirement(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
