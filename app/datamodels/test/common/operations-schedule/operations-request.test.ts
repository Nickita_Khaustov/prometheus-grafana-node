import { cloneDeep } from 'lodash';
import { OperationsRequestCreateInput, OperationsRequest, prisma } from '../../../generated/prisma-client/index';
import {
  operationsRequest,
  operationsRequestProperty,
  segmentRequirement,
} from '../../utils/data-provider/operations-schedule-data';
import { getArray, getFragment, refineObject } from '../../utils/helper';

const resourceName = 'OperationsRequest';
const data: OperationsRequestCreateInput = {
  ...operationsRequest(),
  operationsRequestproperties: { create: getArray(operationsRequestProperty(false), 3) },
  segmentRequirements: { create: getArray(segmentRequirement(false), 3) },
};
test(`${resourceName} Model Test`, async () => {
  const result: OperationsRequest = await prisma
    .createOperationsRequest(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
