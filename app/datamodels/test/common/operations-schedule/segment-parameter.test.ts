import { cloneDeep } from 'lodash';
import { segmentParameter } from '../../utils/data-provider/operations-schedule-data';
import { SegmentParameterCreateInput, SegmentParameter, prisma } from '../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../utils/helper';

const resourceName = 'SegmentParameter';
const data: SegmentParameterCreateInput = {
  ...segmentParameter(),
  parent: { create: segmentParameter() },
  children: { create: getArray(segmentParameter(), 4) },
};
test(`${resourceName} Model Test`, async () => {
  const result: SegmentParameter = await prisma
    .createSegmentParameter(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
