import { cloneDeep } from 'lodash';
import {
  personnelRequirement,
  segmentParameter,
  segmentRequirement, equipmentRequirement, physicalAssetRequirement, materialRequirement,
} from '../../utils/data-provider/operations-schedule-data';
import {
  SegmentRequirementCreateInput, SegmentRequirement, prisma,
} from '../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../utils/helper';
import { segmentResponse } from '../../utils/data-provider/operations-performance-data';

const resourceName = 'SegmentRequirement';
const data: SegmentRequirementCreateInput = {
  ...segmentRequirement(),
  segmentParameters: { create: getArray(segmentParameter(false), 3) },
  personnelRequirements: { create: getArray(personnelRequirement(false), 3) },
  equipmentRequirements: { create: getArray(equipmentRequirement(false), 3) },
  physicalAssetRequirements: { create: getArray(physicalAssetRequirement(false), 3) },
  materialRequirements: { create: getArray(materialRequirement(false), 3) },
  segmentResponses: { create: getArray(segmentResponse(false), 3) },
};
test(`${resourceName} Model Test`, async () => {
  const result: SegmentRequirement = await prisma
    .createSegmentRequirement(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
