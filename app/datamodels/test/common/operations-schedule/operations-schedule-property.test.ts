import { cloneDeep } from 'lodash';
import { operationsScheduleProperty } from '../../utils/data-provider/operations-schedule-data';
import { OperationsSchedulePropertyCreateInput, OperationsScheduleProperty, prisma } from '../../../generated/prisma-client/index';
import { getArray, getFragment, refineObject } from '../../utils/helper';

const resourceName = 'OperationsScheduleProperty';
const data: OperationsSchedulePropertyCreateInput = {
  ...operationsScheduleProperty(),
  parent: { create: operationsScheduleProperty() },
  children: { create: getArray(operationsScheduleProperty(), 4) },
};
test(`${resourceName} Model Test`, async () => {
  const result: OperationsScheduleProperty = await prisma
    .createOperationsScheduleProperty(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
