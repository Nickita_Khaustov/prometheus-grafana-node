import { cloneDeep } from 'lodash';
import { OperationsScheduleCreateInput, OperationsSchedule, prisma } from '../../../generated/prisma-client/index';
import {
  operationsSchedule,
  operationsScheduleProperty,
  operationsRequest,
} from '../../utils/data-provider/operations-schedule-data';
import { getArray, getFragment, refineObject } from '../../utils/helper';

const resourceName = 'OperationsSchedule';
const data: OperationsScheduleCreateInput = {
  ...operationsSchedule(),
  operationsRequests: { create: getArray(operationsRequest(false), 3) },
  operationsScheduleProperties: { create: getArray(operationsScheduleProperty(false), 3) },
};
test(`${resourceName} Model Test`, async () => {
  const result: OperationsSchedule = await prisma
    .createOperationsSchedule(data)
    .$fragment(getFragment(cloneDeep(data), resourceName));
  expect(result).toMatchObject(refineObject(data));
});
