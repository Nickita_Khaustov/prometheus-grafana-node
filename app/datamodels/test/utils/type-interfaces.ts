import {
  EquipmentClassCreateOneInput,
  EquipmentCreateOneInput,
  LookupCreateOneInput,
  OperationsSegmentCreateOneInput,
  ProcessSegmentCreateOneInput,
  UomCategoryCreateOneInput,
  UomCreateOneInput,
  WorkMasterCreateOneInput,
  UomUpdateOneInput,
} from '../../generated/prisma-client';

export interface BasicFields {
  code: string;
  name: string;
  description: string;
  isDeleted: boolean;
  sortOrder: number;
}

export interface BasicPropertyFields extends BasicFields {
  value: number;
  valueStr: string;
  maxValue: string;
  minValue: string;
  isRequired: boolean;
  isSetPoint: boolean;
  scriptAssociation: string;
  valueType: LookupCreateOneInput;
  valueUom: UomCreateOneInput;
  uomCategory: UomCategoryCreateOneInput;
  sensor: EquipmentCreateOneInput;
  sensorType: EquipmentClassCreateOneInput;
}

export interface EquipmentAssetMappingCreateInputBasic {
  description: string;
  isDeleted: boolean;
  startTime: Date | string;
  endTime: Date | string;
}

export interface ResourceSegmentSpecificationCreateInput extends BasicFields {
  quantity: number;
  quantityUom: UomCreateOneInput;
  resourceUse: LookupCreateOneInput;
  processSegment: ProcessSegmentCreateOneInput;
}

export interface ResourceSegmentPropertySpecificationCreateInput extends BasicPropertyFields {
  quantity: number;
  quantityUom: UomCreateOneInput;
}

export interface BasicPropertyFieldsWithQuantity extends BasicPropertyFields {
  quantity: number;
  quantityUom: UomUpdateOneInput;
}

export interface BasicFieldsWithQuantity extends BasicFields {
  quantity: number;
  quantityUom: UomUpdateOneInput;
}

export interface ResourceSpecificationCreateInput extends BasicFields {
  quantity: number;
  quantityUom: UomCreateOneInput;
  resourceUse: LookupCreateOneInput;
  workMaster: WorkMasterCreateOneInput;
  operationsSegment: OperationsSegmentCreateOneInput;
}
