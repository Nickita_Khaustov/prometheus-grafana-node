import * as faker from 'faker';
import {
  lookup, workMaster, equipment, basicFields,
} from './common-data';

import { JobOrderCreateInput } from '../../../generated/prisma-client/index';

export const jobOrder = (): JobOrderCreateInput => ({
  ...basicFields(),
  workMasterVersion: faker.random.word(),
  startTime: '2015-01-01T00:00:00.000Z',
  endTime: '2015-01-01T00:00:00.000Z',
  jobCommand: faker.random.word(),
  jobCommandRule: faker.random.word(),
  priority: { create: lookup() },
  dispatchStatus: { create: lookup() },
  workType: { create: lookup() },
  workMaster: { create: workMaster() },
  hierarchyScope: { create: equipment() },

});

export const workSchedule = null;
