import * as faker from 'faker';
import {
  OperationsRequestPropertyCreateInput,
  SegmentRequirementCreateInput,
  SegmentParameterCreateInput,
  OperationsSchedulePropertyCreateInput,
  EquipmentRequirementCreateInput, EquipmentRequirementPropertyCreateInput,
  PersonnelRequirementCreateInput, PersonnelRequirementPropertyCreateInput,
  PhysicalAssetRequirementCreateInput, PhysicalAssetRequirementPropertyCreateInput,
  MaterialRequirementCreateInput, MaterialRequirementPropertyCreateInput,
  OperationsScheduleCreateInput, OperationsRequestCreateInput,
} from '../../../generated/prisma-client/index';

import {
  workMaster, uom,
  basicPropertyFields,
  basicFieldsWithQuantity,
  equipmentClass, basicPropertyFieldsWithQuantity,
  person, personnelClass, physicalAsset, physicalAssetClass,
  materialClass,
  materialDefinition,
  equipment, lookup, basicFields, materialLot, materialSubLot,
} from './common-data';
import { jobOrder } from './work-schedule-data';
import { operationsSegment, operationsDefinition } from './operations-definition-data';
import { processSegment } from './process-segment-data';


export const operationsSchedule = (): OperationsScheduleCreateInput => ({
  ...basicFields(),
  startTime: '2015-01-01T00:00:00.000Z',
  endTime: '2015-01-01T00:00:00.000Z',
  publishedDate: '2015-01-01T00:00:00.000Z',
  hierarchyScope: { create: equipment() },
  operationType: { create: lookup() },
  scheduledStatus: { create: lookup() },
});

export const operationsRequest = (includeOperationsSchedule = true):
OperationsRequestCreateInput => ({
  ...basicFields(),
  startTime: '2015-01-01T00:00:00.000Z',
  endTime: '2015-01-01T00:00:00.000Z',
  customerId: faker.random.word(),
  customerName: faker.random.word(),
  hierarchyScope: { create: equipment() },
  operationsDefinition: { create: operationsDefinition() },
  ...includeOperationsSchedule && { operationsSchedule: { create: operationsSchedule() } },
  workMaster: { create: workMaster() },
  operationsType: { create: lookup() },
  priority: { create: lookup() },
  orderRequestStatus: { create: lookup() },
});

export const operationsRequestProperty = (includeOperationsRequest = true):
OperationsRequestPropertyCreateInput => ({
  ...basicPropertyFields(),
  ...includeOperationsRequest && { operationsRequest: { create: operationsRequest() } },
});

export const segmentRequirement = (includeOperationsRequest = true):
SegmentRequirementCreateInput => ({
  ...basicFields(),
  earliestStartTime: '2015-01-01T00:00:00.000Z',
  latestEndTime: '2015-01-01T00:00:00.000Z',
  duration: faker.random.number(),
  durationUom: { create: uom() },
  hierarchyScope: { create: equipment() },
  operationsDefinition: { create: operationsDefinition() },
  operationType: { create: lookup() },
  orderRequestStatus: { create: lookup() },
  processSegment: { create: processSegment() },
  operationsSegment: { create: operationsSegment() },
  ...includeOperationsRequest && { operationsRequest: { create: operationsRequest() } },
});

export const segmentParameter = (includeSegmentRequirement = true):
SegmentParameterCreateInput => ({
  ...basicPropertyFields(),
  ...includeSegmentRequirement && { segmentRequirement: { create: segmentRequirement() } },
});

export const operationsScheduleProperty = (includeOperationsSchedule = true):
OperationsSchedulePropertyCreateInput => ({
  ...basicPropertyFields(),
  ...includeOperationsSchedule && { operationsSchedule: { create: operationsSchedule() } },
});

export const equipmentRequirement = (includeSegmentRequirement = true, includeJobOrder = true):
EquipmentRequirementCreateInput => ({
  ...basicFieldsWithQuantity(),
  ...includeJobOrder && { jobOrder: { create: jobOrder() } },
  equipment: { create: equipment() },
  equipmentClass: { create: equipmentClass() },
  resourceUse: { create: lookup() },
  ...includeSegmentRequirement && { segmentRequirement: { create: segmentRequirement() } },
});

export const equipmentRequirementProperty = (includeEquipmentRequirement = true):
EquipmentRequirementPropertyCreateInput => ({
  ...basicPropertyFieldsWithQuantity(),
  ...includeEquipmentRequirement && { equipmentRequirement: { create: equipmentRequirement() } },
});

export const personnelRequirement = (includeSegmentRequirement = true, includeJobOrder = true):
PersonnelRequirementCreateInput => ({
  ...basicFieldsWithQuantity(),
  ...includeJobOrder && { jobOrder: { create: jobOrder() } },
  person: { create: person() },
  personnelClass: { create: personnelClass() },
  resourceUse: { create: lookup() },
  ...includeSegmentRequirement && { segmentRequirement: { create: segmentRequirement() } },
});

export const personnelRequirementProperty = (includePersonnelRequirement = true):
PersonnelRequirementPropertyCreateInput => ({
  ...basicPropertyFieldsWithQuantity(),
  ...includePersonnelRequirement && { personnelRequirement: { create: personnelRequirement() } },
});

export const physicalAssetRequirement = (includeSegmentRequirement = true, includeJobOrder = true):
PhysicalAssetRequirementCreateInput => ({
  ...basicFieldsWithQuantity(),
  ...includeJobOrder && { jobOrder: { create: jobOrder() } },
  physicalAsset: { create: physicalAsset() },
  physicalAssetClass: { create: physicalAssetClass() },
  resourceUse: { create: lookup() },
  equipmentLevel: { create: lookup() },
  ...includeSegmentRequirement && { segmentRequirement: { create: segmentRequirement() } },
});

export const physicalAssetRequirementProperty = (includePhysicalAssetRequirement = true):
PhysicalAssetRequirementPropertyCreateInput => ({
  ...basicPropertyFieldsWithQuantity(),
  ...includePhysicalAssetRequirement
    && { physicalAssetRequirement: { create: physicalAssetRequirement() } },
});

export const materialRequirement = (includeSegmentRequirement = true, includeJobOrder = true):
MaterialRequirementCreateInput => ({
  ...basicFieldsWithQuantity(),
  ...includeJobOrder && { jobOrder: { create: jobOrder() } },
  materialClass: { create: materialClass() },
  materialDefinition: { create: materialDefinition() },
  materialLot: { create: materialLot() },
  materialSubLot: { create: materialSubLot() },
  storageLocation: { create: equipment() },
  resourceUse: { create: lookup() },
  assemblyType: { create: lookup() },
  assemblyRelationship: { create: lookup() },
  ...includeSegmentRequirement && { segmentRequirement: { create: segmentRequirement() } },
});

export const materialRequirementProperty = (includeMaterialRequirement = true):
MaterialRequirementPropertyCreateInput => ({
  ...basicPropertyFieldsWithQuantity(),
  ...includeMaterialRequirement && { materialRequirement: { create: materialRequirement() } },
});
