import { segmentRequirement } from './operations-schedule-data';
import { operationsDefinition, operationsSegment } from './operations-definition-data';
import { processSegment } from './process-segment-data';
import {
  lookup,
  equipment,
  basicFields,
} from './common-data';
import { SegmentResponseCreateInput } from '../../../generated/prisma-client/index';


export const segmentResponse = (includeSegmentRequirement = true):
SegmentResponseCreateInput => ({
  ...basicFields(),
  actualStartTime: '2015-01-01T00:00:00.000Z',
  actualEndTime: '2015-01-01T00:00:00.000Z',
  operationsType: { create: lookup() },
  responseStatus: { create: lookup() },
  hierarchyScope: { create: equipment() },
  processSegment: { create: processSegment() },
  operationsDefinition: { create: operationsDefinition() },
  operationsSegment: { create: operationsSegment() },
  ...includeSegmentRequirement && { segmentRequirement: { create: segmentRequirement() } },
});

export const operationsPerformance = null;
