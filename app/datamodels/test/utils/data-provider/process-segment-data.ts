import * as faker from 'faker';
import {
  EquipmentSegmentSpecificationCreateInput,
  EquipmentSegmentSpecificationPropertyCreateInput,
  MaterialSegmentSpecificationCreateInput,
  MaterialSegmentSpecificationPropertyCreateInput,
  PersonnelSegmentSpecificationCreateInput,
  PersonnelSegmentSpecificationPropertyCreateInput,
  PhysicalAssetSegmentSpecificationCreateInput,
  PhysicalAssetSegmentSpecificationPropertyCreateInput,
  ProcessSegmentCreateInput,
  ProcessSegmentDependencyCreateWithoutSegmentInput,
  ProcessSegmentParameterCreateInput,
} from '../../../generated/prisma-client';
import {
  ResourceSegmentPropertySpecificationCreateInput,
  ResourceSegmentSpecificationCreateInput,
} from '../type-interfaces';
import {
  basicFields,
  basicPropertyFields,
  equipment,
  equipmentClass,
  lookup,
  materialClass,
  materialDefinition, person, personnelClass, physicalAsset, physicalAssetClass,
  uom,
} from './common-data';

export const processSegment = (): ProcessSegmentCreateInput => ({
  ...basicFields(),
  duration: faker.random.number(),
  durationUom: { create: uom() },
  operationsType: { create: lookup() },
  hierarchyScope: { create: equipment() },
});

export const processSegmentParameter = (): ProcessSegmentParameterCreateInput => ({
  ...basicPropertyFields(),
  processSegment: { create: processSegment() },
});

export const processSegmentDependency = (): ProcessSegmentDependencyCreateWithoutSegmentInput => ({
  ...basicFields(),
  value: faker.random.word(),
  maxValue: faker.random.word(),
  minValue: faker.random.word(),
  valueUom: { create: uom() },
  dependencyFactor: { create: lookup() },
  dependencyType: { create: lookup() },
  dependentSegment: { create: processSegment() },
});

const resourceSegmentSpecification = (includeProcessSegment):
ResourceSegmentSpecificationCreateInput => ({
  ...basicFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
  resourceUse: { create: lookup() },
  ...includeProcessSegment && { processSegment: { create: processSegment() } },
});

export const resourceSegmentSpecificationProperty = ():
ResourceSegmentPropertySpecificationCreateInput => ({
  ...basicPropertyFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
});

export const equipmentSegmentSpecification = (includeProcessSegment = true):
EquipmentSegmentSpecificationCreateInput => ({
  ...resourceSegmentSpecification(includeProcessSegment),
  equipment: { create: equipment() },
  equipmentClass: { create: equipmentClass() },
});

export const equipmentSegmentSpecificationProperty = ():
EquipmentSegmentSpecificationPropertyCreateInput => ({
  ...resourceSegmentSpecificationProperty(),
  equipmentSegmentSpecification: { create: equipmentSegmentSpecification() },
});

export const materialSegmentSpecification = (includeProcessSegment = true):
MaterialSegmentSpecificationCreateInput => ({
  ...resourceSegmentSpecification(includeProcessSegment),
  assemblyRelationship: { create: lookup() },
  assemblyType: { create: lookup() },
  materialClass: { create: materialClass() },
  materialDefinition: { create: materialDefinition() },
});

export const materialSegmentSpecificationProperty = ():
MaterialSegmentSpecificationPropertyCreateInput => ({
  ...resourceSegmentSpecificationProperty(),
  materialSegmentSpecification: { create: materialSegmentSpecification() },
});

export const personnelSegmentSpecification = (includeProcessSegment = true):
PersonnelSegmentSpecificationCreateInput => ({
  ...resourceSegmentSpecification(includeProcessSegment),
  personnelClass: { create: personnelClass() },
  person: { create: person() },
});

export const personnelSegmentSpecificationProperty = ():
PersonnelSegmentSpecificationPropertyCreateInput => ({
  ...resourceSegmentSpecificationProperty(),
  personnelSegmentSpecification: { create: personnelSegmentSpecification() },
});

export const physicalAssetSegmentSpecification = (includeProcessSegment = true):
PhysicalAssetSegmentSpecificationCreateInput => ({
  ...resourceSegmentSpecification(includeProcessSegment),
  physicalAssetClass: { create: physicalAssetClass() },
  physicalAsset: { create: physicalAsset() },
});

export const physicalAssetSegmentSpecificationProperty = ():
PhysicalAssetSegmentSpecificationPropertyCreateInput => ({
  ...resourceSegmentSpecificationProperty(),
  physicalAssetSegmentSpecification: { create: physicalAssetSegmentSpecification() },
});
