import * as faker from 'faker';
import {
  MaterialLotPropertyCreateInput, MaterialSubLotCreateInput, MaterialLotCreateInput,
  EquipmentAssetMappingCreateWithoutEquipmentInput,
  EquipmentAssetMappingCreateWithoutPhysicalAssetInput,
  EquipmentClassCreateInput,
  EquipmentClassPropertyCreateInput,
  EquipmentCreateInput,
  EquipmentPropertyCreateInput,
  LookupCategoryCreateInput,
  LookupCreateInput,
  MaterialAssemblyDefinitionCreateWithoutMaterialDefinitionInput,
  MaterialClassCreateInput,
  MaterialClassPropertyCreateInput,
  MaterialDefinitionCreateInput,
  MaterialDefinitionPropertyCreateInput,
  PersonCreateInput,
  PersonnelClassCreateInput,
  PersonnelClassPropertyCreateInput,
  PersonPropertyCreateInput,
  PhysicalAssetClassCreateInput,
  PhysicalAssetClassPropertyCreateInput,
  PhysicalAssetCreateInput,
  PhysicalAssetPropertyCreateInput,
  UomCategoryCreateInput,
  UomCreateInput,
  WorkMasterCreateInput,
} from '../../../generated/prisma-client/index';

import {
  BasicFields,
  BasicPropertyFields,
  BasicPropertyFieldsWithQuantity,
  EquipmentAssetMappingCreateInputBasic,
  BasicFieldsWithQuantity,
} from '../type-interfaces';

export const basicFields = (): BasicFields => ({
  code: faker.name.findName(),
  name: faker.name.findName(),
  description: faker.lorem.words(),
  isDeleted: faker.random.boolean(),
  sortOrder: faker.random.number(),
});

export const lookupCategory = (): LookupCategoryCreateInput => (basicFields());

export const lookup = (): LookupCreateInput => ({
  ...basicFields(),
  value: faker.random.word(),
  isClientSpecific: faker.random.boolean(),
  isHidden: faker.random.boolean(),
  category: { create: lookupCategory() },
});

export const uomCategory = (): UomCategoryCreateInput => (basicFields());

export const uom = (): UomCreateInput => ({
  ...basicFields(),
  symbol: faker.random.word(),
  category: { create: uomCategory() },
});

export const basicFieldsWithQuantity = (): BasicFieldsWithQuantity => ({
  ...basicFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
});

export const equipment = (): EquipmentCreateInput => ({
  ...basicFields(),
});

export const equipmentClass = (): EquipmentClassCreateInput => ({
  ...basicFields(),
  isInstantiable: faker.random.boolean(),
});

export const basicPropertyFields = (): BasicPropertyFields => ({
  ...basicFields(),
  value: faker.random.number(),
  valueStr: faker.random.word(),
  maxValue: faker.random.word(),
  minValue: faker.random.word(),
  isRequired: faker.random.boolean(),
  isSetPoint: faker.random.boolean(),
  scriptAssociation: faker.random.word(),
  valueType: { create: lookup() },
  valueUom: { create: uom() },
  uomCategory: { create: uomCategory() },
  sensor: { create: equipment() },
  sensorType: { create: equipmentClass() },
});

export const basicPropertyFieldsWithQuantity = (): BasicPropertyFieldsWithQuantity => ({
  ...basicPropertyFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
});

export const equipmentProperty = (): EquipmentPropertyCreateInput => ({
  ...basicPropertyFields(),
  equipment: { create: equipment() },
});

export const equipmentClassProperty = (): EquipmentClassPropertyCreateInput => ({
  ...basicPropertyFields(),
  equipmentClass: { create: equipmentClass() },
});

export const materialDefinition = (): MaterialDefinitionCreateInput => ({
  ...basicFields(),
});

export const materialDefinitionProperty = (): MaterialDefinitionPropertyCreateInput => ({
  ...basicPropertyFields(),
  materialDefinition: { create: materialDefinition() },
});

export const materialClass = (): MaterialClassCreateInput => ({
  ...basicFields(),
  isInstantiable: faker.random.boolean(),
});

export const materialClassProperty = (): MaterialClassPropertyCreateInput => ({
  ...basicPropertyFields(),
  materialClass: { create: materialClass() },
});

export const physicalAsset = (): PhysicalAssetCreateInput => ({
  ...basicFields(),
  vendorName: faker.lorem.word(),
  fixedAssetName: faker.lorem.word(),
  physicalLocation: { create: equipment() },
});

export const physicalAssetProperty = (): PhysicalAssetPropertyCreateInput => ({
  ...basicPropertyFields(),
  physicalAsset: { create: physicalAsset() },
});

export const physicalAssetClass = (): PhysicalAssetClassCreateInput => ({
  ...basicFields(),
  manufacturer: faker.lorem.words(),
});

export const physicalAssetClassProperty = (): PhysicalAssetClassPropertyCreateInput => ({
  ...basicPropertyFields(),
  physicalAssetClass: { create: physicalAssetClass() },
});

export const person = (): PersonCreateInput => ({
  ...basicFields(),
});

export const personProperty = (): PersonPropertyCreateInput => ({
  ...basicPropertyFields(),
  person: { create: person() },
});

export const personnelClass = (): PersonnelClassCreateInput => ({
  ...basicFields(),
});

export const personnelClassProperty = (): PersonnelClassPropertyCreateInput => ({
  ...basicPropertyFields(),
  personnelClass: { create: personnelClass() },
});

export const equipmentAssetMapping = (): EquipmentAssetMappingCreateInputBasic => ({
  description: faker.lorem.words(),
  isDeleted: faker.random.boolean(),
  startTime: '2015-01-01T00:00:00.000Z', // the reason to use this is because faker.date.methods producing errors
  endTime: '2015-01-01T00:00:00.000Z', // the reason to use this is because faker.date.methods producing errors
});

export const equipmentAssetMappingForPhysicalAsset = ():
EquipmentAssetMappingCreateWithoutPhysicalAssetInput => ({
  ...equipmentAssetMapping(),
  equipment: { create: equipment() },
});

export const equipmentAssetMappingForEquipment = ():
EquipmentAssetMappingCreateWithoutEquipmentInput => ({
  ...equipmentAssetMapping(),
  physicalAsset: { create: physicalAsset() },
});

export const materialAssemblyDefinition = ():
MaterialAssemblyDefinitionCreateWithoutMaterialDefinitionInput => ({
  assembledFrom: { create: materialDefinition() },
  assemblyRelationship: { create: lookup() },
  assemblyType: { create: lookup() },
});


export const materialLot = (): MaterialLotCreateInput => ({
  ...basicFieldsWithQuantity(),
  assemblyType: { create: lookup() },
  assemblyRelationship: { create: lookup() },
  materialStatus: { create: lookup() },
  storageLocation: { create: equipment() },
  materialDefinition: { create: materialDefinition() },
});

export const materialSubLot = (includeMaterialLot = true): MaterialSubLotCreateInput => ({
  ...basicFieldsWithQuantity(),
  assemblyType: { create: lookup() },
  assemblyRelationship: { create: lookup() },
  materialStatus: { create: lookup() },
  storageLocation: { create: equipment() },
  ...includeMaterialLot && { materialLot: { create: materialLot() } },
});

export const materialLotProperty = (includeMaterialLot = true):
MaterialLotPropertyCreateInput => ({
  ...basicPropertyFields(),
  ...includeMaterialLot && { materialLot: { create: materialLot() } },
});

/*
WORK MASTER
 */

export const workMaster = (): WorkMasterCreateInput => ({
  ...basicFields(),
  version: faker.random.word(),
  duration: faker.random.number(),
  durationUom: { create: uom() },
  workType: { create: lookup() },
  workMasterType: { create: lookup() },
});
