import * as faker from 'faker';
import {
  EquipmentSpecificationCreateInput,
  EquipmentSpecificationPropertyCreateInput,
  MaterialSpecificationCreateInput,
  MaterialSpecificationPropertyCreateInput,
  OperationsDefinitionCreateInput,
  OperationsDefinitionPropertyCreateInput,
  OperationsMaterialBillCreateInput,
  OperationsMaterialBillItemCreateInput,
  OperationsSegmentCreateInput,
  OperationsSegmentDependencyCreateWithoutSegmentInput,
  OperationsSegmentParameterSpecificationCreateInput,
  PersonnelSpecificationCreateInput,
  PersonnelSpecificationPropertyCreateInput,
  PhysicalAssetSpecificationCreateInput,
  PhysicalAssetSpecificationPropertyCreateInput,
} from '../../../generated/prisma-client';

import { ResourceSpecificationCreateInput } from '../type-interfaces';
import {
  basicFields,
  basicPropertyFields,
  equipment, equipmentClass,
  lookup,
  materialClass,
  materialDefinition, person, personnelClass, physicalAsset, physicalAssetClass,
  uom, workMaster,
} from './common-data';

export const operationsDefinition = (): OperationsDefinitionCreateInput => ({
  ...basicFields(),
  customerId: faker.random.word(),
  customerName: faker.random.word(),
  version: faker.random.word(),
  billOfResourceId: faker.random.word(),
  billOfMaterialId: faker.random.word(),
  workMaster: faker.random.word(),
  operationsType: { create: lookup() },
  hierarchyScope: { create: equipment() },
});

export const operationsDefinitionProperty = (includeOperationsDefinition = true):
OperationsDefinitionPropertyCreateInput => ({
  ...basicPropertyFields(),
  ...includeOperationsDefinition && { operationsDefinition: { create: operationsDefinition() } },
});

export const operationsSegment = (includeOperationsDefinition = true):
OperationsSegmentCreateInput => ({
  ...basicFields(),
  duration: faker.random.number(),
  workMaster: faker.random.word(),
  durationUom: { create: uom() },
  operationType: { create: lookup() },
  hierarchyScope: { create: equipment() },
  ...includeOperationsDefinition && { operationsDefinition: { create: operationsDefinition() } },
});

export const operationsSegmentParameterSpecification = (includeOperationsSegment = true):
OperationsSegmentParameterSpecificationCreateInput => ({
  ...basicPropertyFields(),
  ...includeOperationsSegment && { operationsSegment: { create: operationsSegment() } },
});

export const operationsSegmentDependency = ():
OperationsSegmentDependencyCreateWithoutSegmentInput => ({
  ...basicFields(),
  value: faker.random.word(),
  maxValue: faker.random.word(),
  minValue: faker.random.word(),
  valueUom: { create: uom() },
  dependencyFactor: { create: lookup() },
  dependencyType: { create: lookup() },
  dependentSegment: { create: operationsSegment() },
});

export const operationsMaterialBill = (includeOperationsDefinition = true):
OperationsMaterialBillCreateInput => ({
  ...basicFields(),
  ...includeOperationsDefinition && { operationsDefinition: { create: operationsDefinition() } },
});

export const operationsMaterialBillItem = (includeOperationsMaterialBill = true):
OperationsMaterialBillItemCreateInput => ({
  ...basicFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
  materialClass: { create: materialClass() },
  materialDefinition: { create: materialDefinition() },
  resourceUse: { create: lookup() },
  assemblyType: { create: lookup() },
  assemblyRelationship: { create: lookup() },
  ...includeOperationsMaterialBill && {
    operationsMaterialBill: { create: operationsMaterialBill() },
  },
});

const resourceSpecification = (includeOperationsSegment): ResourceSpecificationCreateInput => ({
  ...basicFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
  resourceUse: { create: lookup() },
  workMaster: { create: workMaster() },
  ...includeOperationsSegment && { operationsSegment: { create: operationsSegment() } },
});

export const equipmentSpecification = (includeOperationsSegment = true):
EquipmentSpecificationCreateInput => ({
  ...resourceSpecification(includeOperationsSegment),
  equipment: { create: equipment() },
  equipmentClass: { create: equipmentClass() },
});

export const equipmentSpecificationProperty = (includeEquipmentSpecification = true):
EquipmentSpecificationPropertyCreateInput => ({
  ...basicPropertyFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
  ...includeEquipmentSpecification && {
    equipmentSpecification: { create: equipmentSpecification() },
  },
});

export const materialSpecification = (includeOperationsSegment = true):
MaterialSpecificationCreateInput => ({
  ...resourceSpecification(includeOperationsSegment),
  assemblyRelationship: { create: lookup() },
  assemblyType: { create: lookup() },
  materialClass: { create: materialClass() },
  materialDefinition: { create: materialDefinition() },
  operationsMaterialBillItem: { create: operationsMaterialBillItem() },
});

export const materialSpecificationProperty = (includeMaterialSpecification = true):
MaterialSpecificationPropertyCreateInput => ({
  ...basicPropertyFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
  ...includeMaterialSpecification && {
    materialSpecification: { create: materialSpecification() },
  },
});

export const personnelSpecification = (includeOperationsSegment = true):
PersonnelSpecificationCreateInput => ({
  ...resourceSpecification(includeOperationsSegment),
  personnelClass: { create: personnelClass() },
  person: { create: person() },
});

export const personnelSpecificationProperty = (includePersonnelSpecification = true):
PersonnelSpecificationPropertyCreateInput => ({
  ...basicPropertyFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
  ...includePersonnelSpecification && {
    personnelSpecification: { create: personnelSpecification() },
  },
});

export const physicalAssetSpecification = (includeOperationsSegment = true):
PhysicalAssetSpecificationCreateInput => ({
  ...resourceSpecification(includeOperationsSegment),
  physicalAssetClass: { create: physicalAssetClass() },
  physicalAsset: { create: physicalAsset() },
});

export const physicalAssetSpecificationProperty = (includePhysicalAssetSpecification = true):
PhysicalAssetSpecificationPropertyCreateInput => ({
  ...basicPropertyFields(),
  quantity: faker.random.number(),
  quantityUom: { create: uom() },
  ...includePhysicalAssetSpecification && {
    physicalAssetSpecification: { create: physicalAssetSpecification() },
  },
});
