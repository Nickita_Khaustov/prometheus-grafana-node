import { cloneDeep } from 'lodash';

// Refines object by removing `connect` and `create` from it
export const refineObject = <T>(object: T): T => {
  const data = cloneDeep(object);
  Object.keys(data).forEach(key => {
    if (Array.isArray(data) || typeof data[key] === 'object') {
      data[key] = refineObject(data[key].create || data[key].connect || data[key]);
    }
  });
  return data;
};

// Construct GraphQL fragment out of a refined (mutated) object
const constructFragmentData = (data, resourceName): string => {
  let str = ` ${resourceName} {`;
  Object.keys(data).forEach(key => {
    if (Array.isArray(data[key]) && data[key].length) {
      const flattenObject = {};
      Object.assign(flattenObject, ...data[key]);
      str += constructFragmentData(flattenObject, key);
    } else if (typeof data[key] === 'object') {
      str += constructFragmentData(data[key], key);
    } else {
      str += ` ${key.toString()}`;
    }
  });
  return `${str} }`;
};

// Refine mutation input data and then construct QraphQL fragment out of it
export const getFragment = (object, resourceName): string => `fragment ${resourceName}Fragment on${constructFragmentData(refineObject(object), resourceName)}`;

export const getArray = <T>(object: T, count): T[] => Array.from({ length: count }, () => object);
