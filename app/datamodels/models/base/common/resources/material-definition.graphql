type MaterialDefinition {
    id: ID! @id
    code: String!
    name: String!
    description: String
    sortOrder: Int! @default(value: 0)
    isDeleted: Boolean! @default(value: false)
    assemblyRelationship: Lookup @relation(name: "MaterialDefAssemblyRelationshipLookup")
    assemblyType: Lookup @relation(name: "MaterialDefAssemblyTypeLookup")
    parent: MaterialDefinition @relation(name: "MaterialDefinitions")
    children: [MaterialDefinition!] @relation(name: "MaterialDefinitions")
    materialDefinitionProperties: [MaterialDefinitionProperty!]
    materialClasses: [MaterialClass!] @relation(link: TABLE, name: "MaterialClassMapping")
    materialAssemblyDefinitions: [MaterialAssemblyDefinition!] @relation(name: "MaterialAssemblyDefinition")
}

type MaterialDefinitionProperty {
    id: ID! @id
    name: String!
    code: String!
    description: String
    isDeleted: Boolean! @default(value: false)
    sortOrder: Int! @default(value: 0)
    value: Float
    valueStr: String
    maxValue: String
    minValue: String
    isRequired: Boolean! @default(value: false)
    isSetPoint: Boolean! @default(value: false)
    scriptAssociation: String
    valueType: Lookup!
    valueUom: Uom
    uomCategory: UomCategory
    sensor: Equipment
    sensorType: EquipmentClass
    materialDefinition: MaterialDefinition!
    parent: MaterialDefinitionProperty @relation(name: "MaterialDefinitionProperties")
    children: [MaterialDefinitionProperty!] @relation(name: "MaterialDefinitionProperties")
}

type MaterialClass {
    id: ID! @id
    code: String!
    name: String!
    description: String
    isDeleted: Boolean! @default(value: false)
    sortOrder: Int! @default(value: 0)
    isInstantiable: Boolean! @default(value: true)
    assemblyRelationship: Lookup  @relation(name: "MaterialClassAssemblyRelationshipLookup")
    assemblyType: Lookup @relation(name: "MaterialClassAssemblyTypeLookup")
    parent: MaterialClass @relation(name: "MaterialClasses")
    children: [MaterialClass!] @relation(name: "MaterialClasses")
    materialDefinitions: [MaterialDefinition!] @relation(name: "MaterialClassMapping")
    materialClassProperties: [MaterialClassProperty!]
}

type MaterialClassProperty {
    id: ID! @id
    name: String!
    code: String!
    description: String
    isDeleted: Boolean! @default(value: false)
    sortOrder: Int! @default(value: 0)
    value: Float
    valueStr: String
    maxValue: String
    minValue: String
    isRequired: Boolean! @default(value: false)
    isSetPoint: Boolean! @default(value: false)
    scriptAssociation: String
    valueType: Lookup!
    valueUom: Uom
    uomCategory: UomCategory
    sensor: Equipment
    sensorType: EquipmentClass
    materialClass: MaterialClass!
    parent: MaterialClassProperty @relation(name: "MaterialClassProperties")
    children: [MaterialClassProperty!] @relation(name: "MaterialClassProperties")
}

type MaterialClassMapping @relationTable {
    materialClass: MaterialClass!
    materialDefinition: MaterialDefinition!
}

type MaterialAssemblyDefinition {
    id: ID! @id
    materialDefinition: MaterialDefinition! @relation(name: "MaterialAssemblyDefinition")
    assembledFrom: MaterialDefinition! @relation(name: "MaterialAssemblyDefinitionAssembledFrom")
    assemblyRelationship: Lookup  @relation(name: "MaterialAssemblyDefinitionRelationshipLookup")
    assemblyType: Lookup @relation(name: "MaterialAssemblyDefinitionTypeLookup")
}
