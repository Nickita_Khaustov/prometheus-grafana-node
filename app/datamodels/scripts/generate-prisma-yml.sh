#!/bin/bash

set -e

. ./.env
echo "endpoint: '${PRISMA_SERVER_HOSTNAME}:${PRISMA_SERVER_PORT}'" >prisma.yml

echo "datamodel:" >>prisma.yml
for x in $(tree -faiR ./models/base/ | egrep '.+\.graphql$');
do
  echo "  - " ${x} >>prisma.yml
done

echo "generate:" >>prisma.yml
echo "  - generator: typescript-client" >>prisma.yml
echo "    output: ./generated/prisma-client/" >>prisma.yml

echo "primsa.yml generated"
