module.exports = {
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  extends: [
    'plugin:@typescript-eslint/recommended',
    'airbnb-typescript/base',
  ],
  globals: {
    BABEL_ENV: true,
    test: true,
    expect: true,
  },
  env: {
    node: true,
  },
  rules: {
    semi: ['error', 'always'],
    quotes: ['error', 'single'],
  },
};
