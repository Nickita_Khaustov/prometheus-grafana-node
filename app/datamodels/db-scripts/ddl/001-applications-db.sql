--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10 (Ubuntu 10.10-1.pgdg18.04+1)
-- Dumped by pg_dump version 11.5 (Ubuntu 11.5-1.pgdg18.04+1)

-- TOC entry 5 (class 2615 OID 33821)
-- Name: common; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA common;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 494 (class 1259 OID 39470)
-- Name: equipment_actual; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_actual (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    equipment_class_id bigint NOT NULL,
    equipment_id bigint NOT NULL,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    segment_response_id bigint NOT NULL,
    job_response_id bigint
);


--
-- TOC entry 493 (class 1259 OID 39468)
-- Name: equipment_actual_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_actual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6931 (class 0 OID 0)
-- Dependencies: 493
-- Name: equipment_actual_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_actual_id_seq OWNED BY common.equipment_actual.id;


--
-- TOC entry 514 (class 1259 OID 39968)
-- Name: equipment_actual_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_actual_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    equipment_actual_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 513 (class 1259 OID 39966)
-- Name: equipment_actual_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_actual_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6932 (class 0 OID 0)
-- Dependencies: 513
-- Name: equipment_actual_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_actual_property_id_seq OWNED BY common.equipment_actual_property.id;


--
-- TOC entry 432 (class 1259 OID 37921)
-- Name: equipment_capability; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_capability (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    equipment_class_id bigint NOT NULL,
    equipment_id bigint,
    capability_type_id bigint NOT NULL,
    reason character varying(100) NOT NULL,
    confidence_factor bigint,
    hierarchy_scope_id bigint NOT NULL,
    resource_use_id bigint,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    operations_capability_id bigint,
    work_capability_id bigint,
    work_master_capability_id bigint,
    process_segment_capability_id bigint
);


--
-- TOC entry 431 (class 1259 OID 37919)
-- Name: equipment_capability_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_capability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6933 (class 0 OID 0)
-- Dependencies: 431
-- Name: equipment_capability_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_capability_id_seq OWNED BY common.equipment_capability.id;


--
-- TOC entry 466 (class 1259 OID 38794)
-- Name: equipment_capability_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_capability_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    equipment_capability_id bigint NOT NULL,
    value_type_id bigint NOT NULL,
    value_uom_id bigint,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 465 (class 1259 OID 38792)
-- Name: equipment_capability_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_capability_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6934 (class 0 OID 0)
-- Dependencies: 465
-- Name: equipment_capability_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_capability_property_id_seq OWNED BY common.equipment_capability_property.id;


--
-- TOC entry 374 (class 1259 OID 36675)
-- Name: equipment_capability_test_result; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_capability_test_result (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    test_date timestamp without time zone NOT NULL,
    result character varying(50) NOT NULL,
    result_uom_id bigint NOT NULL,
    expiration timestamp without time zone NOT NULL,
    equipment_capability_test_specification_id bigint NOT NULL
);


--
-- TOC entry 373 (class 1259 OID 36673)
-- Name: equipment_capability_test_result_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_capability_test_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6935 (class 0 OID 0)
-- Dependencies: 373
-- Name: equipment_capability_test_result_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_capability_test_result_id_seq OWNED BY common.equipment_capability_test_result.id;


--
-- TOC entry 316 (class 1259 OID 35558)
-- Name: equipment_capability_test_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_capability_test_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    version character varying(50) NOT NULL,
    equipment_class_id bigint,
    equipment_id bigint,
    equipment_class_property_id bigint,
    equipment_property_id bigint
);


--
-- TOC entry 315 (class 1259 OID 35556)
-- Name: equipment_capability_test_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_capability_test_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6936 (class 0 OID 0)
-- Dependencies: 315
-- Name: equipment_capability_test_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_capability_test_specification_id_seq OWNED BY common.equipment_capability_test_specification.id;


--
-- TOC entry 468 (class 1259 OID 38841)
-- Name: equipment_requirement; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_requirement (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    equipment_class_id bigint NOT NULL,
    equipment_id bigint,
    description character varying(500),
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    segment_requirement_id bigint NOT NULL,
    job_order_id bigint
);


--
-- TOC entry 467 (class 1259 OID 38839)
-- Name: equipment_requirement_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_requirement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6937 (class 0 OID 0)
-- Dependencies: 467
-- Name: equipment_requirement_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_requirement_id_seq OWNED BY common.equipment_requirement.id;


--
-- TOC entry 496 (class 1259 OID 39517)
-- Name: equipment_requirement_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_requirement_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    equipment_requirement_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 495 (class 1259 OID 39515)
-- Name: equipment_requirement_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_requirement_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6938 (class 0 OID 0)
-- Dependencies: 495
-- Name: equipment_requirement_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_requirement_property_id_seq OWNED BY common.equipment_requirement_property.id;


--
-- TOC entry 272 (class 1259 OID 34806)
-- Name: job_list; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.job_list (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_type_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL,
    hierarchy_scope_id bigint NOT NULL
);


--
-- TOC entry 271 (class 1259 OID 34804)
-- Name: job_list_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.job_list_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6939 (class 0 OID 0)
-- Dependencies: 271
-- Name: job_list_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.job_list_id_seq OWNED BY common.job_list.id;


--
-- TOC entry 318 (class 1259 OID 35593)
-- Name: job_list_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.job_list_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    job_list_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 317 (class 1259 OID 35591)
-- Name: job_list_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.job_list_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6940 (class 0 OID 0)
-- Dependencies: 317
-- Name: job_list_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.job_list_property_id_seq OWNED BY common.job_list_property.id;


--
-- TOC entry 408 (class 1259 OID 37385)
-- Name: job_order; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.job_order (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_type_id bigint NOT NULL,
    work_master_id bigint NOT NULL,
    work_master_version character varying(50),
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    priority_id bigint NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    job_command character varying(50),
    dispatch_status_id bigint NOT NULL,
    job_command_rule character varying(100),
    work_request_id bigint NOT NULL,
    job_list_id bigint
);


--
-- TOC entry 407 (class 1259 OID 37383)
-- Name: job_order_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.job_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6941 (class 0 OID 0)
-- Dependencies: 407
-- Name: job_order_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.job_order_id_seq OWNED BY common.job_order.id;


--
-- TOC entry 434 (class 1259 OID 37989)
-- Name: job_order_parameter; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.job_order_parameter (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    job_order_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 433 (class 1259 OID 37987)
-- Name: job_order_parameter_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.job_order_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6942 (class 0 OID 0)
-- Dependencies: 433
-- Name: job_order_parameter_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.job_order_parameter_id_seq OWNED BY common.job_order_parameter.id;


--
-- TOC entry 436 (class 1259 OID 38030)
-- Name: job_response; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.job_response (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_type_id bigint,
    job_order_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    work_response_id bigint NOT NULL,
    response_status_id bigint NOT NULL,
    work_master_id bigint NOT NULL,
    work_master_version character varying(50),
    parent_id bigint
);


--
-- TOC entry 470 (class 1259 OID 38888)
-- Name: job_response_data; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.job_response_data (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    job_response_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 469 (class 1259 OID 38886)
-- Name: job_response_data_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.job_response_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6943 (class 0 OID 0)
-- Dependencies: 469
-- Name: job_response_data_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.job_response_data_id_seq OWNED BY common.job_response_data.id;


--
-- TOC entry 435 (class 1259 OID 38028)
-- Name: job_response_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.job_response_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6944 (class 0 OID 0)
-- Dependencies: 435
-- Name: job_response_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.job_response_id_seq OWNED BY common.job_response.id;


--
-- TOC entry 498 (class 1259 OID 39565)
-- Name: material_actual; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_actual (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    material_class_id bigint NOT NULL,
    material_definition_id bigint NOT NULL,
    material_lot_id bigint NOT NULL,
    material_sub_lot_id bigint NOT NULL,
    resource_use_id bigint NOT NULL,
    storage_location_id bigint NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    segment_response_id bigint NOT NULL,
    job_response_id bigint,
    parent_id bigint
);


--
-- TOC entry 497 (class 1259 OID 39563)
-- Name: material_actual_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_actual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6945 (class 0 OID 0)
-- Dependencies: 497
-- Name: material_actual_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_actual_id_seq OWNED BY common.material_actual.id;


--
-- TOC entry 516 (class 1259 OID 40016)
-- Name: material_actual_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_actual_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    material_actual_id bigint NOT NULL,
    value_type_id bigint NOT NULL,
    value_uom_id bigint,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 515 (class 1259 OID 40014)
-- Name: material_actual_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_actual_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6946 (class 0 OID 0)
-- Dependencies: 515
-- Name: material_actual_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_actual_property_id_seq OWNED BY common.material_actual_property.id;


--
-- TOC entry 438 (class 1259 OID 38082)
-- Name: material_capability; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_capability (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    material_class_id bigint NOT NULL,
    material_definition_id bigint NOT NULL,
    material_lot_id bigint,
    material_sub_lot_id bigint,
    capability_type_id bigint NOT NULL,
    reason character varying(100) NOT NULL,
    confidence_factor bigint,
    hierarchy_scope_id bigint NOT NULL,
    resource_use_id bigint,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    operations_capability_id bigint,
    work_capability_id bigint,
    work_master_capability_id bigint,
    process_segment_capability_id bigint,
    parent_id bigint
);


--
-- TOC entry 437 (class 1259 OID 38080)
-- Name: material_capability_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_capability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6947 (class 0 OID 0)
-- Dependencies: 437
-- Name: material_capability_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_capability_id_seq OWNED BY common.material_capability.id;


--
-- TOC entry 472 (class 1259 OID 38929)
-- Name: material_capability_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_capability_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    material_capability_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 471 (class 1259 OID 38927)
-- Name: material_capability_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_capability_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6948 (class 0 OID 0)
-- Dependencies: 471
-- Name: material_capability_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_capability_property_id_seq OWNED BY common.material_capability_property.id;


--
-- TOC entry 440 (class 1259 OID 38176)
-- Name: material_capability_test_result; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_capability_test_result (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    test_date timestamp without time zone NOT NULL,
    result character varying(50) NOT NULL,
    result_uom_id bigint NOT NULL,
    expiration timestamp without time zone NOT NULL,
    material_capability_test_specification_id bigint NOT NULL
);


--
-- TOC entry 439 (class 1259 OID 38174)
-- Name: material_capability_test_result_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_capability_test_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6949 (class 0 OID 0)
-- Dependencies: 439
-- Name: material_capability_test_result_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_capability_test_result_id_seq OWNED BY common.material_capability_test_result.id;


--
-- TOC entry 410 (class 1259 OID 37436)
-- Name: material_capability_test_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_capability_test_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    version character varying(50) NOT NULL,
    material_class_id bigint,
    material_definition_id bigint,
    material_lot_id bigint,
    material_sub_lot_id bigint,
    material_class_property_id bigint,
    material_property_id bigint,
    material_lot_property_id bigint
);


--
-- TOC entry 409 (class 1259 OID 37434)
-- Name: material_capability_test_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_capability_test_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6950 (class 0 OID 0)
-- Dependencies: 409
-- Name: material_capability_test_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_capability_test_specification_id_seq OWNED BY common.material_capability_test_specification.id;


--
-- TOC entry 274 (class 1259 OID 34832)
-- Name: material_lot; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_lot (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    material_status_id bigint NOT NULL,
    storage_location_id bigint NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    material_definition_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 273 (class 1259 OID 34830)
-- Name: material_lot_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_lot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6951 (class 0 OID 0)
-- Dependencies: 273
-- Name: material_lot_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_lot_id_seq OWNED BY common.material_lot.id;


--
-- TOC entry 376 (class 1259 OID 36700)
-- Name: material_lot_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_lot_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    material_lot_id bigint NOT NULL,
    material_sub_lot_id bigint,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 375 (class 1259 OID 36698)
-- Name: material_lot_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_lot_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6952 (class 0 OID 0)
-- Dependencies: 375
-- Name: material_lot_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_lot_property_id_seq OWNED BY common.material_lot_property.id;


--
-- TOC entry 474 (class 1259 OID 38977)
-- Name: material_requirement; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_requirement (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    material_class_id bigint,
    material_definition_id bigint NOT NULL,
    material_lot_id bigint,
    material_sub_lot_id bigint,
    resource_use_id bigint NOT NULL,
    storage_location_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    segment_requirement_id bigint NOT NULL,
    job_order_id bigint,
    parent_id bigint
);


--
-- TOC entry 473 (class 1259 OID 38975)
-- Name: material_requirement_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_requirement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6953 (class 0 OID 0)
-- Dependencies: 473
-- Name: material_requirement_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_requirement_id_seq OWNED BY common.material_requirement.id;


--
-- TOC entry 500 (class 1259 OID 39643)
-- Name: material_requirement_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_requirement_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    material_requirement_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 499 (class 1259 OID 39641)
-- Name: material_requirement_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_requirement_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6954 (class 0 OID 0)
-- Dependencies: 499
-- Name: material_requirement_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_requirement_property_id_seq OWNED BY common.material_requirement_property.id;


--
-- TOC entry 320 (class 1259 OID 35634)
-- Name: material_sub_lot; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_sub_lot (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    material_status_id bigint NOT NULL,
    storage_location_id bigint NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    material_lot_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 319 (class 1259 OID 35632)
-- Name: material_sub_lot_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_sub_lot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6955 (class 0 OID 0)
-- Dependencies: 319
-- Name: material_sub_lot_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_sub_lot_id_seq OWNED BY common.material_sub_lot.id;


--
-- TOC entry 276 (class 1259 OID 34884)
-- Name: operations_capability; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_capability (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    capability_type_id bigint NOT NULL,
    reason character varying(100) NOT NULL,
    confidence_factor bigint,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL,
    parent_id bigint
);


--
-- TOC entry 275 (class 1259 OID 34882)
-- Name: operations_capability_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_capability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6956 (class 0 OID 0)
-- Dependencies: 275
-- Name: operations_capability_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_capability_id_seq OWNED BY common.operations_capability.id;


--
-- TOC entry 322 (class 1259 OID 35687)
-- Name: operations_capability_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_capability_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_capability_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 321 (class 1259 OID 35685)
-- Name: operations_capability_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_capability_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6957 (class 0 OID 0)
-- Dependencies: 321
-- Name: operations_capability_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_capability_property_id_seq OWNED BY common.operations_capability_property.id;


--
-- TOC entry 278 (class 1259 OID 34916)
-- Name: operations_performance; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_performance (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_type_id bigint NOT NULL,
    operations_schedule_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    performance_status_id bigint NOT NULL,
    published_date timestamp without time zone NOT NULL
);


--
-- TOC entry 277 (class 1259 OID 34914)
-- Name: operations_performance_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_performance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6958 (class 0 OID 0)
-- Dependencies: 277
-- Name: operations_performance_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_performance_id_seq OWNED BY common.operations_performance.id;


--
-- TOC entry 324 (class 1259 OID 35728)
-- Name: operations_performance_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_performance_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_performance_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 323 (class 1259 OID 35726)
-- Name: operations_performance_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_performance_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6959 (class 0 OID 0)
-- Dependencies: 323
-- Name: operations_performance_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_performance_property_id_seq OWNED BY common.operations_performance_property.id;


--
-- TOC entry 412 (class 1259 OID 37486)
-- Name: operations_request; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_request (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_type_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    priority_id bigint NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    operations_definition_id bigint NOT NULL,
    order_request_status_id bigint NOT NULL,
    operations_schedule_id bigint NOT NULL,
    work_master_id bigint,
    customer_id character varying(50),
    customer_name character varying(100)
);


--
-- TOC entry 411 (class 1259 OID 37484)
-- Name: operations_request_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6960 (class 0 OID 0)
-- Dependencies: 411
-- Name: operations_request_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_request_id_seq OWNED BY common.operations_request.id;


--
-- TOC entry 442 (class 1259 OID 38201)
-- Name: operations_request_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_request_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_request_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 441 (class 1259 OID 38199)
-- Name: operations_request_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_request_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6961 (class 0 OID 0)
-- Dependencies: 441
-- Name: operations_request_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_request_property_id_seq OWNED BY common.operations_request_property.id;


--
-- TOC entry 444 (class 1259 OID 38242)
-- Name: operations_response; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_response (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_type_id bigint NOT NULL,
    operations_request_id bigint,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    operations_definition_id bigint,
    response_status_id bigint NOT NULL,
    operations_performance_id bigint NOT NULL
);


--
-- TOC entry 443 (class 1259 OID 38240)
-- Name: operations_response_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_response_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6962 (class 0 OID 0)
-- Dependencies: 443
-- Name: operations_response_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_response_id_seq OWNED BY common.operations_response.id;


--
-- TOC entry 476 (class 1259 OID 39055)
-- Name: operations_response_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_response_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_response_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 475 (class 1259 OID 39053)
-- Name: operations_response_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_response_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6963 (class 0 OID 0)
-- Dependencies: 475
-- Name: operations_response_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_response_property_id_seq OWNED BY common.operations_response_property.id;


--
-- TOC entry 234 (class 1259 OID 34124)
-- Name: operations_schedule; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_schedule (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_type_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL,
    hierarchy_scope bigint NOT NULL,
    scheduled_status_id bigint NOT NULL
);


--
-- TOC entry 233 (class 1259 OID 34122)
-- Name: operations_schedule_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_schedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6964 (class 0 OID 0)
-- Dependencies: 233
-- Name: operations_schedule_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_schedule_id_seq OWNED BY common.operations_schedule.id;


--
-- TOC entry 280 (class 1259 OID 34952)
-- Name: operations_schedule_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_schedule_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_schedule_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 279 (class 1259 OID 34950)
-- Name: operations_schedule_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_schedule_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6965 (class 0 OID 0)
-- Dependencies: 279
-- Name: operations_schedule_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_schedule_property_id_seq OWNED BY common.operations_schedule_property.id;


--
-- TOC entry 502 (class 1259 OID 39691)
-- Name: personnel_actual; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_actual (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    personnel_class_id bigint NOT NULL,
    person_id bigint NOT NULL,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    segment_requirement_id bigint NOT NULL,
    segment_response_id bigint NOT NULL,
    job_response_id bigint
);


--
-- TOC entry 501 (class 1259 OID 39689)
-- Name: personnel_actual_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_actual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6966 (class 0 OID 0)
-- Dependencies: 501
-- Name: personnel_actual_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_actual_id_seq OWNED BY common.personnel_actual.id;


--
-- TOC entry 518 (class 1259 OID 40063)
-- Name: personnel_actual_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_actual_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    personnel_actual_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 517 (class 1259 OID 40061)
-- Name: personnel_actual_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_actual_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6967 (class 0 OID 0)
-- Dependencies: 517
-- Name: personnel_actual_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_actual_property_id_seq OWNED BY common.personnel_actual_property.id;


--
-- TOC entry 446 (class 1259 OID 38288)
-- Name: personnel_capability; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_capability (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    personnel_class_id bigint NOT NULL,
    person_id bigint,
    capability_type_id bigint NOT NULL,
    reason character varying(100) NOT NULL,
    confidence_factor bigint,
    hierarchy_scope_id bigint NOT NULL,
    resource_use_id bigint,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    operations_capability_id bigint,
    work_capability_id bigint,
    work_master_capability_id bigint,
    process_segment_capability_id bigint
);


--
-- TOC entry 445 (class 1259 OID 38286)
-- Name: personnel_capability_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_capability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6968 (class 0 OID 0)
-- Dependencies: 445
-- Name: personnel_capability_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_capability_id_seq OWNED BY common.personnel_capability.id;


--
-- TOC entry 478 (class 1259 OID 39096)
-- Name: personnel_capability_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_capability_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    personnel_capability_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 477 (class 1259 OID 39094)
-- Name: personnel_capability_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_capability_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6969 (class 0 OID 0)
-- Dependencies: 477
-- Name: personnel_capability_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_capability_property_id_seq OWNED BY common.personnel_capability_property.id;


--
-- TOC entry 326 (class 1259 OID 35769)
-- Name: personnel_capability_test_result; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_capability_test_result (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    test_date timestamp without time zone NOT NULL,
    result character varying(50) NOT NULL,
    result_uom_id bigint NOT NULL,
    expiration timestamp without time zone NOT NULL,
    personnel_capability_test_specification_id bigint NOT NULL
);


--
-- TOC entry 325 (class 1259 OID 35767)
-- Name: personnel_capability_test_result_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_capability_test_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6970 (class 0 OID 0)
-- Dependencies: 325
-- Name: personnel_capability_test_result_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_capability_test_result_id_seq OWNED BY common.personnel_capability_test_result.id;


--
-- TOC entry 282 (class 1259 OID 34993)
-- Name: personnel_capability_test_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_capability_test_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    version character varying(50) NOT NULL,
    personnel_class_id bigint,
    person_id bigint,
    personnel_class_property_id bigint,
    person_property_id bigint
);


--
-- TOC entry 281 (class 1259 OID 34991)
-- Name: personnel_capability_test_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_capability_test_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6971 (class 0 OID 0)
-- Dependencies: 281
-- Name: personnel_capability_test_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_capability_test_specification_id_seq OWNED BY common.personnel_capability_test_specification.id;


--
-- TOC entry 480 (class 1259 OID 39144)
-- Name: personnel_requirement; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_requirement (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    personnel_class_id bigint NOT NULL,
    person_id bigint,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    segment_requirement_id bigint NOT NULL,
    job_order_id bigint
);


--
-- TOC entry 479 (class 1259 OID 39142)
-- Name: personnel_requirement_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_requirement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6972 (class 0 OID 0)
-- Dependencies: 479
-- Name: personnel_requirement_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_requirement_id_seq OWNED BY common.personnel_requirement.id;


--
-- TOC entry 504 (class 1259 OID 39743)
-- Name: personnel_requirement_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_requirement_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    personnel_requirement_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 503 (class 1259 OID 39741)
-- Name: personnel_requirement_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_requirement_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6973 (class 0 OID 0)
-- Dependencies: 503
-- Name: personnel_requirement_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_requirement_property_id_seq OWNED BY common.personnel_requirement_property.id;


--
-- TOC entry 506 (class 1259 OID 39791)
-- Name: physical_asset_actual; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_actual (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    physical_asset_class_id bigint NOT NULL,
    physical_asset_id bigint NOT NULL,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    segment_response_id bigint NOT NULL,
    job_response_id bigint
);


--
-- TOC entry 505 (class 1259 OID 39789)
-- Name: physical_asset_actual_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_actual_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6974 (class 0 OID 0)
-- Dependencies: 505
-- Name: physical_asset_actual_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_actual_id_seq OWNED BY common.physical_asset_actual.id;


--
-- TOC entry 520 (class 1259 OID 40111)
-- Name: physical_asset_actual_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_actual_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    physical_asset_actual_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 519 (class 1259 OID 40109)
-- Name: physical_asset_actual_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_actual_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6975 (class 0 OID 0)
-- Dependencies: 519
-- Name: physical_asset_actual_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_actual_property_id_seq OWNED BY common.physical_asset_actual_property.id;


--
-- TOC entry 448 (class 1259 OID 38356)
-- Name: physical_asset_capability; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_capability (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    physical_asset_class_id bigint NOT NULL,
    physical_asset_id bigint,
    capability_type_id bigint NOT NULL,
    reason character varying(100) NOT NULL,
    confidence_factor bigint,
    hierarchy_scope_id bigint NOT NULL,
    resource_use_id bigint,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    operations_capability_id bigint,
    work_capability_id bigint,
    work_master_capability_id bigint,
    process_segment_capability_id bigint
);


--
-- TOC entry 447 (class 1259 OID 38354)
-- Name: physical_asset_capability_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_capability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6976 (class 0 OID 0)
-- Dependencies: 447
-- Name: physical_asset_capability_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_capability_id_seq OWNED BY common.physical_asset_capability.id;


--
-- TOC entry 482 (class 1259 OID 39191)
-- Name: physical_asset_capability_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_capability_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    physical_asset_capability_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 481 (class 1259 OID 39189)
-- Name: physical_asset_capability_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_capability_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6977 (class 0 OID 0)
-- Dependencies: 481
-- Name: physical_asset_capability_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_capability_property_id_seq OWNED BY common.physical_asset_capability_property.id;


--
-- TOC entry 414 (class 1259 OID 37537)
-- Name: physical_asset_capability_test_result; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_capability_test_result (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    test_date timestamp without time zone NOT NULL,
    result character varying(50) NOT NULL,
    result_uom_id bigint NOT NULL,
    expiration timestamp without time zone NOT NULL,
    physical_asset_capability_test_specification_id bigint NOT NULL
);


--
-- TOC entry 413 (class 1259 OID 37535)
-- Name: physical_asset_capability_test_result_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_capability_test_result_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6978 (class 0 OID 0)
-- Dependencies: 413
-- Name: physical_asset_capability_test_result_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_capability_test_result_id_seq OWNED BY common.physical_asset_capability_test_result.id;


--
-- TOC entry 378 (class 1259 OID 36746)
-- Name: physical_asset_capability_test_spec; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_capability_test_spec (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    version character varying(50) NOT NULL,
    physical_asset_class_id bigint,
    physical_asset_id bigint,
    physical_asset_class_property_id bigint,
    physical_asset_property_id bigint
);


--
-- TOC entry 377 (class 1259 OID 36744)
-- Name: physical_asset_capability_test_spec_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_capability_test_spec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6979 (class 0 OID 0)
-- Dependencies: 377
-- Name: physical_asset_capability_test_spec_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_capability_test_spec_id_seq OWNED BY common.physical_asset_capability_test_spec.id;


--
-- TOC entry 484 (class 1259 OID 39239)
-- Name: physical_asset_requirement; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_requirement (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    physical_asset_class_id bigint NOT NULL,
    physical_asset_id bigint,
    resource_use_id bigint,
    equipment_level_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    segment_requirement_id bigint NOT NULL,
    job_order_id bigint
);


--
-- TOC entry 483 (class 1259 OID 39237)
-- Name: physical_asset_requirement_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_requirement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6980 (class 0 OID 0)
-- Dependencies: 483
-- Name: physical_asset_requirement_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_requirement_id_seq OWNED BY common.physical_asset_requirement.id;


--
-- TOC entry 508 (class 1259 OID 39838)
-- Name: physical_asset_requirement_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_requirement_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    physical_asset_requirement_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 507 (class 1259 OID 39836)
-- Name: physical_asset_requirement_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_requirement_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6981 (class 0 OID 0)
-- Dependencies: 507
-- Name: physical_asset_requirement_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_requirement_property_id_seq OWNED BY common.physical_asset_requirement_property.id;


--
-- TOC entry 328 (class 1259 OID 35794)
-- Name: process_segment_capability; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.process_segment_capability (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    process_segment_id bigint,
    capability_type_id bigint NOT NULL,
    reason character varying(100) NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    operations_capability_id bigint,
    parent_id bigint
);


--
-- TOC entry 327 (class 1259 OID 35792)
-- Name: process_segment_capability_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.process_segment_capability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6982 (class 0 OID 0)
-- Dependencies: 327
-- Name: process_segment_capability_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.process_segment_capability_id_seq OWNED BY common.process_segment_capability.id;


--
-- TOC entry 510 (class 1259 OID 39886)
-- Name: segment_data; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.segment_data (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    segment_response_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 509 (class 1259 OID 39884)
-- Name: segment_data_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.segment_data_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6983 (class 0 OID 0)
-- Dependencies: 509
-- Name: segment_data_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.segment_data_id_seq OWNED BY common.segment_data.id;


--
-- TOC entry 486 (class 1259 OID 39291)
-- Name: segment_parameter; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.segment_parameter (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    segment_requirement_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 485 (class 1259 OID 39289)
-- Name: segment_parameter_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.segment_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6984 (class 0 OID 0)
-- Dependencies: 485
-- Name: segment_parameter_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.segment_parameter_id_seq OWNED BY common.segment_parameter.id;


--
-- TOC entry 450 (class 1259 OID 38424)
-- Name: segment_requirement; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.segment_requirement (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_type_id bigint NOT NULL,
    earliest_start_time timestamp without time zone NOT NULL,
    latest_end_time timestamp without time zone NOT NULL,
    duration character varying(50) NOT NULL,
    duration_uom_id bigint NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    operations_definition_id bigint NOT NULL,
    order_request_status_id bigint NOT NULL,
    process_segment_id bigint,
    operations_segment_id bigint,
    operations_request_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 449 (class 1259 OID 38422)
-- Name: segment_requirement_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.segment_requirement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6985 (class 0 OID 0)
-- Dependencies: 449
-- Name: segment_requirement_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.segment_requirement_id_seq OWNED BY common.segment_requirement.id;


--
-- TOC entry 488 (class 1259 OID 39332)
-- Name: segment_response; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.segment_response (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_type_id bigint NOT NULL,
    process_segment_id bigint,
    operations_segment_id bigint,
    hierarchy_scope_id bigint NOT NULL,
    operations_definition_id bigint,
    response_status_id bigint NOT NULL,
    segment_requirement_id bigint,
    operations_response_id bigint NOT NULL,
    actual_start_time timestamp without time zone NOT NULL,
    actual_end_time timestamp without time zone NOT NULL,
    parent_id bigint
);


--
-- TOC entry 487 (class 1259 OID 39330)
-- Name: segment_response_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.segment_response_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6986 (class 0 OID 0)
-- Dependencies: 487
-- Name: segment_response_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.segment_response_id_seq OWNED BY common.segment_response.id;


--
-- TOC entry 330 (class 1259 OID 35836)
-- Name: work_alert; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_alert (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    message_text character varying(100) NOT NULL,
    time_stamp timestamp without time zone NOT NULL,
    priority_id bigint NOT NULL,
    work_alert_category_id bigint NOT NULL,
    work_alert_definition_id bigint NOT NULL,
    hierarchy_scope_id bigint
);


--
-- TOC entry 284 (class 1259 OID 35028)
-- Name: work_alert_definition; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_alert_definition (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    priority_id bigint NOT NULL,
    work_alert_category_id bigint NOT NULL,
    hierarchy_scope_id bigint
);


--
-- TOC entry 283 (class 1259 OID 35026)
-- Name: work_alert_definition_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_alert_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6987 (class 0 OID 0)
-- Dependencies: 283
-- Name: work_alert_definition_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_alert_definition_id_seq OWNED BY common.work_alert_definition.id;


--
-- TOC entry 332 (class 1259 OID 35872)
-- Name: work_alert_definition_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_alert_definition_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_alert_definition_id bigint NOT NULL,
    value_type_id bigint NOT NULL,
    value_uom_id bigint,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 331 (class 1259 OID 35870)
-- Name: work_alert_definition_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_alert_definition_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6988 (class 0 OID 0)
-- Dependencies: 331
-- Name: work_alert_definition_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_alert_definition_property_id_seq OWNED BY common.work_alert_definition_property.id;


--
-- TOC entry 329 (class 1259 OID 35834)
-- Name: work_alert_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_alert_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6989 (class 0 OID 0)
-- Dependencies: 329
-- Name: work_alert_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_alert_id_seq OWNED BY common.work_alert.id;


--
-- TOC entry 380 (class 1259 OID 36781)
-- Name: work_alert_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_alert_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_alert_id bigint NOT NULL,
    work_alert_definition_property_id bigint,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 379 (class 1259 OID 36779)
-- Name: work_alert_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_alert_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6990 (class 0 OID 0)
-- Dependencies: 379
-- Name: work_alert_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_alert_property_id_seq OWNED BY common.work_alert_property.id;


--
-- TOC entry 286 (class 1259 OID 35059)
-- Name: work_capability; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_capability (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    capacity_type_id bigint NOT NULL,
    reason character varying(100) NOT NULL,
    confidence_factor bigint,
    hierarchy_scope_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL
);


--
-- TOC entry 285 (class 1259 OID 35057)
-- Name: work_capability_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_capability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6991 (class 0 OID 0)
-- Dependencies: 285
-- Name: work_capability_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_capability_id_seq OWNED BY common.work_capability.id;


--
-- TOC entry 334 (class 1259 OID 35912)
-- Name: work_capability_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_capability_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_capability_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 333 (class 1259 OID 35910)
-- Name: work_capability_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_capability_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6992 (class 0 OID 0)
-- Dependencies: 333
-- Name: work_capability_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_capability_property_id_seq OWNED BY common.work_capability_property.id;


--
-- TOC entry 416 (class 1259 OID 37562)
-- Name: work_master_capability; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_master_capability (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_master_id bigint NOT NULL,
    capacity_type_id bigint NOT NULL,
    reason character varying(100) NOT NULL,
    confidence_factor bigint,
    hierarchy_scope_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL,
    work_capability_id bigint
);


--
-- TOC entry 415 (class 1259 OID 37560)
-- Name: work_master_capability_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_master_capability_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6993 (class 0 OID 0)
-- Dependencies: 415
-- Name: work_master_capability_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_master_capability_id_seq OWNED BY common.work_master_capability.id;


--
-- TOC entry 452 (class 1259 OID 38486)
-- Name: work_master_capability_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_master_capability_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_master_capability_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 451 (class 1259 OID 38484)
-- Name: work_master_capability_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_master_capability_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6994 (class 0 OID 0)
-- Dependencies: 451
-- Name: work_master_capability_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_master_capability_property_id_seq OWNED BY common.work_master_capability_property.id;


--
-- TOC entry 336 (class 1259 OID 35953)
-- Name: work_performance; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_performance (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_type_id bigint,
    work_schedule_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 335 (class 1259 OID 35951)
-- Name: work_performance_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_performance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6995 (class 0 OID 0)
-- Dependencies: 335
-- Name: work_performance_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_performance_id_seq OWNED BY common.work_performance.id;


--
-- TOC entry 382 (class 1259 OID 36827)
-- Name: work_performance_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_performance_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_performance_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 381 (class 1259 OID 36825)
-- Name: work_performance_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_performance_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6996 (class 0 OID 0)
-- Dependencies: 381
-- Name: work_performance_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_performance_property_id_seq OWNED BY common.work_performance_property.id;


--
-- TOC entry 338 (class 1259 OID 35990)
-- Name: work_request; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_request (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_type_id bigint,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    priority_id bigint NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    work_schedule_id bigint,
    parent_id bigint
);


--
-- TOC entry 337 (class 1259 OID 35988)
-- Name: work_request_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_request_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6997 (class 0 OID 0)
-- Dependencies: 337
-- Name: work_request_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_request_id_seq OWNED BY common.work_request.id;


--
-- TOC entry 384 (class 1259 OID 36868)
-- Name: work_request_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_request_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_request_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 383 (class 1259 OID 36866)
-- Name: work_request_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_request_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6998 (class 0 OID 0)
-- Dependencies: 383
-- Name: work_request_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_request_property_id_seq OWNED BY common.work_request_property.id;


--
-- TOC entry 386 (class 1259 OID 36909)
-- Name: work_response; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_response (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_type_id bigint,
    work_request_id bigint NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    work_performance_id bigint NOT NULL,
    response_status_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 385 (class 1259 OID 36907)
-- Name: work_response_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_response_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 6999 (class 0 OID 0)
-- Dependencies: 385
-- Name: work_response_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_response_id_seq OWNED BY common.work_response.id;


--
-- TOC entry 418 (class 1259 OID 37598)
-- Name: work_response_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_response_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_response_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 417 (class 1259 OID 37596)
-- Name: work_response_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_response_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7000 (class 0 OID 0)
-- Dependencies: 417
-- Name: work_response_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_response_property_id_seq OWNED BY common.work_response_property.id;


--
-- TOC entry 288 (class 1259 OID 35085)
-- Name: work_schedule; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_schedule (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_type_id bigint,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    published_date timestamp without time zone NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 287 (class 1259 OID 35083)
-- Name: work_schedule_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_schedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7001 (class 0 OID 0)
-- Dependencies: 287
-- Name: work_schedule_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_schedule_id_seq OWNED BY common.work_schedule.id;


--
-- TOC entry 340 (class 1259 OID 36032)
-- Name: work_schedule_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_schedule_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_schedule_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 339 (class 1259 OID 36030)
-- Name: work_schedule_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_schedule_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7002 (class 0 OID 0)
-- Dependencies: 339
-- Name: work_schedule_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_schedule_property_id_seq OWNED BY common.work_schedule_property.id;


--
-- TOC entry 532 (class 1259 OID 41624)
-- Name: document; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.document (
    id bigint NOT NULL,
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    description character varying(500),
    sort_order integer DEFAULT 0 NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    path character varying(500) NOT NULL,
    resource_id bigint NOT NULL,
    resource_type_id bigint NOT NULL,
    document_category_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 531 (class 1259 OID 41622)
-- Name: document_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7003 (class 0 OID 0)
-- Dependencies: 531
-- Name: document_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.document_id_seq OWNED BY common.document.id;


--
-- TOC entry 236 (class 1259 OID 34149)
-- Name: equipment; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    equipment_level_id bigint,
    parent_id bigint
);


--
-- TOC entry 342 (class 1259 OID 36073)
-- Name: equipment_asset_mapping; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_asset_mapping (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    equipment_id bigint NOT NULL,
    physical_asset_id bigint NOT NULL
);


--
-- TOC entry 341 (class 1259 OID 36071)
-- Name: equipment_asset_mapping_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_asset_mapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7004 (class 0 OID 0)
-- Dependencies: 341
-- Name: equipment_asset_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_asset_mapping_id_seq OWNED BY common.equipment_asset_mapping.id;


--
-- TOC entry 238 (class 1259 OID 34175)
-- Name: equipment_class; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_class (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    equipment_level_id bigint,
    is_instantiable boolean DEFAULT true NOT NULL,
    parent_id bigint
);


--
-- TOC entry 237 (class 1259 OID 34173)
-- Name: equipment_class_id_seq; Type: S    EQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7005 (class 0 OID 0)
-- Dependencies: 237
-- Name: equipment_class_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_class_id_seq OWNED BY common.equipment_class.id;


--
-- TOC entry 290 (class 1259 OID 35117)
-- Name: equipment_class_mapping; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_class_mapping (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    equipment_class_id bigint NOT NULL,
    equipment_id bigint NOT NULL
);


--
-- TOC entry 289 (class 1259 OID 35115)
-- Name: equipment_class_mapping_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_class_mapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7006 (class 0 OID 0)
-- Dependencies: 289
-- Name: equipment_class_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_class_mapping_id_seq OWNED BY common.equipment_class_mapping.id;


--
-- TOC entry 292 (class 1259 OID 35139)
-- Name: equipment_class_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_class_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    equipment_class_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 291 (class 1259 OID 35137)
-- Name: equipment_class_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_class_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7007 (class 0 OID 0)
-- Dependencies: 291
-- Name: equipment_class_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_class_property_id_seq OWNED BY common.equipment_class_property.id;


--
-- TOC entry 235 (class 1259 OID 34147)
-- Name: equipment_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7008 (class 0 OID 0)
-- Dependencies: 235
-- Name: equipment_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_id_seq OWNED BY common.equipment.id;


--
-- TOC entry 294 (class 1259 OID 35180)
-- Name: equipment_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    equipment_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 293 (class 1259 OID 35178)
-- Name: equipment_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7009 (class 0 OID 0)
-- Dependencies: 293
-- Name: equipment_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_property_id_seq OWNED BY common.equipment_property.id;


--
-- TOC entry 344 (class 1259 OID 36095)
-- Name: equipment_segment_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_segment_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    equipment_class_id bigint NOT NULL,
    equipment_id bigint,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    process_segment_id bigint NOT NULL
);


--
-- TOC entry 343 (class 1259 OID 36093)
-- Name: equipment_segment_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_segment_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7010 (class 0 OID 0)
-- Dependencies: 343
-- Name: equipment_segment_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_segment_specification_id_seq OWNED BY common.equipment_segment_specification.id;


--
-- TOC entry 388 (class 1259 OID 36956)
-- Name: equipment_segment_specification_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_segment_specification_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    equipment_segment_specification_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 387 (class 1259 OID 36954)
-- Name: equipment_segment_specification_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_segment_specification_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7011 (class 0 OID 0)
-- Dependencies: 387
-- Name: equipment_segment_specification_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_segment_specification_property_id_seq OWNED BY common.equipment_segment_specification_property.id;


--
-- TOC entry 530 (class 1259 OID 41595)
-- Name: equipment_template; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_template (
    id bigint NOT NULL,
    equipment_class_id bigint NOT NULL,
    parent_id bigint,
    quantity integer DEFAULT 1 NOT NULL,
    is_instantiable boolean DEFAULT true NOT NULL,
    equipment_level_id bigint,
    is_deleted boolean DEFAULT false NOT NULL
);


--
-- TOC entry 529 (class 1259 OID 41593)
-- Name: equipment_template_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_template_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7012 (class 0 OID 0)
-- Dependencies: 529
-- Name: equipment_template_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_template_id_seq OWNED BY common.equipment_template.id;


--
-- TOC entry 202 (class 1259 OID 33827)
-- Name: event_class; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.event_class (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    parent_id bigint
);


--
-- TOC entry 201 (class 1259 OID 33825)
-- Name: event_class_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.event_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7013 (class 0 OID 0)
-- Dependencies: 201
-- Name: event_class_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.event_class_id_seq OWNED BY common.event_class.id;


--
-- TOC entry 240 (class 1259 OID 34202)
-- Name: event_class_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.event_class_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    is_client_specific boolean DEFAULT false NOT NULL,
    event_class_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 239 (class 1259 OID 34200)
-- Name: event_class_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.event_class_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7014 (class 0 OID 0)
-- Dependencies: 239
-- Name: event_class_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.event_class_property_id_seq OWNED BY common.event_class_property.id;


--
-- TOC entry 226 (class 1259 OID 34033)
-- Name: event_definition; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.event_definition (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    event_class_id bigint
);


--
-- TOC entry 225 (class 1259 OID 34031)
-- Name: event_definition_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.event_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7015 (class 0 OID 0)
-- Dependencies: 225
-- Name: event_definition_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.event_definition_id_seq OWNED BY common.event_definition.id;


--
-- TOC entry 242 (class 1259 OID 34244)
-- Name: event_definition_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.event_definition_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    is_client_specific boolean DEFAULT false NOT NULL,
    event_definition_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 241 (class 1259 OID 34242)
-- Name: event_definition_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.event_definition_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7016 (class 0 OID 0)
-- Dependencies: 241
-- Name: event_definition_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.event_definition_property_id_seq OWNED BY common.event_definition_property.id;


--
-- TOC entry 244 (class 1259 OID 34286)
-- Name: event_object_relationship; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.event_object_relationship (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    object_type_id bigint NOT NULL,
    object_id bigint NOT NULL,
    event_type_id bigint NOT NULL,
    event_definition_id bigint NOT NULL,
    event_tag_code character varying(100) NOT NULL
);


--
-- TOC entry 243 (class 1259 OID 34284)
-- Name: event_object_relationship_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.event_object_relationship_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7017 (class 0 OID 0)
-- Dependencies: 243
-- Name: event_object_relationship_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.event_object_relationship_id_seq OWNED BY common.event_object_relationship.id;


--
-- TOC entry 296 (class 1259 OID 35221)
-- Name: event_object_relationship_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.event_object_relationship_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    is_client_specific boolean DEFAULT false NOT NULL,
    event_object_relationship_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 295 (class 1259 OID 35219)
-- Name: event_object_relationship_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.event_object_relationship_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7018 (class 0 OID 0)
-- Dependencies: 295
-- Name: event_object_relationship_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.event_object_relationship_property_id_seq OWNED BY common.event_object_relationship_property.id;


--
-- TOC entry 346 (class 1259 OID 36137)
-- Name: from_resource_reference; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.from_resource_reference (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    resource_type_id bigint NOT NULL,
    resource_id bigint NOT NULL,
    resource_network_connection_id bigint NOT NULL
);


--
-- TOC entry 345 (class 1259 OID 36135)
-- Name: from_resource_reference_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.from_resource_reference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7019 (class 0 OID 0)
-- Dependencies: 345
-- Name: from_resource_reference_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.from_resource_reference_id_seq OWNED BY common.from_resource_reference.id;


--
-- TOC entry 390 (class 1259 OID 37004)
-- Name: from_resource_reference_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.from_resource_reference_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    from_resource_reference_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 389 (class 1259 OID 37002)
-- Name: from_resource_reference_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.from_resource_reference_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7020 (class 0 OID 0)
-- Dependencies: 389
-- Name: from_resource_reference_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.from_resource_reference_property_id_seq OWNED BY common.from_resource_reference_property.id;


--
-- TOC entry 204 (class 1259 OID 33848)
-- Name: knowledge_category; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.knowledge_category (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    parent_id bigint
);


--
-- TOC entry 203 (class 1259 OID 33846)
-- Name: knowledge_category_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.knowledge_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7021 (class 0 OID 0)
-- Dependencies: 203
-- Name: knowledge_category_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.knowledge_category_id_seq OWNED BY common.knowledge_category.id;


--
-- TOC entry 246 (class 1259 OID 34316)
-- Name: knowledge_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.knowledge_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    category_id bigint NOT NULL,
    is_client_specific boolean DEFAULT false NOT NULL,
    is_hidden boolean DEFAULT false NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 245 (class 1259 OID 34314)
-- Name: knowledge_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.knowledge_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7022 (class 0 OID 0)
-- Dependencies: 245
-- Name: knowledge_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.knowledge_property_id_seq OWNED BY common.knowledge_property.id;


--
-- TOC entry 228 (class 1259 OID 34053)
-- Name: lookup; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.lookup (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    category_id bigint NOT NULL,
    is_client_specific boolean DEFAULT false NOT NULL,
    is_hidden boolean DEFAULT false NOT NULL,
    value character varying(100),
    parent_id bigint
);


--
-- TOC entry 206 (class 1259 OID 33869)
-- Name: lookup_category; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.lookup_category (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 205 (class 1259 OID 33867)
-- Name: lookup_category_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.lookup_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7023 (class 0 OID 0)
-- Dependencies: 205
-- Name: lookup_category_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.lookup_category_id_seq OWNED BY common.lookup_category.id;


--
-- TOC entry 227 (class 1259 OID 34051)
-- Name: lookup_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7024 (class 0 OID 0)
-- Dependencies: 227
-- Name: lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.lookup_id_seq OWNED BY common.lookup.id;


--
-- TOC entry 528 (class 1259 OID 41567)
-- Name: material_assembly_definition; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_assembly_definition (
    id bigint NOT NULL,
    material_definition_id bigint NOT NULL,
    assembled_from_id bigint NOT NULL,
    assembly_type_id bigint,
    assembly_relationship_id bigint
);


--
-- TOC entry 527 (class 1259 OID 41565)
-- Name: material_assembly_definition_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_assembly_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7025 (class 0 OID 0)
-- Dependencies: 527
-- Name: material_assembly_definition_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_assembly_definition_id_seq OWNED BY common.material_assembly_definition.id;


--
-- TOC entry 248 (class 1259 OID 34359)
-- Name: material_class; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_class (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    is_instantiable boolean DEFAULT true NOT NULL,
    parent_id bigint
);


--
-- TOC entry 247 (class 1259 OID 34357)
-- Name: material_class_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7026 (class 0 OID 0)
-- Dependencies: 247
-- Name: material_class_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_class_id_seq OWNED BY common.material_class.id;


--
-- TOC entry 298 (class 1259 OID 35263)
-- Name: material_class_mapping; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_class_mapping (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    material_class_id bigint NOT NULL,
    material_definition_id bigint NOT NULL
);


--
-- TOC entry 297 (class 1259 OID 35261)
-- Name: material_class_mapping_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_class_mapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7027 (class 0 OID 0)
-- Dependencies: 297
-- Name: material_class_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_class_mapping_id_seq OWNED BY common.material_class_mapping.id;


--
-- TOC entry 300 (class 1259 OID 35285)
-- Name: material_class_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_class_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    material_class_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 299 (class 1259 OID 35283)
-- Name: material_class_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_class_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7028 (class 0 OID 0)
-- Dependencies: 299
-- Name: material_class_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_class_property_id_seq OWNED BY common.material_class_property.id;


--
-- TOC entry 250 (class 1259 OID 34391)
-- Name: material_definition; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_definition (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    parent_id bigint
);


--
-- TOC entry 249 (class 1259 OID 34389)
-- Name: material_definition_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7029 (class 0 OID 0)
-- Dependencies: 249
-- Name: material_definition_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_definition_id_seq OWNED BY common.material_definition.id;


--
-- TOC entry 302 (class 1259 OID 35326)
-- Name: material_definition_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_definition_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    material_definition_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 301 (class 1259 OID 35324)
-- Name: material_definition_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_definition_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7030 (class 0 OID 0)
-- Dependencies: 301
-- Name: material_definition_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_definition_property_id_seq OWNED BY common.material_definition_property.id;


--
-- TOC entry 348 (class 1259 OID 36162)
-- Name: material_segment_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_segment_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    material_class_id bigint NOT NULL,
    material_definition_id bigint,
    resource_use_id bigint,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    process_segment_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 347 (class 1259 OID 36160)
-- Name: material_segment_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_segment_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7031 (class 0 OID 0)
-- Dependencies: 347
-- Name: material_segment_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_segment_specification_id_seq OWNED BY common.material_segment_specification.id;


--
-- TOC entry 420 (class 1259 OID 37639)
-- Name: material_segment_specification_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_segment_specification_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    material_segment_specification_id bigint,
    operations_material_bill_item_id bigint,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 419 (class 1259 OID 37637)
-- Name: material_segment_specification_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_segment_specification_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7032 (class 0 OID 0)
-- Dependencies: 419
-- Name: material_segment_specification_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_segment_specification_property_id_seq OWNED BY common.material_segment_specification_property.id;


--
-- TOC entry 208 (class 1259 OID 33884)
-- Name: person; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.person (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 207 (class 1259 OID 33882)
-- Name: person_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.person_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7033 (class 0 OID 0)
-- Dependencies: 207
-- Name: person_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.person_id_seq OWNED BY common.person.id;


--
-- TOC entry 252 (class 1259 OID 34422)
-- Name: person_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.person_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    person_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 251 (class 1259 OID 34420)
-- Name: person_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.person_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7034 (class 0 OID 0)
-- Dependencies: 251
-- Name: person_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.person_property_id_seq OWNED BY common.person_property.id;


--
-- TOC entry 210 (class 1259 OID 33899)
-- Name: personnel_class; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_class (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    is_instantiable boolean DEFAULT true NOT NULL,
    parent_id bigint
);


--
-- TOC entry 209 (class 1259 OID 33897)
-- Name: personnel_class_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7035 (class 0 OID 0)
-- Dependencies: 209
-- Name: personnel_class_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_class_id_seq OWNED BY common.personnel_class.id;


--
-- TOC entry 230 (class 1259 OID 34082)
-- Name: personnel_class_mapping; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_class_mapping (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    personnel_class_id bigint NOT NULL,
    person_id bigint NOT NULL
);


--
-- TOC entry 229 (class 1259 OID 34080)
-- Name: personnel_class_mapping_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_class_mapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7036 (class 0 OID 0)
-- Dependencies: 229
-- Name: personnel_class_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_class_mapping_id_seq OWNED BY common.personnel_class_mapping.id;


--
-- TOC entry 254 (class 1259 OID 34463)
-- Name: personnel_class_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_class_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    personnel_class_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 253 (class 1259 OID 34461)
-- Name: personnel_class_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_class_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7037 (class 0 OID 0)
-- Dependencies: 253
-- Name: personnel_class_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_class_property_id_seq OWNED BY common.personnel_class_property.id;


--
-- TOC entry 350 (class 1259 OID 36220)
-- Name: personnel_segment_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_segment_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    personnel_class_id bigint NOT NULL,
    person_id bigint,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    process_segment_id bigint NOT NULL
);


--
-- TOC entry 349 (class 1259 OID 36218)
-- Name: personnel_segment_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_segment_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7038 (class 0 OID 0)
-- Dependencies: 349
-- Name: personnel_segment_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_segment_specification_id_seq OWNED BY common.personnel_segment_specification.id;


--
-- TOC entry 392 (class 1259 OID 37045)
-- Name: personnel_segment_specification_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_segment_specification_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    personnel_segment_specification_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 391 (class 1259 OID 37043)
-- Name: personnel_segment_specification_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_segment_specification_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7039 (class 0 OID 0)
-- Dependencies: 391
-- Name: personnel_segment_specification_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_segment_specification_property_id_seq OWNED BY common.personnel_segment_specification_property.id;


--
-- TOC entry 304 (class 1259 OID 35367)
-- Name: physical_asset; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    physical_location_id bigint NOT NULL,
    fixed_asset_name character varying(50),
    vendor_name character varying(50),
    parent_id bigint
);


--
-- TOC entry 212 (class 1259 OID 33921)
-- Name: physical_asset_class; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_class (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    manufacturer character varying(100),
    is_instantiable boolean DEFAULT true NOT NULL,
    parent_id bigint
);


--
-- TOC entry 211 (class 1259 OID 33919)
-- Name: physical_asset_class_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_class_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7040 (class 0 OID 0)
-- Dependencies: 211
-- Name: physical_asset_class_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_class_id_seq OWNED BY common.physical_asset_class.id;


--
-- TOC entry 352 (class 1259 OID 36262)
-- Name: physical_asset_class_mapping; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_class_mapping (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    physical_asset_class_id bigint NOT NULL,
    physical_asset_id bigint NOT NULL
);


--
-- TOC entry 351 (class 1259 OID 36260)
-- Name: physical_asset_class_mapping_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_class_mapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7041 (class 0 OID 0)
-- Dependencies: 351
-- Name: physical_asset_class_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_class_mapping_id_seq OWNED BY common.physical_asset_class_mapping.id;


--
-- TOC entry 256 (class 1259 OID 34504)
-- Name: physical_asset_class_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_class_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    physical_asset_class_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 255 (class 1259 OID 34502)
-- Name: physical_asset_class_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_class_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7042 (class 0 OID 0)
-- Dependencies: 255
-- Name: physical_asset_class_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_class_property_id_seq OWNED BY common.physical_asset_class_property.id;


--
-- TOC entry 303 (class 1259 OID 35365)
-- Name: physical_asset_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7043 (class 0 OID 0)
-- Dependencies: 303
-- Name: physical_asset_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_id_seq OWNED BY common.physical_asset.id;


--
-- TOC entry 354 (class 1259 OID 36284)
-- Name: physical_asset_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    physical_asset_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 353 (class 1259 OID 36282)
-- Name: physical_asset_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7044 (class 0 OID 0)
-- Dependencies: 353
-- Name: physical_asset_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_property_id_seq OWNED BY common.physical_asset_property.id;


--
-- TOC entry 356 (class 1259 OID 36325)
-- Name: physical_asset_segment_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_segment_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    physical_asset_class_id bigint NOT NULL,
    physical_asset_id bigint,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    process_segment_id bigint NOT NULL
);


--
-- TOC entry 355 (class 1259 OID 36323)
-- Name: physical_asset_segment_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_segment_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7045 (class 0 OID 0)
-- Dependencies: 355
-- Name: physical_asset_segment_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_segment_specification_id_seq OWNED BY common.physical_asset_segment_specification.id;


--
-- TOC entry 394 (class 1259 OID 37093)
-- Name: physical_asset_segment_specification_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_segment_specification_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    physical_asset_segment_specification_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 393 (class 1259 OID 37091)
-- Name: physical_asset_segment_specification_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_segment_specification_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7046 (class 0 OID 0)
-- Dependencies: 393
-- Name: physical_asset_segment_specification_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_segment_specification_property_id_seq OWNED BY common.physical_asset_segment_specification_property.id;


--
-- TOC entry 306 (class 1259 OID 35393)
-- Name: process_segment; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.process_segment (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    duration numeric(18,0),
    duration_uom_id bigint,
    operations_type_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 358 (class 1259 OID 36367)
-- Name: process_segment_dependency; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.process_segment_dependency (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    segment_id bigint NOT NULL,
    dependency_type_id bigint NOT NULL,
    dependant_segment_id bigint NOT NULL,
    dependency_factor_id bigint NOT NULL,
    value character varying(100),
    max_value character varying(50),
    min_value character varying(50),
    value_uom_id bigint NOT NULL
);


--
-- TOC entry 357 (class 1259 OID 36365)
-- Name: process_segment_dependency_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.process_segment_dependency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7047 (class 0 OID 0)
-- Dependencies: 357
-- Name: process_segment_dependency_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.process_segment_dependency_id_seq OWNED BY common.process_segment_dependency.id;


--
-- TOC entry 305 (class 1259 OID 35391)
-- Name: process_segment_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.process_segment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7048 (class 0 OID 0)
-- Dependencies: 305
-- Name: process_segment_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.process_segment_id_seq OWNED BY common.process_segment.id;


--
-- TOC entry 360 (class 1259 OID 36409)
-- Name: process_segment_parameter; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.process_segment_parameter (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    process_segment_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 359 (class 1259 OID 36407)
-- Name: process_segment_parameter_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.process_segment_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7049 (class 0 OID 0)
-- Dependencies: 359
-- Name: process_segment_parameter_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.process_segment_parameter_id_seq OWNED BY common.process_segment_parameter.id;


--
-- TOC entry 308 (class 1259 OID 35430)
-- Name: resource_network_connection; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.resource_network_connection (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    resource_relationship_network_id bigint NOT NULL,
    resource_network_connection_type_id bigint NOT NULL
);


--
-- TOC entry 307 (class 1259 OID 35428)
-- Name: resource_network_connection_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.resource_network_connection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7050 (class 0 OID 0)
-- Dependencies: 307
-- Name: resource_network_connection_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.resource_network_connection_id_seq OWNED BY common.resource_network_connection.id;


--
-- TOC entry 362 (class 1259 OID 36450)
-- Name: resource_network_connection_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.resource_network_connection_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    resource_network_connection_id bigint NOT NULL,
    resource_network_connection_type_property_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 361 (class 1259 OID 36448)
-- Name: resource_network_connection_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.resource_network_connection_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7051 (class 0 OID 0)
-- Dependencies: 361
-- Name: resource_network_connection_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.resource_network_connection_property_id_seq OWNED BY common.resource_network_connection_property.id;


--
-- TOC entry 214 (class 1259 OID 33943)
-- Name: resource_network_connection_type; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.resource_network_connection_type (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    type character varying(100)
);


--
-- TOC entry 213 (class 1259 OID 33941)
-- Name: resource_network_connection_type_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.resource_network_connection_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7052 (class 0 OID 0)
-- Dependencies: 213
-- Name: resource_network_connection_type_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.resource_network_connection_type_id_seq OWNED BY common.resource_network_connection_type.id;


--
-- TOC entry 258 (class 1259 OID 34545)
-- Name: resource_network_connection_type_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.resource_network_connection_type_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    resource_network_connection_type_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 257 (class 1259 OID 34543)
-- Name: resource_network_connection_type_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.resource_network_connection_type_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7053 (class 0 OID 0)
-- Dependencies: 257
-- Name: resource_network_connection_type_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.resource_network_connection_type_property_id_seq OWNED BY common.resource_network_connection_type_property.id;


--
-- TOC entry 260 (class 1259 OID 34586)
-- Name: resource_relationship_network; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.resource_relationship_network (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    relationship_type_id bigint NOT NULL,
    relationship_form_id bigint NOT NULL
);


--
-- TOC entry 259 (class 1259 OID 34584)
-- Name: resource_relationship_network_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.resource_relationship_network_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7054 (class 0 OID 0)
-- Dependencies: 259
-- Name: resource_relationship_network_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.resource_relationship_network_id_seq OWNED BY common.resource_relationship_network.id;


--
-- TOC entry 364 (class 1259 OID 36496)
-- Name: to_resource_reference; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.to_resource_reference (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    resource_type_id bigint NOT NULL,
    resource_id bigint NOT NULL,
    resource_network_connection_id bigint NOT NULL
);


--
-- TOC entry 363 (class 1259 OID 36494)
-- Name: to_resource_reference_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.to_resource_reference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7055 (class 0 OID 0)
-- Dependencies: 363
-- Name: to_resource_reference_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.to_resource_reference_id_seq OWNED BY common.to_resource_reference.id;


--
-- TOC entry 396 (class 1259 OID 37141)
-- Name: to_resource_reference_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.to_resource_reference_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    to_resource_reference_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 395 (class 1259 OID 37139)
-- Name: to_resource_reference_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.to_resource_reference_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7056 (class 0 OID 0)
-- Dependencies: 395
-- Name: to_resource_reference_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.to_resource_reference_property_id_seq OWNED BY common.to_resource_reference_property.id;


--
-- TOC entry 232 (class 1259 OID 34104)
-- Name: uom; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.uom (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    symbol character varying(50),
    category_id bigint NOT NULL
);


--
-- TOC entry 216 (class 1259 OID 33958)
-- Name: uom_category; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.uom_category (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 215 (class 1259 OID 33956)
-- Name: uom_category_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.uom_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7057 (class 0 OID 0)
-- Dependencies: 215
-- Name: uom_category_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.uom_category_id_seq OWNED BY common.uom_category.id;


--
-- TOC entry 231 (class 1259 OID 34102)
-- Name: uom_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.uom_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7058 (class 0 OID 0)
-- Dependencies: 231
-- Name: uom_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.uom_id_seq OWNED BY common.uom.id;


--
-- TOC entry 218 (class 1259 OID 33973)
-- Name: work_calendar; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_calendar (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 220 (class 1259 OID 33988)
-- Name: work_calendar_definition; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_calendar_definition (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 262 (class 1259 OID 34611)
-- Name: work_calendar_definition_entry; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_calendar_definition_entry (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    start_rule timestamp without time zone NOT NULL,
    recurrence_time_interval_rule character varying(50) NOT NULL,
    duration_rule character varying(100) NOT NULL,
    entry_type_id bigint NOT NULL,
    work_calendar_definition_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 261 (class 1259 OID 34609)
-- Name: work_calendar_definition_entry_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_calendar_definition_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7059 (class 0 OID 0)
-- Dependencies: 261
-- Name: work_calendar_definition_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_calendar_definition_entry_id_seq OWNED BY common.work_calendar_definition_entry.id;


--
-- TOC entry 310 (class 1259 OID 35455)
-- Name: work_calendar_definition_entry_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_calendar_definition_entry_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_calendar_definition_entry_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 309 (class 1259 OID 35453)
-- Name: work_calendar_definition_entry_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_calendar_definition_entry_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7060 (class 0 OID 0)
-- Dependencies: 309
-- Name: work_calendar_definition_entry_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_calendar_definition_entry_property_id_seq OWNED BY common.work_calendar_definition_entry_property.id;


--
-- TOC entry 219 (class 1259 OID 33986)
-- Name: work_calendar_definition_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_calendar_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7061 (class 0 OID 0)
-- Dependencies: 219
-- Name: work_calendar_definition_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_calendar_definition_id_seq OWNED BY common.work_calendar_definition.id;


--
-- TOC entry 264 (class 1259 OID 34642)
-- Name: work_calendar_definition_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_calendar_definition_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_calendar_definition_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 263 (class 1259 OID 34640)
-- Name: work_calendar_definition_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_calendar_definition_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7062 (class 0 OID 0)
-- Dependencies: 263
-- Name: work_calendar_definition_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_calendar_definition_property_id_seq OWNED BY common.work_calendar_definition_property.id;


--
-- TOC entry 312 (class 1259 OID 35496)
-- Name: work_calendar_entry; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_calendar_entry (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    entry_type_id bigint NOT NULL,
    work_calendar_definition_entry_id bigint,
    work_calendar_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 311 (class 1259 OID 35494)
-- Name: work_calendar_entry_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_calendar_entry_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7063 (class 0 OID 0)
-- Dependencies: 311
-- Name: work_calendar_entry_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_calendar_entry_id_seq OWNED BY common.work_calendar_entry.id;


--
-- TOC entry 366 (class 1259 OID 36521)
-- Name: work_calendar_entry_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_calendar_entry_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_calendar_entry_id bigint NOT NULL,
    work_calendar_definition_entry_property_id bigint,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 365 (class 1259 OID 36519)
-- Name: work_calendar_entry_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_calendar_entry_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7064 (class 0 OID 0)
-- Dependencies: 365
-- Name: work_calendar_entry_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_calendar_entry_property_id_seq OWNED BY common.work_calendar_entry_property.id;


--
-- TOC entry 217 (class 1259 OID 33971)
-- Name: work_calendar_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_calendar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7065 (class 0 OID 0)
-- Dependencies: 217
-- Name: work_calendar_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_calendar_id_seq OWNED BY common.work_calendar.id;


--
-- TOC entry 266 (class 1259 OID 34683)
-- Name: work_calendar_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_calendar_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_calendar_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 265 (class 1259 OID 34681)
-- Name: work_calendar_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_calendar_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7066 (class 0 OID 0)
-- Dependencies: 265
-- Name: work_calendar_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_calendar_property_id_seq OWNED BY common.work_calendar_property.id;


--
-- TOC entry 522 (class 1259 OID 40159)
-- Name: privilege; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.au_privilege (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    app_id bigint NOT NULL
);


--
-- TOC entry 521 (class 1259 OID 40157)
-- Name: privilege_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.au_privilege_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7067 (class 0 OID 0)
-- Dependencies: 521
-- Name: privilege_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.au_privilege_id_seq OWNED BY common.au_privilege.id;


--
-- TOC entry 524 (class 1259 OID 40179)
-- Name: role_privileges; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.au_role_privileges (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    role_id bigint NOT NULL,
    privilege_id bigint NOT NULL
);


--
-- TOC entry 523 (class 1259 OID 40177)
-- Name: role_privileges_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.au_role_privileges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7068 (class 0 OID 0)
-- Dependencies: 523
-- Name: role_privileges_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.au_role_privileges_id_seq OWNED BY common.au_role_privileges.id;


--
-- TOC entry 526 (class 1259 OID 40201)
-- Name: user_restricted_privileges; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.au_user_restricted_privileges (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    user_id bigint NOT NULL,
    privilege_id bigint NOT NULL
);


--
-- TOC entry 525 (class 1259 OID 40199)
-- Name: user_restricted_privileges_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.au_user_restricted_privileges_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7069 (class 0 OID 0)
-- Dependencies: 525
-- Name: user_restricted_privileges_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.au_user_restricted_privileges_id_seq OWNED BY common.au_user_restricted_privileges.id;


--
-- TOC entry 422 (class 1259 OID 37692)
-- Name: equipment_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    equipment_class_id bigint NOT NULL,
    equipment_id bigint,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    operations_segment_id bigint NOT NULL,
    work_master_id bigint
);


--
-- TOC entry 421 (class 1259 OID 37690)
-- Name: equipment_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7070 (class 0 OID 0)
-- Dependencies: 421
-- Name: equipment_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_specification_id_seq OWNED BY common.equipment_specification.id;


--
-- TOC entry 454 (class 1259 OID 38527)
-- Name: equipment_specification_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.equipment_specification_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    equipment_specification_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 453 (class 1259 OID 38525)
-- Name: equipment_specification_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.equipment_specification_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7071 (class 0 OID 0)
-- Dependencies: 453
-- Name: equipment_specification_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.equipment_specification_property_id_seq OWNED BY common.equipment_specification_property.id;


--
-- TOC entry 424 (class 1259 OID 37739)
-- Name: material_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    material_class_id bigint NOT NULL,
    material_definition_id bigint,
    resource_use_id bigint NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    operations_material_bill_item_id bigint,
    operations_segment_id bigint NOT NULL,
    work_master_id bigint,
    parent_id bigint
);


--
-- TOC entry 423 (class 1259 OID 37737)
-- Name: material_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7072 (class 0 OID 0)
-- Dependencies: 423
-- Name: material_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_specification_id_seq OWNED BY common.material_specification.id;


--
-- TOC entry 456 (class 1259 OID 38575)
-- Name: material_specification_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.material_specification_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    material_specification_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 455 (class 1259 OID 38573)
-- Name: material_specification_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.material_specification_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7073 (class 0 OID 0)
-- Dependencies: 455
-- Name: material_specification_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.material_specification_property_id_seq OWNED BY common.material_specification_property.id;


--
-- TOC entry 314 (class 1259 OID 35532)
-- Name: operations_definition; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_definition (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    version character varying(50) NOT NULL,
    operations_type_id bigint NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    bill_of_material_id character varying(100),
    work_master character varying(200),
    bill_of_resource_id character varying(100),
    customer_id character varying(50),
    customer_name character varying(100)
);


--
-- TOC entry 313 (class 1259 OID 35530)
-- Name: operations_definition_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_definition_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7074 (class 0 OID 0)
-- Dependencies: 313
-- Name: operations_definition_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_definition_id_seq OWNED BY common.operations_definition.id;


--
-- TOC entry 368 (class 1259 OID 36567)
-- Name: operations_definition_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_definition_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_definition_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 367 (class 1259 OID 36565)
-- Name: operations_definition_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_definition_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7075 (class 0 OID 0)
-- Dependencies: 367
-- Name: operations_definition_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_definition_property_id_seq OWNED BY common.operations_definition_property.id;


--
-- TOC entry 370 (class 1259 OID 36608)
-- Name: operations_material_bill; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_material_bill (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_definition_id bigint NOT NULL
);


--
-- TOC entry 369 (class 1259 OID 36606)
-- Name: operations_material_bill_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_material_bill_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7076 (class 0 OID 0)
-- Dependencies: 369
-- Name: operations_material_bill_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_material_bill_id_seq OWNED BY common.operations_material_bill.id;


--
-- TOC entry 398 (class 1259 OID 37182)
-- Name: operations_material_bill_item; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_material_bill_item (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    material_class_id bigint NOT NULL,
    material_definition_id bigint NOT NULL,
    resource_use_id bigint,
    assembly_type_id bigint,
    assembly_relationship_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    operations_material_bill_id bigint NOT NULL,
    parent_id bigint
);


--
-- TOC entry 397 (class 1259 OID 37180)
-- Name: operations_material_bill_item_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_material_bill_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7077 (class 0 OID 0)
-- Dependencies: 397
-- Name: operations_material_bill_item_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_material_bill_item_id_seq OWNED BY common.operations_material_bill_item.id;


--
-- TOC entry 372 (class 1259 OID 36628)
-- Name: operations_segment; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_segment (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    hierarchy_scope_id bigint NOT NULL,
    duration numeric(18,0) NOT NULL,
    duration_uom_id bigint NOT NULL,
    process_segment_id bigint,
    operations_type_id bigint NOT NULL,
    work_master character varying(200),
    operations_definition_id bigint,
    parent_id bigint
);


--
-- TOC entry 400 (class 1259 OID 37240)
-- Name: operations_segment_dependency; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_segment_dependency (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    segment_id bigint NOT NULL,
    dependency_type_id bigint NOT NULL,
    dependant_segment_id bigint NOT NULL,
    dependency_factor_id bigint NOT NULL,
    value character varying(100),
    max_value character varying(50),
    min_value character varying(50),
    value_uom_id bigint NOT NULL
);


--
-- TOC entry 399 (class 1259 OID 37238)
-- Name: operations_segment_dependency_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_segment_dependency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7078 (class 0 OID 0)
-- Dependencies: 399
-- Name: operations_segment_dependency_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_segment_dependency_id_seq OWNED BY common.operations_segment_dependency.id;


--
-- TOC entry 371 (class 1259 OID 36626)
-- Name: operations_segment_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_segment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7079 (class 0 OID 0)
-- Dependencies: 371
-- Name: operations_segment_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_segment_id_seq OWNED BY common.operations_segment.id;


--
-- TOC entry 402 (class 1259 OID 37282)
-- Name: operations_segment_mapping; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_segment_mapping (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    process_segment_id bigint NOT NULL,
    operations_segment_id bigint NOT NULL
);


--
-- TOC entry 401 (class 1259 OID 37280)
-- Name: operations_segment_mapping_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_segment_mapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7080 (class 0 OID 0)
-- Dependencies: 401
-- Name: operations_segment_mapping_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_segment_mapping_id_seq OWNED BY common.operations_segment_mapping.id;


--
-- TOC entry 404 (class 1259 OID 37304)
-- Name: operations_segment_parameter_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.operations_segment_parameter_specification (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    operations_segment_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 403 (class 1259 OID 37302)
-- Name: operations_segment_parameter_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.operations_segment_parameter_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7081 (class 0 OID 0)
-- Dependencies: 403
-- Name: operations_segment_parameter_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.operations_segment_parameter_specification_id_seq OWNED BY common.operations_segment_parameter_specification.id;


--
-- TOC entry 426 (class 1259 OID 37807)
-- Name: personnel_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    personnel_class_id bigint NOT NULL,
    person_id bigint,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    operations_segment_id bigint NOT NULL,
    work_master_id bigint
);


--
-- TOC entry 425 (class 1259 OID 37805)
-- Name: personnel_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7082 (class 0 OID 0)
-- Dependencies: 425
-- Name: personnel_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_specification_id_seq OWNED BY common.personnel_specification.id;


--
-- TOC entry 458 (class 1259 OID 38623)
-- Name: personnel_specification_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.personnel_specification_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    personnel_specification_id bigint NOT NULL,
    value_type_id bigint NOT NULL,
    value_uom_id bigint,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 457 (class 1259 OID 38621)
-- Name: personnel_specification_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.personnel_specification_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7083 (class 0 OID 0)
-- Dependencies: 457
-- Name: personnel_specification_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.personnel_specification_property_id_seq OWNED BY common.personnel_specification_property.id;


--
-- TOC entry 428 (class 1259 OID 37854)
-- Name: physical_asset_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    physical_asset_class_id bigint NOT NULL,
    physical_asset_id bigint,
    resource_use_id bigint,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    operations_segment_id bigint NOT NULL,
    work_master_id bigint
);


--
-- TOC entry 427 (class 1259 OID 37852)
-- Name: physical_asset_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7084 (class 0 OID 0)
-- Dependencies: 427
-- Name: physical_asset_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_specification_id_seq OWNED BY common.physical_asset_specification.id;


--
-- TOC entry 460 (class 1259 OID 38670)
-- Name: physical_asset_specification_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.physical_asset_specification_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    quantity numeric(18,0),
    quantity_uom_id bigint,
    physical_asset_specification_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 459 (class 1259 OID 38668)
-- Name: physical_asset_specification_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.physical_asset_specification_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7085 (class 0 OID 0)
-- Dependencies: 459
-- Name: physical_asset_specification_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.physical_asset_specification_property_id_seq OWNED BY common.physical_asset_specification_property.id;


--
-- TOC entry 406 (class 1259 OID 37345)
-- Name: work_master; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.work_master (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    version character varying(50) NOT NULL,
    work_type_id bigint NOT NULL,
    work_master_type_id bigint,
    duration numeric(18,0),
    duration_uom_id bigint,
    operations_definition_id bigint,
    operations_segment_id bigint
);


--
-- TOC entry 405 (class 1259 OID 37343)
-- Name: work_master_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.work_master_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7086 (class 0 OID 0)
-- Dependencies: 405
-- Name: work_master_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.work_master_id_seq OWNED BY common.work_master.id;


--
-- TOC entry 430 (class 1259 OID 37901)
-- Name: workflow_specification; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_master_id bigint
);


--
-- TOC entry 490 (class 1259 OID 39394)
-- Name: workflow_specification_connection; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification_connection (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    workflow_specification_id bigint NOT NULL,
    from_workflow_specification_node_id bigint NOT NULL,
    to_workflow_specification_node_id bigint NOT NULL,
    workflow_specification_connection_type_id bigint NOT NULL
);


--
-- TOC entry 489 (class 1259 OID 39392)
-- Name: workflow_specification_connection_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_connection_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7087 (class 0 OID 0)
-- Dependencies: 489
-- Name: workflow_specification_connection_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_connection_id_seq OWNED BY common.workflow_specification_connection.id;


--
-- TOC entry 512 (class 1259 OID 39927)
-- Name: workflow_specification_connection_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification_connection_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    workflow_specification_connection_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 511 (class 1259 OID 39925)
-- Name: workflow_specification_connection_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_connection_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7088 (class 0 OID 0)
-- Dependencies: 511
-- Name: workflow_specification_connection_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_connection_property_id_seq OWNED BY common.workflow_specification_connection_property.id;


--
-- TOC entry 222 (class 1259 OID 34003)
-- Name: workflow_specification_connection_type; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification_connection_type (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    from_multiplicity character varying(200),
    to_multiplicity character varying(200)
);


--
-- TOC entry 221 (class 1259 OID 34001)
-- Name: workflow_specification_connection_type_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_connection_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7089 (class 0 OID 0)
-- Dependencies: 221
-- Name: workflow_specification_connection_type_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_connection_type_id_seq OWNED BY common.workflow_specification_connection_type.id;


--
-- TOC entry 268 (class 1259 OID 34724)
-- Name: workflow_specification_connection_type_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification_connection_type_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    workflow_specification_connection_type_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 267 (class 1259 OID 34722)
-- Name: workflow_specification_connection_type_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_connection_type_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7090 (class 0 OID 0)
-- Dependencies: 267
-- Name: workflow_specification_connection_type_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_connection_type_property_id_seq OWNED BY common.workflow_specification_connection_type_property.id;


--
-- TOC entry 429 (class 1259 OID 37899)
-- Name: workflow_specification_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7091 (class 0 OID 0)
-- Dependencies: 429
-- Name: workflow_specification_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_id_seq OWNED BY common.workflow_specification.id;


--
-- TOC entry 462 (class 1259 OID 38718)
-- Name: workflow_specification_node; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification_node (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    work_master_id bigint,
    workflow_specification_id bigint NOT NULL,
    child_workflow_specification_id bigint NOT NULL,
    workflow_specification_node_type_id bigint NOT NULL
);


--
-- TOC entry 461 (class 1259 OID 38716)
-- Name: workflow_specification_node_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7092 (class 0 OID 0)
-- Dependencies: 461
-- Name: workflow_specification_node_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_node_id_seq OWNED BY common.workflow_specification_node.id;


--
-- TOC entry 492 (class 1259 OID 39429)
-- Name: workflow_specification_node_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification_node_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    workflow_specification_node_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 491 (class 1259 OID 39427)
-- Name: workflow_specification_node_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_node_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7093 (class 0 OID 0)
-- Dependencies: 491
-- Name: workflow_specification_node_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_node_property_id_seq OWNED BY common.workflow_specification_node_property.id;


--
-- TOC entry 224 (class 1259 OID 34018)
-- Name: workflow_specification_node_type; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification_node_type (
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 223 (class 1259 OID 34016)
-- Name: workflow_specification_node_type_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_node_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7094 (class 0 OID 0)
-- Dependencies: 223
-- Name: workflow_specification_node_type_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_node_type_id_seq OWNED BY common.workflow_specification_node_type.id;


--
-- TOC entry 270 (class 1259 OID 34765)
-- Name: workflow_specification_node_type_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification_node_type_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    workflow_specification_node_type_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 269 (class 1259 OID 34763)
-- Name: workflow_specification_node_type_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_node_type_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7095 (class 0 OID 0)
-- Dependencies: 269
-- Name: workflow_specification_node_type_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_node_type_property_id_seq OWNED BY common.workflow_specification_node_type_property.id;


--
-- TOC entry 464 (class 1259 OID 38753)
-- Name: workflow_specification_property; Type: TABLE; Schema: common; Owner: -
--

CREATE TABLE common.workflow_specification_property (
    value text,
    max_value character varying(50),
    min_value character varying(50),
    is_set_point boolean DEFAULT false NOT NULL,
    is_required boolean DEFAULT false NOT NULL,
    script_association character varying(100),
    id bigint NOT NULL,
    is_deleted boolean DEFAULT false NOT NULL,
    description character varying(500),
    code character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    sort_order integer DEFAULT 0 NOT NULL,
    workflow_specification_id bigint NOT NULL,
    value_uom_id bigint,
    value_type_id bigint NOT NULL,
    parent_id bigint,
    value_number numeric(18,0),
    uom_category_id bigint,
    sensor_id bigint,
    sensor_type_id bigint
);


--
-- TOC entry 463 (class 1259 OID 38751)
-- Name: workflow_specification_property_id_seq; Type: SEQUENCE; Schema: common; Owner: -
--

CREATE SEQUENCE common.workflow_specification_property_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 7096 (class 0 OID 0)
-- Dependencies: 463
-- Name: workflow_specification_property_id_seq; Type: SEQUENCE OWNED BY; Schema: common; Owner: -
--

ALTER SEQUENCE common.workflow_specification_property_id_seq OWNED BY common.workflow_specification_property.id;


--
-- TOC entry 4505 (class 2604 OID 39473)
-- Name: equipment_actual id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual ALTER COLUMN id SET DEFAULT nextval('common.equipment_actual_id_seq'::regclass);


--
-- TOC entry 4549 (class 2604 OID 39973)
-- Name: equipment_actual_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property ALTER COLUMN id SET DEFAULT nextval('common.equipment_actual_property_id_seq'::regclass);


--
-- TOC entry 4380 (class 2604 OID 37924)
-- Name: equipment_capability id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability ALTER COLUMN id SET DEFAULT nextval('common.equipment_capability_id_seq'::regclass);


--
-- TOC entry 4449 (class 2604 OID 38799)
-- Name: equipment_capability_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property ALTER COLUMN id SET DEFAULT nextval('common.equipment_capability_property_id_seq'::regclass);


--
-- TOC entry 4270 (class 2604 OID 36678)
-- Name: equipment_capability_test_result id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_result ALTER COLUMN id SET DEFAULT nextval('common.equipment_capability_test_result_id_seq'::regclass);


--
-- TOC entry 4163 (class 2604 OID 35561)
-- Name: equipment_capability_test_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_specification ALTER COLUMN id SET DEFAULT nextval('common.equipment_capability_test_specification_id_seq'::regclass);


--
-- TOC entry 4452 (class 2604 OID 38844)
-- Name: equipment_requirement id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement ALTER COLUMN id SET DEFAULT nextval('common.equipment_requirement_id_seq'::regclass);


--
-- TOC entry 4510 (class 2604 OID 39522)
-- Name: equipment_requirement_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property ALTER COLUMN id SET DEFAULT nextval('common.equipment_requirement_property_id_seq'::regclass);


--
-- TOC entry 4084 (class 2604 OID 34809)
-- Name: job_list id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list ALTER COLUMN id SET DEFAULT nextval('common.job_list_id_seq'::regclass);


--
-- TOC entry 4168 (class 2604 OID 35598)
-- Name: job_list_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list_property ALTER COLUMN id SET DEFAULT nextval('common.job_list_property_id_seq'::regclass);


--
-- TOC entry 4340 (class 2604 OID 37388)
-- Name: job_order id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order ALTER COLUMN id SET DEFAULT nextval('common.job_order_id_seq'::regclass);


--
-- TOC entry 4385 (class 2604 OID 37994)
-- Name: job_order_parameter id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order_parameter ALTER COLUMN id SET DEFAULT nextval('common.job_order_parameter_id_seq'::regclass);


--
-- TOC entry 4388 (class 2604 OID 38033)
-- Name: job_response id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response ALTER COLUMN id SET DEFAULT nextval('common.job_response_id_seq'::regclass);


--
-- TOC entry 4457 (class 2604 OID 38893)
-- Name: job_response_data id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response_data ALTER COLUMN id SET DEFAULT nextval('common.job_response_data_id_seq'::regclass);


--
-- TOC entry 4513 (class 2604 OID 39568)
-- Name: material_actual id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual ALTER COLUMN id SET DEFAULT nextval('common.material_actual_id_seq'::regclass);


--
-- TOC entry 4554 (class 2604 OID 40021)
-- Name: material_actual_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property ALTER COLUMN id SET DEFAULT nextval('common.material_actual_property_id_seq'::regclass);


--
-- TOC entry 4391 (class 2604 OID 38085)
-- Name: material_capability id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability ALTER COLUMN id SET DEFAULT nextval('common.material_capability_id_seq'::regclass);


--
-- TOC entry 4462 (class 2604 OID 38934)
-- Name: material_capability_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property ALTER COLUMN id SET DEFAULT nextval('common.material_capability_property_id_seq'::regclass);


--
-- TOC entry 4394 (class 2604 OID 38179)
-- Name: material_capability_test_result id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_result ALTER COLUMN id SET DEFAULT nextval('common.material_capability_test_result_id_seq'::regclass);


--
-- TOC entry 4343 (class 2604 OID 37439)
-- Name: material_capability_test_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_specification ALTER COLUMN id SET DEFAULT nextval('common.material_capability_test_specification_id_seq'::regclass);


--
-- TOC entry 4087 (class 2604 OID 34835)
-- Name: material_lot id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot ALTER COLUMN id SET DEFAULT nextval('common.material_lot_id_seq'::regclass);


--
-- TOC entry 4275 (class 2604 OID 36705)
-- Name: material_lot_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property ALTER COLUMN id SET DEFAULT nextval('common.material_lot_property_id_seq'::regclass);


--
-- TOC entry 4465 (class 2604 OID 38980)
-- Name: material_requirement id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement ALTER COLUMN id SET DEFAULT nextval('common.material_requirement_id_seq'::regclass);


--
-- TOC entry 4518 (class 2604 OID 39648)
-- Name: material_requirement_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property ALTER COLUMN id SET DEFAULT nextval('common.material_requirement_property_id_seq'::regclass);


--
-- TOC entry 4171 (class 2604 OID 35637)
-- Name: material_sub_lot id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_sub_lot ALTER COLUMN id SET DEFAULT nextval('common.material_sub_lot_id_seq'::regclass);


--
-- TOC entry 4090 (class 2604 OID 34887)
-- Name: operations_capability id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability ALTER COLUMN id SET DEFAULT nextval('common.operations_capability_id_seq'::regclass);


--
-- TOC entry 4176 (class 2604 OID 35692)
-- Name: operations_capability_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability_property ALTER COLUMN id SET DEFAULT nextval('common.operations_capability_property_id_seq'::regclass);


--
-- TOC entry 4093 (class 2604 OID 34919)
-- Name: operations_performance id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance ALTER COLUMN id SET DEFAULT nextval('common.operations_performance_id_seq'::regclass);


--
-- TOC entry 4181 (class 2604 OID 35733)
-- Name: operations_performance_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance_property ALTER COLUMN id SET DEFAULT nextval('common.operations_performance_property_id_seq'::regclass);


--
-- TOC entry 4346 (class 2604 OID 37489)
-- Name: operations_request id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request ALTER COLUMN id SET DEFAULT nextval('common.operations_request_id_seq'::regclass);


--
-- TOC entry 4399 (class 2604 OID 38206)
-- Name: operations_request_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request_property ALTER COLUMN id SET DEFAULT nextval('common.operations_request_property_id_seq'::regclass);


--
-- TOC entry 4402 (class 2604 OID 38245)
-- Name: operations_response id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response ALTER COLUMN id SET DEFAULT nextval('common.operations_response_id_seq'::regclass);


--
-- TOC entry 4470 (class 2604 OID 39060)
-- Name: operations_response_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response_property ALTER COLUMN id SET DEFAULT nextval('common.operations_response_property_id_seq'::regclass);


--
-- TOC entry 3999 (class 2604 OID 34127)
-- Name: operations_schedule id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule ALTER COLUMN id SET DEFAULT nextval('common.operations_schedule_id_seq'::regclass);


--
-- TOC entry 4098 (class 2604 OID 34957)
-- Name: operations_schedule_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule_property ALTER COLUMN id SET DEFAULT nextval('common.operations_schedule_property_id_seq'::regclass);


--
-- TOC entry 4521 (class 2604 OID 39694)
-- Name: personnel_actual id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual ALTER COLUMN id SET DEFAULT nextval('common.personnel_actual_id_seq'::regclass);


--
-- TOC entry 4559 (class 2604 OID 40068)
-- Name: personnel_actual_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property ALTER COLUMN id SET DEFAULT nextval('common.personnel_actual_property_id_seq'::regclass);


--
-- TOC entry 4405 (class 2604 OID 38291)
-- Name: personnel_capability id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability ALTER COLUMN id SET DEFAULT nextval('common.personnel_capability_id_seq'::regclass);


--
-- TOC entry 4475 (class 2604 OID 39101)
-- Name: personnel_capability_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property ALTER COLUMN id SET DEFAULT nextval('common.personnel_capability_property_id_seq'::regclass);


--
-- TOC entry 4184 (class 2604 OID 35772)
-- Name: personnel_capability_test_result id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_result ALTER COLUMN id SET DEFAULT nextval('common.personnel_capability_test_result_id_seq'::regclass);


--
-- TOC entry 4101 (class 2604 OID 34996)
-- Name: personnel_capability_test_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_specification ALTER COLUMN id SET DEFAULT nextval('common.personnel_capability_test_specification_id_seq'::regclass);


--
-- TOC entry 4478 (class 2604 OID 39147)
-- Name: personnel_requirement id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement ALTER COLUMN id SET DEFAULT nextval('common.personnel_requirement_id_seq'::regclass);


--
-- TOC entry 4526 (class 2604 OID 39748)
-- Name: personnel_requirement_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property ALTER COLUMN id SET DEFAULT nextval('common.personnel_requirement_property_id_seq'::regclass);


--
-- TOC entry 4529 (class 2604 OID 39794)
-- Name: physical_asset_actual id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_actual_id_seq'::regclass);


--
-- TOC entry 4564 (class 2604 OID 40116)
-- Name: physical_asset_actual_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_actual_property_id_seq'::regclass);


--
-- TOC entry 4408 (class 2604 OID 38359)
-- Name: physical_asset_capability id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_capability_id_seq'::regclass);


--
-- TOC entry 4483 (class 2604 OID 39196)
-- Name: physical_asset_capability_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_capability_property_id_seq'::regclass);


--
-- TOC entry 4349 (class 2604 OID 37540)
-- Name: physical_asset_capability_test_result id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_result ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_capability_test_result_id_seq'::regclass);


--
-- TOC entry 4278 (class 2604 OID 36749)
-- Name: physical_asset_capability_test_spec id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_spec ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_capability_test_spec_id_seq'::regclass);


--
-- TOC entry 4486 (class 2604 OID 39242)
-- Name: physical_asset_requirement id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_requirement_id_seq'::regclass);


--
-- TOC entry 4534 (class 2604 OID 39843)
-- Name: physical_asset_requirement_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_requirement_property_id_seq'::regclass);


--
-- TOC entry 4187 (class 2604 OID 35797)
-- Name: process_segment_capability id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_capability ALTER COLUMN id SET DEFAULT nextval('common.process_segment_capability_id_seq'::regclass);


--
-- TOC entry 4539 (class 2604 OID 39891)
-- Name: segment_data id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_data ALTER COLUMN id SET DEFAULT nextval('common.segment_data_id_seq'::regclass);


--
-- TOC entry 4491 (class 2604 OID 39296)
-- Name: segment_parameter id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_parameter ALTER COLUMN id SET DEFAULT nextval('common.segment_parameter_id_seq'::regclass);


--
-- TOC entry 4411 (class 2604 OID 38427)
-- Name: segment_requirement id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement ALTER COLUMN id SET DEFAULT nextval('common.segment_requirement_id_seq'::regclass);


--
-- TOC entry 4494 (class 2604 OID 39335)
-- Name: segment_response id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response ALTER COLUMN id SET DEFAULT nextval('common.segment_response_id_seq'::regclass);


--
-- TOC entry 4190 (class 2604 OID 35839)
-- Name: work_alert id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert ALTER COLUMN id SET DEFAULT nextval('common.work_alert_id_seq'::regclass);


--
-- TOC entry 4104 (class 2604 OID 35031)
-- Name: work_alert_definition id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition ALTER COLUMN id SET DEFAULT nextval('common.work_alert_definition_id_seq'::regclass);


--
-- TOC entry 4195 (class 2604 OID 35877)
-- Name: work_alert_definition_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition_property ALTER COLUMN id SET DEFAULT nextval('common.work_alert_definition_property_id_seq'::regclass);


--
-- TOC entry 4283 (class 2604 OID 36786)
-- Name: work_alert_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property ALTER COLUMN id SET DEFAULT nextval('common.work_alert_property_id_seq'::regclass);


--
-- TOC entry 4107 (class 2604 OID 35062)
-- Name: work_capability id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability ALTER COLUMN id SET DEFAULT nextval('common.work_capability_id_seq'::regclass);


--
-- TOC entry 4200 (class 2604 OID 35917)
-- Name: work_capability_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability_property ALTER COLUMN id SET DEFAULT nextval('common.work_capability_property_id_seq'::regclass);


--
-- TOC entry 4352 (class 2604 OID 37565)
-- Name: work_master_capability id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability ALTER COLUMN id SET DEFAULT nextval('common.work_master_capability_id_seq'::regclass);


--
-- TOC entry 4416 (class 2604 OID 38491)
-- Name: work_master_capability_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability_property ALTER COLUMN id SET DEFAULT nextval('common.work_master_capability_property_id_seq'::regclass);


--
-- TOC entry 4203 (class 2604 OID 35956)
-- Name: work_performance id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance ALTER COLUMN id SET DEFAULT nextval('common.work_performance_id_seq'::regclass);


--
-- TOC entry 4288 (class 2604 OID 36832)
-- Name: work_performance_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance_property ALTER COLUMN id SET DEFAULT nextval('common.work_performance_property_id_seq'::regclass);


--
-- TOC entry 4206 (class 2604 OID 35993)
-- Name: work_request id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request ALTER COLUMN id SET DEFAULT nextval('common.work_request_id_seq'::regclass);


--
-- TOC entry 4293 (class 2604 OID 36873)
-- Name: work_request_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request_property ALTER COLUMN id SET DEFAULT nextval('common.work_request_property_id_seq'::regclass);


--
-- TOC entry 4296 (class 2604 OID 36912)
-- Name: work_response id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response ALTER COLUMN id SET DEFAULT nextval('common.work_response_id_seq'::regclass);


--
-- TOC entry 4357 (class 2604 OID 37603)
-- Name: work_response_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response_property ALTER COLUMN id SET DEFAULT nextval('common.work_response_property_id_seq'::regclass);


--
-- TOC entry 4110 (class 2604 OID 35088)
-- Name: work_schedule id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule ALTER COLUMN id SET DEFAULT nextval('common.work_schedule_id_seq'::regclass);


--
-- TOC entry 4211 (class 2604 OID 36037)
-- Name: work_schedule_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule_property ALTER COLUMN id SET DEFAULT nextval('common.work_schedule_property_id_seq'::regclass);


--
-- TOC entry 4579 (class 2604 OID 41627)
-- Name: document id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.document ALTER COLUMN id SET DEFAULT nextval('common.document_id_seq'::regclass);


--
-- TOC entry 4002 (class 2604 OID 34152)
-- Name: equipment id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment ALTER COLUMN id SET DEFAULT nextval('common.equipment_id_seq'::regclass);


--
-- TOC entry 4214 (class 2604 OID 36076)
-- Name: equipment_asset_mapping id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_asset_mapping ALTER COLUMN id SET DEFAULT nextval('common.equipment_asset_mapping_id_seq'::regclass);


--
-- TOC entry 4005 (class 2604 OID 34178)
-- Name: equipment_class id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class ALTER COLUMN id SET DEFAULT nextval('common.equipment_class_id_seq'::regclass);


--
-- TOC entry 4113 (class 2604 OID 35120)
-- Name: equipment_class_mapping id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_mapping ALTER COLUMN id SET DEFAULT nextval('common.equipment_class_mapping_id_seq'::regclass);


--
-- TOC entry 4117 (class 2604 OID 35144)
-- Name: equipment_class_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_property ALTER COLUMN id SET DEFAULT nextval('common.equipment_class_property_id_seq'::regclass);


--
-- TOC entry 4122 (class 2604 OID 35185)
-- Name: equipment_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_property ALTER COLUMN id SET DEFAULT nextval('common.equipment_property_id_seq'::regclass);


--
-- TOC entry 4216 (class 2604 OID 36098)
-- Name: equipment_segment_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification ALTER COLUMN id SET DEFAULT nextval('common.equipment_segment_specification_id_seq'::regclass);


--
-- TOC entry 4301 (class 2604 OID 36961)
-- Name: equipment_segment_specification_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property ALTER COLUMN id SET DEFAULT nextval('common.equipment_segment_specification_property_id_seq'::regclass);


--
-- TOC entry 4575 (class 2604 OID 41598)
-- Name: equipment_template id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_template ALTER COLUMN id SET DEFAULT nextval('common.equipment_template_id_seq'::regclass);


--
-- TOC entry 3948 (class 2604 OID 33830)
-- Name: event_class id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class ALTER COLUMN id SET DEFAULT nextval('common.event_class_id_seq'::regclass);


--
-- TOC entry 4011 (class 2604 OID 34207)
-- Name: event_class_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class_property ALTER COLUMN id SET DEFAULT nextval('common.event_class_property_id_seq'::regclass);


--
-- TOC entry 3986 (class 2604 OID 34036)
-- Name: event_definition id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition ALTER COLUMN id SET DEFAULT nextval('common.event_definition_id_seq'::regclass);


--
-- TOC entry 4017 (class 2604 OID 34249)
-- Name: event_definition_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition_property ALTER COLUMN id SET DEFAULT nextval('common.event_definition_property_id_seq'::regclass);


--
-- TOC entry 4021 (class 2604 OID 34289)
-- Name: event_object_relationship id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship ALTER COLUMN id SET DEFAULT nextval('common.event_object_relationship_id_seq'::regclass);


--
-- TOC entry 4127 (class 2604 OID 35226)
-- Name: event_object_relationship_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship_property ALTER COLUMN id SET DEFAULT nextval('common.event_object_relationship_property_id_seq'::regclass);


--
-- TOC entry 4219 (class 2604 OID 36140)
-- Name: from_resource_reference id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference ALTER COLUMN id SET DEFAULT nextval('common.from_resource_reference_id_seq'::regclass);


--
-- TOC entry 4306 (class 2604 OID 37009)
-- Name: from_resource_reference_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference_property ALTER COLUMN id SET DEFAULT nextval('common.from_resource_reference_property_id_seq'::regclass);


--
-- TOC entry 3951 (class 2604 OID 33851)
-- Name: knowledge_category id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_category ALTER COLUMN id SET DEFAULT nextval('common.knowledge_category_id_seq'::regclass);


--
-- TOC entry 4026 (class 2604 OID 34321)
-- Name: knowledge_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_property ALTER COLUMN id SET DEFAULT nextval('common.knowledge_property_id_seq'::regclass);


--
-- TOC entry 3989 (class 2604 OID 34056)
-- Name: lookup id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.lookup ALTER COLUMN id SET DEFAULT nextval('common.lookup_id_seq'::regclass);


--
-- TOC entry 3954 (class 2604 OID 33872)
-- Name: lookup_category id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.lookup_category ALTER COLUMN id SET DEFAULT nextval('common.lookup_category_id_seq'::regclass);


--
-- TOC entry 4574 (class 2604 OID 41570)
-- Name: material_assembly_definition id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_assembly_definition ALTER COLUMN id SET DEFAULT nextval('common.material_assembly_definition_id_seq'::regclass);


--
-- TOC entry 4031 (class 2604 OID 34362)
-- Name: material_class id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class ALTER COLUMN id SET DEFAULT nextval('common.material_class_id_seq'::regclass);


--
-- TOC entry 4131 (class 2604 OID 35266)
-- Name: material_class_mapping id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_mapping ALTER COLUMN id SET DEFAULT nextval('common.material_class_mapping_id_seq'::regclass);


--
-- TOC entry 4135 (class 2604 OID 35290)
-- Name: material_class_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_property ALTER COLUMN id SET DEFAULT nextval('common.material_class_property_id_seq'::regclass);


--
-- TOC entry 4035 (class 2604 OID 34394)
-- Name: material_definition id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition ALTER COLUMN id SET DEFAULT nextval('common.material_definition_id_seq'::regclass);


--
-- TOC entry 4140 (class 2604 OID 35331)
-- Name: material_definition_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition_property ALTER COLUMN id SET DEFAULT nextval('common.material_definition_property_id_seq'::regclass);


--
-- TOC entry 4222 (class 2604 OID 36165)
-- Name: material_segment_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification ALTER COLUMN id SET DEFAULT nextval('common.material_segment_specification_id_seq'::regclass);


--
-- TOC entry 4362 (class 2604 OID 37644)
-- Name: material_segment_specification_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property ALTER COLUMN id SET DEFAULT nextval('common.material_segment_specification_property_id_seq'::regclass);


--
-- TOC entry 3957 (class 2604 OID 33887)
-- Name: person id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person ALTER COLUMN id SET DEFAULT nextval('common.person_id_seq'::regclass);


--
-- TOC entry 4040 (class 2604 OID 34427)
-- Name: person_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person_property ALTER COLUMN id SET DEFAULT nextval('common.person_property_id_seq'::regclass);


--
-- TOC entry 3960 (class 2604 OID 33902)
-- Name: personnel_class id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class ALTER COLUMN id SET DEFAULT nextval('common.personnel_class_id_seq'::regclass);


--
-- TOC entry 3994 (class 2604 OID 34085)
-- Name: personnel_class_mapping id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_mapping ALTER COLUMN id SET DEFAULT nextval('common.personnel_class_mapping_id_seq'::regclass);


--
-- TOC entry 4045 (class 2604 OID 34468)
-- Name: personnel_class_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_property ALTER COLUMN id SET DEFAULT nextval('common.personnel_class_property_id_seq'::regclass);


--
-- TOC entry 4225 (class 2604 OID 36223)
-- Name: personnel_segment_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification ALTER COLUMN id SET DEFAULT nextval('common.personnel_segment_specification_id_seq'::regclass);


--
-- TOC entry 4311 (class 2604 OID 37050)
-- Name: personnel_segment_specification_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property ALTER COLUMN id SET DEFAULT nextval('common.personnel_segment_specification_property_id_seq'::regclass);


--
-- TOC entry 4143 (class 2604 OID 35370)
-- Name: physical_asset id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_id_seq'::regclass);


--
-- TOC entry 3964 (class 2604 OID 33924)
-- Name: physical_asset_class id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_class_id_seq'::regclass);


--
-- TOC entry 4228 (class 2604 OID 36265)
-- Name: physical_asset_class_mapping id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_mapping ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_class_mapping_id_seq'::regclass);


--
-- TOC entry 4050 (class 2604 OID 34509)
-- Name: physical_asset_class_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_property ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_class_property_id_seq'::regclass);


--
-- TOC entry 4232 (class 2604 OID 36289)
-- Name: physical_asset_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_property ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_property_id_seq'::regclass);


--
-- TOC entry 4235 (class 2604 OID 36328)
-- Name: physical_asset_segment_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_segment_specification_id_seq'::regclass);


--
-- TOC entry 4316 (class 2604 OID 37098)
-- Name: physical_asset_segment_specification_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_segment_specification_property_id_seq'::regclass);


--
-- TOC entry 4146 (class 2604 OID 35396)
-- Name: process_segment id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment ALTER COLUMN id SET DEFAULT nextval('common.process_segment_id_seq'::regclass);


--
-- TOC entry 4238 (class 2604 OID 36370)
-- Name: process_segment_dependency id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_dependency ALTER COLUMN id SET DEFAULT nextval('common.process_segment_dependency_id_seq'::regclass);


--
-- TOC entry 4243 (class 2604 OID 36414)
-- Name: process_segment_parameter id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_parameter ALTER COLUMN id SET DEFAULT nextval('common.process_segment_parameter_id_seq'::regclass);


--
-- TOC entry 4149 (class 2604 OID 35433)
-- Name: resource_network_connection id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection ALTER COLUMN id SET DEFAULT nextval('common.resource_network_connection_id_seq'::regclass);


--
-- TOC entry 4248 (class 2604 OID 36455)
-- Name: resource_network_connection_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property ALTER COLUMN id SET DEFAULT nextval('common.resource_network_connection_property_id_seq'::regclass);


--
-- TOC entry 3968 (class 2604 OID 33946)
-- Name: resource_network_connection_type id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type ALTER COLUMN id SET DEFAULT nextval('common.resource_network_connection_type_id_seq'::regclass);


--
-- TOC entry 4055 (class 2604 OID 34550)
-- Name: resource_network_connection_type_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type_property ALTER COLUMN id SET DEFAULT nextval('common.resource_network_connection_type_property_id_seq'::regclass);


--
-- TOC entry 4058 (class 2604 OID 34589)
-- Name: resource_relationship_network id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_relationship_network ALTER COLUMN id SET DEFAULT nextval('common.resource_relationship_network_id_seq'::regclass);


--
-- TOC entry 4251 (class 2604 OID 36499)
-- Name: to_resource_reference id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference ALTER COLUMN id SET DEFAULT nextval('common.to_resource_reference_id_seq'::regclass);


--
-- TOC entry 4321 (class 2604 OID 37146)
-- Name: to_resource_reference_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference_property ALTER COLUMN id SET DEFAULT nextval('common.to_resource_reference_property_id_seq'::regclass);


--
-- TOC entry 3996 (class 2604 OID 34107)
-- Name: uom id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.uom ALTER COLUMN id SET DEFAULT nextval('common.uom_id_seq'::regclass);


--
-- TOC entry 3971 (class 2604 OID 33961)
-- Name: uom_category id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.uom_category ALTER COLUMN id SET DEFAULT nextval('common.uom_category_id_seq'::regclass);


--
-- TOC entry 3974 (class 2604 OID 33976)
-- Name: work_calendar id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar ALTER COLUMN id SET DEFAULT nextval('common.work_calendar_id_seq'::regclass);


--
-- TOC entry 3977 (class 2604 OID 33991)
-- Name: work_calendar_definition id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition ALTER COLUMN id SET DEFAULT nextval('common.work_calendar_definition_id_seq'::regclass);


--
-- TOC entry 4061 (class 2604 OID 34614)
-- Name: work_calendar_definition_entry id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry ALTER COLUMN id SET DEFAULT nextval('common.work_calendar_definition_entry_id_seq'::regclass);


--
-- TOC entry 4154 (class 2604 OID 35460)
-- Name: work_calendar_definition_entry_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry_property ALTER COLUMN id SET DEFAULT nextval('common.work_calendar_definition_entry_property_id_seq'::regclass);


--
-- TOC entry 4066 (class 2604 OID 34647)
-- Name: work_calendar_definition_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_property ALTER COLUMN id SET DEFAULT nextval('common.work_calendar_definition_property_id_seq'::regclass);


--
-- TOC entry 4157 (class 2604 OID 35499)
-- Name: work_calendar_entry id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry ALTER COLUMN id SET DEFAULT nextval('common.work_calendar_entry_id_seq'::regclass);


--
-- TOC entry 4256 (class 2604 OID 36526)
-- Name: work_calendar_entry_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property ALTER COLUMN id SET DEFAULT nextval('common.work_calendar_entry_property_id_seq'::regclass);


--
-- TOC entry 4071 (class 2604 OID 34688)
-- Name: work_calendar_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_property ALTER COLUMN id SET DEFAULT nextval('common.work_calendar_property_id_seq'::regclass);


--
-- TOC entry 4567 (class 2604 OID 40162)
-- Name: privilege id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_privilege ALTER COLUMN id SET DEFAULT nextval('common.au_privilege_id_seq'::regclass);


--
-- TOC entry 4570 (class 2604 OID 40182)
-- Name: role_privileges id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_role_privileges ALTER COLUMN id SET DEFAULT nextval('common.au_role_privileges_id_seq'::regclass);


--
-- TOC entry 4572 (class 2604 OID 40204)
-- Name: user_restricted_privileges id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_user_restricted_privileges ALTER COLUMN id SET DEFAULT nextval('common.au_user_restricted_privileges_id_seq'::regclass);


--
-- TOC entry 4365 (class 2604 OID 37695)
-- Name: equipment_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification ALTER COLUMN id SET DEFAULT nextval('common.equipment_specification_id_seq'::regclass);


--
-- TOC entry 4421 (class 2604 OID 38532)
-- Name: equipment_specification_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property ALTER COLUMN id SET DEFAULT nextval('common.equipment_specification_property_id_seq'::regclass);


--
-- TOC entry 4368 (class 2604 OID 37742)
-- Name: material_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification ALTER COLUMN id SET DEFAULT nextval('common.material_specification_id_seq'::regclass);


--
-- TOC entry 4426 (class 2604 OID 38580)
-- Name: material_specification_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property ALTER COLUMN id SET DEFAULT nextval('common.material_specification_property_id_seq'::regclass);


--
-- TOC entry 4160 (class 2604 OID 35535)
-- Name: operations_definition id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition ALTER COLUMN id SET DEFAULT nextval('common.operations_definition_id_seq'::regclass);


--
-- TOC entry 4261 (class 2604 OID 36572)
-- Name: operations_definition_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition_property ALTER COLUMN id SET DEFAULT nextval('common.operations_definition_property_id_seq'::regclass);


--
-- TOC entry 4264 (class 2604 OID 36611)
-- Name: operations_material_bill id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill ALTER COLUMN id SET DEFAULT nextval('common.operations_material_bill_id_seq'::regclass);


--
-- TOC entry 4324 (class 2604 OID 37185)
-- Name: operations_material_bill_item id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item ALTER COLUMN id SET DEFAULT nextval('common.operations_material_bill_item_id_seq'::regclass);


--
-- TOC entry 4267 (class 2604 OID 36631)
-- Name: operations_segment id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment ALTER COLUMN id SET DEFAULT nextval('common.operations_segment_id_seq'::regclass);


--
-- TOC entry 4327 (class 2604 OID 37243)
-- Name: operations_segment_dependency id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_dependency ALTER COLUMN id SET DEFAULT nextval('common.operations_segment_dependency_id_seq'::regclass);


--
-- TOC entry 4330 (class 2604 OID 37285)
-- Name: operations_segment_mapping id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_mapping ALTER COLUMN id SET DEFAULT nextval('common.operations_segment_mapping_id_seq'::regclass);


--
-- TOC entry 4334 (class 2604 OID 37309)
-- Name: operations_segment_parameter_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_parameter_specification ALTER COLUMN id SET DEFAULT nextval('common.operations_segment_parameter_specification_id_seq'::regclass);


--
-- TOC entry 4371 (class 2604 OID 37810)
-- Name: personnel_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification ALTER COLUMN id SET DEFAULT nextval('common.personnel_specification_id_seq'::regclass);


--
-- TOC entry 4431 (class 2604 OID 38628)
-- Name: personnel_specification_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property ALTER COLUMN id SET DEFAULT nextval('common.personnel_specification_property_id_seq'::regclass);


--
-- TOC entry 4374 (class 2604 OID 37857)
-- Name: physical_asset_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_specification_id_seq'::regclass);


--
-- TOC entry 4436 (class 2604 OID 38675)
-- Name: physical_asset_specification_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property ALTER COLUMN id SET DEFAULT nextval('common.physical_asset_specification_property_id_seq'::regclass);


--
-- TOC entry 4337 (class 2604 OID 37348)
-- Name: work_master id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master ALTER COLUMN id SET DEFAULT nextval('common.work_master_id_seq'::regclass);


--
-- TOC entry 4377 (class 2604 OID 37904)
-- Name: workflow_specification id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_id_seq'::regclass);


--
-- TOC entry 4497 (class 2604 OID 39397)
-- Name: workflow_specification_connection id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_connection_id_seq'::regclass);


--
-- TOC entry 4544 (class 2604 OID 39932)
-- Name: workflow_specification_connection_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_property ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_connection_property_id_seq'::regclass);


--
-- TOC entry 3980 (class 2604 OID 34006)
-- Name: workflow_specification_connection_type id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_connection_type_id_seq'::regclass);


--
-- TOC entry 4076 (class 2604 OID 34729)
-- Name: workflow_specification_connection_type_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type_property ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_connection_type_property_id_seq'::regclass);


--
-- TOC entry 4439 (class 2604 OID 38721)
-- Name: workflow_specification_node id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_node_id_seq'::regclass);


--
-- TOC entry 4502 (class 2604 OID 39434)
-- Name: workflow_specification_node_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_property ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_node_property_id_seq'::regclass);


--
-- TOC entry 3983 (class 2604 OID 34021)
-- Name: workflow_specification_node_type id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_node_type_id_seq'::regclass);


--
-- TOC entry 4081 (class 2604 OID 34770)
-- Name: workflow_specification_node_type_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type_property ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_node_type_property_id_seq'::regclass);


--
-- TOC entry 4444 (class 2604 OID 38758)
-- Name: workflow_specification_property id; Type: DEFAULT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_property ALTER COLUMN id SET DEFAULT nextval('common.workflow_specification_property_id_seq'::regclass);


--
-- TOC entry 5731 (class 2606 OID 39480)
-- Name: equipment_actual equipment_actual_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual
    ADD CONSTRAINT equipment_actual_pkey PRIMARY KEY (id);


--
-- TOC entry 5836 (class 2606 OID 39980)
-- Name: equipment_actual_property equipment_actual_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property
    ADD CONSTRAINT equipment_actual_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5436 (class 2606 OID 37931)
-- Name: equipment_capability equipment_capability_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_pkey PRIMARY KEY (id);


--
-- TOC entry 5593 (class 2606 OID 38806)
-- Name: equipment_capability_property equipment_capability_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property
    ADD CONSTRAINT equipment_capability_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5199 (class 2606 OID 36685)
-- Name: equipment_capability_test_result equipment_capability_test_result_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_result
    ADD CONSTRAINT equipment_capability_test_result_pkey PRIMARY KEY (id);


--
-- TOC entry 4977 (class 2606 OID 35568)
-- Name: equipment_capability_test_specification equipment_capability_test_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_specification
    ADD CONSTRAINT equipment_capability_test_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5606 (class 2606 OID 38851)
-- Name: equipment_requirement equipment_requirement_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement
    ADD CONSTRAINT equipment_requirement_pkey PRIMARY KEY (id);


--
-- TOC entry 5737 (class 2606 OID 39529)
-- Name: equipment_requirement_property equipment_requirement_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property
    ADD CONSTRAINT equipment_requirement_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4825 (class 2606 OID 34816)
-- Name: job_list job_list_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list
    ADD CONSTRAINT job_list_pkey PRIMARY KEY (id);


--
-- TOC entry 4991 (class 2606 OID 35605)
-- Name: job_list_property job_list_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list_property
    ADD CONSTRAINT job_list_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5453 (class 2606 OID 38001)
-- Name: job_order_parameter job_order_parameter_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order_parameter
    ADD CONSTRAINT job_order_parameter_pkey PRIMARY KEY (id);


--
-- TOC entry 5361 (class 2606 OID 37395)
-- Name: job_order job_order_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order
    ADD CONSTRAINT job_order_pkey PRIMARY KEY (id);


--
-- TOC entry 5622 (class 2606 OID 38900)
-- Name: job_response_data job_response_data_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response_data
    ADD CONSTRAINT job_response_data_pkey PRIMARY KEY (id);


--
-- TOC entry 5459 (class 2606 OID 38040)
-- Name: job_response job_response_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response
    ADD CONSTRAINT job_response_pkey PRIMARY KEY (id);


--
-- TOC entry 5756 (class 2606 OID 39575)
-- Name: material_actual material_actual_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_pkey PRIMARY KEY (id);


--
-- TOC entry 5861 (class 2606 OID 40028)
-- Name: material_actual_property material_actual_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property
    ADD CONSTRAINT material_actual_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5467 (class 2606 OID 38092)
-- Name: material_capability material_capability_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_pkey PRIMARY KEY (id);


--
-- TOC entry 5636 (class 2606 OID 38941)
-- Name: material_capability_property material_capability_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property
    ADD CONSTRAINT material_capability_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5471 (class 2606 OID 38186)
-- Name: material_capability_test_result material_capability_test_result_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_result
    ADD CONSTRAINT material_capability_test_result_pkey PRIMARY KEY (id);


--
-- TOC entry 5365 (class 2606 OID 37446)
-- Name: material_capability_test_specification material_capability_test_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_specification
    ADD CONSTRAINT material_capability_test_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 4831 (class 2606 OID 34842)
-- Name: material_lot material_lot_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot
    ADD CONSTRAINT material_lot_pkey PRIMARY KEY (id);


--
-- TOC entry 5213 (class 2606 OID 36712)
-- Name: material_lot_property material_lot_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property
    ADD CONSTRAINT material_lot_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5643 (class 2606 OID 38987)
-- Name: material_requirement material_requirement_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_pkey PRIMARY KEY (id);


--
-- TOC entry 5770 (class 2606 OID 39655)
-- Name: material_requirement_property material_requirement_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property
    ADD CONSTRAINT material_requirement_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4998 (class 2606 OID 35644)
-- Name: material_sub_lot material_sub_lot_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_sub_lot
    ADD CONSTRAINT material_sub_lot_pkey PRIMARY KEY (id);


--
-- TOC entry 4837 (class 2606 OID 34894)
-- Name: operations_capability operations_capability_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability
    ADD CONSTRAINT operations_capability_pkey PRIMARY KEY (id);


--
-- TOC entry 5010 (class 2606 OID 35699)
-- Name: operations_capability_property operations_capability_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability_property
    ADD CONSTRAINT operations_capability_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4842 (class 2606 OID 34926)
-- Name: operations_performance operations_performance_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance
    ADD CONSTRAINT operations_performance_pkey PRIMARY KEY (id);


--
-- TOC entry 5022 (class 2606 OID 35740)
-- Name: operations_performance_property operations_performance_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance_property
    ADD CONSTRAINT operations_performance_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5370 (class 2606 OID 37496)
-- Name: operations_request operations_request_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request
    ADD CONSTRAINT operations_request_pkey PRIMARY KEY (id);


--
-- TOC entry 5483 (class 2606 OID 38213)
-- Name: operations_request_property operations_request_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request_property
    ADD CONSTRAINT operations_request_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5488 (class 2606 OID 38252)
-- Name: operations_response operations_response_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response
    ADD CONSTRAINT operations_response_pkey PRIMARY KEY (id);


--
-- TOC entry 5655 (class 2606 OID 39067)
-- Name: operations_response_property operations_response_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response_property
    ADD CONSTRAINT operations_response_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4655 (class 2606 OID 34134)
-- Name: operations_schedule operations_schedule_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule
    ADD CONSTRAINT operations_schedule_pkey PRIMARY KEY (id);


--
-- TOC entry 4854 (class 2606 OID 34964)
-- Name: operations_schedule_property operations_schedule_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule_property
    ADD CONSTRAINT operations_schedule_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5776 (class 2606 OID 39701)
-- Name: personnel_actual personnel_actual_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual
    ADD CONSTRAINT personnel_actual_pkey PRIMARY KEY (id);


--
-- TOC entry 5875 (class 2606 OID 40075)
-- Name: personnel_actual_property personnel_actual_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property
    ADD CONSTRAINT personnel_actual_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5495 (class 2606 OID 38298)
-- Name: personnel_capability personnel_capability_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_pkey PRIMARY KEY (id);


--
-- TOC entry 5669 (class 2606 OID 39108)
-- Name: personnel_capability_property personnel_capability_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property
    ADD CONSTRAINT personnel_capability_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5026 (class 2606 OID 35779)
-- Name: personnel_capability_test_result personnel_capability_test_result_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_result
    ADD CONSTRAINT personnel_capability_test_result_pkey PRIMARY KEY (id);


--
-- TOC entry 4858 (class 2606 OID 35003)
-- Name: personnel_capability_test_specification personnel_capability_test_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_specification
    ADD CONSTRAINT personnel_capability_test_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5675 (class 2606 OID 39154)
-- Name: personnel_requirement personnel_requirement_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement
    ADD CONSTRAINT personnel_requirement_pkey PRIMARY KEY (id);


--
-- TOC entry 5790 (class 2606 OID 39755)
-- Name: personnel_requirement_property personnel_requirement_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property
    ADD CONSTRAINT personnel_requirement_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5796 (class 2606 OID 39801)
-- Name: physical_asset_actual physical_asset_actual_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual
    ADD CONSTRAINT physical_asset_actual_pkey PRIMARY KEY (id);


--
-- TOC entry 5889 (class 2606 OID 40123)
-- Name: physical_asset_actual_property physical_asset_actual_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property
    ADD CONSTRAINT physical_asset_actual_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5502 (class 2606 OID 38366)
-- Name: physical_asset_capability physical_asset_capability_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_pkey PRIMARY KEY (id);


--
-- TOC entry 5689 (class 2606 OID 39203)
-- Name: physical_asset_capability_property physical_asset_capability_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property
    ADD CONSTRAINT physical_asset_capability_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5374 (class 2606 OID 37547)
-- Name: physical_asset_capability_test_result physical_asset_capability_test_result_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_result
    ADD CONSTRAINT physical_asset_capability_test_result_pkey PRIMARY KEY (id);


--
-- TOC entry 5217 (class 2606 OID 36756)
-- Name: physical_asset_capability_test_spec physical_asset_capability_test_spec_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_spec
    ADD CONSTRAINT physical_asset_capability_test_spec_pkey PRIMARY KEY (id);


--
-- TOC entry 5695 (class 2606 OID 39249)
-- Name: physical_asset_requirement physical_asset_requirement_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement
    ADD CONSTRAINT physical_asset_requirement_pkey PRIMARY KEY (id);


--
-- TOC entry 5810 (class 2606 OID 39850)
-- Name: physical_asset_requirement_property physical_asset_requirement_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property
    ADD CONSTRAINT physical_asset_requirement_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5032 (class 2606 OID 35804)
-- Name: process_segment_capability process_segment_capability_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_capability
    ADD CONSTRAINT process_segment_capability_pkey PRIMARY KEY (id);


--
-- TOC entry 5822 (class 2606 OID 39898)
-- Name: segment_data segment_data_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_data
    ADD CONSTRAINT segment_data_pkey PRIMARY KEY (id);


--
-- TOC entry 5707 (class 2606 OID 39303)
-- Name: segment_parameter segment_parameter_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_parameter
    ADD CONSTRAINT segment_parameter_pkey PRIMARY KEY (id);


--
-- TOC entry 5508 (class 2606 OID 38434)
-- Name: segment_requirement segment_requirement_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_pkey PRIMARY KEY (id);


--
-- TOC entry 5713 (class 2606 OID 39342)
-- Name: segment_response segment_response_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_pkey PRIMARY KEY (id);


--
-- TOC entry 4863 (class 2606 OID 35038)
-- Name: work_alert_definition work_alert_definition_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition
    ADD CONSTRAINT work_alert_definition_pkey PRIMARY KEY (id);


--
-- TOC entry 5048 (class 2606 OID 35884)
-- Name: work_alert_definition_property work_alert_definition_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition_property
    ADD CONSTRAINT work_alert_definition_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5037 (class 2606 OID 35846)
-- Name: work_alert work_alert_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert
    ADD CONSTRAINT work_alert_pkey PRIMARY KEY (id);


--
-- TOC entry 5229 (class 2606 OID 36793)
-- Name: work_alert_property work_alert_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property
    ADD CONSTRAINT work_alert_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4868 (class 2606 OID 35069)
-- Name: work_capability work_capability_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability
    ADD CONSTRAINT work_capability_pkey PRIMARY KEY (id);


--
-- TOC entry 5060 (class 2606 OID 35924)
-- Name: work_capability_property work_capability_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability_property
    ADD CONSTRAINT work_capability_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5379 (class 2606 OID 37572)
-- Name: work_master_capability work_master_capability_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability
    ADD CONSTRAINT work_master_capability_pkey PRIMARY KEY (id);


--
-- TOC entry 5520 (class 2606 OID 38498)
-- Name: work_master_capability_property work_master_capability_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability_property
    ADD CONSTRAINT work_master_capability_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5066 (class 2606 OID 35963)
-- Name: work_performance work_performance_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance
    ADD CONSTRAINT work_performance_pkey PRIMARY KEY (id);


--
-- TOC entry 5241 (class 2606 OID 36839)
-- Name: work_performance_property work_performance_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance_property
    ADD CONSTRAINT work_performance_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5072 (class 2606 OID 36000)
-- Name: work_request work_request_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request
    ADD CONSTRAINT work_request_pkey PRIMARY KEY (id);


--
-- TOC entry 5253 (class 2606 OID 36880)
-- Name: work_request_property work_request_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request_property
    ADD CONSTRAINT work_request_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5259 (class 2606 OID 36919)
-- Name: work_response work_response_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response
    ADD CONSTRAINT work_response_pkey PRIMARY KEY (id);


--
-- TOC entry 5391 (class 2606 OID 37610)
-- Name: work_response_property work_response_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response_property
    ADD CONSTRAINT work_response_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4874 (class 2606 OID 35095)
-- Name: work_schedule work_schedule_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule
    ADD CONSTRAINT work_schedule_pkey PRIMARY KEY (id);


--
-- TOC entry 5084 (class 2606 OID 36044)
-- Name: work_schedule_property work_schedule_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule_property
    ADD CONSTRAINT work_schedule_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5906 (class 2606 OID 41634)
-- Name: document document_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.document
    ADD CONSTRAINT document_pkey PRIMARY KEY (id);


--
-- TOC entry 5086 (class 2606 OID 36082)
-- Name: equipment_asset_mapping equipment_asset_mapping_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_asset_mapping
    ADD CONSTRAINT equipment_asset_mapping_pkey PRIMARY KEY (id);


--
-- TOC entry 4876 (class 2606 OID 35126)
-- Name: equipment_class_mapping equipment_class_mapping_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_mapping
    ADD CONSTRAINT equipment_class_mapping_pkey PRIMARY KEY (id);


--
-- TOC entry 4662 (class 2606 OID 34186)
-- Name: equipment_class equipment_class_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class
    ADD CONSTRAINT equipment_class_pkey PRIMARY KEY (id);


--
-- TOC entry 4878 (class 2606 OID 35151)
-- Name: equipment_class_property equipment_class_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_property
    ADD CONSTRAINT equipment_class_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4657 (class 2606 OID 34159)
-- Name: equipment equipment_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment
    ADD CONSTRAINT equipment_pkey PRIMARY KEY (id);


--
-- TOC entry 4890 (class 2606 OID 35192)
-- Name: equipment_property equipment_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_property
    ADD CONSTRAINT equipment_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5088 (class 2606 OID 36105)
-- Name: equipment_segment_specification equipment_segment_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification
    ADD CONSTRAINT equipment_segment_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5261 (class 2606 OID 36968)
-- Name: equipment_segment_specification_property equipment_segment_specification_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property
    ADD CONSTRAINT equipment_segment_specification_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5901 (class 2606 OID 41603)
-- Name: equipment_template equipment_template_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_template
    ADD CONSTRAINT equipment_template_pkey PRIMARY KEY (id);


--
-- TOC entry 4585 (class 2606 OID 33837)
-- Name: event_class event_class_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class
    ADD CONSTRAINT event_class_pkey PRIMARY KEY (id);


--
-- TOC entry 4667 (class 2606 OID 34215)
-- Name: event_class_property event_class_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class_property
    ADD CONSTRAINT event_class_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4637 (class 2606 OID 34043)
-- Name: event_definition event_definition_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition
    ADD CONSTRAINT event_definition_pkey PRIMARY KEY (id);


--
-- TOC entry 4679 (class 2606 OID 34257)
-- Name: event_definition_property event_definition_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition_property
    ADD CONSTRAINT event_definition_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4691 (class 2606 OID 34296)
-- Name: event_object_relationship event_object_relationship_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship
    ADD CONSTRAINT event_object_relationship_pkey PRIMARY KEY (id);


--
-- TOC entry 4902 (class 2606 OID 35234)
-- Name: event_object_relationship_property event_object_relationship_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship_property
    ADD CONSTRAINT event_object_relationship_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5094 (class 2606 OID 36147)
-- Name: from_resource_reference from_resource_reference_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference
    ADD CONSTRAINT from_resource_reference_pkey PRIMARY KEY (id);


--
-- TOC entry 5275 (class 2606 OID 37016)
-- Name: from_resource_reference_property from_resource_reference_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference_property
    ADD CONSTRAINT from_resource_reference_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4593 (class 2606 OID 33858)
-- Name: knowledge_category knowledge_category_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_category
    ADD CONSTRAINT knowledge_category_pkey PRIMARY KEY (id);


--
-- TOC entry 4705 (class 2606 OID 34330)
-- Name: knowledge_property knowledge_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_property
    ADD CONSTRAINT knowledge_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4597 (class 2606 OID 33879)
-- Name: lookup_category lookup_category_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.lookup_category
    ADD CONSTRAINT lookup_category_pkey PRIMARY KEY (id);


--
-- TOC entry 4645 (class 2606 OID 34065)
-- Name: lookup lookup_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.lookup
    ADD CONSTRAINT lookup_pkey PRIMARY KEY (id);


--
-- TOC entry 5899 (class 2606 OID 41572)
-- Name: material_assembly_definition material_assembly_definition_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_assembly_definition
    ADD CONSTRAINT material_assembly_definition_pkey PRIMARY KEY (id);


--
-- TOC entry 4914 (class 2606 OID 35272)
-- Name: material_class_mapping material_class_mapping_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_mapping
    ADD CONSTRAINT material_class_mapping_pkey PRIMARY KEY (id);


--
-- TOC entry 4710 (class 2606 OID 34370)
-- Name: material_class material_class_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class
    ADD CONSTRAINT material_class_pkey PRIMARY KEY (id);


--
-- TOC entry 4926 (class 2606 OID 35297)
-- Name: material_class_property material_class_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_property
    ADD CONSTRAINT material_class_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4715 (class 2606 OID 34401)
-- Name: material_definition material_definition_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition
    ADD CONSTRAINT material_definition_pkey PRIMARY KEY (id);


--
-- TOC entry 4938 (class 2606 OID 35338)
-- Name: material_definition_property material_definition_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition_property
    ADD CONSTRAINT material_definition_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5103 (class 2606 OID 36172)
-- Name: material_segment_specification material_segment_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification
    ADD CONSTRAINT material_segment_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5405 (class 2606 OID 37651)
-- Name: material_segment_specification_property material_segment_specification_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specification_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4601 (class 2606 OID 33894)
-- Name: person person_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- TOC entry 4727 (class 2606 OID 34434)
-- Name: person_property person_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person_property
    ADD CONSTRAINT person_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4647 (class 2606 OID 34091)
-- Name: personnel_class_mapping personnel_class_mapping_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_mapping
    ADD CONSTRAINT personnel_class_mapping_pkey PRIMARY KEY (id);


--
-- TOC entry 4606 (class 2606 OID 33910)
-- Name: personnel_class personnel_class_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class
    ADD CONSTRAINT personnel_class_pkey PRIMARY KEY (id);


--
-- TOC entry 4739 (class 2606 OID 34475)
-- Name: personnel_class_property personnel_class_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_property
    ADD CONSTRAINT personnel_class_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5109 (class 2606 OID 36230)
-- Name: personnel_segment_specification personnel_segment_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification
    ADD CONSTRAINT personnel_segment_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5299 (class 2606 OID 37057)
-- Name: personnel_segment_specification_property personnel_segment_specification_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property
    ADD CONSTRAINT personnel_segment_specification_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5111 (class 2606 OID 36271)
-- Name: physical_asset_class_mapping physical_asset_class_mapping_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_mapping
    ADD CONSTRAINT physical_asset_class_mapping_pkey PRIMARY KEY (id);


--
-- TOC entry 4611 (class 2606 OID 33932)
-- Name: physical_asset_class physical_asset_class_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class
    ADD CONSTRAINT physical_asset_class_pkey PRIMARY KEY (id);


--
-- TOC entry 4751 (class 2606 OID 34516)
-- Name: physical_asset_class_property physical_asset_class_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_property
    ADD CONSTRAINT physical_asset_class_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4943 (class 2606 OID 35377)
-- Name: physical_asset physical_asset_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset
    ADD CONSTRAINT physical_asset_pkey PRIMARY KEY (id);


--
-- TOC entry 5123 (class 2606 OID 36296)
-- Name: physical_asset_property physical_asset_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_property
    ADD CONSTRAINT physical_asset_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5129 (class 2606 OID 36335)
-- Name: physical_asset_segment_specification physical_asset_segment_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification
    ADD CONSTRAINT physical_asset_segment_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5313 (class 2606 OID 37105)
-- Name: physical_asset_segment_specification_property physical_asset_segment_specification_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property
    ADD CONSTRAINT physical_asset_segment_specification_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5135 (class 2606 OID 36377)
-- Name: process_segment_dependency process_segment_dependency_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_dependency
    ADD CONSTRAINT process_segment_dependency_pkey PRIMARY KEY (id);


--
-- TOC entry 5147 (class 2606 OID 36421)
-- Name: process_segment_parameter process_segment_parameter_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_parameter
    ADD CONSTRAINT process_segment_parameter_pkey PRIMARY KEY (id);


--
-- TOC entry 4949 (class 2606 OID 35403)
-- Name: process_segment process_segment_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment
    ADD CONSTRAINT process_segment_pkey PRIMARY KEY (id);


--
-- TOC entry 4953 (class 2606 OID 35440)
-- Name: resource_network_connection resource_network_connection_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection
    ADD CONSTRAINT resource_network_connection_pkey PRIMARY KEY (id);


--
-- TOC entry 5159 (class 2606 OID 36462)
-- Name: resource_network_connection_property resource_network_connection_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property
    ADD CONSTRAINT resource_network_connection_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4615 (class 2606 OID 33953)
-- Name: resource_network_connection_type resource_network_connection_type_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type
    ADD CONSTRAINT resource_network_connection_type_pkey PRIMARY KEY (id);


--
-- TOC entry 4763 (class 2606 OID 34557)
-- Name: resource_network_connection_type_property resource_network_connection_type_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type_property
    ADD CONSTRAINT resource_network_connection_type_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4767 (class 2606 OID 34596)
-- Name: resource_relationship_network resource_relationship_network_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_relationship_network
    ADD CONSTRAINT resource_relationship_network_pkey PRIMARY KEY (id);


--
-- TOC entry 5163 (class 2606 OID 36506)
-- Name: to_resource_reference to_resource_reference_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference
    ADD CONSTRAINT to_resource_reference_pkey PRIMARY KEY (id);


--
-- TOC entry 5325 (class 2606 OID 37153)
-- Name: to_resource_reference_property to_resource_reference_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference_property
    ADD CONSTRAINT to_resource_reference_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4619 (class 2606 OID 33968)
-- Name: uom_category uom_category_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.uom_category
    ADD CONSTRAINT uom_category_pkey PRIMARY KEY (id);


--
-- TOC entry 4651 (class 2606 OID 34114)
-- Name: uom uom_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.uom
    ADD CONSTRAINT uom_pkey PRIMARY KEY (id);


--
-- TOC entry 4772 (class 2606 OID 34621)
-- Name: work_calendar_definition_entry work_calendar_definition_entry_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry
    ADD CONSTRAINT work_calendar_definition_entry_pkey PRIMARY KEY (id);


--
-- TOC entry 4965 (class 2606 OID 35467)
-- Name: work_calendar_definition_entry_property work_calendar_definition_entry_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry_property
    ADD CONSTRAINT work_calendar_definition_entry_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4627 (class 2606 OID 33998)
-- Name: work_calendar_definition work_calendar_definition_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition
    ADD CONSTRAINT work_calendar_definition_pkey PRIMARY KEY (id);


--
-- TOC entry 4784 (class 2606 OID 34654)
-- Name: work_calendar_definition_property work_calendar_definition_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_property
    ADD CONSTRAINT work_calendar_definition_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4970 (class 2606 OID 35506)
-- Name: work_calendar_entry work_calendar_entry_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry
    ADD CONSTRAINT work_calendar_entry_pkey PRIMARY KEY (id);


--
-- TOC entry 5175 (class 2606 OID 36533)
-- Name: work_calendar_entry_property work_calendar_entry_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property
    ADD CONSTRAINT work_calendar_entry_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4623 (class 2606 OID 33983)
-- Name: work_calendar work_calendar_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar
    ADD CONSTRAINT work_calendar_pkey PRIMARY KEY (id);


--
-- TOC entry 4796 (class 2606 OID 34695)
-- Name: work_calendar_property work_calendar_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_property
    ADD CONSTRAINT work_calendar_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5893 (class 2606 OID 40169)
-- Name: privilege privilege_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_privilege
    ADD CONSTRAINT privilege_pkey PRIMARY KEY (id);


--
-- TOC entry 5895 (class 2606 OID 40188)
-- Name: role_privileges role_privileges_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_role_privileges
    ADD CONSTRAINT role_privileges_pkey PRIMARY KEY (id);


--
-- TOC entry 5897 (class 2606 OID 40210)
-- Name: user_restricted_privileges user_restricted_privileges_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_user_restricted_privileges
    ADD CONSTRAINT user_restricted_privileges_pkey PRIMARY KEY (id);


--
-- TOC entry 5407 (class 2606 OID 37702)
-- Name: equipment_specification equipment_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification
    ADD CONSTRAINT equipment_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5522 (class 2606 OID 38539)
-- Name: equipment_specification_property equipment_specification_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property
    ADD CONSTRAINT equipment_specification_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5418 (class 2606 OID 37749)
-- Name: material_specification material_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5548 (class 2606 OID 38587)
-- Name: material_specification_property material_specification_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property
    ADD CONSTRAINT material_specification_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4975 (class 2606 OID 35542)
-- Name: operations_definition operations_definition_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition
    ADD CONSTRAINT operations_definition_pkey PRIMARY KEY (id);


--
-- TOC entry 5187 (class 2606 OID 36579)
-- Name: operations_definition_property operations_definition_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition_property
    ADD CONSTRAINT operations_definition_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5332 (class 2606 OID 37192)
-- Name: operations_material_bill_item operations_material_bill_item_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item
    ADD CONSTRAINT operations_material_bill_item_pkey PRIMARY KEY (id);


--
-- TOC entry 5191 (class 2606 OID 36618)
-- Name: operations_material_bill operations_material_bill_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill
    ADD CONSTRAINT operations_material_bill_pkey PRIMARY KEY (id);


--
-- TOC entry 5338 (class 2606 OID 37250)
-- Name: operations_segment_dependency operations_segment_dependency_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_dependency
    ADD CONSTRAINT operations_segment_dependency_pkey PRIMARY KEY (id);


--
-- TOC entry 5340 (class 2606 OID 37291)
-- Name: operations_segment_mapping operations_segment_mapping_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_mapping
    ADD CONSTRAINT operations_segment_mapping_pkey PRIMARY KEY (id);


--
-- TOC entry 5352 (class 2606 OID 37316)
-- Name: operations_segment_parameter_specification operations_segment_parameter_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_parameter_specification
    ADD CONSTRAINT operations_segment_parameter_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5197 (class 2606 OID 36638)
-- Name: operations_segment operations_segment_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment
    ADD CONSTRAINT operations_segment_pkey PRIMARY KEY (id);


--
-- TOC entry 5424 (class 2606 OID 37817)
-- Name: personnel_specification personnel_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification
    ADD CONSTRAINT personnel_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5561 (class 2606 OID 38635)
-- Name: personnel_specification_property personnel_specification_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property
    ADD CONSTRAINT personnel_specification_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5430 (class 2606 OID 37864)
-- Name: physical_asset_specification physical_asset_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification
    ADD CONSTRAINT physical_asset_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5575 (class 2606 OID 38682)
-- Name: physical_asset_specification_property physical_asset_specification_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property
    ADD CONSTRAINT physical_asset_specification_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5356 (class 2606 OID 37355)
-- Name: work_master work_master_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master
    ADD CONSTRAINT work_master_pkey PRIMARY KEY (id);


--
-- TOC entry 5717 (class 2606 OID 39404)
-- Name: workflow_specification_connection workflow_specification_connection_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection
    ADD CONSTRAINT workflow_specification_connection_pkey PRIMARY KEY (id);


--
-- TOC entry 5834 (class 2606 OID 39939)
-- Name: workflow_specification_connection_property workflow_specification_connection_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_property
    ADD CONSTRAINT workflow_specification_connection_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4631 (class 2606 OID 34013)
-- Name: workflow_specification_connection_type workflow_specification_connection_type_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type
    ADD CONSTRAINT workflow_specification_connection_type_pkey PRIMARY KEY (id);


--
-- TOC entry 4808 (class 2606 OID 34736)
-- Name: workflow_specification_connection_type_property workflow_specification_connection_type_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type_property
    ADD CONSTRAINT workflow_specification_connection_type_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5579 (class 2606 OID 38728)
-- Name: workflow_specification_node workflow_specification_node_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node
    ADD CONSTRAINT workflow_specification_node_pkey PRIMARY KEY (id);


--
-- TOC entry 5729 (class 2606 OID 39441)
-- Name: workflow_specification_node_property workflow_specification_node_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_property
    ADD CONSTRAINT workflow_specification_node_property_pkey PRIMARY KEY (id);


--
-- TOC entry 4635 (class 2606 OID 34028)
-- Name: workflow_specification_node_type workflow_specification_node_type_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type
    ADD CONSTRAINT workflow_specification_node_type_pkey PRIMARY KEY (id);


--
-- TOC entry 4820 (class 2606 OID 34777)
-- Name: workflow_specification_node_type_property workflow_specification_node_type_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type_property
    ADD CONSTRAINT workflow_specification_node_type_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5434 (class 2606 OID 37911)
-- Name: workflow_specification workflow_specification_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification
    ADD CONSTRAINT workflow_specification_pkey PRIMARY KEY (id);


--
-- TOC entry 5591 (class 2606 OID 38765)
-- Name: workflow_specification_property workflow_specification_property_pkey; Type: CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_property
    ADD CONSTRAINT workflow_specification_property_pkey PRIMARY KEY (id);


--
-- TOC entry 5837 (class 1259 OID 40317)
-- Name: ix_common_equipment_actual_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_actual_property_value_number ON common.equipment_actual_property USING btree (value_number);


--
-- TOC entry 5594 (class 1259 OID 40321)
-- Name: ix_common_equipment_capability_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_capability_property_value_number ON common.equipment_capability_property USING btree (value_number);


--
-- TOC entry 5738 (class 1259 OID 40325)
-- Name: ix_common_equipment_requirement_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_requirement_property_value_number ON common.equipment_requirement_property USING btree (value_number);


--
-- TOC entry 4980 (class 1259 OID 40330)
-- Name: ix_common_job_list_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_job_list_property_value_number ON common.job_list_property USING btree (value_number);


--
-- TOC entry 5442 (class 1259 OID 40331)
-- Name: ix_common_job_order_parameter_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_job_order_parameter_value_number ON common.job_order_parameter USING btree (value_number);


--
-- TOC entry 5611 (class 1259 OID 40332)
-- Name: ix_common_job_response_data_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_job_response_data_value_number ON common.job_response_data USING btree (value_number);


--
-- TOC entry 5849 (class 1259 OID 40320)
-- Name: ix_common_material_actual_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_actual_property_value_number ON common.material_actual_property USING btree (value_number);


--
-- TOC entry 5623 (class 1259 OID 40324)
-- Name: ix_common_material_capability_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_capability_property_value_number ON common.material_capability_property USING btree (value_number);


--
-- TOC entry 5202 (class 1259 OID 40329)
-- Name: ix_common_material_lot_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_lot_property_value_number ON common.material_lot_property USING btree (value_number);


--
-- TOC entry 5757 (class 1259 OID 40328)
-- Name: ix_common_material_requirement_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_requirement_property_value_number ON common.material_requirement_property USING btree (value_number);


--
-- TOC entry 4999 (class 1259 OID 40333)
-- Name: ix_common_capability_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_value_number ON common.operations_capability_property USING btree (value_number);


--
-- TOC entry 5011 (class 1259 OID 40334)
-- Name: ix_common_performance_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_value_number ON common.operations_performance_property USING btree (value_number);


--
-- TOC entry 5472 (class 1259 OID 40335)
-- Name: ix_common_request_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_value_number ON common.operations_request_property USING btree (value_number);


--
-- TOC entry 5644 (class 1259 OID 40336)
-- Name: ix_common_response_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_value_number ON common.operations_response_property USING btree (value_number);


--
-- TOC entry 4843 (class 1259 OID 40337)
-- Name: ix_common_schedule_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_value_number ON common.operations_schedule_property USING btree (value_number);


--
-- TOC entry 5862 (class 1259 OID 40318)
-- Name: ix_common_personnel_actual_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_actual_property_value_number ON common.personnel_actual_property USING btree (value_number);


--
-- TOC entry 5656 (class 1259 OID 40322)
-- Name: ix_common_personnel_capability_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_capability_property_value_number ON common.personnel_capability_property USING btree (value_number);


--
-- TOC entry 5777 (class 1259 OID 40326)
-- Name: ix_common_personnel_requirement_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_requirement_property_value_number ON common.personnel_requirement_property USING btree (value_number);


--
-- TOC entry 5876 (class 1259 OID 40319)
-- Name: ix_common_physical_asset_actual_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_actual_property_value_number ON common.physical_asset_actual_property USING btree (value_number);


--
-- TOC entry 5676 (class 1259 OID 40323)
-- Name: ix_common_physical_asset_capability_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_capability_property_value_number ON common.physical_asset_capability_property USING btree (value_number);


--
-- TOC entry 5797 (class 1259 OID 40327)
-- Name: ix_common_physical_asset_requirement_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_requirement_property_value_number ON common.physical_asset_requirement_property USING btree (value_number);


--
-- TOC entry 5811 (class 1259 OID 40338)
-- Name: ix_common_segment_data_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_data_value_number ON common.segment_data USING btree (value_number);


--
-- TOC entry 5696 (class 1259 OID 40339)
-- Name: ix_common_segment_parameter_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_value_number ON common.segment_parameter USING btree (value_number);


--
-- TOC entry 5038 (class 1259 OID 40341)
-- Name: ix_common_work_alert_definition_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_alert_definition_property_value_number ON common.work_alert_definition_property USING btree (value_number);


--
-- TOC entry 5218 (class 1259 OID 40340)
-- Name: ix_common_work_alert_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_alert_property_value_number ON common.work_alert_property USING btree (value_number);


--
-- TOC entry 5049 (class 1259 OID 40342)
-- Name: ix_common_work_capability_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_capability_property_value_number ON common.work_capability_property USING btree (value_number);


--
-- TOC entry 5509 (class 1259 OID 40343)
-- Name: ix_common_work_master_capability_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_master_capability_property_value_number ON common.work_master_capability_property USING btree (value_number);


--
-- TOC entry 5230 (class 1259 OID 40344)
-- Name: ix_common_work_performance_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_performance_property_value_number ON common.work_performance_property USING btree (value_number);


--
-- TOC entry 5242 (class 1259 OID 40345)
-- Name: ix_common_work_request_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_request_property_value_number ON common.work_request_property USING btree (value_number);


--
-- TOC entry 5380 (class 1259 OID 40346)
-- Name: ix_common_work_response_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_response_property_value_number ON common.work_response_property USING btree (value_number);


--
-- TOC entry 5073 (class 1259 OID 40347)
-- Name: ix_common_work_schedule_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_schedule_property_value_number ON common.work_schedule_property USING btree (value_number);


--
-- TOC entry 5732 (class 1259 OID 39511)
-- Name: ix_applications_equipment_actual_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_code ON common.equipment_actual USING btree (code);


--
-- TOC entry 5733 (class 1259 OID 39512)
-- Name: ix_applications_equipment_actual_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_name ON common.equipment_actual USING btree (name);


--
-- TOC entry 5838 (class 1259 OID 40006)
-- Name: ix_applications_equipment_actual_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_code ON common.equipment_actual_property USING btree (code);


--
-- TOC entry 5839 (class 1259 OID 40007)
-- Name: ix_applications_equipment_actual_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_name ON common.equipment_actual_property USING btree (name);


--
-- TOC entry 5840 (class 1259 OID 40008)
-- Name: ix_applications_equipment_actual_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_parent_id ON common.equipment_actual_property USING btree (parent_id);


--
-- TOC entry 5841 (class 1259 OID 40009)
-- Name: ix_applications_equipment_actual_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_quantity ON common.equipment_actual_property USING btree (quantity);


--
-- TOC entry 5842 (class 1259 OID 40010)
-- Name: ix_applications_equipment_actual_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_quantity_uom_id ON common.equipment_actual_property USING btree (quantity_uom_id);


--
-- TOC entry 5843 (class 1259 OID 40825)
-- Name: ix_applications_equipment_actual_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_sensor_id ON common.equipment_actual_property USING btree (sensor_id);


--
-- TOC entry 5844 (class 1259 OID 40826)
-- Name: ix_applications_equipment_actual_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_sensor_type_id ON common.equipment_actual_property USING btree (sensor_type_id);


--
-- TOC entry 5845 (class 1259 OID 40824)
-- Name: ix_applications_equipment_actual_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_uom_category_id ON common.equipment_actual_property USING btree (uom_category_id);


--
-- TOC entry 5846 (class 1259 OID 40250)
-- Name: ix_applications_equipment_actual_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_value ON common.equipment_actual_property USING btree (value);


--
-- TOC entry 5847 (class 1259 OID 40012)
-- Name: ix_applications_equipment_actual_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_value_type_id ON common.equipment_actual_property USING btree (value_type_id);


--
-- TOC entry 5848 (class 1259 OID 40013)
-- Name: ix_applications_equipment_actual_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_property_value_uom_id ON common.equipment_actual_property USING btree (value_uom_id);


--
-- TOC entry 5734 (class 1259 OID 39513)
-- Name: ix_applications_equipment_actual_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_quantity ON common.equipment_actual USING btree (quantity);


--
-- TOC entry 5735 (class 1259 OID 39514)
-- Name: ix_applications_equipment_actual_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_actual_quantity_uom_id ON common.equipment_actual USING btree (quantity_uom_id);


--
-- TOC entry 5437 (class 1259 OID 37982)
-- Name: ix_applications_equipment_capability_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_code ON common.equipment_capability USING btree (code);


--
-- TOC entry 5438 (class 1259 OID 37983)
-- Name: ix_applications_equipment_capability_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_hierarchy_scope_id ON common.equipment_capability USING btree (hierarchy_scope_id);


--
-- TOC entry 5439 (class 1259 OID 37984)
-- Name: ix_applications_equipment_capability_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_name ON common.equipment_capability USING btree (name);


--
-- TOC entry 5595 (class 1259 OID 38832)
-- Name: ix_applications_equipment_capability_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_code ON common.equipment_capability_property USING btree (code);


--
-- TOC entry 5596 (class 1259 OID 38833)
-- Name: ix_applications_equipment_capability_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_name ON common.equipment_capability_property USING btree (name);


--
-- TOC entry 5597 (class 1259 OID 38834)
-- Name: ix_applications_equipment_capability_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_parent_id ON common.equipment_capability_property USING btree (parent_id);


--
-- TOC entry 5598 (class 1259 OID 38835)
-- Name: ix_applications_equipment_capability_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_quantity ON common.equipment_capability_property USING btree (quantity);


--
-- TOC entry 5599 (class 1259 OID 38836)
-- Name: ix_applications_equipment_capability_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_quantity_uom_id ON common.equipment_capability_property USING btree (quantity_uom_id);


--
-- TOC entry 5600 (class 1259 OID 40897)
-- Name: ix_applications_equipment_capability_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_sensor_id ON common.equipment_capability_property USING btree (sensor_id);


--
-- TOC entry 5601 (class 1259 OID 40898)
-- Name: ix_applications_equipment_capability_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_sensor_type_id ON common.equipment_capability_property USING btree (sensor_type_id);


--
-- TOC entry 5602 (class 1259 OID 40896)
-- Name: ix_applications_equipment_capability_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_uom_category_id ON common.equipment_capability_property USING btree (uom_category_id);


--
-- TOC entry 5603 (class 1259 OID 40254)
-- Name: ix_applications_equipment_capability_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_value ON common.equipment_capability_property USING btree (value);


--
-- TOC entry 5604 (class 1259 OID 38838)
-- Name: ix_applications_equipment_capability_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_property_value_uom_id ON common.equipment_capability_property USING btree (value_uom_id);


--
-- TOC entry 5440 (class 1259 OID 37985)
-- Name: ix_applications_equipment_capability_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_quantity ON common.equipment_capability USING btree (quantity);


--
-- TOC entry 5441 (class 1259 OID 37986)
-- Name: ix_applications_equipment_capability_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_quantity_uom_id ON common.equipment_capability USING btree (quantity_uom_id);


--
-- TOC entry 5200 (class 1259 OID 36696)
-- Name: ix_applications_equipment_capability_test_result_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_test_result_code ON common.equipment_capability_test_result USING btree (code);


--
-- TOC entry 5201 (class 1259 OID 36697)
-- Name: ix_applications_equipment_capability_test_result_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_test_result_name ON common.equipment_capability_test_result USING btree (name);


--
-- TOC entry 4978 (class 1259 OID 35589)
-- Name: ix_applications_equipment_capability_test_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_test_specification_code ON common.equipment_capability_test_specification USING btree (code);


--
-- TOC entry 4979 (class 1259 OID 35590)
-- Name: ix_applications_equipment_capability_test_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_capability_test_specification_name ON common.equipment_capability_test_specification USING btree (name);


--
-- TOC entry 5607 (class 1259 OID 38882)
-- Name: ix_applications_equipment_requirement_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_code ON common.equipment_requirement USING btree (code);


--
-- TOC entry 5608 (class 1259 OID 38883)
-- Name: ix_applications_equipment_requirement_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_name ON common.equipment_requirement USING btree (name);


--
-- TOC entry 5739 (class 1259 OID 39555)
-- Name: ix_applications_equipment_requirement_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_code ON common.equipment_requirement_property USING btree (code);


--
-- TOC entry 5740 (class 1259 OID 39556)
-- Name: ix_applications_equipment_requirement_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_name ON common.equipment_requirement_property USING btree (name);


--
-- TOC entry 5741 (class 1259 OID 39557)
-- Name: ix_applications_equipment_requirement_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_parent_id ON common.equipment_requirement_property USING btree (parent_id);


--
-- TOC entry 5742 (class 1259 OID 39558)
-- Name: ix_applications_equipment_requirement_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_quantity ON common.equipment_requirement_property USING btree (quantity);


--
-- TOC entry 5743 (class 1259 OID 39559)
-- Name: ix_applications_equipment_requirement_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_quantity_uom_id ON common.equipment_requirement_property USING btree (quantity_uom_id);


--
-- TOC entry 5744 (class 1259 OID 40969)
-- Name: ix_applications_equipment_requirement_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_sensor_id ON common.equipment_requirement_property USING btree (sensor_id);


--
-- TOC entry 5745 (class 1259 OID 40970)
-- Name: ix_applications_equipment_requirement_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_sensor_type_id ON common.equipment_requirement_property USING btree (sensor_type_id);


--
-- TOC entry 5746 (class 1259 OID 40968)
-- Name: ix_applications_equipment_requirement_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_uom_category_id ON common.equipment_requirement_property USING btree (uom_category_id);


--
-- TOC entry 5747 (class 1259 OID 40258)
-- Name: ix_applications_equipment_requirement_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_value ON common.equipment_requirement_property USING btree (value);


--
-- TOC entry 5748 (class 1259 OID 39561)
-- Name: ix_applications_equipment_requirement_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_value_type_id ON common.equipment_requirement_property USING btree (value_type_id);


--
-- TOC entry 5749 (class 1259 OID 39562)
-- Name: ix_applications_equipment_requirement_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_property_value_uom_id ON common.equipment_requirement_property USING btree (value_uom_id);


--
-- TOC entry 5609 (class 1259 OID 38884)
-- Name: ix_applications_equipment_requirement_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_quantity ON common.equipment_requirement USING btree (quantity);


--
-- TOC entry 5610 (class 1259 OID 38885)
-- Name: ix_applications_equipment_requirement_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_equipment_requirement_quantity_uom_id ON common.equipment_requirement USING btree (quantity_uom_id);


--
-- TOC entry 4821 (class 1259 OID 34827)
-- Name: ix_applications_job_list_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_code ON common.job_list USING btree (code);


--
-- TOC entry 4822 (class 1259 OID 34828)
-- Name: ix_applications_job_list_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_hierarchy_scope_id ON common.job_list USING btree (hierarchy_scope_id);


--
-- TOC entry 4823 (class 1259 OID 34829)
-- Name: ix_applications_job_list_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_name ON common.job_list USING btree (name);


--
-- TOC entry 4981 (class 1259 OID 35626)
-- Name: ix_applications_job_list_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_property_code ON common.job_list_property USING btree (code);


--
-- TOC entry 4982 (class 1259 OID 35627)
-- Name: ix_applications_job_list_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_property_name ON common.job_list_property USING btree (name);


--
-- TOC entry 4983 (class 1259 OID 35628)
-- Name: ix_applications_job_list_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_property_parent_id ON common.job_list_property USING btree (parent_id);


--
-- TOC entry 4984 (class 1259 OID 41059)
-- Name: ix_applications_job_list_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_property_sensor_id ON common.job_list_property USING btree (sensor_id);


--
-- TOC entry 4985 (class 1259 OID 41060)
-- Name: ix_applications_job_list_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_property_sensor_type_id ON common.job_list_property USING btree (sensor_type_id);


--
-- TOC entry 4986 (class 1259 OID 41058)
-- Name: ix_applications_job_list_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_property_uom_category_id ON common.job_list_property USING btree (uom_category_id);


--
-- TOC entry 4987 (class 1259 OID 40263)
-- Name: ix_applications_job_list_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_property_value ON common.job_list_property USING btree (value);


--
-- TOC entry 4988 (class 1259 OID 35630)
-- Name: ix_applications_job_list_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_property_value_type_id ON common.job_list_property USING btree (value_type_id);


--
-- TOC entry 4989 (class 1259 OID 35631)
-- Name: ix_applications_job_list_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_list_property_value_uom_id ON common.job_list_property USING btree (value_uom_id);


--
-- TOC entry 5357 (class 1259 OID 37431)
-- Name: ix_applications_job_order_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_code ON common.job_order USING btree (code);


--
-- TOC entry 5358 (class 1259 OID 37432)
-- Name: ix_applications_job_order_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_hierarchy_scope_id ON common.job_order USING btree (hierarchy_scope_id);


--
-- TOC entry 5359 (class 1259 OID 37433)
-- Name: ix_applications_job_order_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_name ON common.job_order USING btree (name);


--
-- TOC entry 5443 (class 1259 OID 38022)
-- Name: ix_applications_job_order_parameter_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_parameter_code ON common.job_order_parameter USING btree (code);


--
-- TOC entry 5444 (class 1259 OID 38023)
-- Name: ix_applications_job_order_parameter_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_parameter_name ON common.job_order_parameter USING btree (name);


--
-- TOC entry 5445 (class 1259 OID 38024)
-- Name: ix_applications_job_order_parameter_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_parameter_parent_id ON common.job_order_parameter USING btree (parent_id);


--
-- TOC entry 5446 (class 1259 OID 41077)
-- Name: ix_applications_job_order_parameter_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_parameter_sensor_id ON common.job_order_parameter USING btree (sensor_id);


--
-- TOC entry 5447 (class 1259 OID 41078)
-- Name: ix_applications_job_order_parameter_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_parameter_sensor_type_id ON common.job_order_parameter USING btree (sensor_type_id);


--
-- TOC entry 5448 (class 1259 OID 41076)
-- Name: ix_applications_job_order_parameter_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_parameter_uom_category_id ON common.job_order_parameter USING btree (uom_category_id);


--
-- TOC entry 5449 (class 1259 OID 40264)
-- Name: ix_applications_job_order_parameter_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_parameter_value ON common.job_order_parameter USING btree (value);


--
-- TOC entry 5450 (class 1259 OID 38026)
-- Name: ix_applications_job_order_parameter_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_parameter_value_type_id ON common.job_order_parameter USING btree (value_type_id);


--
-- TOC entry 5451 (class 1259 OID 38027)
-- Name: ix_applications_job_order_parameter_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_order_parameter_value_uom_id ON common.job_order_parameter USING btree (value_uom_id);


--
-- TOC entry 5454 (class 1259 OID 38076)
-- Name: ix_applications_job_response_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_code ON common.job_response USING btree (code);


--
-- TOC entry 5612 (class 1259 OID 38921)
-- Name: ix_applications_job_response_data_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_data_code ON common.job_response_data USING btree (code);


--
-- TOC entry 5613 (class 1259 OID 38922)
-- Name: ix_applications_job_response_data_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_data_name ON common.job_response_data USING btree (name);


--
-- TOC entry 5614 (class 1259 OID 38923)
-- Name: ix_applications_job_response_data_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_data_parent_id ON common.job_response_data USING btree (parent_id);


--
-- TOC entry 5615 (class 1259 OID 41095)
-- Name: ix_applications_job_response_data_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_data_sensor_id ON common.job_response_data USING btree (sensor_id);


--
-- TOC entry 5616 (class 1259 OID 41096)
-- Name: ix_applications_job_response_data_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_data_sensor_type_id ON common.job_response_data USING btree (sensor_type_id);


--
-- TOC entry 5617 (class 1259 OID 41094)
-- Name: ix_applications_job_response_data_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_data_uom_category_id ON common.job_response_data USING btree (uom_category_id);


--
-- TOC entry 5618 (class 1259 OID 40265)
-- Name: ix_applications_job_response_data_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_data_value ON common.job_response_data USING btree (value);


--
-- TOC entry 5619 (class 1259 OID 38925)
-- Name: ix_applications_job_response_data_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_data_value_type_id ON common.job_response_data USING btree (value_type_id);


--
-- TOC entry 5620 (class 1259 OID 38926)
-- Name: ix_applications_job_response_data_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_data_value_uom_id ON common.job_response_data USING btree (value_uom_id);


--
-- TOC entry 5455 (class 1259 OID 38077)
-- Name: ix_applications_job_response_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_hierarchy_scope_id ON common.job_response USING btree (hierarchy_scope_id);


--
-- TOC entry 5456 (class 1259 OID 38078)
-- Name: ix_applications_job_response_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_name ON common.job_response USING btree (name);


--
-- TOC entry 5457 (class 1259 OID 38079)
-- Name: ix_applications_job_response_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_job_response_parent_id ON common.job_response USING btree (parent_id);


--
-- TOC entry 5750 (class 1259 OID 39636)
-- Name: ix_applications_material_actual_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_code ON common.material_actual USING btree (code);


--
-- TOC entry 5751 (class 1259 OID 39637)
-- Name: ix_applications_material_actual_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_name ON common.material_actual USING btree (name);


--
-- TOC entry 5752 (class 1259 OID 39638)
-- Name: ix_applications_material_actual_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_parent_id ON common.material_actual USING btree (parent_id);


--
-- TOC entry 5850 (class 1259 OID 40054)
-- Name: ix_applications_material_actual_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_code ON common.material_actual_property USING btree (code);


--
-- TOC entry 5851 (class 1259 OID 40055)
-- Name: ix_applications_material_actual_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_name ON common.material_actual_property USING btree (name);


--
-- TOC entry 5852 (class 1259 OID 40056)
-- Name: ix_applications_material_actual_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_parent_id ON common.material_actual_property USING btree (parent_id);


--
-- TOC entry 5853 (class 1259 OID 40057)
-- Name: ix_applications_material_actual_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_quantity ON common.material_actual_property USING btree (quantity);


--
-- TOC entry 5854 (class 1259 OID 40058)
-- Name: ix_applications_material_actual_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_quantity_uom_id ON common.material_actual_property USING btree (quantity_uom_id);


--
-- TOC entry 5855 (class 1259 OID 40879)
-- Name: ix_applications_material_actual_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_sensor_id ON common.material_actual_property USING btree (sensor_id);


--
-- TOC entry 5856 (class 1259 OID 40880)
-- Name: ix_applications_material_actual_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_sensor_type_id ON common.material_actual_property USING btree (sensor_type_id);


--
-- TOC entry 5857 (class 1259 OID 40878)
-- Name: ix_applications_material_actual_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_uom_category_id ON common.material_actual_property USING btree (uom_category_id);


--
-- TOC entry 5858 (class 1259 OID 40253)
-- Name: ix_applications_material_actual_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_value ON common.material_actual_property USING btree (value);


--
-- TOC entry 5859 (class 1259 OID 40060)
-- Name: ix_applications_material_actual_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_property_value_uom_id ON common.material_actual_property USING btree (value_uom_id);


--
-- TOC entry 5753 (class 1259 OID 39639)
-- Name: ix_applications_material_actual_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_quantity ON common.material_actual USING btree (quantity);


--
-- TOC entry 5754 (class 1259 OID 39640)
-- Name: ix_applications_material_actual_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_actual_quantity_uom_id ON common.material_actual USING btree (quantity_uom_id);


--
-- TOC entry 5460 (class 1259 OID 38168)
-- Name: ix_applications_material_capability_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_code ON common.material_capability USING btree (code);


--
-- TOC entry 5461 (class 1259 OID 38169)
-- Name: ix_applications_material_capability_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_hierarchy_scope_id ON common.material_capability USING btree (hierarchy_scope_id);


--
-- TOC entry 5462 (class 1259 OID 38170)
-- Name: ix_applications_material_capability_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_name ON common.material_capability USING btree (name);


--
-- TOC entry 5463 (class 1259 OID 38171)
-- Name: ix_applications_material_capability_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_parent_id ON common.material_capability USING btree (parent_id);


--
-- TOC entry 5624 (class 1259 OID 38967)
-- Name: ix_applications_material_capability_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_code ON common.material_capability_property USING btree (code);


--
-- TOC entry 5625 (class 1259 OID 38968)
-- Name: ix_applications_material_capability_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_name ON common.material_capability_property USING btree (name);


--
-- TOC entry 5626 (class 1259 OID 38969)
-- Name: ix_applications_material_capability_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_parent_id ON common.material_capability_property USING btree (parent_id);


--
-- TOC entry 5627 (class 1259 OID 38970)
-- Name: ix_applications_material_capability_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_quantity ON common.material_capability_property USING btree (quantity);


--
-- TOC entry 5628 (class 1259 OID 38971)
-- Name: ix_applications_material_capability_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_quantity_uom_id ON common.material_capability_property USING btree (quantity_uom_id);


--
-- TOC entry 5629 (class 1259 OID 40951)
-- Name: ix_applications_material_capability_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_sensor_id ON common.material_capability_property USING btree (sensor_id);


--
-- TOC entry 5630 (class 1259 OID 40952)
-- Name: ix_applications_material_capability_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_sensor_type_id ON common.material_capability_property USING btree (sensor_type_id);


--
-- TOC entry 5631 (class 1259 OID 40950)
-- Name: ix_applications_material_capability_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_uom_category_id ON common.material_capability_property USING btree (uom_category_id);


--
-- TOC entry 5632 (class 1259 OID 40257)
-- Name: ix_applications_material_capability_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_value ON common.material_capability_property USING btree (value);


--
-- TOC entry 5633 (class 1259 OID 38973)
-- Name: ix_applications_material_capability_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_value_type_id ON common.material_capability_property USING btree (value_type_id);


--
-- TOC entry 5634 (class 1259 OID 38974)
-- Name: ix_applications_material_capability_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_property_value_uom_id ON common.material_capability_property USING btree (value_uom_id);


--
-- TOC entry 5464 (class 1259 OID 38172)
-- Name: ix_applications_material_capability_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_quantity ON common.material_capability USING btree (quantity);


--
-- TOC entry 5465 (class 1259 OID 38173)
-- Name: ix_applications_material_capability_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_quantity_uom_id ON common.material_capability USING btree (quantity_uom_id);


--
-- TOC entry 5468 (class 1259 OID 38197)
-- Name: ix_applications_material_capability_test_result_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_test_result_code ON common.material_capability_test_result USING btree (code);


--
-- TOC entry 5469 (class 1259 OID 38198)
-- Name: ix_applications_material_capability_test_result_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_test_result_name ON common.material_capability_test_result USING btree (name);


--
-- TOC entry 5362 (class 1259 OID 37482)
-- Name: ix_applications_material_capability_test_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_test_specification_code ON common.material_capability_test_specification USING btree (code);


--
-- TOC entry 5363 (class 1259 OID 37483)
-- Name: ix_applications_material_capability_test_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_capability_test_specification_name ON common.material_capability_test_specification USING btree (name);


--
-- TOC entry 4826 (class 1259 OID 34878)
-- Name: ix_applications_material_lot_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_code ON common.material_lot USING btree (code);


--
-- TOC entry 4827 (class 1259 OID 34879)
-- Name: ix_applications_material_lot_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_name ON common.material_lot USING btree (name);


--
-- TOC entry 5203 (class 1259 OID 36738)
-- Name: ix_applications_material_lot_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_property_code ON common.material_lot_property USING btree (code);


--
-- TOC entry 5204 (class 1259 OID 36739)
-- Name: ix_applications_material_lot_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_property_name ON common.material_lot_property USING btree (name);


--
-- TOC entry 5205 (class 1259 OID 36740)
-- Name: ix_applications_material_lot_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_property_parent_id ON common.material_lot_property USING btree (parent_id);


--
-- TOC entry 5206 (class 1259 OID 41041)
-- Name: ix_applications_material_lot_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_property_sensor_id ON common.material_lot_property USING btree (sensor_id);


--
-- TOC entry 5207 (class 1259 OID 41042)
-- Name: ix_applications_material_lot_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_property_sensor_type_id ON common.material_lot_property USING btree (sensor_type_id);


--
-- TOC entry 5208 (class 1259 OID 41040)
-- Name: ix_applications_material_lot_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_property_uom_category_id ON common.material_lot_property USING btree (uom_category_id);


--
-- TOC entry 5209 (class 1259 OID 40262)
-- Name: ix_applications_material_lot_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_property_value ON common.material_lot_property USING btree (value);


--
-- TOC entry 5210 (class 1259 OID 36742)
-- Name: ix_applications_material_lot_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_property_value_type_id ON common.material_lot_property USING btree (value_type_id);


--
-- TOC entry 5211 (class 1259 OID 36743)
-- Name: ix_applications_material_lot_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_property_value_uom_id ON common.material_lot_property USING btree (value_uom_id);


--
-- TOC entry 4828 (class 1259 OID 34880)
-- Name: ix_applications_material_lot_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_quantity ON common.material_lot USING btree (quantity);


--
-- TOC entry 4829 (class 1259 OID 34881)
-- Name: ix_applications_material_lot_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_lot_quantity_uom_id ON common.material_lot USING btree (quantity_uom_id);


--
-- TOC entry 5637 (class 1259 OID 39048)
-- Name: ix_applications_material_requirement_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_code ON common.material_requirement USING btree (code);


--
-- TOC entry 5638 (class 1259 OID 39049)
-- Name: ix_applications_material_requirement_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_name ON common.material_requirement USING btree (name);


--
-- TOC entry 5639 (class 1259 OID 39050)
-- Name: ix_applications_material_requirement_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_parent_id ON common.material_requirement USING btree (parent_id);


--
-- TOC entry 5758 (class 1259 OID 39681)
-- Name: ix_applications_material_requirement_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_code ON common.material_requirement_property USING btree (code);


--
-- TOC entry 5759 (class 1259 OID 39682)
-- Name: ix_applications_material_requirement_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_name ON common.material_requirement_property USING btree (name);


--
-- TOC entry 5760 (class 1259 OID 39683)
-- Name: ix_applications_material_requirement_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_parent_id ON common.material_requirement_property USING btree (parent_id);


--
-- TOC entry 5761 (class 1259 OID 39684)
-- Name: ix_applications_material_requirement_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_quantity ON common.material_requirement_property USING btree (quantity);


--
-- TOC entry 5762 (class 1259 OID 39685)
-- Name: ix_applications_material_requirement_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_quantity_uom_id ON common.material_requirement_property USING btree (quantity_uom_id);


--
-- TOC entry 5763 (class 1259 OID 41023)
-- Name: ix_applications_material_requirement_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_sensor_id ON common.material_requirement_property USING btree (sensor_id);


--
-- TOC entry 5764 (class 1259 OID 41024)
-- Name: ix_applications_material_requirement_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_sensor_type_id ON common.material_requirement_property USING btree (sensor_type_id);


--
-- TOC entry 5765 (class 1259 OID 41022)
-- Name: ix_applications_material_requirement_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_uom_category_id ON common.material_requirement_property USING btree (uom_category_id);


--
-- TOC entry 5766 (class 1259 OID 40261)
-- Name: ix_applications_material_requirement_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_value ON common.material_requirement_property USING btree (value);


--
-- TOC entry 5767 (class 1259 OID 39687)
-- Name: ix_applications_material_requirement_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_value_type_id ON common.material_requirement_property USING btree (value_type_id);


--
-- TOC entry 5768 (class 1259 OID 39688)
-- Name: ix_applications_material_requirement_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_property_value_uom_id ON common.material_requirement_property USING btree (value_uom_id);


--
-- TOC entry 5640 (class 1259 OID 39051)
-- Name: ix_applications_material_requirement_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_quantity ON common.material_requirement USING btree (quantity);


--
-- TOC entry 5641 (class 1259 OID 39052)
-- Name: ix_applications_material_requirement_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_requirement_quantity_uom_id ON common.material_requirement USING btree (quantity_uom_id);


--
-- TOC entry 4992 (class 1259 OID 35680)
-- Name: ix_applications_material_sub_lot_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_sub_lot_code ON common.material_sub_lot USING btree (code);


--
-- TOC entry 4993 (class 1259 OID 35681)
-- Name: ix_applications_material_sub_lot_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_sub_lot_name ON common.material_sub_lot USING btree (name);


--
-- TOC entry 4994 (class 1259 OID 35682)
-- Name: ix_applications_material_sub_lot_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_sub_lot_parent_id ON common.material_sub_lot USING btree (parent_id);


--
-- TOC entry 4995 (class 1259 OID 35683)
-- Name: ix_applications_material_sub_lot_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_sub_lot_quantity ON common.material_sub_lot USING btree (quantity);


--
-- TOC entry 4996 (class 1259 OID 35684)
-- Name: ix_applications_material_sub_lot_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_material_sub_lot_quantity_uom_id ON common.material_sub_lot USING btree (quantity_uom_id);


--
-- TOC entry 4832 (class 1259 OID 34910)
-- Name: ix_common_capability_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_code ON common.operations_capability USING btree (code);


--
-- TOC entry 4833 (class 1259 OID 34911)
-- Name: ix_common_capability_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_hierarchy_scope_id ON common.operations_capability USING btree (hierarchy_scope_id);


--
-- TOC entry 4834 (class 1259 OID 34912)
-- Name: ix_common_capability_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_name ON common.operations_capability USING btree (name);


--
-- TOC entry 4835 (class 1259 OID 34913)
-- Name: ix_common_capability_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_parent_id ON common.operations_capability USING btree (parent_id);


--
-- TOC entry 5000 (class 1259 OID 35720)
-- Name: ix_common_capability_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_code ON common.operations_capability_property USING btree (code);


--
-- TOC entry 5001 (class 1259 OID 35721)
-- Name: ix_common_capability_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_name ON common.operations_capability_property USING btree (name);


--
-- TOC entry 5002 (class 1259 OID 35722)
-- Name: ix_common_capability_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_parent_id ON common.operations_capability_property USING btree (parent_id);


--
-- TOC entry 5003 (class 1259 OID 41113)
-- Name: ix_common_capability_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_sensor_id ON common.operations_capability_property USING btree (sensor_id);


--
-- TOC entry 5004 (class 1259 OID 41114)
-- Name: ix_common_capability_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_sensor_type_id ON common.operations_capability_property USING btree (sensor_type_id);


--
-- TOC entry 5005 (class 1259 OID 41112)
-- Name: ix_common_capability_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_uom_category_id ON common.operations_capability_property USING btree (uom_category_id);


--
-- TOC entry 5006 (class 1259 OID 40266)
-- Name: ix_common_capability_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_value ON common.operations_capability_property USING btree (value);


--
-- TOC entry 5007 (class 1259 OID 35724)
-- Name: ix_common_capability_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_value_type_id ON common.operations_capability_property USING btree (value_type_id);


--
-- TOC entry 5008 (class 1259 OID 35725)
-- Name: ix_common_capability_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_capability_property_value_uom_id ON common.operations_capability_property USING btree (value_uom_id);


--
-- TOC entry 4838 (class 1259 OID 34947)
-- Name: ix_common_performance_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_code ON common.operations_performance USING btree (code);


--
-- TOC entry 4839 (class 1259 OID 34948)
-- Name: ix_common_performance_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_hierarchy_scope_id ON common.operations_performance USING btree (hierarchy_scope_id);


--
-- TOC entry 4840 (class 1259 OID 34949)
-- Name: ix_common_performance_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_name ON common.operations_performance USING btree (name);


--
-- TOC entry 5012 (class 1259 OID 35761)
-- Name: ix_common_performance_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_code ON common.operations_performance_property USING btree (code);


--
-- TOC entry 5013 (class 1259 OID 35762)
-- Name: ix_common_performance_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_name ON common.operations_performance_property USING btree (name);


--
-- TOC entry 5014 (class 1259 OID 35763)
-- Name: ix_common_performance_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_parent_id ON common.operations_performance_property USING btree (parent_id);


--
-- TOC entry 5015 (class 1259 OID 41131)
-- Name: ix_common_performance_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_sensor_id ON common.operations_performance_property USING btree (sensor_id);


--
-- TOC entry 5016 (class 1259 OID 41132)
-- Name: ix_common_performance_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_sensor_type_id ON common.operations_performance_property USING btree (sensor_type_id);


--
-- TOC entry 5017 (class 1259 OID 41130)
-- Name: ix_common_performance_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_uom_category_id ON common.operations_performance_property USING btree (uom_category_id);


--
-- TOC entry 5018 (class 1259 OID 40267)
-- Name: ix_common_performance_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_value ON common.operations_performance_property USING btree (value);


--
-- TOC entry 5019 (class 1259 OID 35765)
-- Name: ix_common_performance_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_value_type_id ON common.operations_performance_property USING btree (value_type_id);


--
-- TOC entry 5020 (class 1259 OID 35766)
-- Name: ix_common_performance_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_performance_property_value_uom_id ON common.operations_performance_property USING btree (value_uom_id);


--
-- TOC entry 5366 (class 1259 OID 37532)
-- Name: ix_common_request_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_code ON common.operations_request USING btree (code);


--
-- TOC entry 5367 (class 1259 OID 37533)
-- Name: ix_common_request_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_hierarchy_scope_id ON common.operations_request USING btree (hierarchy_scope_id);


--
-- TOC entry 5368 (class 1259 OID 37534)
-- Name: ix_common_request_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_name ON common.operations_request USING btree (name);


--
-- TOC entry 5473 (class 1259 OID 38234)
-- Name: ix_common_request_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_code ON common.operations_request_property USING btree (code);


--
-- TOC entry 5474 (class 1259 OID 38235)
-- Name: ix_common_request_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_name ON common.operations_request_property USING btree (name);


--
-- TOC entry 5475 (class 1259 OID 38236)
-- Name: ix_common_request_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_parent_id ON common.operations_request_property USING btree (parent_id);


--
-- TOC entry 5476 (class 1259 OID 41149)
-- Name: ix_common_request_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_sensor_id ON common.operations_request_property USING btree (sensor_id);


--
-- TOC entry 5477 (class 1259 OID 41150)
-- Name: ix_common_request_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_sensor_type_id ON common.operations_request_property USING btree (sensor_type_id);


--
-- TOC entry 5478 (class 1259 OID 41148)
-- Name: ix_common_request_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_uom_category_id ON common.operations_request_property USING btree (uom_category_id);


--
-- TOC entry 5479 (class 1259 OID 40268)
-- Name: ix_common_request_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_value ON common.operations_request_property USING btree (value);


--
-- TOC entry 5480 (class 1259 OID 38238)
-- Name: ix_common_request_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_value_type_id ON common.operations_request_property USING btree (value_type_id);


--
-- TOC entry 5481 (class 1259 OID 38239)
-- Name: ix_common_request_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_request_property_value_uom_id ON common.operations_request_property USING btree (value_uom_id);


--
-- TOC entry 5484 (class 1259 OID 38283)
-- Name: ix_common_response_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_code ON common.operations_response USING btree (code);


--
-- TOC entry 5485 (class 1259 OID 38284)
-- Name: ix_common_response_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_hierarchy_scope_id ON common.operations_response USING btree (hierarchy_scope_id);


--
-- TOC entry 5486 (class 1259 OID 38285)
-- Name: ix_common_response_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_name ON common.operations_response USING btree (name);


--
-- TOC entry 5645 (class 1259 OID 39088)
-- Name: ix_common_response_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_code ON common.operations_response_property USING btree (code);


--
-- TOC entry 5646 (class 1259 OID 39089)
-- Name: ix_common_response_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_name ON common.operations_response_property USING btree (name);


--
-- TOC entry 5647 (class 1259 OID 39090)
-- Name: ix_common_response_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_parent_id ON common.operations_response_property USING btree (parent_id);


--
-- TOC entry 5648 (class 1259 OID 41167)
-- Name: ix_common_response_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_sensor_id ON common.operations_response_property USING btree (sensor_id);


--
-- TOC entry 5649 (class 1259 OID 41168)
-- Name: ix_common_response_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_sensor_type_id ON common.operations_response_property USING btree (sensor_type_id);


--
-- TOC entry 5650 (class 1259 OID 41166)
-- Name: ix_common_response_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_uom_category_id ON common.operations_response_property USING btree (uom_category_id);


--
-- TOC entry 5651 (class 1259 OID 40269)
-- Name: ix_common_response_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_value ON common.operations_response_property USING btree (value);


--
-- TOC entry 5652 (class 1259 OID 39092)
-- Name: ix_common_response_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_value_type_id ON common.operations_response_property USING btree (value_type_id);


--
-- TOC entry 5653 (class 1259 OID 39093)
-- Name: ix_common_response_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_response_property_value_uom_id ON common.operations_response_property USING btree (value_uom_id);


--
-- TOC entry 4652 (class 1259 OID 34145)
-- Name: ix_common_schedule_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_code ON common.operations_schedule USING btree (code);


--
-- TOC entry 4653 (class 1259 OID 34146)
-- Name: ix_common_schedule_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_name ON common.operations_schedule USING btree (name);


--
-- TOC entry 4844 (class 1259 OID 34985)
-- Name: ix_common_schedule_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_code ON common.operations_schedule_property USING btree (code);


--
-- TOC entry 4845 (class 1259 OID 34986)
-- Name: ix_common_schedule_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_name ON common.operations_schedule_property USING btree (name);


--
-- TOC entry 4846 (class 1259 OID 34987)
-- Name: ix_common_schedule_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_parent_id ON common.operations_schedule_property USING btree (parent_id);


--
-- TOC entry 4847 (class 1259 OID 41185)
-- Name: ix_common_schedule_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_sensor_id ON common.operations_schedule_property USING btree (sensor_id);


--
-- TOC entry 4848 (class 1259 OID 41186)
-- Name: ix_common_schedule_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_sensor_type_id ON common.operations_schedule_property USING btree (sensor_type_id);


--
-- TOC entry 4849 (class 1259 OID 41184)
-- Name: ix_common_schedule_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_uom_category_id ON common.operations_schedule_property USING btree (uom_category_id);


--
-- TOC entry 4850 (class 1259 OID 40270)
-- Name: ix_common_schedule_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_value ON common.operations_schedule_property USING btree (value);


--
-- TOC entry 4851 (class 1259 OID 34989)
-- Name: ix_common_schedule_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_value_type_id ON common.operations_schedule_property USING btree (value_type_id);


--
-- TOC entry 4852 (class 1259 OID 34990)
-- Name: ix_common_schedule_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_schedule_property_value_uom_id ON common.operations_schedule_property USING btree (value_uom_id);


--
-- TOC entry 5771 (class 1259 OID 39737)
-- Name: ix_applications_personnel_actual_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_code ON common.personnel_actual USING btree (code);


--
-- TOC entry 5772 (class 1259 OID 39738)
-- Name: ix_applications_personnel_actual_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_name ON common.personnel_actual USING btree (name);


--
-- TOC entry 5863 (class 1259 OID 40101)
-- Name: ix_applications_personnel_actual_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_code ON common.personnel_actual_property USING btree (code);


--
-- TOC entry 5864 (class 1259 OID 40102)
-- Name: ix_applications_personnel_actual_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_name ON common.personnel_actual_property USING btree (name);


--
-- TOC entry 5865 (class 1259 OID 40103)
-- Name: ix_applications_personnel_actual_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_parent_id ON common.personnel_actual_property USING btree (parent_id);


--
-- TOC entry 5866 (class 1259 OID 40104)
-- Name: ix_applications_personnel_actual_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_quantity ON common.personnel_actual_property USING btree (quantity);


--
-- TOC entry 5867 (class 1259 OID 40105)
-- Name: ix_applications_personnel_actual_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_quantity_uom_id ON common.personnel_actual_property USING btree (quantity_uom_id);


--
-- TOC entry 5868 (class 1259 OID 40843)
-- Name: ix_applications_personnel_actual_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_sensor_id ON common.personnel_actual_property USING btree (sensor_id);


--
-- TOC entry 5869 (class 1259 OID 40844)
-- Name: ix_applications_personnel_actual_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_sensor_type_id ON common.personnel_actual_property USING btree (sensor_type_id);


--
-- TOC entry 5870 (class 1259 OID 40842)
-- Name: ix_applications_personnel_actual_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_uom_category_id ON common.personnel_actual_property USING btree (uom_category_id);


--
-- TOC entry 5871 (class 1259 OID 40251)
-- Name: ix_applications_personnel_actual_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_value ON common.personnel_actual_property USING btree (value);


--
-- TOC entry 5872 (class 1259 OID 40107)
-- Name: ix_applications_personnel_actual_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_value_type_id ON common.personnel_actual_property USING btree (value_type_id);


--
-- TOC entry 5873 (class 1259 OID 40108)
-- Name: ix_applications_personnel_actual_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_property_value_uom_id ON common.personnel_actual_property USING btree (value_uom_id);


--
-- TOC entry 5773 (class 1259 OID 39739)
-- Name: ix_applications_personnel_actual_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_quantity ON common.personnel_actual USING btree (quantity);


--
-- TOC entry 5774 (class 1259 OID 39740)
-- Name: ix_applications_personnel_actual_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_actual_quantity_uom_id ON common.personnel_actual USING btree (quantity_uom_id);


--
-- TOC entry 5489 (class 1259 OID 38349)
-- Name: ix_applications_personnel_capability_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_code ON common.personnel_capability USING btree (code);


--
-- TOC entry 5490 (class 1259 OID 38350)
-- Name: ix_applications_personnel_capability_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_hierarchy_scope_id ON common.personnel_capability USING btree (hierarchy_scope_id);


--
-- TOC entry 5491 (class 1259 OID 38351)
-- Name: ix_applications_personnel_capability_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_name ON common.personnel_capability USING btree (name);


--
-- TOC entry 5657 (class 1259 OID 39134)
-- Name: ix_applications_personnel_capability_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_code ON common.personnel_capability_property USING btree (code);


--
-- TOC entry 5658 (class 1259 OID 39135)
-- Name: ix_applications_personnel_capability_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_name ON common.personnel_capability_property USING btree (name);


--
-- TOC entry 5659 (class 1259 OID 39136)
-- Name: ix_applications_personnel_capability_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_parent_id ON common.personnel_capability_property USING btree (parent_id);


--
-- TOC entry 5660 (class 1259 OID 39137)
-- Name: ix_applications_personnel_capability_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_quantity ON common.personnel_capability_property USING btree (quantity);


--
-- TOC entry 5661 (class 1259 OID 39138)
-- Name: ix_applications_personnel_capability_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_quantity_uom_id ON common.personnel_capability_property USING btree (quantity_uom_id);


--
-- TOC entry 5662 (class 1259 OID 40915)
-- Name: ix_applications_personnel_capability_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_sensor_id ON common.personnel_capability_property USING btree (sensor_id);


--
-- TOC entry 5663 (class 1259 OID 40916)
-- Name: ix_applications_personnel_capability_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_sensor_type_id ON common.personnel_capability_property USING btree (sensor_type_id);


--
-- TOC entry 5664 (class 1259 OID 40914)
-- Name: ix_applications_personnel_capability_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_uom_category_id ON common.personnel_capability_property USING btree (uom_category_id);


--
-- TOC entry 5665 (class 1259 OID 40255)
-- Name: ix_applications_personnel_capability_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_value ON common.personnel_capability_property USING btree (value);


--
-- TOC entry 5666 (class 1259 OID 39140)
-- Name: ix_applications_personnel_capability_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_value_type_id ON common.personnel_capability_property USING btree (value_type_id);


--
-- TOC entry 5667 (class 1259 OID 39141)
-- Name: ix_applications_personnel_capability_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_property_value_uom_id ON common.personnel_capability_property USING btree (value_uom_id);


--
-- TOC entry 5492 (class 1259 OID 38352)
-- Name: ix_applications_personnel_capability_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_quantity ON common.personnel_capability USING btree (quantity);


--
-- TOC entry 5493 (class 1259 OID 38353)
-- Name: ix_applications_personnel_capability_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_quantity_uom_id ON common.personnel_capability USING btree (quantity_uom_id);


--
-- TOC entry 5023 (class 1259 OID 35790)
-- Name: ix_applications_personnel_capability_test_result_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_test_result_code ON common.personnel_capability_test_result USING btree (code);


--
-- TOC entry 5024 (class 1259 OID 35791)
-- Name: ix_applications_personnel_capability_test_result_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_test_result_name ON common.personnel_capability_test_result USING btree (name);


--
-- TOC entry 4855 (class 1259 OID 35024)
-- Name: ix_applications_personnel_capability_test_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_test_specification_code ON common.personnel_capability_test_specification USING btree (code);


--
-- TOC entry 4856 (class 1259 OID 35025)
-- Name: ix_applications_personnel_capability_test_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_capability_test_specification_name ON common.personnel_capability_test_specification USING btree (name);


--
-- TOC entry 5670 (class 1259 OID 39185)
-- Name: ix_applications_personnel_requirement_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_code ON common.personnel_requirement USING btree (code);


--
-- TOC entry 5671 (class 1259 OID 39186)
-- Name: ix_applications_personnel_requirement_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_name ON common.personnel_requirement USING btree (name);


--
-- TOC entry 5778 (class 1259 OID 39781)
-- Name: ix_applications_personnel_requirement_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_code ON common.personnel_requirement_property USING btree (code);


--
-- TOC entry 5779 (class 1259 OID 39782)
-- Name: ix_applications_personnel_requirement_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_name ON common.personnel_requirement_property USING btree (name);


--
-- TOC entry 5780 (class 1259 OID 39783)
-- Name: ix_applications_personnel_requirement_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_parent_id ON common.personnel_requirement_property USING btree (parent_id);


--
-- TOC entry 5781 (class 1259 OID 39784)
-- Name: ix_applications_personnel_requirement_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_quantity ON common.personnel_requirement_property USING btree (quantity);


--
-- TOC entry 5782 (class 1259 OID 39785)
-- Name: ix_applications_personnel_requirement_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_quantity_uom_id ON common.personnel_requirement_property USING btree (quantity_uom_id);


--
-- TOC entry 5783 (class 1259 OID 40987)
-- Name: ix_applications_personnel_requirement_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_sensor_id ON common.personnel_requirement_property USING btree (sensor_id);


--
-- TOC entry 5784 (class 1259 OID 40988)
-- Name: ix_applications_personnel_requirement_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_sensor_type_id ON common.personnel_requirement_property USING btree (sensor_type_id);


--
-- TOC entry 5785 (class 1259 OID 40986)
-- Name: ix_applications_personnel_requirement_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_uom_category_id ON common.personnel_requirement_property USING btree (uom_category_id);


--
-- TOC entry 5786 (class 1259 OID 40259)
-- Name: ix_applications_personnel_requirement_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_value ON common.personnel_requirement_property USING btree (value);


--
-- TOC entry 5787 (class 1259 OID 39787)
-- Name: ix_applications_personnel_requirement_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_value_type_id ON common.personnel_requirement_property USING btree (value_type_id);


--
-- TOC entry 5788 (class 1259 OID 39788)
-- Name: ix_applications_personnel_requirement_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_property_value_uom_id ON common.personnel_requirement_property USING btree (value_uom_id);


--
-- TOC entry 5672 (class 1259 OID 39187)
-- Name: ix_applications_personnel_requirement_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_quantity ON common.personnel_requirement USING btree (quantity);


--
-- TOC entry 5673 (class 1259 OID 39188)
-- Name: ix_applications_personnel_requirement_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_personnel_requirement_quantity_uom_id ON common.personnel_requirement USING btree (quantity_uom_id);


--
-- TOC entry 5791 (class 1259 OID 39832)
-- Name: ix_applications_physical_asset_actual_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_code ON common.physical_asset_actual USING btree (code);


--
-- TOC entry 5792 (class 1259 OID 39833)
-- Name: ix_applications_physical_asset_actual_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_name ON common.physical_asset_actual USING btree (name);


--
-- TOC entry 5877 (class 1259 OID 40149)
-- Name: ix_applications_physical_asset_actual_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_code ON common.physical_asset_actual_property USING btree (code);


--
-- TOC entry 5878 (class 1259 OID 40150)
-- Name: ix_applications_physical_asset_actual_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_name ON common.physical_asset_actual_property USING btree (name);


--
-- TOC entry 5879 (class 1259 OID 40151)
-- Name: ix_applications_physical_asset_actual_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_parent_id ON common.physical_asset_actual_property USING btree (parent_id);


--
-- TOC entry 5880 (class 1259 OID 40152)
-- Name: ix_applications_physical_asset_actual_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_quantity ON common.physical_asset_actual_property USING btree (quantity);


--
-- TOC entry 5881 (class 1259 OID 40153)
-- Name: ix_applications_physical_asset_actual_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_quantity_uom_id ON common.physical_asset_actual_property USING btree (quantity_uom_id);


--
-- TOC entry 5882 (class 1259 OID 40861)
-- Name: ix_applications_physical_asset_actual_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_sensor_id ON common.physical_asset_actual_property USING btree (sensor_id);


--
-- TOC entry 5883 (class 1259 OID 40862)
-- Name: ix_applications_physical_asset_actual_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_sensor_type_id ON common.physical_asset_actual_property USING btree (sensor_type_id);


--
-- TOC entry 5884 (class 1259 OID 40860)
-- Name: ix_applications_physical_asset_actual_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_uom_category_id ON common.physical_asset_actual_property USING btree (uom_category_id);


--
-- TOC entry 5885 (class 1259 OID 40252)
-- Name: ix_applications_physical_asset_actual_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_value ON common.physical_asset_actual_property USING btree (value);


--
-- TOC entry 5886 (class 1259 OID 40155)
-- Name: ix_applications_physical_asset_actual_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_value_type_id ON common.physical_asset_actual_property USING btree (value_type_id);


--
-- TOC entry 5887 (class 1259 OID 40156)
-- Name: ix_applications_physical_asset_actual_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_property_value_uom_id ON common.physical_asset_actual_property USING btree (value_uom_id);


--
-- TOC entry 5793 (class 1259 OID 39834)
-- Name: ix_applications_physical_asset_actual_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_quantity ON common.physical_asset_actual USING btree (quantity);


--
-- TOC entry 5794 (class 1259 OID 39835)
-- Name: ix_applications_physical_asset_actual_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_actual_quantity_uom_id ON common.physical_asset_actual USING btree (quantity_uom_id);


--
-- TOC entry 5496 (class 1259 OID 38417)
-- Name: ix_applications_physical_asset_capability_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_code ON common.physical_asset_capability USING btree (code);


--
-- TOC entry 5497 (class 1259 OID 38418)
-- Name: ix_applications_physical_asset_capability_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_hierarchy_scope_id ON common.physical_asset_capability USING btree (hierarchy_scope_id);


--
-- TOC entry 5498 (class 1259 OID 38419)
-- Name: ix_applications_physical_asset_capability_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_name ON common.physical_asset_capability USING btree (name);


--
-- TOC entry 5677 (class 1259 OID 39229)
-- Name: ix_applications_physical_asset_capability_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_code ON common.physical_asset_capability_property USING btree (code);


--
-- TOC entry 5678 (class 1259 OID 39230)
-- Name: ix_applications_physical_asset_capability_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_name ON common.physical_asset_capability_property USING btree (name);


--
-- TOC entry 5679 (class 1259 OID 39231)
-- Name: ix_applications_physical_asset_capability_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_parent_id ON common.physical_asset_capability_property USING btree (parent_id);


--
-- TOC entry 5680 (class 1259 OID 39232)
-- Name: ix_applications_physical_asset_capability_property_quan_bbc1; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_quan_bbc1 ON common.physical_asset_capability_property USING btree (quantity_uom_id);


--
-- TOC entry 5681 (class 1259 OID 39233)
-- Name: ix_applications_physical_asset_capability_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_quantity ON common.physical_asset_capability_property USING btree (quantity);


--
-- TOC entry 5682 (class 1259 OID 40934)
-- Name: ix_applications_physical_asset_capability_property_sens_f3be; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_sens_f3be ON common.physical_asset_capability_property USING btree (sensor_type_id);


--
-- TOC entry 5683 (class 1259 OID 40933)
-- Name: ix_applications_physical_asset_capability_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_sensor_id ON common.physical_asset_capability_property USING btree (sensor_id);


--
-- TOC entry 5684 (class 1259 OID 40932)
-- Name: ix_applications_physical_asset_capability_property_uom__fff9; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_uom__fff9 ON common.physical_asset_capability_property USING btree (uom_category_id);


--
-- TOC entry 5685 (class 1259 OID 39234)
-- Name: ix_applications_physical_asset_capability_property_valu_16f4; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_valu_16f4 ON common.physical_asset_capability_property USING btree (value_type_id);


--
-- TOC entry 5686 (class 1259 OID 40256)
-- Name: ix_applications_physical_asset_capability_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_value ON common.physical_asset_capability_property USING btree (value);


--
-- TOC entry 5687 (class 1259 OID 39236)
-- Name: ix_applications_physical_asset_capability_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_property_value_uom_id ON common.physical_asset_capability_property USING btree (value_uom_id);


--
-- TOC entry 5499 (class 1259 OID 38420)
-- Name: ix_applications_physical_asset_capability_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_quantity ON common.physical_asset_capability USING btree (quantity);


--
-- TOC entry 5500 (class 1259 OID 38421)
-- Name: ix_applications_physical_asset_capability_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_quantity_uom_id ON common.physical_asset_capability USING btree (quantity_uom_id);


--
-- TOC entry 5371 (class 1259 OID 37558)
-- Name: ix_applications_physical_asset_capability_test_result_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_test_result_code ON common.physical_asset_capability_test_result USING btree (code);


--
-- TOC entry 5372 (class 1259 OID 37559)
-- Name: ix_applications_physical_asset_capability_test_result_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_test_result_name ON common.physical_asset_capability_test_result USING btree (name);


--
-- TOC entry 5214 (class 1259 OID 36777)
-- Name: ix_applications_physical_asset_capability_test_spec_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_test_spec_code ON common.physical_asset_capability_test_spec USING btree (code);


--
-- TOC entry 5215 (class 1259 OID 36778)
-- Name: ix_applications_physical_asset_capability_test_spec_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_capability_test_spec_name ON common.physical_asset_capability_test_spec USING btree (name);


--
-- TOC entry 5690 (class 1259 OID 39285)
-- Name: ix_applications_physical_asset_requirement_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_code ON common.physical_asset_requirement USING btree (code);


--
-- TOC entry 5691 (class 1259 OID 39286)
-- Name: ix_applications_physical_asset_requirement_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_name ON common.physical_asset_requirement USING btree (name);


--
-- TOC entry 5798 (class 1259 OID 39876)
-- Name: ix_applications_physical_asset_requirement_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_code ON common.physical_asset_requirement_property USING btree (code);


--
-- TOC entry 5799 (class 1259 OID 39877)
-- Name: ix_applications_physical_asset_requirement_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_name ON common.physical_asset_requirement_property USING btree (name);


--
-- TOC entry 5800 (class 1259 OID 39878)
-- Name: ix_applications_physical_asset_requirement_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_parent_id ON common.physical_asset_requirement_property USING btree (parent_id);


--
-- TOC entry 5801 (class 1259 OID 39879)
-- Name: ix_applications_physical_asset_requirement_property_qua_af24; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_qua_af24 ON common.physical_asset_requirement_property USING btree (quantity_uom_id);


--
-- TOC entry 5802 (class 1259 OID 39880)
-- Name: ix_applications_physical_asset_requirement_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_quantity ON common.physical_asset_requirement_property USING btree (quantity);


--
-- TOC entry 5803 (class 1259 OID 41006)
-- Name: ix_applications_physical_asset_requirement_property_sen_9324; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_sen_9324 ON common.physical_asset_requirement_property USING btree (sensor_type_id);


--
-- TOC entry 5804 (class 1259 OID 41005)
-- Name: ix_applications_physical_asset_requirement_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_sensor_id ON common.physical_asset_requirement_property USING btree (sensor_id);


--
-- TOC entry 5805 (class 1259 OID 41004)
-- Name: ix_applications_physical_asset_requirement_property_uom_9d54; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_uom_9d54 ON common.physical_asset_requirement_property USING btree (uom_category_id);


--
-- TOC entry 5806 (class 1259 OID 39881)
-- Name: ix_applications_physical_asset_requirement_property_val_3660; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_val_3660 ON common.physical_asset_requirement_property USING btree (value_type_id);


--
-- TOC entry 5807 (class 1259 OID 39882)
-- Name: ix_applications_physical_asset_requirement_property_val_ed84; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_val_ed84 ON common.physical_asset_requirement_property USING btree (value_uom_id);


--
-- TOC entry 5808 (class 1259 OID 40260)
-- Name: ix_applications_physical_asset_requirement_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_property_value ON common.physical_asset_requirement_property USING btree (value);


--
-- TOC entry 5692 (class 1259 OID 39287)
-- Name: ix_applications_physical_asset_requirement_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_quantity ON common.physical_asset_requirement USING btree (quantity);


--
-- TOC entry 5693 (class 1259 OID 39288)
-- Name: ix_applications_physical_asset_requirement_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_physical_asset_requirement_quantity_uom_id ON common.physical_asset_requirement USING btree (quantity_uom_id);


--
-- TOC entry 5027 (class 1259 OID 35830)
-- Name: ix_applications_process_segment_capability_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_process_segment_capability_code ON common.process_segment_capability USING btree (code);


--
-- TOC entry 5028 (class 1259 OID 35831)
-- Name: ix_applications_process_segment_capability_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_process_segment_capability_hierarchy_scope_id ON common.process_segment_capability USING btree (hierarchy_scope_id);


--
-- TOC entry 5029 (class 1259 OID 35832)
-- Name: ix_applications_process_segment_capability_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_process_segment_capability_name ON common.process_segment_capability USING btree (name);


--
-- TOC entry 5030 (class 1259 OID 35833)
-- Name: ix_applications_process_segment_capability_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_process_segment_capability_parent_id ON common.process_segment_capability USING btree (parent_id);


--
-- TOC entry 5812 (class 1259 OID 39919)
-- Name: ix_applications_segment_data_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_data_code ON common.segment_data USING btree (code);


--
-- TOC entry 5813 (class 1259 OID 39920)
-- Name: ix_applications_segment_data_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_data_name ON common.segment_data USING btree (name);


--
-- TOC entry 5814 (class 1259 OID 39921)
-- Name: ix_applications_segment_data_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_data_parent_id ON common.segment_data USING btree (parent_id);


--
-- TOC entry 5815 (class 1259 OID 41203)
-- Name: ix_applications_segment_data_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_data_sensor_id ON common.segment_data USING btree (sensor_id);


--
-- TOC entry 5816 (class 1259 OID 41204)
-- Name: ix_applications_segment_data_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_data_sensor_type_id ON common.segment_data USING btree (sensor_type_id);


--
-- TOC entry 5817 (class 1259 OID 41202)
-- Name: ix_applications_segment_data_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_data_uom_category_id ON common.segment_data USING btree (uom_category_id);


--
-- TOC entry 5818 (class 1259 OID 40271)
-- Name: ix_applications_segment_data_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_data_value ON common.segment_data USING btree (value);


--
-- TOC entry 5819 (class 1259 OID 39923)
-- Name: ix_applications_segment_data_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_data_value_type_id ON common.segment_data USING btree (value_type_id);


--
-- TOC entry 5820 (class 1259 OID 39924)
-- Name: ix_applications_segment_data_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_data_value_uom_id ON common.segment_data USING btree (value_uom_id);


--
-- TOC entry 5697 (class 1259 OID 39324)
-- Name: ix_applications_segment_parameter_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_parameter_code ON common.segment_parameter USING btree (code);


--
-- TOC entry 5698 (class 1259 OID 39325)
-- Name: ix_applications_segment_parameter_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_parameter_name ON common.segment_parameter USING btree (name);


--
-- TOC entry 5699 (class 1259 OID 39326)
-- Name: ix_applications_segment_parameter_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_parameter_parent_id ON common.segment_parameter USING btree (parent_id);


--
-- TOC entry 5700 (class 1259 OID 41221)
-- Name: ix_applications_segment_parameter_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_parameter_sensor_id ON common.segment_parameter USING btree (sensor_id);


--
-- TOC entry 5701 (class 1259 OID 41222)
-- Name: ix_applications_segment_parameter_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_parameter_sensor_type_id ON common.segment_parameter USING btree (sensor_type_id);


--
-- TOC entry 5702 (class 1259 OID 41220)
-- Name: ix_applications_segment_parameter_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_parameter_uom_category_id ON common.segment_parameter USING btree (uom_category_id);


--
-- TOC entry 5703 (class 1259 OID 40272)
-- Name: ix_applications_segment_parameter_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_parameter_value ON common.segment_parameter USING btree (value);


--
-- TOC entry 5704 (class 1259 OID 39328)
-- Name: ix_applications_segment_parameter_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_parameter_value_type_id ON common.segment_parameter USING btree (value_type_id);


--
-- TOC entry 5705 (class 1259 OID 39329)
-- Name: ix_applications_segment_parameter_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_parameter_value_uom_id ON common.segment_parameter USING btree (value_uom_id);


--
-- TOC entry 5503 (class 1259 OID 38480)
-- Name: ix_applications_segment_requirement_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_requirement_code ON common.segment_requirement USING btree (code);


--
-- TOC entry 5504 (class 1259 OID 38481)
-- Name: ix_applications_segment_requirement_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_requirement_hierarchy_scope_id ON common.segment_requirement USING btree (hierarchy_scope_id);


--
-- TOC entry 5505 (class 1259 OID 38482)
-- Name: ix_applications_segment_requirement_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_requirement_name ON common.segment_requirement USING btree (name);


--
-- TOC entry 5506 (class 1259 OID 38483)
-- Name: ix_applications_segment_requirement_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_requirement_parent_id ON common.segment_requirement USING btree (parent_id);


--
-- TOC entry 5708 (class 1259 OID 39388)
-- Name: ix_applications_segment_response_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_response_code ON common.segment_response USING btree (code);


--
-- TOC entry 5709 (class 1259 OID 39389)
-- Name: ix_applications_segment_response_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_response_hierarchy_scope_id ON common.segment_response USING btree (hierarchy_scope_id);


--
-- TOC entry 5710 (class 1259 OID 39390)
-- Name: ix_applications_segment_response_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_response_name ON common.segment_response USING btree (name);


--
-- TOC entry 5711 (class 1259 OID 39391)
-- Name: ix_applications_segment_response_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_segment_response_parent_id ON common.segment_response USING btree (parent_id);


--
-- TOC entry 5033 (class 1259 OID 35867)
-- Name: ix_applications_work_alert_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_code ON common.work_alert USING btree (code);


--
-- TOC entry 4859 (class 1259 OID 35054)
-- Name: ix_applications_work_alert_definition_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_code ON common.work_alert_definition USING btree (code);


--
-- TOC entry 4860 (class 1259 OID 35055)
-- Name: ix_applications_work_alert_definition_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_hierarchy_scope_id ON common.work_alert_definition USING btree (hierarchy_scope_id);


--
-- TOC entry 4861 (class 1259 OID 35056)
-- Name: ix_applications_work_alert_definition_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_name ON common.work_alert_definition USING btree (name);


--
-- TOC entry 5039 (class 1259 OID 35905)
-- Name: ix_applications_work_alert_definition_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_property_code ON common.work_alert_definition_property USING btree (code);


--
-- TOC entry 5040 (class 1259 OID 35906)
-- Name: ix_applications_work_alert_definition_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_property_name ON common.work_alert_definition_property USING btree (name);


--
-- TOC entry 5041 (class 1259 OID 35907)
-- Name: ix_applications_work_alert_definition_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_property_parent_id ON common.work_alert_definition_property USING btree (parent_id);


--
-- TOC entry 5042 (class 1259 OID 41257)
-- Name: ix_applications_work_alert_definition_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_property_sensor_id ON common.work_alert_definition_property USING btree (sensor_id);


--
-- TOC entry 5043 (class 1259 OID 41258)
-- Name: ix_applications_work_alert_definition_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_property_sensor_type_id ON common.work_alert_definition_property USING btree (sensor_type_id);


--
-- TOC entry 5044 (class 1259 OID 41256)
-- Name: ix_applications_work_alert_definition_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_property_uom_category_id ON common.work_alert_definition_property USING btree (uom_category_id);


--
-- TOC entry 5045 (class 1259 OID 40274)
-- Name: ix_applications_work_alert_definition_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_property_value ON common.work_alert_definition_property USING btree (value);


--
-- TOC entry 5046 (class 1259 OID 35909)
-- Name: ix_applications_work_alert_definition_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_definition_property_value_uom_id ON common.work_alert_definition_property USING btree (value_uom_id);


--
-- TOC entry 5034 (class 1259 OID 35868)
-- Name: ix_applications_work_alert_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_hierarchy_scope_id ON common.work_alert USING btree (hierarchy_scope_id);


--
-- TOC entry 5035 (class 1259 OID 35869)
-- Name: ix_applications_work_alert_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_name ON common.work_alert USING btree (name);


--
-- TOC entry 5219 (class 1259 OID 36819)
-- Name: ix_applications_work_alert_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_property_code ON common.work_alert_property USING btree (code);


--
-- TOC entry 5220 (class 1259 OID 36820)
-- Name: ix_applications_work_alert_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_property_name ON common.work_alert_property USING btree (name);


--
-- TOC entry 5221 (class 1259 OID 36821)
-- Name: ix_applications_work_alert_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_property_parent_id ON common.work_alert_property USING btree (parent_id);


--
-- TOC entry 5222 (class 1259 OID 41239)
-- Name: ix_applications_work_alert_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_property_sensor_id ON common.work_alert_property USING btree (sensor_id);


--
-- TOC entry 5223 (class 1259 OID 41240)
-- Name: ix_applications_work_alert_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_property_sensor_type_id ON common.work_alert_property USING btree (sensor_type_id);


--
-- TOC entry 5224 (class 1259 OID 41238)
-- Name: ix_applications_work_alert_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_property_uom_category_id ON common.work_alert_property USING btree (uom_category_id);


--
-- TOC entry 5225 (class 1259 OID 40273)
-- Name: ix_applications_work_alert_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_property_value ON common.work_alert_property USING btree (value);


--
-- TOC entry 5226 (class 1259 OID 36823)
-- Name: ix_applications_work_alert_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_property_value_type_id ON common.work_alert_property USING btree (value_type_id);


--
-- TOC entry 5227 (class 1259 OID 36824)
-- Name: ix_applications_work_alert_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_alert_property_value_uom_id ON common.work_alert_property USING btree (value_uom_id);


--
-- TOC entry 4864 (class 1259 OID 35080)
-- Name: ix_applications_work_capability_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_code ON common.work_capability USING btree (code);


--
-- TOC entry 4865 (class 1259 OID 35081)
-- Name: ix_applications_work_capability_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_hierarchy_scope_id ON common.work_capability USING btree (hierarchy_scope_id);


--
-- TOC entry 4866 (class 1259 OID 35082)
-- Name: ix_applications_work_capability_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_name ON common.work_capability USING btree (name);


--
-- TOC entry 5050 (class 1259 OID 35945)
-- Name: ix_applications_work_capability_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_property_code ON common.work_capability_property USING btree (code);


--
-- TOC entry 5051 (class 1259 OID 35946)
-- Name: ix_applications_work_capability_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_property_name ON common.work_capability_property USING btree (name);


--
-- TOC entry 5052 (class 1259 OID 35947)
-- Name: ix_applications_work_capability_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_property_parent_id ON common.work_capability_property USING btree (parent_id);


--
-- TOC entry 5053 (class 1259 OID 41275)
-- Name: ix_applications_work_capability_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_property_sensor_id ON common.work_capability_property USING btree (sensor_id);


--
-- TOC entry 5054 (class 1259 OID 41276)
-- Name: ix_applications_work_capability_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_property_sensor_type_id ON common.work_capability_property USING btree (sensor_type_id);


--
-- TOC entry 5055 (class 1259 OID 41274)
-- Name: ix_applications_work_capability_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_property_uom_category_id ON common.work_capability_property USING btree (uom_category_id);


--
-- TOC entry 5056 (class 1259 OID 40275)
-- Name: ix_applications_work_capability_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_property_value ON common.work_capability_property USING btree (value);


--
-- TOC entry 5057 (class 1259 OID 35949)
-- Name: ix_applications_work_capability_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_property_value_type_id ON common.work_capability_property USING btree (value_type_id);


--
-- TOC entry 5058 (class 1259 OID 35950)
-- Name: ix_applications_work_capability_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_capability_property_value_uom_id ON common.work_capability_property USING btree (value_uom_id);


--
-- TOC entry 5375 (class 1259 OID 37593)
-- Name: ix_applications_work_master_capability_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_code ON common.work_master_capability USING btree (code);


--
-- TOC entry 5376 (class 1259 OID 37594)
-- Name: ix_applications_work_master_capability_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_hierarchy_scope_id ON common.work_master_capability USING btree (hierarchy_scope_id);


--
-- TOC entry 5377 (class 1259 OID 37595)
-- Name: ix_applications_work_master_capability_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_name ON common.work_master_capability USING btree (name);


--
-- TOC entry 5510 (class 1259 OID 38519)
-- Name: ix_applications_work_master_capability_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_property_code ON common.work_master_capability_property USING btree (code);


--
-- TOC entry 5511 (class 1259 OID 38520)
-- Name: ix_applications_work_master_capability_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_property_name ON common.work_master_capability_property USING btree (name);


--
-- TOC entry 5512 (class 1259 OID 38521)
-- Name: ix_applications_work_master_capability_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_property_parent_id ON common.work_master_capability_property USING btree (parent_id);


--
-- TOC entry 5513 (class 1259 OID 41293)
-- Name: ix_applications_work_master_capability_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_property_sensor_id ON common.work_master_capability_property USING btree (sensor_id);


--
-- TOC entry 5514 (class 1259 OID 41294)
-- Name: ix_applications_work_master_capability_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_property_sensor_type_id ON common.work_master_capability_property USING btree (sensor_type_id);


--
-- TOC entry 5515 (class 1259 OID 41292)
-- Name: ix_applications_work_master_capability_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_property_uom_category_id ON common.work_master_capability_property USING btree (uom_category_id);


--
-- TOC entry 5516 (class 1259 OID 40276)
-- Name: ix_applications_work_master_capability_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_property_value ON common.work_master_capability_property USING btree (value);


--
-- TOC entry 5517 (class 1259 OID 38523)
-- Name: ix_applications_work_master_capability_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_property_value_type_id ON common.work_master_capability_property USING btree (value_type_id);


--
-- TOC entry 5518 (class 1259 OID 38524)
-- Name: ix_applications_work_master_capability_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_master_capability_property_value_uom_id ON common.work_master_capability_property USING btree (value_uom_id);


--
-- TOC entry 5061 (class 1259 OID 35984)
-- Name: ix_applications_work_performance_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_code ON common.work_performance USING btree (code);


--
-- TOC entry 5062 (class 1259 OID 35985)
-- Name: ix_applications_work_performance_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_hierarchy_scope_id ON common.work_performance USING btree (hierarchy_scope_id);


--
-- TOC entry 5063 (class 1259 OID 35986)
-- Name: ix_applications_work_performance_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_name ON common.work_performance USING btree (name);


--
-- TOC entry 5064 (class 1259 OID 35987)
-- Name: ix_applications_work_performance_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_parent_id ON common.work_performance USING btree (parent_id);


--
-- TOC entry 5231 (class 1259 OID 36860)
-- Name: ix_applications_work_performance_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_property_code ON common.work_performance_property USING btree (code);


--
-- TOC entry 5232 (class 1259 OID 36861)
-- Name: ix_applications_work_performance_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_property_name ON common.work_performance_property USING btree (name);


--
-- TOC entry 5233 (class 1259 OID 36862)
-- Name: ix_applications_work_performance_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_property_parent_id ON common.work_performance_property USING btree (parent_id);


--
-- TOC entry 5234 (class 1259 OID 41311)
-- Name: ix_applications_work_performance_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_property_sensor_id ON common.work_performance_property USING btree (sensor_id);


--
-- TOC entry 5235 (class 1259 OID 41312)
-- Name: ix_applications_work_performance_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_property_sensor_type_id ON common.work_performance_property USING btree (sensor_type_id);


--
-- TOC entry 5236 (class 1259 OID 41310)
-- Name: ix_applications_work_performance_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_property_uom_category_id ON common.work_performance_property USING btree (uom_category_id);


--
-- TOC entry 5237 (class 1259 OID 40277)
-- Name: ix_applications_work_performance_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_property_value ON common.work_performance_property USING btree (value);


--
-- TOC entry 5238 (class 1259 OID 36864)
-- Name: ix_applications_work_performance_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_property_value_type_id ON common.work_performance_property USING btree (value_type_id);


--
-- TOC entry 5239 (class 1259 OID 36865)
-- Name: ix_applications_work_performance_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_performance_property_value_uom_id ON common.work_performance_property USING btree (value_uom_id);


--
-- TOC entry 5067 (class 1259 OID 36026)
-- Name: ix_applications_work_request_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_code ON common.work_request USING btree (code);


--
-- TOC entry 5068 (class 1259 OID 36027)
-- Name: ix_applications_work_request_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_hierarchy_scope_id ON common.work_request USING btree (hierarchy_scope_id);


--
-- TOC entry 5069 (class 1259 OID 36028)
-- Name: ix_applications_work_request_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_name ON common.work_request USING btree (name);


--
-- TOC entry 5070 (class 1259 OID 36029)
-- Name: ix_applications_work_request_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_parent_id ON common.work_request USING btree (parent_id);


--
-- TOC entry 5243 (class 1259 OID 36901)
-- Name: ix_applications_work_request_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_property_code ON common.work_request_property USING btree (code);


--
-- TOC entry 5244 (class 1259 OID 36902)
-- Name: ix_applications_work_request_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_property_name ON common.work_request_property USING btree (name);


--
-- TOC entry 5245 (class 1259 OID 36903)
-- Name: ix_applications_work_request_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_property_parent_id ON common.work_request_property USING btree (parent_id);


--
-- TOC entry 5246 (class 1259 OID 41329)
-- Name: ix_applications_work_request_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_property_sensor_id ON common.work_request_property USING btree (sensor_id);


--
-- TOC entry 5247 (class 1259 OID 41330)
-- Name: ix_applications_work_request_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_property_sensor_type_id ON common.work_request_property USING btree (sensor_type_id);


--
-- TOC entry 5248 (class 1259 OID 41328)
-- Name: ix_applications_work_request_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_property_uom_category_id ON common.work_request_property USING btree (uom_category_id);


--
-- TOC entry 5249 (class 1259 OID 40278)
-- Name: ix_applications_work_request_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_property_value ON common.work_request_property USING btree (value);


--
-- TOC entry 5250 (class 1259 OID 36905)
-- Name: ix_applications_work_request_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_property_value_type_id ON common.work_request_property USING btree (value_type_id);


--
-- TOC entry 5251 (class 1259 OID 36906)
-- Name: ix_applications_work_request_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_request_property_value_uom_id ON common.work_request_property USING btree (value_uom_id);


--
-- TOC entry 5254 (class 1259 OID 36950)
-- Name: ix_applications_work_response_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_code ON common.work_response USING btree (code);


--
-- TOC entry 5255 (class 1259 OID 36951)
-- Name: ix_applications_work_response_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_hierarchy_scope_id ON common.work_response USING btree (hierarchy_scope_id);


--
-- TOC entry 5256 (class 1259 OID 36952)
-- Name: ix_applications_work_response_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_name ON common.work_response USING btree (name);


--
-- TOC entry 5257 (class 1259 OID 36953)
-- Name: ix_applications_work_response_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_parent_id ON common.work_response USING btree (parent_id);


--
-- TOC entry 5381 (class 1259 OID 37631)
-- Name: ix_applications_work_response_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_property_code ON common.work_response_property USING btree (code);


--
-- TOC entry 5382 (class 1259 OID 37632)
-- Name: ix_applications_work_response_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_property_name ON common.work_response_property USING btree (name);


--
-- TOC entry 5383 (class 1259 OID 37633)
-- Name: ix_applications_work_response_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_property_parent_id ON common.work_response_property USING btree (parent_id);


--
-- TOC entry 5384 (class 1259 OID 41347)
-- Name: ix_applications_work_response_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_property_sensor_id ON common.work_response_property USING btree (sensor_id);


--
-- TOC entry 5385 (class 1259 OID 41348)
-- Name: ix_applications_work_response_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_property_sensor_type_id ON common.work_response_property USING btree (sensor_type_id);


--
-- TOC entry 5386 (class 1259 OID 41346)
-- Name: ix_applications_work_response_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_property_uom_category_id ON common.work_response_property USING btree (uom_category_id);


--
-- TOC entry 5387 (class 1259 OID 40279)
-- Name: ix_applications_work_response_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_property_value ON common.work_response_property USING btree (value);


--
-- TOC entry 5388 (class 1259 OID 37635)
-- Name: ix_applications_work_response_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_property_value_type_id ON common.work_response_property USING btree (value_type_id);


--
-- TOC entry 5389 (class 1259 OID 37636)
-- Name: ix_applications_work_response_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_response_property_value_uom_id ON common.work_response_property USING btree (value_uom_id);


--
-- TOC entry 4869 (class 1259 OID 35111)
-- Name: ix_applications_work_schedule_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_code ON common.work_schedule USING btree (code);


--
-- TOC entry 4870 (class 1259 OID 35112)
-- Name: ix_applications_work_schedule_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_hierarchy_scope_id ON common.work_schedule USING btree (hierarchy_scope_id);


--
-- TOC entry 4871 (class 1259 OID 35113)
-- Name: ix_applications_work_schedule_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_name ON common.work_schedule USING btree (name);


--
-- TOC entry 4872 (class 1259 OID 35114)
-- Name: ix_applications_work_schedule_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_parent_id ON common.work_schedule USING btree (parent_id);


--
-- TOC entry 5074 (class 1259 OID 36065)
-- Name: ix_applications_work_schedule_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_property_code ON common.work_schedule_property USING btree (code);


--
-- TOC entry 5075 (class 1259 OID 36066)
-- Name: ix_applications_work_schedule_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_property_name ON common.work_schedule_property USING btree (name);


--
-- TOC entry 5076 (class 1259 OID 36067)
-- Name: ix_applications_work_schedule_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_property_parent_id ON common.work_schedule_property USING btree (parent_id);


--
-- TOC entry 5077 (class 1259 OID 41365)
-- Name: ix_applications_work_schedule_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_property_sensor_id ON common.work_schedule_property USING btree (sensor_id);


--
-- TOC entry 5078 (class 1259 OID 41366)
-- Name: ix_applications_work_schedule_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_property_sensor_type_id ON common.work_schedule_property USING btree (sensor_type_id);


--
-- TOC entry 5079 (class 1259 OID 41364)
-- Name: ix_applications_work_schedule_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_property_uom_category_id ON common.work_schedule_property USING btree (uom_category_id);


--
-- TOC entry 5080 (class 1259 OID 40280)
-- Name: ix_applications_work_schedule_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_property_value ON common.work_schedule_property USING btree (value);


--
-- TOC entry 5081 (class 1259 OID 36069)
-- Name: ix_applications_work_schedule_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_property_value_type_id ON common.work_schedule_property USING btree (value_type_id);


--
-- TOC entry 5082 (class 1259 OID 36070)
-- Name: ix_applications_work_schedule_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_applications_work_schedule_property_value_uom_id ON common.work_schedule_property USING btree (value_uom_id);


--
-- TOC entry 5907 (class 1259 OID 41650)
-- Name: ix_common_document_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_document_code ON common.document USING btree (code);


--
-- TOC entry 5908 (class 1259 OID 41651)
-- Name: ix_common_document_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_document_name ON common.document USING btree (name);


--
-- TOC entry 5909 (class 1259 OID 41652)
-- Name: ix_common_document_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_document_parent_id ON common.document USING btree (parent_id);


--
-- TOC entry 4663 (class 1259 OID 34197)
-- Name: ix_common_equipment_class_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_code ON common.equipment_class USING btree (code);


--
-- TOC entry 4664 (class 1259 OID 34198)
-- Name: ix_common_equipment_class_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_name ON common.equipment_class USING btree (name);


--
-- TOC entry 4665 (class 1259 OID 34199)
-- Name: ix_common_equipment_class_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_parent_id ON common.equipment_class USING btree (parent_id);


--
-- TOC entry 4879 (class 1259 OID 35172)
-- Name: ix_common_equipment_class_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_code ON common.equipment_class_property USING btree (code);


--
-- TOC entry 4880 (class 1259 OID 35173)
-- Name: ix_common_equipment_class_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_name ON common.equipment_class_property USING btree (name);


--
-- TOC entry 4881 (class 1259 OID 35174)
-- Name: ix_common_equipment_class_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_parent_id ON common.equipment_class_property USING btree (parent_id);


--
-- TOC entry 4882 (class 1259 OID 40447)
-- Name: ix_common_equipment_class_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_sensor_id ON common.equipment_class_property USING btree (sensor_id);


--
-- TOC entry 4883 (class 1259 OID 40448)
-- Name: ix_common_equipment_class_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_sensor_type_id ON common.equipment_class_property USING btree (sensor_type_id);


--
-- TOC entry 4884 (class 1259 OID 40446)
-- Name: ix_common_equipment_class_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_uom_category_id ON common.equipment_class_property USING btree (uom_category_id);


--
-- TOC entry 4885 (class 1259 OID 40229)
-- Name: ix_common_equipment_class_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_value ON common.equipment_class_property USING btree (value);


--
-- TOC entry 4886 (class 1259 OID 40296)
-- Name: ix_common_equipment_class_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_value_number ON common.equipment_class_property USING btree (value_number);


--
-- TOC entry 4887 (class 1259 OID 35176)
-- Name: ix_common_equipment_class_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_value_type_id ON common.equipment_class_property USING btree (value_type_id);


--
-- TOC entry 4888 (class 1259 OID 35177)
-- Name: ix_common_equipment_class_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_class_property_value_uom_id ON common.equipment_class_property USING btree (value_uom_id);


--
-- TOC entry 4658 (class 1259 OID 34170)
-- Name: ix_common_equipment_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_code ON common.equipment USING btree (code);


--
-- TOC entry 4659 (class 1259 OID 34171)
-- Name: ix_common_equipment_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_name ON common.equipment USING btree (name);


--
-- TOC entry 4660 (class 1259 OID 34172)
-- Name: ix_common_equipment_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_parent_id ON common.equipment USING btree (parent_id);


--
-- TOC entry 4891 (class 1259 OID 35213)
-- Name: ix_common_equipment_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_code ON common.equipment_property USING btree (code);


--
-- TOC entry 4892 (class 1259 OID 35214)
-- Name: ix_common_equipment_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_name ON common.equipment_property USING btree (name);


--
-- TOC entry 4893 (class 1259 OID 35215)
-- Name: ix_common_equipment_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_parent_id ON common.equipment_property USING btree (parent_id);


--
-- TOC entry 4894 (class 1259 OID 40375)
-- Name: ix_common_equipment_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_sensor_id ON common.equipment_property USING btree (sensor_id);


--
-- TOC entry 4895 (class 1259 OID 40376)
-- Name: ix_common_equipment_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_sensor_type_id ON common.equipment_property USING btree (sensor_type_id);


--
-- TOC entry 4896 (class 1259 OID 40374)
-- Name: ix_common_equipment_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_uom_category_id ON common.equipment_property USING btree (uom_category_id);


--
-- TOC entry 4897 (class 1259 OID 40225)
-- Name: ix_common_equipment_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_value ON common.equipment_property USING btree (value);


--
-- TOC entry 4898 (class 1259 OID 40292)
-- Name: ix_common_equipment_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_value_number ON common.equipment_property USING btree (value_number);


--
-- TOC entry 4899 (class 1259 OID 35217)
-- Name: ix_common_equipment_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_value_type_id ON common.equipment_property USING btree (value_type_id);


--
-- TOC entry 4900 (class 1259 OID 35218)
-- Name: ix_common_equipment_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_property_value_uom_id ON common.equipment_property USING btree (value_uom_id);


--
-- TOC entry 5089 (class 1259 OID 36131)
-- Name: ix_common_equipment_segment_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_code ON common.equipment_segment_specification USING btree (code);


--
-- TOC entry 5090 (class 1259 OID 36132)
-- Name: ix_common_equipment_segment_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_name ON common.equipment_segment_specification USING btree (name);


--
-- TOC entry 5262 (class 1259 OID 36994)
-- Name: ix_common_equipment_segment_specification_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_code ON common.equipment_segment_specification_property USING btree (code);


--
-- TOC entry 5263 (class 1259 OID 36995)
-- Name: ix_common_equipment_segment_specification_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_name ON common.equipment_segment_specification_property USING btree (name);


--
-- TOC entry 5264 (class 1259 OID 36996)
-- Name: ix_common_equipment_segment_specification_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_parent_id ON common.equipment_segment_specification_property USING btree (parent_id);


--
-- TOC entry 5265 (class 1259 OID 36997)
-- Name: ix_common_equipment_segment_specification_property_quan_f381; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_quan_f381 ON common.equipment_segment_specification_property USING btree (quantity_uom_id);


--
-- TOC entry 5266 (class 1259 OID 36998)
-- Name: ix_common_equipment_segment_specification_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_quantity ON common.equipment_segment_specification_property USING btree (quantity);


--
-- TOC entry 5267 (class 1259 OID 40538)
-- Name: ix_common_equipment_segment_specification_property_sens_fe0c; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_sens_fe0c ON common.equipment_segment_specification_property USING btree (sensor_type_id);


--
-- TOC entry 5268 (class 1259 OID 40537)
-- Name: ix_common_equipment_segment_specification_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_sensor_id ON common.equipment_segment_specification_property USING btree (sensor_id);


--
-- TOC entry 5269 (class 1259 OID 40536)
-- Name: ix_common_equipment_segment_specification_property_uom__e792; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_uom__e792 ON common.equipment_segment_specification_property USING btree (uom_category_id);


--
-- TOC entry 5270 (class 1259 OID 36999)
-- Name: ix_common_equipment_segment_specification_property_valu_5df6; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_valu_5df6 ON common.equipment_segment_specification_property USING btree (value_type_id);


--
-- TOC entry 5271 (class 1259 OID 40234)
-- Name: ix_common_equipment_segment_specification_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_value ON common.equipment_segment_specification_property USING btree (value);


--
-- TOC entry 5272 (class 1259 OID 40301)
-- Name: ix_common_equipment_segment_specification_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_value_number ON common.equipment_segment_specification_property USING btree (value_number);


--
-- TOC entry 5273 (class 1259 OID 37001)
-- Name: ix_common_equipment_segment_specification_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_property_value_uom_id ON common.equipment_segment_specification_property USING btree (value_uom_id);


--
-- TOC entry 5091 (class 1259 OID 36133)
-- Name: ix_common_equipment_segment_specification_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_quantity ON common.equipment_segment_specification USING btree (quantity);


--
-- TOC entry 5092 (class 1259 OID 36134)
-- Name: ix_common_equipment_segment_specification_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_segment_specification_quantity_uom_id ON common.equipment_segment_specification USING btree (quantity_uom_id);


--
-- TOC entry 5902 (class 1259 OID 41620)
-- Name: ix_common_equipment_template_class_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_template_class_id ON common.equipment_template USING btree (equipment_class_id);


--
-- TOC entry 5903 (class 1259 OID 41619)
-- Name: ix_common_equipment_template_level_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_template_level_id ON common.equipment_template USING btree (equipment_level_id);


--
-- TOC entry 5904 (class 1259 OID 41621)
-- Name: ix_common_equipment_template_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_template_parent_id ON common.equipment_template USING btree (parent_id);


--
-- TOC entry 4586 (class 1259 OID 33843)
-- Name: ix_common_event_class_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_code ON common.event_class USING btree (code);


--
-- TOC entry 4587 (class 1259 OID 33844)
-- Name: ix_common_event_class_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_name ON common.event_class USING btree (name);


--
-- TOC entry 4588 (class 1259 OID 33845)
-- Name: ix_common_event_class_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_parent_id ON common.event_class USING btree (parent_id);


--
-- TOC entry 4668 (class 1259 OID 34236)
-- Name: ix_common_event_class_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_code ON common.event_class_property USING btree (code);


--
-- TOC entry 4669 (class 1259 OID 34237)
-- Name: ix_common_event_class_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_name ON common.event_class_property USING btree (name);


--
-- TOC entry 4670 (class 1259 OID 34238)
-- Name: ix_common_event_class_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_parent_id ON common.event_class_property USING btree (parent_id);


--
-- TOC entry 4671 (class 1259 OID 40771)
-- Name: ix_common_event_class_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_sensor_id ON common.event_class_property USING btree (sensor_id);


--
-- TOC entry 4672 (class 1259 OID 40772)
-- Name: ix_common_event_class_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_sensor_type_id ON common.event_class_property USING btree (sensor_type_id);


--
-- TOC entry 4673 (class 1259 OID 40770)
-- Name: ix_common_event_class_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_uom_category_id ON common.event_class_property USING btree (uom_category_id);


--
-- TOC entry 4674 (class 1259 OID 40247)
-- Name: ix_common_event_class_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_value ON common.event_class_property USING btree (value);


--
-- TOC entry 4675 (class 1259 OID 40314)
-- Name: ix_common_event_class_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_value_number ON common.event_class_property USING btree (value_number);


--
-- TOC entry 4676 (class 1259 OID 34240)
-- Name: ix_common_event_class_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_value_type_id ON common.event_class_property USING btree (value_type_id);


--
-- TOC entry 4677 (class 1259 OID 34241)
-- Name: ix_common_event_class_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_class_property_value_uom_id ON common.event_class_property USING btree (value_uom_id);


--
-- TOC entry 4638 (class 1259 OID 34049)
-- Name: ix_common_event_definition_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_code ON common.event_definition USING btree (code);


--
-- TOC entry 4639 (class 1259 OID 34050)
-- Name: ix_common_event_definition_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_name ON common.event_definition USING btree (name);


--
-- TOC entry 4680 (class 1259 OID 34278)
-- Name: ix_common_event_definition_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_code ON common.event_definition_property USING btree (code);


--
-- TOC entry 4681 (class 1259 OID 34279)
-- Name: ix_common_event_definition_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_name ON common.event_definition_property USING btree (name);


--
-- TOC entry 4682 (class 1259 OID 34280)
-- Name: ix_common_event_definition_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_parent_id ON common.event_definition_property USING btree (parent_id);


--
-- TOC entry 4683 (class 1259 OID 40789)
-- Name: ix_common_event_definition_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_sensor_id ON common.event_definition_property USING btree (sensor_id);


--
-- TOC entry 4684 (class 1259 OID 40790)
-- Name: ix_common_event_definition_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_sensor_type_id ON common.event_definition_property USING btree (sensor_type_id);


--
-- TOC entry 4685 (class 1259 OID 40788)
-- Name: ix_common_event_definition_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_uom_category_id ON common.event_definition_property USING btree (uom_category_id);


--
-- TOC entry 4686 (class 1259 OID 40248)
-- Name: ix_common_event_definition_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_value ON common.event_definition_property USING btree (value);


--
-- TOC entry 4687 (class 1259 OID 40315)
-- Name: ix_common_event_definition_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_value_number ON common.event_definition_property USING btree (value_number);


--
-- TOC entry 4688 (class 1259 OID 34282)
-- Name: ix_common_event_definition_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_value_type_id ON common.event_definition_property USING btree (value_type_id);


--
-- TOC entry 4689 (class 1259 OID 34283)
-- Name: ix_common_event_definition_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_definition_property_value_uom_id ON common.event_definition_property USING btree (value_uom_id);


--
-- TOC entry 4692 (class 1259 OID 34312)
-- Name: ix_common_event_object_relationship_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_code ON common.event_object_relationship USING btree (code);


--
-- TOC entry 4693 (class 1259 OID 34313)
-- Name: ix_common_event_object_relationship_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_name ON common.event_object_relationship USING btree (name);


--
-- TOC entry 4903 (class 1259 OID 35255)
-- Name: ix_common_event_object_relationship_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_code ON common.event_object_relationship_property USING btree (code);


--
-- TOC entry 4904 (class 1259 OID 35256)
-- Name: ix_common_event_object_relationship_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_name ON common.event_object_relationship_property USING btree (name);


--
-- TOC entry 4905 (class 1259 OID 35257)
-- Name: ix_common_event_object_relationship_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_parent_id ON common.event_object_relationship_property USING btree (parent_id);


--
-- TOC entry 4906 (class 1259 OID 40807)
-- Name: ix_common_event_object_relationship_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_sensor_id ON common.event_object_relationship_property USING btree (sensor_id);


--
-- TOC entry 4907 (class 1259 OID 40808)
-- Name: ix_common_event_object_relationship_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_sensor_type_id ON common.event_object_relationship_property USING btree (sensor_type_id);


--
-- TOC entry 4908 (class 1259 OID 40806)
-- Name: ix_common_event_object_relationship_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_uom_category_id ON common.event_object_relationship_property USING btree (uom_category_id);


--
-- TOC entry 4909 (class 1259 OID 40249)
-- Name: ix_common_event_object_relationship_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_value ON common.event_object_relationship_property USING btree (value);


--
-- TOC entry 4910 (class 1259 OID 40316)
-- Name: ix_common_event_object_relationship_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_value_number ON common.event_object_relationship_property USING btree (value_number);


--
-- TOC entry 4911 (class 1259 OID 35259)
-- Name: ix_common_event_object_relationship_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_value_type_id ON common.event_object_relationship_property USING btree (value_type_id);


--
-- TOC entry 4912 (class 1259 OID 35260)
-- Name: ix_common_event_object_relationship_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_event_object_relationship_property_value_uom_id ON common.event_object_relationship_property USING btree (value_uom_id);


--
-- TOC entry 5095 (class 1259 OID 36158)
-- Name: ix_common_from_resource_reference_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_code ON common.from_resource_reference USING btree (code);


--
-- TOC entry 5096 (class 1259 OID 36159)
-- Name: ix_common_from_resource_reference_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_name ON common.from_resource_reference USING btree (name);


--
-- TOC entry 5276 (class 1259 OID 37037)
-- Name: ix_common_from_resource_reference_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_code ON common.from_resource_reference_property USING btree (code);


--
-- TOC entry 5277 (class 1259 OID 37038)
-- Name: ix_common_from_resource_reference_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_name ON common.from_resource_reference_property USING btree (name);


--
-- TOC entry 5278 (class 1259 OID 37039)
-- Name: ix_common_from_resource_reference_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_parent_id ON common.from_resource_reference_property USING btree (parent_id);


--
-- TOC entry 5279 (class 1259 OID 40627)
-- Name: ix_common_from_resource_reference_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_sensor_id ON common.from_resource_reference_property USING btree (sensor_id);


--
-- TOC entry 5280 (class 1259 OID 40628)
-- Name: ix_common_from_resource_reference_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_sensor_type_id ON common.from_resource_reference_property USING btree (sensor_type_id);


--
-- TOC entry 5281 (class 1259 OID 40626)
-- Name: ix_common_from_resource_reference_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_uom_category_id ON common.from_resource_reference_property USING btree (uom_category_id);


--
-- TOC entry 5282 (class 1259 OID 40239)
-- Name: ix_common_from_resource_reference_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_value ON common.from_resource_reference_property USING btree (value);


--
-- TOC entry 5283 (class 1259 OID 40306)
-- Name: ix_common_from_resource_reference_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_value_number ON common.from_resource_reference_property USING btree (value_number);


--
-- TOC entry 5284 (class 1259 OID 37041)
-- Name: ix_common_from_resource_reference_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_value_type_id ON common.from_resource_reference_property USING btree (value_type_id);


--
-- TOC entry 5285 (class 1259 OID 37042)
-- Name: ix_common_from_resource_reference_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_from_resource_reference_property_value_uom_id ON common.from_resource_reference_property USING btree (value_uom_id);


--
-- TOC entry 4589 (class 1259 OID 33864)
-- Name: ix_common_knowledge_category_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_category_code ON common.knowledge_category USING btree (code);


--
-- TOC entry 4590 (class 1259 OID 33865)
-- Name: ix_common_knowledge_category_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_category_name ON common.knowledge_category USING btree (name);


--
-- TOC entry 4591 (class 1259 OID 33866)
-- Name: ix_common_knowledge_category_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_category_parent_id ON common.knowledge_category USING btree (parent_id);


--
-- TOC entry 4694 (class 1259 OID 34351)
-- Name: ix_common_knowledge_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_code ON common.knowledge_property USING btree (code);


--
-- TOC entry 4695 (class 1259 OID 34352)
-- Name: ix_common_knowledge_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_name ON common.knowledge_property USING btree (name);


--
-- TOC entry 4696 (class 1259 OID 34353)
-- Name: ix_common_knowledge_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_parent_id ON common.knowledge_property USING btree (parent_id);


--
-- TOC entry 4697 (class 1259 OID 40609)
-- Name: ix_common_knowledge_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_sensor_id ON common.knowledge_property USING btree (sensor_id);


--
-- TOC entry 4698 (class 1259 OID 40610)
-- Name: ix_common_knowledge_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_sensor_type_id ON common.knowledge_property USING btree (sensor_type_id);


--
-- TOC entry 4699 (class 1259 OID 40608)
-- Name: ix_common_knowledge_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_uom_category_id ON common.knowledge_property USING btree (uom_category_id);


--
-- TOC entry 4700 (class 1259 OID 40238)
-- Name: ix_common_knowledge_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_value ON common.knowledge_property USING btree (value);


--
-- TOC entry 4701 (class 1259 OID 40305)
-- Name: ix_common_knowledge_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_value_number ON common.knowledge_property USING btree (value_number);


--
-- TOC entry 4702 (class 1259 OID 34355)
-- Name: ix_common_knowledge_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_value_type_id ON common.knowledge_property USING btree (value_type_id);


--
-- TOC entry 4703 (class 1259 OID 34356)
-- Name: ix_common_knowledge_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_knowledge_property_value_uom_id ON common.knowledge_property USING btree (value_uom_id);


--
-- TOC entry 4594 (class 1259 OID 33880)
-- Name: ix_common_lookup_category_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_lookup_category_code ON common.lookup_category USING btree (code);


--
-- TOC entry 4595 (class 1259 OID 33881)
-- Name: ix_common_lookup_category_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_lookup_category_name ON common.lookup_category USING btree (name);


--
-- TOC entry 4640 (class 1259 OID 34076)
-- Name: ix_common_lookup_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_lookup_code ON common.lookup USING btree (code);


--
-- TOC entry 4641 (class 1259 OID 34077)
-- Name: ix_common_lookup_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_lookup_name ON common.lookup USING btree (name);


--
-- TOC entry 4642 (class 1259 OID 34078)
-- Name: ix_common_lookup_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_lookup_parent_id ON common.lookup USING btree (parent_id);


--
-- TOC entry 4643 (class 1259 OID 34079)
-- Name: ix_common_lookup_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_lookup_value ON common.lookup USING btree (value);


--
-- TOC entry 4706 (class 1259 OID 34386)
-- Name: ix_common_material_class_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_code ON common.material_class USING btree (code);


--
-- TOC entry 4707 (class 1259 OID 34387)
-- Name: ix_common_material_class_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_name ON common.material_class USING btree (name);


--
-- TOC entry 4708 (class 1259 OID 34388)
-- Name: ix_common_material_class_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_parent_id ON common.material_class USING btree (parent_id);


--
-- TOC entry 4915 (class 1259 OID 35318)
-- Name: ix_common_material_class_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_code ON common.material_class_property USING btree (code);


--
-- TOC entry 4916 (class 1259 OID 35319)
-- Name: ix_common_material_class_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_name ON common.material_class_property USING btree (name);


--
-- TOC entry 4917 (class 1259 OID 35320)
-- Name: ix_common_material_class_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_parent_id ON common.material_class_property USING btree (parent_id);


--
-- TOC entry 4918 (class 1259 OID 40501)
-- Name: ix_common_material_class_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_sensor_id ON common.material_class_property USING btree (sensor_id);


--
-- TOC entry 4919 (class 1259 OID 40502)
-- Name: ix_common_material_class_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_sensor_type_id ON common.material_class_property USING btree (sensor_type_id);


--
-- TOC entry 4920 (class 1259 OID 40500)
-- Name: ix_common_material_class_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_uom_category_id ON common.material_class_property USING btree (uom_category_id);


--
-- TOC entry 4921 (class 1259 OID 40232)
-- Name: ix_common_material_class_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_value ON common.material_class_property USING btree (value);


--
-- TOC entry 4922 (class 1259 OID 40299)
-- Name: ix_common_material_class_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_value_number ON common.material_class_property USING btree (value_number);


--
-- TOC entry 4923 (class 1259 OID 35322)
-- Name: ix_common_material_class_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_value_type_id ON common.material_class_property USING btree (value_type_id);


--
-- TOC entry 4924 (class 1259 OID 35323)
-- Name: ix_common_material_class_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_class_property_value_uom_id ON common.material_class_property USING btree (value_uom_id);


--
-- TOC entry 4711 (class 1259 OID 34417)
-- Name: ix_common_material_definition_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_code ON common.material_definition USING btree (code);


--
-- TOC entry 4712 (class 1259 OID 34418)
-- Name: ix_common_material_definition_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_name ON common.material_definition USING btree (name);


--
-- TOC entry 4713 (class 1259 OID 34419)
-- Name: ix_common_material_definition_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_parent_id ON common.material_definition USING btree (parent_id);


--
-- TOC entry 4927 (class 1259 OID 35359)
-- Name: ix_common_material_definition_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_code ON common.material_definition_property USING btree (code);


--
-- TOC entry 4928 (class 1259 OID 35360)
-- Name: ix_common_material_definition_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_name ON common.material_definition_property USING btree (name);


--
-- TOC entry 4929 (class 1259 OID 35361)
-- Name: ix_common_material_definition_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_parent_id ON common.material_definition_property USING btree (parent_id);


--
-- TOC entry 4930 (class 1259 OID 40429)
-- Name: ix_common_material_definition_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_sensor_id ON common.material_definition_property USING btree (sensor_id);


--
-- TOC entry 4931 (class 1259 OID 40430)
-- Name: ix_common_material_definition_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_sensor_type_id ON common.material_definition_property USING btree (sensor_type_id);


--
-- TOC entry 4932 (class 1259 OID 40428)
-- Name: ix_common_material_definition_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_uom_category_id ON common.material_definition_property USING btree (uom_category_id);


--
-- TOC entry 4933 (class 1259 OID 40228)
-- Name: ix_common_material_definition_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_value ON common.material_definition_property USING btree (value);


--
-- TOC entry 4934 (class 1259 OID 40295)
-- Name: ix_common_material_definition_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_value_number ON common.material_definition_property USING btree (value_number);


--
-- TOC entry 4935 (class 1259 OID 35363)
-- Name: ix_common_material_definition_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_value_type_id ON common.material_definition_property USING btree (value_type_id);


--
-- TOC entry 4936 (class 1259 OID 35364)
-- Name: ix_common_material_definition_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_definition_property_value_uom_id ON common.material_definition_property USING btree (value_uom_id);


--
-- TOC entry 5097 (class 1259 OID 36213)
-- Name: ix_common_material_segment_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_code ON common.material_segment_specification USING btree (code);


--
-- TOC entry 5098 (class 1259 OID 36214)
-- Name: ix_common_material_segment_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_name ON common.material_segment_specification USING btree (name);


--
-- TOC entry 5099 (class 1259 OID 36215)
-- Name: ix_common_material_segment_specification_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_parent_id ON common.material_segment_specification USING btree (parent_id);


--
-- TOC entry 5392 (class 1259 OID 37682)
-- Name: ix_common_material_segment_specification_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_code ON common.material_segment_specification_property USING btree (code);


--
-- TOC entry 5393 (class 1259 OID 37683)
-- Name: ix_common_material_segment_specification_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_name ON common.material_segment_specification_property USING btree (name);


--
-- TOC entry 5394 (class 1259 OID 37684)
-- Name: ix_common_material_segment_specification_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_parent_id ON common.material_segment_specification_property USING btree (parent_id);


--
-- TOC entry 5395 (class 1259 OID 37685)
-- Name: ix_common_material_segment_specification_property_quant_be7f; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_quant_be7f ON common.material_segment_specification_property USING btree (quantity_uom_id);


--
-- TOC entry 5396 (class 1259 OID 37686)
-- Name: ix_common_material_segment_specification_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_quantity ON common.material_segment_specification_property USING btree (quantity);


--
-- TOC entry 5397 (class 1259 OID 40592)
-- Name: ix_common_material_segment_specification_property_senso_3fac; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_senso_3fac ON common.material_segment_specification_property USING btree (sensor_type_id);


--
-- TOC entry 5398 (class 1259 OID 40591)
-- Name: ix_common_material_segment_specification_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_sensor_id ON common.material_segment_specification_property USING btree (sensor_id);


--
-- TOC entry 5399 (class 1259 OID 40590)
-- Name: ix_common_material_segment_specification_property_uom_c_2c65; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_uom_c_2c65 ON common.material_segment_specification_property USING btree (uom_category_id);


--
-- TOC entry 5400 (class 1259 OID 40237)
-- Name: ix_common_material_segment_specification_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_value ON common.material_segment_specification_property USING btree (value);


--
-- TOC entry 5401 (class 1259 OID 40304)
-- Name: ix_common_material_segment_specification_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_value_number ON common.material_segment_specification_property USING btree (value_number);


--
-- TOC entry 5402 (class 1259 OID 37688)
-- Name: ix_common_material_segment_specification_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_value_type_id ON common.material_segment_specification_property USING btree (value_type_id);


--
-- TOC entry 5403 (class 1259 OID 37689)
-- Name: ix_common_material_segment_specification_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_property_value_uom_id ON common.material_segment_specification_property USING btree (value_uom_id);


--
-- TOC entry 5100 (class 1259 OID 36216)
-- Name: ix_common_material_segment_specification_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_quantity ON common.material_segment_specification USING btree (quantity);


--
-- TOC entry 5101 (class 1259 OID 36217)
-- Name: ix_common_material_segment_specification_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_segment_specification_quantity_uom_id ON common.material_segment_specification USING btree (quantity_uom_id);


--
-- TOC entry 4598 (class 1259 OID 33895)
-- Name: ix_common_person_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_code ON common.person USING btree (code);


--
-- TOC entry 4599 (class 1259 OID 33896)
-- Name: ix_common_person_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_name ON common.person USING btree (name);


--
-- TOC entry 4716 (class 1259 OID 34455)
-- Name: ix_common_person_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_code ON common.person_property USING btree (code);


--
-- TOC entry 4717 (class 1259 OID 34456)
-- Name: ix_common_person_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_name ON common.person_property USING btree (name);


--
-- TOC entry 4718 (class 1259 OID 34457)
-- Name: ix_common_person_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_parent_id ON common.person_property USING btree (parent_id);


--
-- TOC entry 4719 (class 1259 OID 40393)
-- Name: ix_common_person_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_sensor_id ON common.person_property USING btree (sensor_id);


--
-- TOC entry 4720 (class 1259 OID 40394)
-- Name: ix_common_person_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_sensor_type_id ON common.person_property USING btree (sensor_type_id);


--
-- TOC entry 4721 (class 1259 OID 40392)
-- Name: ix_common_person_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_uom_category_id ON common.person_property USING btree (uom_category_id);


--
-- TOC entry 4722 (class 1259 OID 40226)
-- Name: ix_common_person_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_value ON common.person_property USING btree (value);


--
-- TOC entry 4723 (class 1259 OID 40293)
-- Name: ix_common_person_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_value_number ON common.person_property USING btree (value_number);


--
-- TOC entry 4724 (class 1259 OID 34459)
-- Name: ix_common_person_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_value_type_id ON common.person_property USING btree (value_type_id);


--
-- TOC entry 4725 (class 1259 OID 34460)
-- Name: ix_common_person_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_person_property_value_uom_id ON common.person_property USING btree (value_uom_id);


--
-- TOC entry 4602 (class 1259 OID 33916)
-- Name: ix_common_personnel_class_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_code ON common.personnel_class USING btree (code);


--
-- TOC entry 4603 (class 1259 OID 33917)
-- Name: ix_common_personnel_class_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_name ON common.personnel_class USING btree (name);


--
-- TOC entry 4604 (class 1259 OID 33918)
-- Name: ix_common_personnel_class_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_parent_id ON common.personnel_class USING btree (parent_id);


--
-- TOC entry 4728 (class 1259 OID 34496)
-- Name: ix_common_personnel_class_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_code ON common.personnel_class_property USING btree (code);


--
-- TOC entry 4729 (class 1259 OID 34497)
-- Name: ix_common_personnel_class_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_name ON common.personnel_class_property USING btree (name);


--
-- TOC entry 4730 (class 1259 OID 34498)
-- Name: ix_common_personnel_class_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_parent_id ON common.personnel_class_property USING btree (parent_id);


--
-- TOC entry 4731 (class 1259 OID 40465)
-- Name: ix_common_personnel_class_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_sensor_id ON common.personnel_class_property USING btree (sensor_id);


--
-- TOC entry 4732 (class 1259 OID 40466)
-- Name: ix_common_personnel_class_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_sensor_type_id ON common.personnel_class_property USING btree (sensor_type_id);


--
-- TOC entry 4733 (class 1259 OID 40464)
-- Name: ix_common_personnel_class_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_uom_category_id ON common.personnel_class_property USING btree (uom_category_id);


--
-- TOC entry 4734 (class 1259 OID 40230)
-- Name: ix_common_personnel_class_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_value ON common.personnel_class_property USING btree (value);


--
-- TOC entry 4735 (class 1259 OID 40297)
-- Name: ix_common_personnel_class_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_value_number ON common.personnel_class_property USING btree (value_number);


--
-- TOC entry 4736 (class 1259 OID 34500)
-- Name: ix_common_personnel_class_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_value_type_id ON common.personnel_class_property USING btree (value_type_id);


--
-- TOC entry 4737 (class 1259 OID 34501)
-- Name: ix_common_personnel_class_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_class_property_value_uom_id ON common.personnel_class_property USING btree (value_uom_id);


--
-- TOC entry 5104 (class 1259 OID 36256)
-- Name: ix_common_personnel_segment_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_code ON common.personnel_segment_specification USING btree (code);


--
-- TOC entry 5105 (class 1259 OID 36257)
-- Name: ix_common_personnel_segment_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_name ON common.personnel_segment_specification USING btree (name);


--
-- TOC entry 5286 (class 1259 OID 37083)
-- Name: ix_common_personnel_segment_specification_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_code ON common.personnel_segment_specification_property USING btree (code);


--
-- TOC entry 5287 (class 1259 OID 37084)
-- Name: ix_common_personnel_segment_specification_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_name ON common.personnel_segment_specification_property USING btree (name);


--
-- TOC entry 5288 (class 1259 OID 37085)
-- Name: ix_common_personnel_segment_specification_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_parent_id ON common.personnel_segment_specification_property USING btree (parent_id);


--
-- TOC entry 5289 (class 1259 OID 37086)
-- Name: ix_common_personnel_segment_specification_property_quan_a4fa; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_quan_a4fa ON common.personnel_segment_specification_property USING btree (quantity_uom_id);


--
-- TOC entry 5290 (class 1259 OID 37087)
-- Name: ix_common_personnel_segment_specification_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_quantity ON common.personnel_segment_specification_property USING btree (quantity);


--
-- TOC entry 5291 (class 1259 OID 40556)
-- Name: ix_common_personnel_segment_specification_property_sens_a1bf; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_sens_a1bf ON common.personnel_segment_specification_property USING btree (sensor_type_id);


--
-- TOC entry 5292 (class 1259 OID 40555)
-- Name: ix_common_personnel_segment_specification_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_sensor_id ON common.personnel_segment_specification_property USING btree (sensor_id);


--
-- TOC entry 5293 (class 1259 OID 40554)
-- Name: ix_common_personnel_segment_specification_property_uom__0512; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_uom__0512 ON common.personnel_segment_specification_property USING btree (uom_category_id);


--
-- TOC entry 5294 (class 1259 OID 37088)
-- Name: ix_common_personnel_segment_specification_property_valu_913f; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_valu_913f ON common.personnel_segment_specification_property USING btree (value_type_id);


--
-- TOC entry 5295 (class 1259 OID 40235)
-- Name: ix_common_personnel_segment_specification_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_value ON common.personnel_segment_specification_property USING btree (value);


--
-- TOC entry 5296 (class 1259 OID 40302)
-- Name: ix_common_personnel_segment_specification_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_value_number ON common.personnel_segment_specification_property USING btree (value_number);


--
-- TOC entry 5297 (class 1259 OID 37090)
-- Name: ix_common_personnel_segment_specification_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_property_value_uom_id ON common.personnel_segment_specification_property USING btree (value_uom_id);


--
-- TOC entry 5106 (class 1259 OID 36258)
-- Name: ix_common_personnel_segment_specification_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_quantity ON common.personnel_segment_specification USING btree (quantity);


--
-- TOC entry 5107 (class 1259 OID 36259)
-- Name: ix_common_personnel_segment_specification_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_segment_specification_quantity_uom_id ON common.personnel_segment_specification USING btree (quantity_uom_id);


--
-- TOC entry 4607 (class 1259 OID 33938)
-- Name: ix_common_physical_asset_class_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_code ON common.physical_asset_class USING btree (code);


--
-- TOC entry 4608 (class 1259 OID 33939)
-- Name: ix_common_physical_asset_class_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_name ON common.physical_asset_class USING btree (name);


--
-- TOC entry 4609 (class 1259 OID 33940)
-- Name: ix_common_physical_asset_class_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_parent_id ON common.physical_asset_class USING btree (parent_id);


--
-- TOC entry 4740 (class 1259 OID 34537)
-- Name: ix_common_physical_asset_class_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_code ON common.physical_asset_class_property USING btree (code);


--
-- TOC entry 4741 (class 1259 OID 34538)
-- Name: ix_common_physical_asset_class_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_name ON common.physical_asset_class_property USING btree (name);


--
-- TOC entry 4742 (class 1259 OID 34539)
-- Name: ix_common_physical_asset_class_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_parent_id ON common.physical_asset_class_property USING btree (parent_id);


--
-- TOC entry 4743 (class 1259 OID 40483)
-- Name: ix_common_physical_asset_class_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_sensor_id ON common.physical_asset_class_property USING btree (sensor_id);


--
-- TOC entry 4744 (class 1259 OID 40484)
-- Name: ix_common_physical_asset_class_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_sensor_type_id ON common.physical_asset_class_property USING btree (sensor_type_id);


--
-- TOC entry 4745 (class 1259 OID 40482)
-- Name: ix_common_physical_asset_class_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_uom_category_id ON common.physical_asset_class_property USING btree (uom_category_id);


--
-- TOC entry 4746 (class 1259 OID 40231)
-- Name: ix_common_physical_asset_class_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_value ON common.physical_asset_class_property USING btree (value);


--
-- TOC entry 4747 (class 1259 OID 40298)
-- Name: ix_common_physical_asset_class_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_value_number ON common.physical_asset_class_property USING btree (value_number);


--
-- TOC entry 4748 (class 1259 OID 34541)
-- Name: ix_common_physical_asset_class_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_value_type_id ON common.physical_asset_class_property USING btree (value_type_id);


--
-- TOC entry 4749 (class 1259 OID 34542)
-- Name: ix_common_physical_asset_class_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_class_property_value_uom_id ON common.physical_asset_class_property USING btree (value_uom_id);


--
-- TOC entry 4939 (class 1259 OID 35388)
-- Name: ix_common_physical_asset_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_code ON common.physical_asset USING btree (code);


--
-- TOC entry 4940 (class 1259 OID 35389)
-- Name: ix_common_physical_asset_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_name ON common.physical_asset USING btree (name);


--
-- TOC entry 4941 (class 1259 OID 35390)
-- Name: ix_common_physical_asset_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_parent_id ON common.physical_asset USING btree (parent_id);


--
-- TOC entry 5112 (class 1259 OID 36317)
-- Name: ix_common_physical_asset_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_code ON common.physical_asset_property USING btree (code);


--
-- TOC entry 5113 (class 1259 OID 36318)
-- Name: ix_common_physical_asset_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_name ON common.physical_asset_property USING btree (name);


--
-- TOC entry 5114 (class 1259 OID 36319)
-- Name: ix_common_physical_asset_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_parent_id ON common.physical_asset_property USING btree (parent_id);


--
-- TOC entry 5115 (class 1259 OID 40411)
-- Name: ix_common_physical_asset_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_sensor_id ON common.physical_asset_property USING btree (sensor_id);


--
-- TOC entry 5116 (class 1259 OID 40412)
-- Name: ix_common_physical_asset_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_sensor_type_id ON common.physical_asset_property USING btree (sensor_type_id);


--
-- TOC entry 5117 (class 1259 OID 40410)
-- Name: ix_common_physical_asset_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_uom_category_id ON common.physical_asset_property USING btree (uom_category_id);


--
-- TOC entry 5118 (class 1259 OID 40227)
-- Name: ix_common_physical_asset_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_value ON common.physical_asset_property USING btree (value);


--
-- TOC entry 5119 (class 1259 OID 40294)
-- Name: ix_common_physical_asset_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_value_number ON common.physical_asset_property USING btree (value_number);


--
-- TOC entry 5120 (class 1259 OID 36321)
-- Name: ix_common_physical_asset_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_value_type_id ON common.physical_asset_property USING btree (value_type_id);


--
-- TOC entry 5121 (class 1259 OID 36322)
-- Name: ix_common_physical_asset_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_property_value_uom_id ON common.physical_asset_property USING btree (value_uom_id);


--
-- TOC entry 5124 (class 1259 OID 36361)
-- Name: ix_common_physical_asset_segment_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_code ON common.physical_asset_segment_specification USING btree (code);


--
-- TOC entry 5125 (class 1259 OID 36362)
-- Name: ix_common_physical_asset_segment_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_name ON common.physical_asset_segment_specification USING btree (name);


--
-- TOC entry 5300 (class 1259 OID 40303)
-- Name: ix_common_physical_asset_segment_specification_property_03fa; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_03fa ON common.physical_asset_segment_specification_property USING btree (value_number);


--
-- TOC entry 5301 (class 1259 OID 40572)
-- Name: ix_common_physical_asset_segment_specification_property_04c6; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_04c6 ON common.physical_asset_segment_specification_property USING btree (uom_category_id);


--
-- TOC entry 5302 (class 1259 OID 37131)
-- Name: ix_common_physical_asset_segment_specification_property_5427; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_5427 ON common.physical_asset_segment_specification_property USING btree (value_uom_id);


--
-- TOC entry 5303 (class 1259 OID 37132)
-- Name: ix_common_physical_asset_segment_specification_property_54f1; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_54f1 ON common.physical_asset_segment_specification_property USING btree (parent_id);


--
-- TOC entry 5304 (class 1259 OID 37133)
-- Name: ix_common_physical_asset_segment_specification_property_9695; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_9695 ON common.physical_asset_segment_specification_property USING btree (quantity_uom_id);


--
-- TOC entry 5305 (class 1259 OID 37134)
-- Name: ix_common_physical_asset_segment_specification_property_af60; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_af60 ON common.physical_asset_segment_specification_property USING btree (quantity);


--
-- TOC entry 5306 (class 1259 OID 37135)
-- Name: ix_common_physical_asset_segment_specification_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_code ON common.physical_asset_segment_specification_property USING btree (code);


--
-- TOC entry 5307 (class 1259 OID 37136)
-- Name: ix_common_physical_asset_segment_specification_property_d10e; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_d10e ON common.physical_asset_segment_specification_property USING btree (value_type_id);


--
-- TOC entry 5308 (class 1259 OID 40574)
-- Name: ix_common_physical_asset_segment_specification_property_e242; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_e242 ON common.physical_asset_segment_specification_property USING btree (sensor_type_id);


--
-- TOC entry 5309 (class 1259 OID 40573)
-- Name: ix_common_physical_asset_segment_specification_property_fc16; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_fc16 ON common.physical_asset_segment_specification_property USING btree (sensor_id);


--
-- TOC entry 5310 (class 1259 OID 37137)
-- Name: ix_common_physical_asset_segment_specification_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_name ON common.physical_asset_segment_specification_property USING btree (name);


--
-- TOC entry 5311 (class 1259 OID 40236)
-- Name: ix_common_physical_asset_segment_specification_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_property_value ON common.physical_asset_segment_specification_property USING btree (value);


--
-- TOC entry 5126 (class 1259 OID 36363)
-- Name: ix_common_physical_asset_segment_specification_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_quantity ON common.physical_asset_segment_specification USING btree (quantity);


--
-- TOC entry 5127 (class 1259 OID 36364)
-- Name: ix_common_physical_asset_segment_specification_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_segment_specification_quantity_uom_id ON common.physical_asset_segment_specification USING btree (quantity_uom_id);


--
-- TOC entry 4944 (class 1259 OID 35424)
-- Name: ix_common_process_segment_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_code ON common.process_segment USING btree (code);


--
-- TOC entry 5130 (class 1259 OID 36403)
-- Name: ix_common_process_segment_dependency_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_dependency_code ON common.process_segment_dependency USING btree (code);


--
-- TOC entry 5131 (class 1259 OID 36404)
-- Name: ix_common_process_segment_dependency_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_dependency_name ON common.process_segment_dependency USING btree (name);


--
-- TOC entry 5132 (class 1259 OID 36405)
-- Name: ix_common_process_segment_dependency_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_dependency_value ON common.process_segment_dependency USING btree (value);


--
-- TOC entry 5133 (class 1259 OID 36406)
-- Name: ix_common_process_segment_dependency_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_dependency_value_uom_id ON common.process_segment_dependency USING btree (value_uom_id);


--
-- TOC entry 4945 (class 1259 OID 35425)
-- Name: ix_common_process_segment_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_hierarchy_scope_id ON common.process_segment USING btree (hierarchy_scope_id);


--
-- TOC entry 4946 (class 1259 OID 35426)
-- Name: ix_common_process_segment_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_name ON common.process_segment USING btree (name);


--
-- TOC entry 5136 (class 1259 OID 36442)
-- Name: ix_common_process_segment_parameter_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_code ON common.process_segment_parameter USING btree (code);


--
-- TOC entry 5137 (class 1259 OID 36443)
-- Name: ix_common_process_segment_parameter_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_name ON common.process_segment_parameter USING btree (name);


--
-- TOC entry 5138 (class 1259 OID 36444)
-- Name: ix_common_process_segment_parameter_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_parent_id ON common.process_segment_parameter USING btree (parent_id);


--
-- TOC entry 5139 (class 1259 OID 40519)
-- Name: ix_common_process_segment_parameter_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_sensor_id ON common.process_segment_parameter USING btree (sensor_id);


--
-- TOC entry 5140 (class 1259 OID 40520)
-- Name: ix_common_process_segment_parameter_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_sensor_type_id ON common.process_segment_parameter USING btree (sensor_type_id);


--
-- TOC entry 5141 (class 1259 OID 40518)
-- Name: ix_common_process_segment_parameter_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_uom_category_id ON common.process_segment_parameter USING btree (uom_category_id);


--
-- TOC entry 5142 (class 1259 OID 40233)
-- Name: ix_common_process_segment_parameter_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_value ON common.process_segment_parameter USING btree (value);


--
-- TOC entry 5143 (class 1259 OID 40300)
-- Name: ix_common_process_segment_parameter_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_value_number ON common.process_segment_parameter USING btree (value_number);


--
-- TOC entry 5144 (class 1259 OID 36446)
-- Name: ix_common_process_segment_parameter_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_value_type_id ON common.process_segment_parameter USING btree (value_type_id);


--
-- TOC entry 5145 (class 1259 OID 36447)
-- Name: ix_common_process_segment_parameter_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parameter_value_uom_id ON common.process_segment_parameter USING btree (value_uom_id);


--
-- TOC entry 4947 (class 1259 OID 35427)
-- Name: ix_common_process_segment_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_process_segment_parent_id ON common.process_segment USING btree (parent_id);


--
-- TOC entry 4950 (class 1259 OID 35451)
-- Name: ix_common_resource_network_connection_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_code ON common.resource_network_connection USING btree (code);


--
-- TOC entry 4951 (class 1259 OID 35452)
-- Name: ix_common_resource_network_connection_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_name ON common.resource_network_connection USING btree (name);


--
-- TOC entry 5148 (class 1259 OID 36488)
-- Name: ix_common_resource_network_connection_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_code ON common.resource_network_connection_property USING btree (code);


--
-- TOC entry 5149 (class 1259 OID 36489)
-- Name: ix_common_resource_network_connection_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_name ON common.resource_network_connection_property USING btree (name);


--
-- TOC entry 5150 (class 1259 OID 36490)
-- Name: ix_common_resource_network_connection_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_parent_id ON common.resource_network_connection_property USING btree (parent_id);


--
-- TOC entry 5151 (class 1259 OID 40663)
-- Name: ix_common_resource_network_connection_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_sensor_id ON common.resource_network_connection_property USING btree (sensor_id);


--
-- TOC entry 5152 (class 1259 OID 40664)
-- Name: ix_common_resource_network_connection_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_sensor_type_id ON common.resource_network_connection_property USING btree (sensor_type_id);


--
-- TOC entry 5153 (class 1259 OID 40662)
-- Name: ix_common_resource_network_connection_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_uom_category_id ON common.resource_network_connection_property USING btree (uom_category_id);


--
-- TOC entry 5154 (class 1259 OID 40241)
-- Name: ix_common_resource_network_connection_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_value ON common.resource_network_connection_property USING btree (value);


--
-- TOC entry 5155 (class 1259 OID 40308)
-- Name: ix_common_resource_network_connection_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_value_number ON common.resource_network_connection_property USING btree (value_number);


--
-- TOC entry 5156 (class 1259 OID 36492)
-- Name: ix_common_resource_network_connection_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_value_type_id ON common.resource_network_connection_property USING btree (value_type_id);


--
-- TOC entry 5157 (class 1259 OID 36493)
-- Name: ix_common_resource_network_connection_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_property_value_uom_id ON common.resource_network_connection_property USING btree (value_uom_id);


--
-- TOC entry 4612 (class 1259 OID 33954)
-- Name: ix_common_resource_network_connection_type_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_code ON common.resource_network_connection_type USING btree (code);


--
-- TOC entry 4613 (class 1259 OID 33955)
-- Name: ix_common_resource_network_connection_type_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_name ON common.resource_network_connection_type USING btree (name);


--
-- TOC entry 4752 (class 1259 OID 34578)
-- Name: ix_common_resource_network_connection_type_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_code ON common.resource_network_connection_type_property USING btree (code);


--
-- TOC entry 4753 (class 1259 OID 34579)
-- Name: ix_common_resource_network_connection_type_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_name ON common.resource_network_connection_type_property USING btree (name);


--
-- TOC entry 4754 (class 1259 OID 34580)
-- Name: ix_common_resource_network_connection_type_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_parent_id ON common.resource_network_connection_type_property USING btree (parent_id);


--
-- TOC entry 4755 (class 1259 OID 40682)
-- Name: ix_common_resource_network_connection_type_property_sen_bcec; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_sen_bcec ON common.resource_network_connection_type_property USING btree (sensor_type_id);


--
-- TOC entry 4756 (class 1259 OID 40681)
-- Name: ix_common_resource_network_connection_type_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_sensor_id ON common.resource_network_connection_type_property USING btree (sensor_id);


--
-- TOC entry 4757 (class 1259 OID 40680)
-- Name: ix_common_resource_network_connection_type_property_uom_e43d; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_uom_e43d ON common.resource_network_connection_type_property USING btree (uom_category_id);


--
-- TOC entry 4758 (class 1259 OID 40309)
-- Name: ix_common_resource_network_connection_type_property_val_6b71; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_val_6b71 ON common.resource_network_connection_type_property USING btree (value_number);


--
-- TOC entry 4759 (class 1259 OID 34581)
-- Name: ix_common_resource_network_connection_type_property_val_750b; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_val_750b ON common.resource_network_connection_type_property USING btree (value_type_id);


--
-- TOC entry 4760 (class 1259 OID 34582)
-- Name: ix_common_resource_network_connection_type_property_val_b898; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_val_b898 ON common.resource_network_connection_type_property USING btree (value_uom_id);


--
-- TOC entry 4761 (class 1259 OID 40242)
-- Name: ix_common_resource_network_connection_type_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_network_connection_type_property_value ON common.resource_network_connection_type_property USING btree (value);


--
-- TOC entry 4764 (class 1259 OID 34607)
-- Name: ix_common_resource_relationship_network_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_relationship_network_code ON common.resource_relationship_network USING btree (code);


--
-- TOC entry 4765 (class 1259 OID 34608)
-- Name: ix_common_resource_relationship_network_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_resource_relationship_network_name ON common.resource_relationship_network USING btree (name);


--
-- TOC entry 5160 (class 1259 OID 36517)
-- Name: ix_common_to_resource_reference_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_code ON common.to_resource_reference USING btree (code);


--
-- TOC entry 5161 (class 1259 OID 36518)
-- Name: ix_common_to_resource_reference_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_name ON common.to_resource_reference USING btree (name);


--
-- TOC entry 5314 (class 1259 OID 37174)
-- Name: ix_common_to_resource_reference_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_code ON common.to_resource_reference_property USING btree (code);


--
-- TOC entry 5315 (class 1259 OID 37175)
-- Name: ix_common_to_resource_reference_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_name ON common.to_resource_reference_property USING btree (name);


--
-- TOC entry 5316 (class 1259 OID 37176)
-- Name: ix_common_to_resource_reference_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_parent_id ON common.to_resource_reference_property USING btree (parent_id);


--
-- TOC entry 5317 (class 1259 OID 40645)
-- Name: ix_common_to_resource_reference_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_sensor_id ON common.to_resource_reference_property USING btree (sensor_id);


--
-- TOC entry 5318 (class 1259 OID 40646)
-- Name: ix_common_to_resource_reference_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_sensor_type_id ON common.to_resource_reference_property USING btree (sensor_type_id);


--
-- TOC entry 5319 (class 1259 OID 40644)
-- Name: ix_common_to_resource_reference_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_uom_category_id ON common.to_resource_reference_property USING btree (uom_category_id);


--
-- TOC entry 5320 (class 1259 OID 40240)
-- Name: ix_common_to_resource_reference_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_value ON common.to_resource_reference_property USING btree (value);


--
-- TOC entry 5321 (class 1259 OID 40307)
-- Name: ix_common_to_resource_reference_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_value_number ON common.to_resource_reference_property USING btree (value_number);


--
-- TOC entry 5322 (class 1259 OID 37178)
-- Name: ix_common_to_resource_reference_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_value_type_id ON common.to_resource_reference_property USING btree (value_type_id);


--
-- TOC entry 5323 (class 1259 OID 37179)
-- Name: ix_common_to_resource_reference_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_to_resource_reference_property_value_uom_id ON common.to_resource_reference_property USING btree (value_uom_id);


--
-- TOC entry 4616 (class 1259 OID 33969)
-- Name: ix_common_uom_category_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_uom_category_code ON common.uom_category USING btree (code);


--
-- TOC entry 4617 (class 1259 OID 33970)
-- Name: ix_common_uom_category_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_uom_category_name ON common.uom_category USING btree (name);


--
-- TOC entry 4648 (class 1259 OID 34120)
-- Name: ix_common_uom_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_uom_code ON common.uom USING btree (code);


--
-- TOC entry 4649 (class 1259 OID 34121)
-- Name: ix_common_uom_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_uom_name ON common.uom USING btree (name);


--
-- TOC entry 4620 (class 1259 OID 33984)
-- Name: ix_common_work_calendar_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_code ON common.work_calendar USING btree (code);


--
-- TOC entry 4624 (class 1259 OID 33999)
-- Name: ix_common_work_calendar_definition_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_code ON common.work_calendar_definition USING btree (code);


--
-- TOC entry 4768 (class 1259 OID 34637)
-- Name: ix_common_work_calendar_definition_entry_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_code ON common.work_calendar_definition_entry USING btree (code);


--
-- TOC entry 4769 (class 1259 OID 34638)
-- Name: ix_common_work_calendar_definition_entry_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_name ON common.work_calendar_definition_entry USING btree (name);


--
-- TOC entry 4770 (class 1259 OID 34639)
-- Name: ix_common_work_calendar_definition_entry_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_parent_id ON common.work_calendar_definition_entry USING btree (parent_id);


--
-- TOC entry 4954 (class 1259 OID 35488)
-- Name: ix_common_work_calendar_definition_entry_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_code ON common.work_calendar_definition_entry_property USING btree (code);


--
-- TOC entry 4955 (class 1259 OID 35489)
-- Name: ix_common_work_calendar_definition_entry_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_name ON common.work_calendar_definition_entry_property USING btree (name);


--
-- TOC entry 4956 (class 1259 OID 35490)
-- Name: ix_common_work_calendar_definition_entry_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_parent_id ON common.work_calendar_definition_entry_property USING btree (parent_id);


--
-- TOC entry 4957 (class 1259 OID 40718)
-- Name: ix_common_work_calendar_definition_entry_property_senso_d8fe; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_senso_d8fe ON common.work_calendar_definition_entry_property USING btree (sensor_type_id);


--
-- TOC entry 4958 (class 1259 OID 40717)
-- Name: ix_common_work_calendar_definition_entry_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_sensor_id ON common.work_calendar_definition_entry_property USING btree (sensor_id);


--
-- TOC entry 4959 (class 1259 OID 40716)
-- Name: ix_common_work_calendar_definition_entry_property_uom_c_8933; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_uom_c_8933 ON common.work_calendar_definition_entry_property USING btree (uom_category_id);


--
-- TOC entry 4960 (class 1259 OID 40244)
-- Name: ix_common_work_calendar_definition_entry_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_value ON common.work_calendar_definition_entry_property USING btree (value);


--
-- TOC entry 4961 (class 1259 OID 40311)
-- Name: ix_common_work_calendar_definition_entry_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_value_number ON common.work_calendar_definition_entry_property USING btree (value_number);


--
-- TOC entry 4962 (class 1259 OID 35492)
-- Name: ix_common_work_calendar_definition_entry_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_value_type_id ON common.work_calendar_definition_entry_property USING btree (value_type_id);


--
-- TOC entry 4963 (class 1259 OID 35493)
-- Name: ix_common_work_calendar_definition_entry_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_entry_property_value_uom_id ON common.work_calendar_definition_entry_property USING btree (value_uom_id);


--
-- TOC entry 4625 (class 1259 OID 34000)
-- Name: ix_common_work_calendar_definition_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_name ON common.work_calendar_definition USING btree (name);


--
-- TOC entry 4773 (class 1259 OID 34675)
-- Name: ix_common_work_calendar_definition_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_code ON common.work_calendar_definition_property USING btree (code);


--
-- TOC entry 4774 (class 1259 OID 34676)
-- Name: ix_common_work_calendar_definition_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_name ON common.work_calendar_definition_property USING btree (name);


--
-- TOC entry 4775 (class 1259 OID 34677)
-- Name: ix_common_work_calendar_definition_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_parent_id ON common.work_calendar_definition_property USING btree (parent_id);


--
-- TOC entry 4776 (class 1259 OID 40735)
-- Name: ix_common_work_calendar_definition_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_sensor_id ON common.work_calendar_definition_property USING btree (sensor_id);


--
-- TOC entry 4777 (class 1259 OID 40736)
-- Name: ix_common_work_calendar_definition_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_sensor_type_id ON common.work_calendar_definition_property USING btree (sensor_type_id);


--
-- TOC entry 4778 (class 1259 OID 40734)
-- Name: ix_common_work_calendar_definition_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_uom_category_id ON common.work_calendar_definition_property USING btree (uom_category_id);


--
-- TOC entry 4779 (class 1259 OID 40245)
-- Name: ix_common_work_calendar_definition_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_value ON common.work_calendar_definition_property USING btree (value);


--
-- TOC entry 4780 (class 1259 OID 40312)
-- Name: ix_common_work_calendar_definition_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_value_number ON common.work_calendar_definition_property USING btree (value_number);


--
-- TOC entry 4781 (class 1259 OID 34679)
-- Name: ix_common_work_calendar_definition_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_value_type_id ON common.work_calendar_definition_property USING btree (value_type_id);


--
-- TOC entry 4782 (class 1259 OID 34680)
-- Name: ix_common_work_calendar_definition_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_definition_property_value_uom_id ON common.work_calendar_definition_property USING btree (value_uom_id);


--
-- TOC entry 4966 (class 1259 OID 35527)
-- Name: ix_common_work_calendar_entry_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_code ON common.work_calendar_entry USING btree (code);


--
-- TOC entry 4967 (class 1259 OID 35528)
-- Name: ix_common_work_calendar_entry_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_name ON common.work_calendar_entry USING btree (name);


--
-- TOC entry 4968 (class 1259 OID 35529)
-- Name: ix_common_work_calendar_entry_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_parent_id ON common.work_calendar_entry USING btree (parent_id);


--
-- TOC entry 5164 (class 1259 OID 36559)
-- Name: ix_common_work_calendar_entry_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_code ON common.work_calendar_entry_property USING btree (code);


--
-- TOC entry 5165 (class 1259 OID 36560)
-- Name: ix_common_work_calendar_entry_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_name ON common.work_calendar_entry_property USING btree (name);


--
-- TOC entry 5166 (class 1259 OID 36561)
-- Name: ix_common_work_calendar_entry_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_parent_id ON common.work_calendar_entry_property USING btree (parent_id);


--
-- TOC entry 5167 (class 1259 OID 40753)
-- Name: ix_common_work_calendar_entry_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_sensor_id ON common.work_calendar_entry_property USING btree (sensor_id);


--
-- TOC entry 5168 (class 1259 OID 40754)
-- Name: ix_common_work_calendar_entry_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_sensor_type_id ON common.work_calendar_entry_property USING btree (sensor_type_id);


--
-- TOC entry 5169 (class 1259 OID 40752)
-- Name: ix_common_work_calendar_entry_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_uom_category_id ON common.work_calendar_entry_property USING btree (uom_category_id);


--
-- TOC entry 5170 (class 1259 OID 40246)
-- Name: ix_common_work_calendar_entry_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_value ON common.work_calendar_entry_property USING btree (value);


--
-- TOC entry 5171 (class 1259 OID 40313)
-- Name: ix_common_work_calendar_entry_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_value_number ON common.work_calendar_entry_property USING btree (value_number);


--
-- TOC entry 5172 (class 1259 OID 36563)
-- Name: ix_common_work_calendar_entry_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_value_type_id ON common.work_calendar_entry_property USING btree (value_type_id);


--
-- TOC entry 5173 (class 1259 OID 36564)
-- Name: ix_common_work_calendar_entry_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_entry_property_value_uom_id ON common.work_calendar_entry_property USING btree (value_uom_id);


--
-- TOC entry 4621 (class 1259 OID 33985)
-- Name: ix_common_work_calendar_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_name ON common.work_calendar USING btree (name);


--
-- TOC entry 4785 (class 1259 OID 34716)
-- Name: ix_common_work_calendar_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_code ON common.work_calendar_property USING btree (code);


--
-- TOC entry 4786 (class 1259 OID 34717)
-- Name: ix_common_work_calendar_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_name ON common.work_calendar_property USING btree (name);


--
-- TOC entry 4787 (class 1259 OID 34718)
-- Name: ix_common_work_calendar_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_parent_id ON common.work_calendar_property USING btree (parent_id);


--
-- TOC entry 4788 (class 1259 OID 40699)
-- Name: ix_common_work_calendar_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_sensor_id ON common.work_calendar_property USING btree (sensor_id);


--
-- TOC entry 4789 (class 1259 OID 40700)
-- Name: ix_common_work_calendar_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_sensor_type_id ON common.work_calendar_property USING btree (sensor_type_id);


--
-- TOC entry 4790 (class 1259 OID 40698)
-- Name: ix_common_work_calendar_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_uom_category_id ON common.work_calendar_property USING btree (uom_category_id);


--
-- TOC entry 4791 (class 1259 OID 40243)
-- Name: ix_common_work_calendar_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_value ON common.work_calendar_property USING btree (value);


--
-- TOC entry 4792 (class 1259 OID 40310)
-- Name: ix_common_work_calendar_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_value_number ON common.work_calendar_property USING btree (value_number);


--
-- TOC entry 4793 (class 1259 OID 34720)
-- Name: ix_common_work_calendar_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_value_type_id ON common.work_calendar_property USING btree (value_type_id);


--
-- TOC entry 4794 (class 1259 OID 34721)
-- Name: ix_common_work_calendar_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_calendar_property_value_uom_id ON common.work_calendar_property USING btree (value_uom_id);


--
-- TOC entry 5890 (class 1259 OID 40175)
-- Name: ix_guardian_privilege_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_guardian_privilege_code ON common.au_privilege USING btree (code);


--
-- TOC entry 5891 (class 1259 OID 40176)
-- Name: ix_guardian_privilege_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_guardian_privilege_name ON common.au_privilege USING btree (name);


--
-- TOC entry 5408 (class 1259 OID 37733)
-- Name: ix_common_equipment_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_code ON common.equipment_specification USING btree (code);


--
-- TOC entry 5409 (class 1259 OID 37734)
-- Name: ix_common_equipment_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_name ON common.equipment_specification USING btree (name);


--
-- TOC entry 5523 (class 1259 OID 38565)
-- Name: ix_common_equipment_specification_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_code ON common.equipment_specification_property USING btree (code);


--
-- TOC entry 5524 (class 1259 OID 38566)
-- Name: ix_common_equipment_specification_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_name ON common.equipment_specification_property USING btree (name);


--
-- TOC entry 5525 (class 1259 OID 38567)
-- Name: ix_common_equipment_specification_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_parent_id ON common.equipment_specification_property USING btree (parent_id);


--
-- TOC entry 5526 (class 1259 OID 38568)
-- Name: ix_common_equipment_specification_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_quantity ON common.equipment_specification_property USING btree (quantity);


--
-- TOC entry 5527 (class 1259 OID 38569)
-- Name: ix_common_equipment_specification_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_quantity_uom_id ON common.equipment_specification_property USING btree (quantity_uom_id);


--
-- TOC entry 5528 (class 1259 OID 41383)
-- Name: ix_common_equipment_specification_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_sensor_id ON common.equipment_specification_property USING btree (sensor_id);


--
-- TOC entry 5529 (class 1259 OID 41384)
-- Name: ix_common_equipment_specification_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_sensor_type_id ON common.equipment_specification_property USING btree (sensor_type_id);


--
-- TOC entry 5530 (class 1259 OID 41382)
-- Name: ix_common_equipment_specification_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_uom_category_id ON common.equipment_specification_property USING btree (uom_category_id);


--
-- TOC entry 5531 (class 1259 OID 40281)
-- Name: ix_common_equipment_specification_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_value ON common.equipment_specification_property USING btree (value);


--
-- TOC entry 5532 (class 1259 OID 40348)
-- Name: ix_common_equipment_specification_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_value_number ON common.equipment_specification_property USING btree (value_number);


--
-- TOC entry 5533 (class 1259 OID 38571)
-- Name: ix_common_equipment_specification_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_value_type_id ON common.equipment_specification_property USING btree (value_type_id);


--
-- TOC entry 5534 (class 1259 OID 38572)
-- Name: ix_common_equipment_specification_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_property_value_uom_id ON common.equipment_specification_property USING btree (value_uom_id);


--
-- TOC entry 5410 (class 1259 OID 37735)
-- Name: ix_common_equipment_specification_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_quantity ON common.equipment_specification USING btree (quantity);


--
-- TOC entry 5411 (class 1259 OID 37736)
-- Name: ix_common_equipment_specification_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_equipment_specification_quantity_uom_id ON common.equipment_specification USING btree (quantity_uom_id);


--
-- TOC entry 5412 (class 1259 OID 37800)
-- Name: ix_common_material_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_code ON common.material_specification USING btree (code);


--
-- TOC entry 5413 (class 1259 OID 37801)
-- Name: ix_common_material_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_name ON common.material_specification USING btree (name);


--
-- TOC entry 5414 (class 1259 OID 37802)
-- Name: ix_common_material_specification_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_parent_id ON common.material_specification USING btree (parent_id);


--
-- TOC entry 5535 (class 1259 OID 38613)
-- Name: ix_common_material_specification_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_code ON common.material_specification_property USING btree (code);


--
-- TOC entry 5536 (class 1259 OID 38614)
-- Name: ix_common_material_specification_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_name ON common.material_specification_property USING btree (name);


--
-- TOC entry 5537 (class 1259 OID 38615)
-- Name: ix_common_material_specification_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_parent_id ON common.material_specification_property USING btree (parent_id);


--
-- TOC entry 5538 (class 1259 OID 38616)
-- Name: ix_common_material_specification_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_quantity ON common.material_specification_property USING btree (quantity);


--
-- TOC entry 5539 (class 1259 OID 38617)
-- Name: ix_common_material_specification_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_quantity_uom_id ON common.material_specification_property USING btree (quantity_uom_id);


--
-- TOC entry 5540 (class 1259 OID 41437)
-- Name: ix_common_material_specification_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_sensor_id ON common.material_specification_property USING btree (sensor_id);


--
-- TOC entry 5541 (class 1259 OID 41438)
-- Name: ix_common_material_specification_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_sensor_type_id ON common.material_specification_property USING btree (sensor_type_id);


--
-- TOC entry 5542 (class 1259 OID 41436)
-- Name: ix_common_material_specification_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_uom_category_id ON common.material_specification_property USING btree (uom_category_id);


--
-- TOC entry 5543 (class 1259 OID 40284)
-- Name: ix_common_material_specification_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_value ON common.material_specification_property USING btree (value);


--
-- TOC entry 5544 (class 1259 OID 40351)
-- Name: ix_common_material_specification_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_value_number ON common.material_specification_property USING btree (value_number);


--
-- TOC entry 5545 (class 1259 OID 38619)
-- Name: ix_common_material_specification_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_value_type_id ON common.material_specification_property USING btree (value_type_id);


--
-- TOC entry 5546 (class 1259 OID 38620)
-- Name: ix_common_material_specification_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_property_value_uom_id ON common.material_specification_property USING btree (value_uom_id);


--
-- TOC entry 5415 (class 1259 OID 37803)
-- Name: ix_common_material_specification_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_quantity ON common.material_specification USING btree (quantity);


--
-- TOC entry 5416 (class 1259 OID 37804)
-- Name: ix_common_material_specification_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_specification_quantity_uom_id ON common.material_specification USING btree (quantity_uom_id);


--
-- TOC entry 4971 (class 1259 OID 35553)
-- Name: ix_common_definition_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_code ON common.operations_definition USING btree (code);


--
-- TOC entry 4972 (class 1259 OID 35554)
-- Name: ix_common_definition_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_hierarchy_scope_id ON common.operations_definition USING btree (hierarchy_scope_id);


--
-- TOC entry 4973 (class 1259 OID 35555)
-- Name: ix_common_definition_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_name ON common.operations_definition USING btree (name);


--
-- TOC entry 5176 (class 1259 OID 36600)
-- Name: ix_common_definition_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_code ON common.operations_definition_property USING btree (code);


--
-- TOC entry 5177 (class 1259 OID 36601)
-- Name: ix_common_definition_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_name ON common.operations_definition_property USING btree (name);


--
-- TOC entry 5178 (class 1259 OID 36602)
-- Name: ix_common_definition_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_parent_id ON common.operations_definition_property USING btree (parent_id);


--
-- TOC entry 5179 (class 1259 OID 41455)
-- Name: ix_common_definition_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_sensor_id ON common.operations_definition_property USING btree (sensor_id);


--
-- TOC entry 5180 (class 1259 OID 41456)
-- Name: ix_common_definition_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_sensor_type_id ON common.operations_definition_property USING btree (sensor_type_id);


--
-- TOC entry 5181 (class 1259 OID 41454)
-- Name: ix_common_definition_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_uom_category_id ON common.operations_definition_property USING btree (uom_category_id);


--
-- TOC entry 5182 (class 1259 OID 40285)
-- Name: ix_common_definition_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_value ON common.operations_definition_property USING btree (value);


--
-- TOC entry 5183 (class 1259 OID 40352)
-- Name: ix_common_definition_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_value_number ON common.operations_definition_property USING btree (value_number);


--
-- TOC entry 5184 (class 1259 OID 36604)
-- Name: ix_common_definition_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_value_type_id ON common.operations_definition_property USING btree (value_type_id);


--
-- TOC entry 5185 (class 1259 OID 36605)
-- Name: ix_common_definition_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_definition_property_value_uom_id ON common.operations_definition_property USING btree (value_uom_id);


--
-- TOC entry 5188 (class 1259 OID 36624)
-- Name: ix_common_material_bill_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_bill_code ON common.operations_material_bill USING btree (code);


--
-- TOC entry 5326 (class 1259 OID 37233)
-- Name: ix_common_material_bill_item_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_bill_item_code ON common.operations_material_bill_item USING btree (code);


--
-- TOC entry 5327 (class 1259 OID 37234)
-- Name: ix_common_material_bill_item_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_bill_item_name ON common.operations_material_bill_item USING btree (name);


--
-- TOC entry 5328 (class 1259 OID 37235)
-- Name: ix_common_material_bill_item_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_bill_item_parent_id ON common.operations_material_bill_item USING btree (parent_id);


--
-- TOC entry 5329 (class 1259 OID 37236)
-- Name: ix_common_material_bill_item_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_bill_item_quantity ON common.operations_material_bill_item USING btree (quantity);


--
-- TOC entry 5330 (class 1259 OID 37237)
-- Name: ix_common_material_bill_item_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_bill_item_quantity_uom_id ON common.operations_material_bill_item USING btree (quantity_uom_id);


--
-- TOC entry 5189 (class 1259 OID 36625)
-- Name: ix_common_material_bill_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_material_bill_name ON common.operations_material_bill USING btree (name);


--
-- TOC entry 5192 (class 1259 OID 36669)
-- Name: ix_common_segment_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_code ON common.operations_segment USING btree (code);


--
-- TOC entry 5333 (class 1259 OID 37276)
-- Name: ix_common_segment_dependency_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_dependency_code ON common.operations_segment_dependency USING btree (code);


--
-- TOC entry 5334 (class 1259 OID 37277)
-- Name: ix_common_segment_dependency_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_dependency_name ON common.operations_segment_dependency USING btree (name);


--
-- TOC entry 5335 (class 1259 OID 37278)
-- Name: ix_common_segment_dependency_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_dependency_value ON common.operations_segment_dependency USING btree (value);


--
-- TOC entry 5336 (class 1259 OID 37279)
-- Name: ix_common_segment_dependency_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_dependency_value_uom_id ON common.operations_segment_dependency USING btree (value_uom_id);


--
-- TOC entry 5193 (class 1259 OID 36670)
-- Name: ix_common_segment_hierarchy_scope_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_hierarchy_scope_id ON common.operations_segment USING btree (hierarchy_scope_id);


--
-- TOC entry 5194 (class 1259 OID 36671)
-- Name: ix_common_segment_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_name ON common.operations_segment USING btree (name);


--
-- TOC entry 5341 (class 1259 OID 41474)
-- Name: ix_common_segment_parameter_specificatio_1965; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specificatio_1965 ON common.operations_segment_parameter_specification USING btree (sensor_type_id);


--
-- TOC entry 5342 (class 1259 OID 37337)
-- Name: ix_common_segment_parameter_specificatio_75db; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specificatio_75db ON common.operations_segment_parameter_specification USING btree (value_uom_id);


--
-- TOC entry 5343 (class 1259 OID 41472)
-- Name: ix_common_segment_parameter_specificatio_763a; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specificatio_763a ON common.operations_segment_parameter_specification USING btree (uom_category_id);


--
-- TOC entry 5344 (class 1259 OID 40353)
-- Name: ix_common_segment_parameter_specificatio_848e; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specificatio_848e ON common.operations_segment_parameter_specification USING btree (value_number);


--
-- TOC entry 5345 (class 1259 OID 37338)
-- Name: ix_common_segment_parameter_specificatio_a3e7; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specificatio_a3e7 ON common.operations_segment_parameter_specification USING btree (parent_id);


--
-- TOC entry 5346 (class 1259 OID 41473)
-- Name: ix_common_segment_parameter_specificatio_b001; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specificatio_b001 ON common.operations_segment_parameter_specification USING btree (sensor_id);


--
-- TOC entry 5347 (class 1259 OID 37339)
-- Name: ix_common_segment_parameter_specificatio_d596; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specificatio_d596 ON common.operations_segment_parameter_specification USING btree (value_type_id);


--
-- TOC entry 5348 (class 1259 OID 37340)
-- Name: ix_common_segment_parameter_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specification_code ON common.operations_segment_parameter_specification USING btree (code);


--
-- TOC entry 5349 (class 1259 OID 37341)
-- Name: ix_common_segment_parameter_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specification_name ON common.operations_segment_parameter_specification USING btree (name);


--
-- TOC entry 5350 (class 1259 OID 40286)
-- Name: ix_common_segment_parameter_specification_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parameter_specification_value ON common.operations_segment_parameter_specification USING btree (value);


--
-- TOC entry 5195 (class 1259 OID 36672)
-- Name: ix_common_segment_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_segment_parent_id ON common.operations_segment USING btree (parent_id);


--
-- TOC entry 5419 (class 1259 OID 37848)
-- Name: ix_common_personnel_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_code ON common.personnel_specification USING btree (code);


--
-- TOC entry 5420 (class 1259 OID 37849)
-- Name: ix_common_personnel_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_name ON common.personnel_specification USING btree (name);


--
-- TOC entry 5549 (class 1259 OID 38661)
-- Name: ix_common_personnel_specification_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_code ON common.personnel_specification_property USING btree (code);


--
-- TOC entry 5550 (class 1259 OID 38662)
-- Name: ix_common_personnel_specification_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_name ON common.personnel_specification_property USING btree (name);


--
-- TOC entry 5551 (class 1259 OID 38663)
-- Name: ix_common_personnel_specification_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_parent_id ON common.personnel_specification_property USING btree (parent_id);


--
-- TOC entry 5552 (class 1259 OID 38664)
-- Name: ix_common_personnel_specification_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_quantity ON common.personnel_specification_property USING btree (quantity);


--
-- TOC entry 5553 (class 1259 OID 38665)
-- Name: ix_common_personnel_specification_property_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_quantity_uom_id ON common.personnel_specification_property USING btree (quantity_uom_id);


--
-- TOC entry 5554 (class 1259 OID 41401)
-- Name: ix_common_personnel_specification_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_sensor_id ON common.personnel_specification_property USING btree (sensor_id);


--
-- TOC entry 5555 (class 1259 OID 41402)
-- Name: ix_common_personnel_specification_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_sensor_type_id ON common.personnel_specification_property USING btree (sensor_type_id);


--
-- TOC entry 5556 (class 1259 OID 41400)
-- Name: ix_common_personnel_specification_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_uom_category_id ON common.personnel_specification_property USING btree (uom_category_id);


--
-- TOC entry 5557 (class 1259 OID 40282)
-- Name: ix_common_personnel_specification_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_value ON common.personnel_specification_property USING btree (value);


--
-- TOC entry 5558 (class 1259 OID 40349)
-- Name: ix_common_personnel_specification_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_value_number ON common.personnel_specification_property USING btree (value_number);


--
-- TOC entry 5559 (class 1259 OID 38667)
-- Name: ix_common_personnel_specification_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_property_value_uom_id ON common.personnel_specification_property USING btree (value_uom_id);


--
-- TOC entry 5421 (class 1259 OID 37850)
-- Name: ix_common_personnel_specification_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_quantity ON common.personnel_specification USING btree (quantity);


--
-- TOC entry 5422 (class 1259 OID 37851)
-- Name: ix_common_personnel_specification_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_personnel_specification_quantity_uom_id ON common.personnel_specification USING btree (quantity_uom_id);


--
-- TOC entry 5425 (class 1259 OID 37895)
-- Name: ix_common_physical_asset_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_code ON common.physical_asset_specification USING btree (code);


--
-- TOC entry 5426 (class 1259 OID 37896)
-- Name: ix_common_physical_asset_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_name ON common.physical_asset_specification USING btree (name);


--
-- TOC entry 5562 (class 1259 OID 38708)
-- Name: ix_common_physical_asset_specification_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_code ON common.physical_asset_specification_property USING btree (code);


--
-- TOC entry 5563 (class 1259 OID 38709)
-- Name: ix_common_physical_asset_specification_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_name ON common.physical_asset_specification_property USING btree (name);


--
-- TOC entry 5564 (class 1259 OID 38710)
-- Name: ix_common_physical_asset_specification_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_parent_id ON common.physical_asset_specification_property USING btree (parent_id);


--
-- TOC entry 5565 (class 1259 OID 38711)
-- Name: ix_common_physical_asset_specification_property_qua_88d9; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_qua_88d9 ON common.physical_asset_specification_property USING btree (quantity_uom_id);


--
-- TOC entry 5566 (class 1259 OID 38712)
-- Name: ix_common_physical_asset_specification_property_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_quantity ON common.physical_asset_specification_property USING btree (quantity);


--
-- TOC entry 5567 (class 1259 OID 41420)
-- Name: ix_common_physical_asset_specification_property_sen_e01c; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_sen_e01c ON common.physical_asset_specification_property USING btree (sensor_type_id);


--
-- TOC entry 5568 (class 1259 OID 41419)
-- Name: ix_common_physical_asset_specification_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_sensor_id ON common.physical_asset_specification_property USING btree (sensor_id);


--
-- TOC entry 5569 (class 1259 OID 41418)
-- Name: ix_common_physical_asset_specification_property_uom_3984; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_uom_3984 ON common.physical_asset_specification_property USING btree (uom_category_id);


--
-- TOC entry 5570 (class 1259 OID 38713)
-- Name: ix_common_physical_asset_specification_property_val_3719; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_val_3719 ON common.physical_asset_specification_property USING btree (value_uom_id);


--
-- TOC entry 5571 (class 1259 OID 38714)
-- Name: ix_common_physical_asset_specification_property_val_a1cf; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_val_a1cf ON common.physical_asset_specification_property USING btree (value_type_id);


--
-- TOC entry 5572 (class 1259 OID 40350)
-- Name: ix_common_physical_asset_specification_property_val_ef7e; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_val_ef7e ON common.physical_asset_specification_property USING btree (value_number);


--
-- TOC entry 5573 (class 1259 OID 40283)
-- Name: ix_common_physical_asset_specification_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_property_value ON common.physical_asset_specification_property USING btree (value);


--
-- TOC entry 5427 (class 1259 OID 37897)
-- Name: ix_common_physical_asset_specification_quantity; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_quantity ON common.physical_asset_specification USING btree (quantity);


--
-- TOC entry 5428 (class 1259 OID 37898)
-- Name: ix_common_physical_asset_specification_quantity_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_physical_asset_specification_quantity_uom_id ON common.physical_asset_specification USING btree (quantity_uom_id);


--
-- TOC entry 5353 (class 1259 OID 37381)
-- Name: ix_common_work_master_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_master_code ON common.work_master USING btree (code);


--
-- TOC entry 5354 (class 1259 OID 37382)
-- Name: ix_common_work_master_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_work_master_name ON common.work_master USING btree (name);


--
-- TOC entry 5431 (class 1259 OID 37917)
-- Name: ix_common_workflow_specification_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_code ON common.workflow_specification USING btree (code);


--
-- TOC entry 5714 (class 1259 OID 39425)
-- Name: ix_common_workflow_specification_connection_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_code ON common.workflow_specification_connection USING btree (code);


--
-- TOC entry 5715 (class 1259 OID 39426)
-- Name: ix_common_workflow_specification_connection_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_name ON common.workflow_specification_connection USING btree (name);


--
-- TOC entry 5823 (class 1259 OID 41508)
-- Name: ix_common_workflow_specification_connection_propert_28c6; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_propert_28c6 ON common.workflow_specification_connection_property USING btree (uom_category_id);


--
-- TOC entry 5824 (class 1259 OID 39960)
-- Name: ix_common_workflow_specification_connection_propert_7207; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_propert_7207 ON common.workflow_specification_connection_property USING btree (value_type_id);


--
-- TOC entry 5825 (class 1259 OID 40355)
-- Name: ix_common_workflow_specification_connection_propert_926f; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_propert_926f ON common.workflow_specification_connection_property USING btree (value_number);


--
-- TOC entry 5826 (class 1259 OID 41510)
-- Name: ix_common_workflow_specification_connection_propert_b672; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_propert_b672 ON common.workflow_specification_connection_property USING btree (sensor_type_id);


--
-- TOC entry 5827 (class 1259 OID 41509)
-- Name: ix_common_workflow_specification_connection_propert_b730; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_propert_b730 ON common.workflow_specification_connection_property USING btree (sensor_id);


--
-- TOC entry 5828 (class 1259 OID 39961)
-- Name: ix_common_workflow_specification_connection_propert_c893; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_propert_c893 ON common.workflow_specification_connection_property USING btree (value_uom_id);


--
-- TOC entry 5829 (class 1259 OID 39962)
-- Name: ix_common_workflow_specification_connection_propert_fdfc; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_propert_fdfc ON common.workflow_specification_connection_property USING btree (parent_id);


--
-- TOC entry 5830 (class 1259 OID 39963)
-- Name: ix_common_workflow_specification_connection_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_property_code ON common.workflow_specification_connection_property USING btree (code);


--
-- TOC entry 5831 (class 1259 OID 39964)
-- Name: ix_common_workflow_specification_connection_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_property_name ON common.workflow_specification_connection_property USING btree (name);


--
-- TOC entry 5832 (class 1259 OID 40288)
-- Name: ix_common_workflow_specification_connection_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_property_value ON common.workflow_specification_connection_property USING btree (value);


--
-- TOC entry 4628 (class 1259 OID 34014)
-- Name: ix_common_workflow_specification_connection_type_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_code ON common.workflow_specification_connection_type USING btree (code);


--
-- TOC entry 4629 (class 1259 OID 34015)
-- Name: ix_common_workflow_specification_connection_type_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_name ON common.workflow_specification_connection_type USING btree (name);


--
-- TOC entry 4797 (class 1259 OID 34757)
-- Name: ix_common_workflow_specification_connection_type_pr_2657; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_2657 ON common.workflow_specification_connection_type_property USING btree (parent_id);


--
-- TOC entry 4798 (class 1259 OID 34758)
-- Name: ix_common_workflow_specification_connection_type_pr_2a09; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_2a09 ON common.workflow_specification_connection_type_property USING btree (value_uom_id);


--
-- TOC entry 4799 (class 1259 OID 41526)
-- Name: ix_common_workflow_specification_connection_type_pr_3a51; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_3a51 ON common.workflow_specification_connection_type_property USING btree (uom_category_id);


--
-- TOC entry 4800 (class 1259 OID 34759)
-- Name: ix_common_workflow_specification_connection_type_pr_3b6e; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_3b6e ON common.workflow_specification_connection_type_property USING btree (value_type_id);


--
-- TOC entry 4801 (class 1259 OID 41527)
-- Name: ix_common_workflow_specification_connection_type_pr_42fc; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_42fc ON common.workflow_specification_connection_type_property USING btree (sensor_id);


--
-- TOC entry 4802 (class 1259 OID 40356)
-- Name: ix_common_workflow_specification_connection_type_pr_4438; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_4438 ON common.workflow_specification_connection_type_property USING btree (value_number);


--
-- TOC entry 4803 (class 1259 OID 34760)
-- Name: ix_common_workflow_specification_connection_type_pr_6b69; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_6b69 ON common.workflow_specification_connection_type_property USING btree (code);


--
-- TOC entry 4804 (class 1259 OID 40289)
-- Name: ix_common_workflow_specification_connection_type_pr_74aa; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_74aa ON common.workflow_specification_connection_type_property USING btree (value);


--
-- TOC entry 4805 (class 1259 OID 41528)
-- Name: ix_common_workflow_specification_connection_type_pr_a116; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_a116 ON common.workflow_specification_connection_type_property USING btree (sensor_type_id);


--
-- TOC entry 4806 (class 1259 OID 34762)
-- Name: ix_common_workflow_specification_connection_type_pr_a1d8; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_connection_type_pr_a1d8 ON common.workflow_specification_connection_type_property USING btree (name);


--
-- TOC entry 5432 (class 1259 OID 37918)
-- Name: ix_common_workflow_specification_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_name ON common.workflow_specification USING btree (name);


--
-- TOC entry 5576 (class 1259 OID 38749)
-- Name: ix_common_workflow_specification_node_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_code ON common.workflow_specification_node USING btree (code);


--
-- TOC entry 5577 (class 1259 OID 38750)
-- Name: ix_common_workflow_specification_node_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_name ON common.workflow_specification_node USING btree (name);


--
-- TOC entry 5718 (class 1259 OID 39462)
-- Name: ix_common_workflow_specification_node_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_code ON common.workflow_specification_node_property USING btree (code);


--
-- TOC entry 5719 (class 1259 OID 39463)
-- Name: ix_common_workflow_specification_node_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_name ON common.workflow_specification_node_property USING btree (name);


--
-- TOC entry 5720 (class 1259 OID 39464)
-- Name: ix_common_workflow_specification_node_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_parent_id ON common.workflow_specification_node_property USING btree (parent_id);


--
-- TOC entry 5721 (class 1259 OID 41546)
-- Name: ix_common_workflow_specification_node_property_sens_050e; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_sens_050e ON common.workflow_specification_node_property USING btree (sensor_type_id);


--
-- TOC entry 5722 (class 1259 OID 41545)
-- Name: ix_common_workflow_specification_node_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_sensor_id ON common.workflow_specification_node_property USING btree (sensor_id);


--
-- TOC entry 5723 (class 1259 OID 41544)
-- Name: ix_common_workflow_specification_node_property_uom__5967; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_uom__5967 ON common.workflow_specification_node_property USING btree (uom_category_id);


--
-- TOC entry 5724 (class 1259 OID 39465)
-- Name: ix_common_workflow_specification_node_property_valu_d1eb; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_valu_d1eb ON common.workflow_specification_node_property USING btree (value_type_id);


--
-- TOC entry 5725 (class 1259 OID 40290)
-- Name: ix_common_workflow_specification_node_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_value ON common.workflow_specification_node_property USING btree (value);


--
-- TOC entry 5726 (class 1259 OID 40357)
-- Name: ix_common_workflow_specification_node_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_value_number ON common.workflow_specification_node_property USING btree (value_number);


--
-- TOC entry 5727 (class 1259 OID 39467)
-- Name: ix_common_workflow_specification_node_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_property_value_uom_id ON common.workflow_specification_node_property USING btree (value_uom_id);


--
-- TOC entry 4632 (class 1259 OID 34029)
-- Name: ix_common_workflow_specification_node_type_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_code ON common.workflow_specification_node_type USING btree (code);


--
-- TOC entry 4633 (class 1259 OID 34030)
-- Name: ix_common_workflow_specification_node_type_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_name ON common.workflow_specification_node_type USING btree (name);


--
-- TOC entry 4809 (class 1259 OID 40358)
-- Name: ix_common_workflow_specification_node_type_property_1e96; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_1e96 ON common.workflow_specification_node_type_property USING btree (value_number);


--
-- TOC entry 4810 (class 1259 OID 34798)
-- Name: ix_common_workflow_specification_node_type_property_3927; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_3927 ON common.workflow_specification_node_type_property USING btree (value_uom_id);


--
-- TOC entry 4811 (class 1259 OID 34799)
-- Name: ix_common_workflow_specification_node_type_property_3dae; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_3dae ON common.workflow_specification_node_type_property USING btree (parent_id);


--
-- TOC entry 4812 (class 1259 OID 34800)
-- Name: ix_common_workflow_specification_node_type_property_613b; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_613b ON common.workflow_specification_node_type_property USING btree (value_type_id);


--
-- TOC entry 4813 (class 1259 OID 41562)
-- Name: ix_common_workflow_specification_node_type_property_6975; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_6975 ON common.workflow_specification_node_type_property USING btree (uom_category_id);


--
-- TOC entry 4814 (class 1259 OID 41563)
-- Name: ix_common_workflow_specification_node_type_property_b6ce; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_b6ce ON common.workflow_specification_node_type_property USING btree (sensor_id);


--
-- TOC entry 4815 (class 1259 OID 34801)
-- Name: ix_common_workflow_specification_node_type_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_code ON common.workflow_specification_node_type_property USING btree (code);


--
-- TOC entry 4816 (class 1259 OID 41564)
-- Name: ix_common_workflow_specification_node_type_property_f7f7; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_f7f7 ON common.workflow_specification_node_type_property USING btree (sensor_type_id);


--
-- TOC entry 4817 (class 1259 OID 34802)
-- Name: ix_common_workflow_specification_node_type_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_name ON common.workflow_specification_node_type_property USING btree (name);


--
-- TOC entry 4818 (class 1259 OID 40291)
-- Name: ix_common_workflow_specification_node_type_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_node_type_property_value ON common.workflow_specification_node_type_property USING btree (value);


--
-- TOC entry 5580 (class 1259 OID 38786)
-- Name: ix_common_workflow_specification_property_code; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_code ON common.workflow_specification_property USING btree (code);


--
-- TOC entry 5581 (class 1259 OID 38787)
-- Name: ix_common_workflow_specification_property_name; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_name ON common.workflow_specification_property USING btree (name);


--
-- TOC entry 5582 (class 1259 OID 38788)
-- Name: ix_common_workflow_specification_property_parent_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_parent_id ON common.workflow_specification_property USING btree (parent_id);


--
-- TOC entry 5583 (class 1259 OID 41491)
-- Name: ix_common_workflow_specification_property_sensor_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_sensor_id ON common.workflow_specification_property USING btree (sensor_id);


--
-- TOC entry 5584 (class 1259 OID 41492)
-- Name: ix_common_workflow_specification_property_sensor_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_sensor_type_id ON common.workflow_specification_property USING btree (sensor_type_id);


--
-- TOC entry 5585 (class 1259 OID 41490)
-- Name: ix_common_workflow_specification_property_uom_category_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_uom_category_id ON common.workflow_specification_property USING btree (uom_category_id);


--
-- TOC entry 5586 (class 1259 OID 40287)
-- Name: ix_common_workflow_specification_property_value; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_value ON common.workflow_specification_property USING btree (value);


--
-- TOC entry 5587 (class 1259 OID 40354)
-- Name: ix_common_workflow_specification_property_value_number; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_value_number ON common.workflow_specification_property USING btree (value_number);


--
-- TOC entry 5588 (class 1259 OID 38790)
-- Name: ix_common_workflow_specification_property_value_type_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_value_type_id ON common.workflow_specification_property USING btree (value_type_id);


--
-- TOC entry 5589 (class 1259 OID 38791)
-- Name: ix_common_workflow_specification_property_value_uom_id; Type: INDEX; Schema: common; Owner: -
--

CREATE INDEX ix_common_workflow_specification_property_value_uom_id ON common.workflow_specification_property USING btree (value_uom_id);


--
-- TOC entry 6686 (class 2606 OID 39481)
-- Name: equipment_actual equipment_actual_equipment_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual
    ADD CONSTRAINT equipment_actual_equipment_class_id_fkey FOREIGN KEY (equipment_class_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6685 (class 2606 OID 39486)
-- Name: equipment_actual equipment_actual_equipment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual
    ADD CONSTRAINT equipment_actual_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES common.equipment(id);


--
-- TOC entry 6684 (class 2606 OID 39491)
-- Name: equipment_actual equipment_actual_job_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual
    ADD CONSTRAINT equipment_actual_job_response_id_fkey FOREIGN KEY (job_response_id) REFERENCES common.job_response(id);


--
-- TOC entry 6765 (class 2606 OID 39981)
-- Name: equipment_actual_property equipment_actual_property_equipment_actual_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property
    ADD CONSTRAINT equipment_actual_property_equipment_actual_id_fkey FOREIGN KEY (equipment_actual_id) REFERENCES common.equipment_actual(id);


--
-- TOC entry 6764 (class 2606 OID 39986)
-- Name: equipment_actual_property equipment_actual_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property
    ADD CONSTRAINT equipment_actual_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment_actual_property(id);


--
-- TOC entry 6763 (class 2606 OID 39991)
-- Name: equipment_actual_property equipment_actual_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property
    ADD CONSTRAINT equipment_actual_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6759 (class 2606 OID 40814)
-- Name: equipment_actual_property equipment_actual_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property
    ADD CONSTRAINT equipment_actual_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6758 (class 2606 OID 40819)
-- Name: equipment_actual_property equipment_actual_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property
    ADD CONSTRAINT equipment_actual_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6760 (class 2606 OID 40809)
-- Name: equipment_actual_property equipment_actual_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property
    ADD CONSTRAINT equipment_actual_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6762 (class 2606 OID 39996)
-- Name: equipment_actual_property equipment_actual_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property
    ADD CONSTRAINT equipment_actual_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6761 (class 2606 OID 40001)
-- Name: equipment_actual_property equipment_actual_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual_property
    ADD CONSTRAINT equipment_actual_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6683 (class 2606 OID 39496)
-- Name: equipment_actual equipment_actual_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual
    ADD CONSTRAINT equipment_actual_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6682 (class 2606 OID 39501)
-- Name: equipment_actual equipment_actual_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual
    ADD CONSTRAINT equipment_actual_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6681 (class 2606 OID 39506)
-- Name: equipment_actual equipment_actual_segment_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_actual
    ADD CONSTRAINT equipment_actual_segment_response_id_fkey FOREIGN KEY (segment_response_id) REFERENCES common.segment_response(id);


--
-- TOC entry 6453 (class 2606 OID 37932)
-- Name: equipment_capability equipment_capability_capability_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_capability_type_id_fkey FOREIGN KEY (capability_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6452 (class 2606 OID 37937)
-- Name: equipment_capability equipment_capability_equipment_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_equipment_class_id_fkey FOREIGN KEY (equipment_class_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6451 (class 2606 OID 37942)
-- Name: equipment_capability equipment_capability_equipment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES common.equipment(id);


--
-- TOC entry 6450 (class 2606 OID 37947)
-- Name: equipment_capability equipment_capability_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6449 (class 2606 OID 37952)
-- Name: equipment_capability equipment_capability_common_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_common_capability_id_fkey FOREIGN KEY (operations_capability_id) REFERENCES common.operations_capability(id);


--
-- TOC entry 6448 (class 2606 OID 37957)
-- Name: equipment_capability equipment_capability_process_segment_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_process_segment_capability_id_fkey FOREIGN KEY (process_segment_capability_id) REFERENCES common.process_segment_capability(id);


--
-- TOC entry 6584 (class 2606 OID 38807)
-- Name: equipment_capability_property equipment_capability_property_equipment_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property
    ADD CONSTRAINT equipment_capability_property_equipment_capability_id_fkey FOREIGN KEY (equipment_capability_id) REFERENCES common.equipment_capability(id);


--
-- TOC entry 6583 (class 2606 OID 38812)
-- Name: equipment_capability_property equipment_capability_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property
    ADD CONSTRAINT equipment_capability_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment_capability_property(id);


--
-- TOC entry 6582 (class 2606 OID 38817)
-- Name: equipment_capability_property equipment_capability_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property
    ADD CONSTRAINT equipment_capability_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6578 (class 2606 OID 40886)
-- Name: equipment_capability_property equipment_capability_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property
    ADD CONSTRAINT equipment_capability_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6577 (class 2606 OID 40891)
-- Name: equipment_capability_property equipment_capability_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property
    ADD CONSTRAINT equipment_capability_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6579 (class 2606 OID 40881)
-- Name: equipment_capability_property equipment_capability_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property
    ADD CONSTRAINT equipment_capability_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6581 (class 2606 OID 38822)
-- Name: equipment_capability_property equipment_capability_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property
    ADD CONSTRAINT equipment_capability_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6580 (class 2606 OID 38827)
-- Name: equipment_capability_property equipment_capability_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_property
    ADD CONSTRAINT equipment_capability_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6447 (class 2606 OID 37962)
-- Name: equipment_capability equipment_capability_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6446 (class 2606 OID 37967)
-- Name: equipment_capability equipment_capability_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6266 (class 2606 OID 36686)
-- Name: equipment_capability_test_result equipment_capability_test_res_equipment_capability_test_sp_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_result
    ADD CONSTRAINT equipment_capability_test_res_equipment_capability_test_sp_fkey FOREIGN KEY (equipment_capability_test_specification_id) REFERENCES common.equipment_capability_test_specification(id);


--
-- TOC entry 6265 (class 2606 OID 36691)
-- Name: equipment_capability_test_result equipment_capability_test_result_result_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_result
    ADD CONSTRAINT equipment_capability_test_result_result_uom_id_fkey FOREIGN KEY (result_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6114 (class 2606 OID 35574)
-- Name: equipment_capability_test_specification equipment_capability_test_spec_equipment_class_property_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_specification
    ADD CONSTRAINT equipment_capability_test_spec_equipment_class_property_id_fkey FOREIGN KEY (equipment_class_property_id) REFERENCES common.equipment_class_property(id);


--
-- TOC entry 6112 (class 2606 OID 35584)
-- Name: equipment_capability_test_specification equipment_capability_test_specificat_equipment_property_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_specification
    ADD CONSTRAINT equipment_capability_test_specificat_equipment_property_id_fkey FOREIGN KEY (equipment_property_id) REFERENCES common.equipment_property(id);


--
-- TOC entry 6115 (class 2606 OID 35569)
-- Name: equipment_capability_test_specification equipment_capability_test_specification_equipment_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_specification
    ADD CONSTRAINT equipment_capability_test_specification_equipment_class_id_fkey FOREIGN KEY (equipment_class_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6113 (class 2606 OID 35579)
-- Name: equipment_capability_test_specification equipment_capability_test_specification_equipment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability_test_specification
    ADD CONSTRAINT equipment_capability_test_specification_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES common.equipment(id);


--
-- TOC entry 6445 (class 2606 OID 37972)
-- Name: equipment_capability equipment_capability_work_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_work_capability_id_fkey FOREIGN KEY (work_capability_id) REFERENCES common.work_capability(id);


--
-- TOC entry 6444 (class 2606 OID 37977)
-- Name: equipment_capability equipment_capability_work_master_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_capability
    ADD CONSTRAINT equipment_capability_work_master_capability_id_fkey FOREIGN KEY (work_master_capability_id) REFERENCES common.work_master_capability(id);


--
-- TOC entry 6590 (class 2606 OID 38852)
-- Name: equipment_requirement equipment_requirement_equipment_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement
    ADD CONSTRAINT equipment_requirement_equipment_class_id_fkey FOREIGN KEY (equipment_class_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6589 (class 2606 OID 38857)
-- Name: equipment_requirement equipment_requirement_equipment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement
    ADD CONSTRAINT equipment_requirement_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES common.equipment(id);


--
-- TOC entry 6588 (class 2606 OID 38862)
-- Name: equipment_requirement equipment_requirement_job_order_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement
    ADD CONSTRAINT equipment_requirement_job_order_id_fkey FOREIGN KEY (job_order_id) REFERENCES common.job_order(id);


--
-- TOC entry 6694 (class 2606 OID 39530)
-- Name: equipment_requirement_property equipment_requirement_property_equipment_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property
    ADD CONSTRAINT equipment_requirement_property_equipment_requirement_id_fkey FOREIGN KEY (equipment_requirement_id) REFERENCES common.equipment_requirement(id);


--
-- TOC entry 6693 (class 2606 OID 39535)
-- Name: equipment_requirement_property equipment_requirement_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property
    ADD CONSTRAINT equipment_requirement_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment_requirement_property(id);


--
-- TOC entry 6692 (class 2606 OID 39540)
-- Name: equipment_requirement_property equipment_requirement_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property
    ADD CONSTRAINT equipment_requirement_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6688 (class 2606 OID 40958)
-- Name: equipment_requirement_property equipment_requirement_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property
    ADD CONSTRAINT equipment_requirement_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6687 (class 2606 OID 40963)
-- Name: equipment_requirement_property equipment_requirement_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property
    ADD CONSTRAINT equipment_requirement_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6689 (class 2606 OID 40953)
-- Name: equipment_requirement_property equipment_requirement_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property
    ADD CONSTRAINT equipment_requirement_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6691 (class 2606 OID 39545)
-- Name: equipment_requirement_property equipment_requirement_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property
    ADD CONSTRAINT equipment_requirement_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6690 (class 2606 OID 39550)
-- Name: equipment_requirement_property equipment_requirement_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement_property
    ADD CONSTRAINT equipment_requirement_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6587 (class 2606 OID 38867)
-- Name: equipment_requirement equipment_requirement_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement
    ADD CONSTRAINT equipment_requirement_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6586 (class 2606 OID 38872)
-- Name: equipment_requirement equipment_requirement_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement
    ADD CONSTRAINT equipment_requirement_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6585 (class 2606 OID 38877)
-- Name: equipment_requirement equipment_requirement_segment_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_requirement
    ADD CONSTRAINT equipment_requirement_segment_requirement_id_fkey FOREIGN KEY (segment_requirement_id) REFERENCES common.segment_requirement(id);


--
-- TOC entry 6018 (class 2606 OID 34817)
-- Name: job_list job_list_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list
    ADD CONSTRAINT job_list_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6122 (class 2606 OID 35606)
-- Name: job_list_property job_list_property_job_list_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list_property
    ADD CONSTRAINT job_list_property_job_list_id_fkey FOREIGN KEY (job_list_id) REFERENCES common.job_list(id);


--
-- TOC entry 6121 (class 2606 OID 35611)
-- Name: job_list_property job_list_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list_property
    ADD CONSTRAINT job_list_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.job_list_property(id);


--
-- TOC entry 6117 (class 2606 OID 41048)
-- Name: job_list_property job_list_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list_property
    ADD CONSTRAINT job_list_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6116 (class 2606 OID 41053)
-- Name: job_list_property job_list_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list_property
    ADD CONSTRAINT job_list_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6118 (class 2606 OID 41043)
-- Name: job_list_property job_list_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list_property
    ADD CONSTRAINT job_list_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6120 (class 2606 OID 35616)
-- Name: job_list_property job_list_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list_property
    ADD CONSTRAINT job_list_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6119 (class 2606 OID 35621)
-- Name: job_list_property job_list_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list_property
    ADD CONSTRAINT job_list_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6017 (class 2606 OID 34822)
-- Name: job_list job_list_work_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_list
    ADD CONSTRAINT job_list_work_type_id_fkey FOREIGN KEY (work_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6378 (class 2606 OID 37396)
-- Name: job_order job_order_dispatch_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order
    ADD CONSTRAINT job_order_dispatch_status_id_fkey FOREIGN KEY (dispatch_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6377 (class 2606 OID 37401)
-- Name: job_order job_order_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order
    ADD CONSTRAINT job_order_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6376 (class 2606 OID 37406)
-- Name: job_order job_order_job_list_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order
    ADD CONSTRAINT job_order_job_list_id_fkey FOREIGN KEY (job_list_id) REFERENCES common.job_list(id);


--
-- TOC entry 6460 (class 2606 OID 38002)
-- Name: job_order_parameter job_order_parameter_job_order_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order_parameter
    ADD CONSTRAINT job_order_parameter_job_order_id_fkey FOREIGN KEY (job_order_id) REFERENCES common.job_order(id);


--
-- TOC entry 6459 (class 2606 OID 38007)
-- Name: job_order_parameter job_order_parameter_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order_parameter
    ADD CONSTRAINT job_order_parameter_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.job_order_parameter(id);


--
-- TOC entry 6455 (class 2606 OID 41066)
-- Name: job_order_parameter job_order_parameter_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order_parameter
    ADD CONSTRAINT job_order_parameter_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6454 (class 2606 OID 41071)
-- Name: job_order_parameter job_order_parameter_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order_parameter
    ADD CONSTRAINT job_order_parameter_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6456 (class 2606 OID 41061)
-- Name: job_order_parameter job_order_parameter_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order_parameter
    ADD CONSTRAINT job_order_parameter_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6458 (class 2606 OID 38012)
-- Name: job_order_parameter job_order_parameter_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order_parameter
    ADD CONSTRAINT job_order_parameter_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6457 (class 2606 OID 38017)
-- Name: job_order_parameter job_order_parameter_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order_parameter
    ADD CONSTRAINT job_order_parameter_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6375 (class 2606 OID 37411)
-- Name: job_order job_order_priority_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order
    ADD CONSTRAINT job_order_priority_id_fkey FOREIGN KEY (priority_id) REFERENCES common.lookup(id);


--
-- TOC entry 6374 (class 2606 OID 37416)
-- Name: job_order job_order_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order
    ADD CONSTRAINT job_order_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


--
-- TOC entry 6373 (class 2606 OID 37421)
-- Name: job_order job_order_work_request_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order
    ADD CONSTRAINT job_order_work_request_id_fkey FOREIGN KEY (work_request_id) REFERENCES common.work_request(id);


--
-- TOC entry 6372 (class 2606 OID 37426)
-- Name: job_order job_order_work_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_order
    ADD CONSTRAINT job_order_work_type_id_fkey FOREIGN KEY (work_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6597 (class 2606 OID 38901)
-- Name: job_response_data job_response_data_job_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response_data
    ADD CONSTRAINT job_response_data_job_response_id_fkey FOREIGN KEY (job_response_id) REFERENCES common.job_response(id);


--
-- TOC entry 6596 (class 2606 OID 38906)
-- Name: job_response_data job_response_data_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response_data
    ADD CONSTRAINT job_response_data_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.job_response_data(id);


--
-- TOC entry 6592 (class 2606 OID 41084)
-- Name: job_response_data job_response_data_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response_data
    ADD CONSTRAINT job_response_data_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6591 (class 2606 OID 41089)
-- Name: job_response_data job_response_data_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response_data
    ADD CONSTRAINT job_response_data_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6593 (class 2606 OID 41079)
-- Name: job_response_data job_response_data_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response_data
    ADD CONSTRAINT job_response_data_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6595 (class 2606 OID 38911)
-- Name: job_response_data job_response_data_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response_data
    ADD CONSTRAINT job_response_data_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6594 (class 2606 OID 38916)
-- Name: job_response_data job_response_data_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response_data
    ADD CONSTRAINT job_response_data_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6467 (class 2606 OID 38041)
-- Name: job_response job_response_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response
    ADD CONSTRAINT job_response_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6466 (class 2606 OID 38046)
-- Name: job_response job_response_job_order_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response
    ADD CONSTRAINT job_response_job_order_id_fkey FOREIGN KEY (job_order_id) REFERENCES common.job_order(id);


--
-- TOC entry 6465 (class 2606 OID 38051)
-- Name: job_response job_response_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response
    ADD CONSTRAINT job_response_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.job_response(id);


--
-- TOC entry 6464 (class 2606 OID 38056)
-- Name: job_response job_response_response_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response
    ADD CONSTRAINT job_response_response_status_id_fkey FOREIGN KEY (response_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6463 (class 2606 OID 38061)
-- Name: job_response job_response_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response
    ADD CONSTRAINT job_response_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


--
-- TOC entry 6462 (class 2606 OID 38066)
-- Name: job_response job_response_work_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response
    ADD CONSTRAINT job_response_work_response_id_fkey FOREIGN KEY (work_response_id) REFERENCES common.work_response(id);


--
-- TOC entry 6461 (class 2606 OID 38071)
-- Name: job_response job_response_work_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.job_response
    ADD CONSTRAINT job_response_work_type_id_fkey FOREIGN KEY (work_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6695 (class 2606 OID 39576)
-- Name: material_actual material_actual_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 6696 (class 2606 OID 39581)
-- Name: material_actual material_actual_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6697 (class 2606 OID 39586)
-- Name: material_actual material_actual_job_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_job_response_id_fkey FOREIGN KEY (job_response_id) REFERENCES common.job_response(id);


--
-- TOC entry 6698 (class 2606 OID 39591)
-- Name: material_actual material_actual_material_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_material_class_id_fkey FOREIGN KEY (material_class_id) REFERENCES common.material_class(id);


--
-- TOC entry 6699 (class 2606 OID 39596)
-- Name: material_actual material_actual_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6700 (class 2606 OID 39601)
-- Name: material_actual material_actual_material_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_material_lot_id_fkey FOREIGN KEY (material_lot_id) REFERENCES common.material_lot(id);


--
-- TOC entry 6701 (class 2606 OID 39606)
-- Name: material_actual material_actual_material_sub_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_material_sub_lot_id_fkey FOREIGN KEY (material_sub_lot_id) REFERENCES common.material_sub_lot(id);


--
-- TOC entry 6702 (class 2606 OID 39611)
-- Name: material_actual material_actual_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_actual(id);


--
-- TOC entry 6773 (class 2606 OID 40029)
-- Name: material_actual_property material_actual_property_material_actual_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property
    ADD CONSTRAINT material_actual_property_material_actual_id_fkey FOREIGN KEY (material_actual_id) REFERENCES common.material_actual(id);


--
-- TOC entry 6772 (class 2606 OID 40034)
-- Name: material_actual_property material_actual_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property
    ADD CONSTRAINT material_actual_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_actual_property(id);


--
-- TOC entry 6771 (class 2606 OID 40039)
-- Name: material_actual_property material_actual_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property
    ADD CONSTRAINT material_actual_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6767 (class 2606 OID 40868)
-- Name: material_actual_property material_actual_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property
    ADD CONSTRAINT material_actual_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6766 (class 2606 OID 40873)
-- Name: material_actual_property material_actual_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property
    ADD CONSTRAINT material_actual_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6768 (class 2606 OID 40863)
-- Name: material_actual_property material_actual_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property
    ADD CONSTRAINT material_actual_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6770 (class 2606 OID 40044)
-- Name: material_actual_property material_actual_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property
    ADD CONSTRAINT material_actual_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6769 (class 2606 OID 40049)
-- Name: material_actual_property material_actual_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual_property
    ADD CONSTRAINT material_actual_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6703 (class 2606 OID 39616)
-- Name: material_actual material_actual_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6704 (class 2606 OID 39621)
-- Name: material_actual material_actual_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6705 (class 2606 OID 39626)
-- Name: material_actual material_actual_segment_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_segment_response_id_fkey FOREIGN KEY (segment_response_id) REFERENCES common.segment_response(id);


--
-- TOC entry 6706 (class 2606 OID 39631)
-- Name: material_actual material_actual_storage_location_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_actual
    ADD CONSTRAINT material_actual_storage_location_id_fkey FOREIGN KEY (storage_location_id) REFERENCES common.equipment(id);


--
-- TOC entry 6468 (class 2606 OID 38093)
-- Name: material_capability material_capability_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 6469 (class 2606 OID 38098)
-- Name: material_capability material_capability_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6470 (class 2606 OID 38103)
-- Name: material_capability material_capability_capability_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_capability_type_id_fkey FOREIGN KEY (capability_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6471 (class 2606 OID 38108)
-- Name: material_capability material_capability_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6472 (class 2606 OID 38113)
-- Name: material_capability material_capability_material_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_material_class_id_fkey FOREIGN KEY (material_class_id) REFERENCES common.material_class(id);


--
-- TOC entry 6473 (class 2606 OID 38118)
-- Name: material_capability material_capability_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6474 (class 2606 OID 38123)
-- Name: material_capability material_capability_material_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_material_lot_id_fkey FOREIGN KEY (material_lot_id) REFERENCES common.material_lot(id);


--
-- TOC entry 6475 (class 2606 OID 38128)
-- Name: material_capability material_capability_material_sub_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_material_sub_lot_id_fkey FOREIGN KEY (material_sub_lot_id) REFERENCES common.material_sub_lot(id);


--
-- TOC entry 6476 (class 2606 OID 38133)
-- Name: material_capability material_capability_common_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_common_capability_id_fkey FOREIGN KEY (operations_capability_id) REFERENCES common.operations_capability(id);


--
-- TOC entry 6477 (class 2606 OID 38138)
-- Name: material_capability material_capability_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_capability(id);


--
-- TOC entry 6478 (class 2606 OID 38143)
-- Name: material_capability material_capability_process_segment_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_process_segment_capability_id_fkey FOREIGN KEY (process_segment_capability_id) REFERENCES common.process_segment_capability(id);


--
-- TOC entry 6605 (class 2606 OID 38942)
-- Name: material_capability_property material_capability_property_material_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property
    ADD CONSTRAINT material_capability_property_material_capability_id_fkey FOREIGN KEY (material_capability_id) REFERENCES common.material_capability(id);


--
-- TOC entry 6604 (class 2606 OID 38947)
-- Name: material_capability_property material_capability_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property
    ADD CONSTRAINT material_capability_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_capability_property(id);


--
-- TOC entry 6603 (class 2606 OID 38952)
-- Name: material_capability_property material_capability_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property
    ADD CONSTRAINT material_capability_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6599 (class 2606 OID 40940)
-- Name: material_capability_property material_capability_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property
    ADD CONSTRAINT material_capability_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6598 (class 2606 OID 40945)
-- Name: material_capability_property material_capability_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property
    ADD CONSTRAINT material_capability_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6600 (class 2606 OID 40935)
-- Name: material_capability_property material_capability_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property
    ADD CONSTRAINT material_capability_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6602 (class 2606 OID 38957)
-- Name: material_capability_property material_capability_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property
    ADD CONSTRAINT material_capability_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6601 (class 2606 OID 38962)
-- Name: material_capability_property material_capability_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_property
    ADD CONSTRAINT material_capability_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6479 (class 2606 OID 38148)
-- Name: material_capability material_capability_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6480 (class 2606 OID 38153)
-- Name: material_capability material_capability_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6484 (class 2606 OID 38187)
-- Name: material_capability_test_result material_capability_test_resu_material_capability_test_spe_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_result
    ADD CONSTRAINT material_capability_test_resu_material_capability_test_spe_fkey FOREIGN KEY (material_capability_test_specification_id) REFERENCES common.material_capability_test_specification(id);


--
-- TOC entry 6483 (class 2606 OID 38192)
-- Name: material_capability_test_result material_capability_test_result_result_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_result
    ADD CONSTRAINT material_capability_test_result_result_uom_id_fkey FOREIGN KEY (result_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6384 (class 2606 OID 37452)
-- Name: material_capability_test_specification material_capability_test_specif_material_class_property_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_specification
    ADD CONSTRAINT material_capability_test_specif_material_class_property_id_fkey FOREIGN KEY (material_class_property_id) REFERENCES common.material_class_property(id);


--
-- TOC entry 6381 (class 2606 OID 37467)
-- Name: material_capability_test_specification material_capability_test_specific_material_lot_property_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_specification
    ADD CONSTRAINT material_capability_test_specific_material_lot_property_id_fkey FOREIGN KEY (material_lot_property_id) REFERENCES common.material_lot_property(id);


--
-- TOC entry 6383 (class 2606 OID 37457)
-- Name: material_capability_test_specification material_capability_test_specificat_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_specification
    ADD CONSTRAINT material_capability_test_specificat_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6380 (class 2606 OID 37472)
-- Name: material_capability_test_specification material_capability_test_specificatio_material_property_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_specification
    ADD CONSTRAINT material_capability_test_specificatio_material_property_id_fkey FOREIGN KEY (material_property_id) REFERENCES common.material_definition_property(id);


--
-- TOC entry 6385 (class 2606 OID 37447)
-- Name: material_capability_test_specification material_capability_test_specification_material_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_specification
    ADD CONSTRAINT material_capability_test_specification_material_class_id_fkey FOREIGN KEY (material_class_id) REFERENCES common.material_class(id);


--
-- TOC entry 6382 (class 2606 OID 37462)
-- Name: material_capability_test_specification material_capability_test_specification_material_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_specification
    ADD CONSTRAINT material_capability_test_specification_material_lot_id_fkey FOREIGN KEY (material_lot_id) REFERENCES common.material_lot(id);


--
-- TOC entry 6379 (class 2606 OID 37477)
-- Name: material_capability_test_specification material_capability_test_specification_material_sub_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability_test_specification
    ADD CONSTRAINT material_capability_test_specification_material_sub_lot_id_fkey FOREIGN KEY (material_sub_lot_id) REFERENCES common.material_sub_lot(id);


--
-- TOC entry 6481 (class 2606 OID 38158)
-- Name: material_capability material_capability_work_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_work_capability_id_fkey FOREIGN KEY (work_capability_id) REFERENCES common.work_capability(id);


--
-- TOC entry 6482 (class 2606 OID 38163)
-- Name: material_capability material_capability_work_master_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_capability
    ADD CONSTRAINT material_capability_work_master_capability_id_fkey FOREIGN KEY (work_master_capability_id) REFERENCES common.work_master_capability(id);


--
-- TOC entry 6025 (class 2606 OID 34843)
-- Name: material_lot material_lot_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot
    ADD CONSTRAINT material_lot_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 6024 (class 2606 OID 34848)
-- Name: material_lot material_lot_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot
    ADD CONSTRAINT material_lot_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6023 (class 2606 OID 34853)
-- Name: material_lot material_lot_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot
    ADD CONSTRAINT material_lot_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6022 (class 2606 OID 34858)
-- Name: material_lot material_lot_material_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot
    ADD CONSTRAINT material_lot_material_status_id_fkey FOREIGN KEY (material_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6021 (class 2606 OID 34863)
-- Name: material_lot material_lot_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot
    ADD CONSTRAINT material_lot_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_lot(id);


--
-- TOC entry 6274 (class 2606 OID 36713)
-- Name: material_lot_property material_lot_property_material_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property
    ADD CONSTRAINT material_lot_property_material_lot_id_fkey FOREIGN KEY (material_lot_id) REFERENCES common.material_lot(id);


--
-- TOC entry 6273 (class 2606 OID 36718)
-- Name: material_lot_property material_lot_property_material_sub_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property
    ADD CONSTRAINT material_lot_property_material_sub_lot_id_fkey FOREIGN KEY (material_sub_lot_id) REFERENCES common.material_sub_lot(id);


--
-- TOC entry 6272 (class 2606 OID 36723)
-- Name: material_lot_property material_lot_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property
    ADD CONSTRAINT material_lot_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_lot_property(id);


--
-- TOC entry 6268 (class 2606 OID 41030)
-- Name: material_lot_property material_lot_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property
    ADD CONSTRAINT material_lot_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6267 (class 2606 OID 41035)
-- Name: material_lot_property material_lot_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property
    ADD CONSTRAINT material_lot_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6269 (class 2606 OID 41025)
-- Name: material_lot_property material_lot_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property
    ADD CONSTRAINT material_lot_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6271 (class 2606 OID 36728)
-- Name: material_lot_property material_lot_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property
    ADD CONSTRAINT material_lot_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6270 (class 2606 OID 36733)
-- Name: material_lot_property material_lot_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot_property
    ADD CONSTRAINT material_lot_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6020 (class 2606 OID 34868)
-- Name: material_lot material_lot_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot
    ADD CONSTRAINT material_lot_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6019 (class 2606 OID 34873)
-- Name: material_lot material_lot_storage_location_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_lot
    ADD CONSTRAINT material_lot_storage_location_id_fkey FOREIGN KEY (storage_location_id) REFERENCES common.equipment(id);


--
-- TOC entry 6606 (class 2606 OID 38988)
-- Name: material_requirement material_requirement_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 6607 (class 2606 OID 38993)
-- Name: material_requirement material_requirement_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6608 (class 2606 OID 38998)
-- Name: material_requirement material_requirement_job_order_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_job_order_id_fkey FOREIGN KEY (job_order_id) REFERENCES common.job_order(id);


--
-- TOC entry 6609 (class 2606 OID 39003)
-- Name: material_requirement material_requirement_material_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_material_class_id_fkey FOREIGN KEY (material_class_id) REFERENCES common.material_class(id);


--
-- TOC entry 6610 (class 2606 OID 39008)
-- Name: material_requirement material_requirement_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6611 (class 2606 OID 39013)
-- Name: material_requirement material_requirement_material_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_material_lot_id_fkey FOREIGN KEY (material_lot_id) REFERENCES common.material_lot(id);


--
-- TOC entry 6612 (class 2606 OID 39018)
-- Name: material_requirement material_requirement_material_sub_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_material_sub_lot_id_fkey FOREIGN KEY (material_sub_lot_id) REFERENCES common.material_sub_lot(id);


--
-- TOC entry 6613 (class 2606 OID 39023)
-- Name: material_requirement material_requirement_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_requirement(id);


--
-- TOC entry 6714 (class 2606 OID 39656)
-- Name: material_requirement_property material_requirement_property_material_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property
    ADD CONSTRAINT material_requirement_property_material_requirement_id_fkey FOREIGN KEY (material_requirement_id) REFERENCES common.material_requirement(id);


--
-- TOC entry 6713 (class 2606 OID 39661)
-- Name: material_requirement_property material_requirement_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property
    ADD CONSTRAINT material_requirement_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_requirement_property(id);


--
-- TOC entry 6712 (class 2606 OID 39666)
-- Name: material_requirement_property material_requirement_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property
    ADD CONSTRAINT material_requirement_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6708 (class 2606 OID 41012)
-- Name: material_requirement_property material_requirement_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property
    ADD CONSTRAINT material_requirement_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6707 (class 2606 OID 41017)
-- Name: material_requirement_property material_requirement_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property
    ADD CONSTRAINT material_requirement_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6709 (class 2606 OID 41007)
-- Name: material_requirement_property material_requirement_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property
    ADD CONSTRAINT material_requirement_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6711 (class 2606 OID 39671)
-- Name: material_requirement_property material_requirement_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property
    ADD CONSTRAINT material_requirement_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6710 (class 2606 OID 39676)
-- Name: material_requirement_property material_requirement_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement_property
    ADD CONSTRAINT material_requirement_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6614 (class 2606 OID 39028)
-- Name: material_requirement material_requirement_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6615 (class 2606 OID 39033)
-- Name: material_requirement material_requirement_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6616 (class 2606 OID 39038)
-- Name: material_requirement material_requirement_segment_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_segment_requirement_id_fkey FOREIGN KEY (segment_requirement_id) REFERENCES common.segment_requirement(id);


--
-- TOC entry 6617 (class 2606 OID 39043)
-- Name: material_requirement material_requirement_storage_location_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_requirement
    ADD CONSTRAINT material_requirement_storage_location_id_fkey FOREIGN KEY (storage_location_id) REFERENCES common.equipment(id);


--
-- TOC entry 6129 (class 2606 OID 35645)
-- Name: material_sub_lot material_sub_lot_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_sub_lot
    ADD CONSTRAINT material_sub_lot_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 6128 (class 2606 OID 35650)
-- Name: material_sub_lot material_sub_lot_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_sub_lot
    ADD CONSTRAINT material_sub_lot_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6127 (class 2606 OID 35655)
-- Name: material_sub_lot material_sub_lot_material_lot_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_sub_lot
    ADD CONSTRAINT material_sub_lot_material_lot_id_fkey FOREIGN KEY (material_lot_id) REFERENCES common.material_lot(id);


--
-- TOC entry 6126 (class 2606 OID 35660)
-- Name: material_sub_lot material_sub_lot_material_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_sub_lot
    ADD CONSTRAINT material_sub_lot_material_status_id_fkey FOREIGN KEY (material_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6125 (class 2606 OID 35665)
-- Name: material_sub_lot material_sub_lot_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_sub_lot
    ADD CONSTRAINT material_sub_lot_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_sub_lot(id);


--
-- TOC entry 6124 (class 2606 OID 35670)
-- Name: material_sub_lot material_sub_lot_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_sub_lot
    ADD CONSTRAINT material_sub_lot_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6123 (class 2606 OID 35675)
-- Name: material_sub_lot material_sub_lot_storage_location_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_sub_lot
    ADD CONSTRAINT material_sub_lot_storage_location_id_fkey FOREIGN KEY (storage_location_id) REFERENCES common.equipment(id);


--
-- TOC entry 6028 (class 2606 OID 34895)
-- Name: operations_capability operations_capability_capability_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability
    ADD CONSTRAINT operations_capability_capability_type_id_fkey FOREIGN KEY (capability_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6027 (class 2606 OID 34900)
-- Name: operations_capability operations_capability_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability
    ADD CONSTRAINT operations_capability_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6026 (class 2606 OID 34905)
-- Name: operations_capability operations_capability_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability
    ADD CONSTRAINT operations_capability_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_capability(id);


--
-- TOC entry 6136 (class 2606 OID 35700)
-- Name: operations_capability_property operations_capability_property_common_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability_property
    ADD CONSTRAINT operations_capability_property_common_capability_id_fkey FOREIGN KEY (operations_capability_id) REFERENCES common.operations_capability(id);


--
-- TOC entry 6135 (class 2606 OID 35705)
-- Name: operations_capability_property operations_capability_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability_property
    ADD CONSTRAINT operations_capability_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_capability_property(id);


--
-- TOC entry 6131 (class 2606 OID 41102)
-- Name: operations_capability_property operations_capability_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability_property
    ADD CONSTRAINT operations_capability_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6130 (class 2606 OID 41107)
-- Name: operations_capability_property operations_capability_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability_property
    ADD CONSTRAINT operations_capability_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6132 (class 2606 OID 41097)
-- Name: operations_capability_property operations_capability_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability_property
    ADD CONSTRAINT operations_capability_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6134 (class 2606 OID 35710)
-- Name: operations_capability_property operations_capability_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability_property
    ADD CONSTRAINT operations_capability_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6133 (class 2606 OID 35715)
-- Name: operations_capability_property operations_capability_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_capability_property
    ADD CONSTRAINT operations_capability_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6032 (class 2606 OID 34927)
-- Name: operations_performance operations_performance_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance
    ADD CONSTRAINT operations_performance_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6031 (class 2606 OID 34932)
-- Name: operations_performance operations_performance_common_schedule_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance
    ADD CONSTRAINT operations_performance_common_schedule_id_fkey FOREIGN KEY (operations_schedule_id) REFERENCES common.operations_schedule(id);


--
-- TOC entry 6030 (class 2606 OID 34937)
-- Name: operations_performance operations_performance_common_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance
    ADD CONSTRAINT operations_performance_common_type_id_fkey FOREIGN KEY (operations_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6029 (class 2606 OID 34942)
-- Name: operations_performance operations_performance_performance_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance
    ADD CONSTRAINT operations_performance_performance_status_id_fkey FOREIGN KEY (performance_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6143 (class 2606 OID 35741)
-- Name: operations_performance_property operations_performance_property_common_performance_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance_property
    ADD CONSTRAINT operations_performance_property_common_performance_id_fkey FOREIGN KEY (operations_performance_id) REFERENCES common.operations_performance(id);


--
-- TOC entry 6142 (class 2606 OID 35746)
-- Name: operations_performance_property operations_performance_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance_property
    ADD CONSTRAINT operations_performance_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_performance_property(id);


--
-- TOC entry 6138 (class 2606 OID 41120)
-- Name: operations_performance_property operations_performance_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance_property
    ADD CONSTRAINT operations_performance_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6137 (class 2606 OID 41125)
-- Name: operations_performance_property operations_performance_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance_property
    ADD CONSTRAINT operations_performance_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6139 (class 2606 OID 41115)
-- Name: operations_performance_property operations_performance_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance_property
    ADD CONSTRAINT operations_performance_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6141 (class 2606 OID 35751)
-- Name: operations_performance_property operations_performance_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance_property
    ADD CONSTRAINT operations_performance_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6140 (class 2606 OID 35756)
-- Name: operations_performance_property operations_performance_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_performance_property
    ADD CONSTRAINT operations_performance_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6392 (class 2606 OID 37497)
-- Name: operations_request operations_request_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request
    ADD CONSTRAINT operations_request_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6391 (class 2606 OID 37502)
-- Name: operations_request operations_request_common_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request
    ADD CONSTRAINT operations_request_common_definition_id_fkey FOREIGN KEY (operations_definition_id) REFERENCES common.operations_definition(id);


--
-- TOC entry 6390 (class 2606 OID 37507)
-- Name: operations_request operations_request_common_schedule_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request
    ADD CONSTRAINT operations_request_common_schedule_id_fkey FOREIGN KEY (operations_schedule_id) REFERENCES common.operations_schedule(id);


--
-- TOC entry 6389 (class 2606 OID 37512)
-- Name: operations_request operations_request_common_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request
    ADD CONSTRAINT operations_request_common_type_id_fkey FOREIGN KEY (operations_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6388 (class 2606 OID 37517)
-- Name: operations_request operations_request_order_request_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request
    ADD CONSTRAINT operations_request_order_request_status_id_fkey FOREIGN KEY (order_request_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6387 (class 2606 OID 37522)
-- Name: operations_request operations_request_priority_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request
    ADD CONSTRAINT operations_request_priority_id_fkey FOREIGN KEY (priority_id) REFERENCES common.lookup(id);


--
-- TOC entry 6491 (class 2606 OID 38214)
-- Name: operations_request_property operations_request_property_common_request_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request_property
    ADD CONSTRAINT operations_request_property_common_request_id_fkey FOREIGN KEY (operations_request_id) REFERENCES common.operations_request(id);


--
-- TOC entry 6490 (class 2606 OID 38219)
-- Name: operations_request_property operations_request_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request_property
    ADD CONSTRAINT operations_request_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_request_property(id);


--
-- TOC entry 6486 (class 2606 OID 41138)
-- Name: operations_request_property operations_request_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request_property
    ADD CONSTRAINT operations_request_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6485 (class 2606 OID 41143)
-- Name: operations_request_property operations_request_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request_property
    ADD CONSTRAINT operations_request_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6487 (class 2606 OID 41133)
-- Name: operations_request_property operations_request_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request_property
    ADD CONSTRAINT operations_request_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6489 (class 2606 OID 38224)
-- Name: operations_request_property operations_request_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request_property
    ADD CONSTRAINT operations_request_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6488 (class 2606 OID 38229)
-- Name: operations_request_property operations_request_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request_property
    ADD CONSTRAINT operations_request_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6386 (class 2606 OID 37527)
-- Name: operations_request operations_request_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_request
    ADD CONSTRAINT operations_request_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


--
-- TOC entry 6497 (class 2606 OID 38253)
-- Name: operations_response operations_response_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response
    ADD CONSTRAINT operations_response_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6496 (class 2606 OID 38258)
-- Name: operations_response operations_response_common_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response
    ADD CONSTRAINT operations_response_common_definition_id_fkey FOREIGN KEY (operations_definition_id) REFERENCES common.operations_definition(id);


--
-- TOC entry 6495 (class 2606 OID 38263)
-- Name: operations_response operations_response_common_performance_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response
    ADD CONSTRAINT operations_response_common_performance_id_fkey FOREIGN KEY (operations_performance_id) REFERENCES common.operations_performance(id);


--
-- TOC entry 6494 (class 2606 OID 38268)
-- Name: operations_response operations_response_common_request_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response
    ADD CONSTRAINT operations_response_common_request_id_fkey FOREIGN KEY (operations_request_id) REFERENCES common.operations_request(id);


--
-- TOC entry 6493 (class 2606 OID 38273)
-- Name: operations_response operations_response_common_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response
    ADD CONSTRAINT operations_response_common_type_id_fkey FOREIGN KEY (operations_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6624 (class 2606 OID 39068)
-- Name: operations_response_property operations_response_property_common_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response_property
    ADD CONSTRAINT operations_response_property_common_response_id_fkey FOREIGN KEY (operations_response_id) REFERENCES common.operations_response(id);


--
-- TOC entry 6623 (class 2606 OID 39073)
-- Name: operations_response_property operations_response_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response_property
    ADD CONSTRAINT operations_response_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_response_property(id);


--
-- TOC entry 6619 (class 2606 OID 41156)
-- Name: operations_response_property operations_response_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response_property
    ADD CONSTRAINT operations_response_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6618 (class 2606 OID 41161)
-- Name: operations_response_property operations_response_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response_property
    ADD CONSTRAINT operations_response_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6620 (class 2606 OID 41151)
-- Name: operations_response_property operations_response_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response_property
    ADD CONSTRAINT operations_response_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6622 (class 2606 OID 39078)
-- Name: operations_response_property operations_response_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response_property
    ADD CONSTRAINT operations_response_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6621 (class 2606 OID 39083)
-- Name: operations_response_property operations_response_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response_property
    ADD CONSTRAINT operations_response_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6492 (class 2606 OID 38278)
-- Name: operations_response operations_response_response_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_response
    ADD CONSTRAINT operations_response_response_status_id_fkey FOREIGN KEY (response_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 5921 (class 2606 OID 34135)
-- Name: operations_schedule operations_schedule_common_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule
    ADD CONSTRAINT operations_schedule_common_type_id_fkey FOREIGN KEY (operations_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6039 (class 2606 OID 34965)
-- Name: operations_schedule_property operations_schedule_property_common_schedule_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule_property
    ADD CONSTRAINT operations_schedule_property_common_schedule_id_fkey FOREIGN KEY (operations_schedule_id) REFERENCES common.operations_schedule(id);


--
-- TOC entry 6038 (class 2606 OID 34970)
-- Name: operations_schedule_property operations_schedule_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule_property
    ADD CONSTRAINT operations_schedule_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_schedule_property(id);


--
-- TOC entry 6034 (class 2606 OID 41174)
-- Name: operations_schedule_property operations_schedule_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule_property
    ADD CONSTRAINT operations_schedule_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6033 (class 2606 OID 41179)
-- Name: operations_schedule_property operations_schedule_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule_property
    ADD CONSTRAINT operations_schedule_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6035 (class 2606 OID 41169)
-- Name: operations_schedule_property operations_schedule_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule_property
    ADD CONSTRAINT operations_schedule_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6037 (class 2606 OID 34975)
-- Name: operations_schedule_property operations_schedule_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule_property
    ADD CONSTRAINT operations_schedule_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6036 (class 2606 OID 34980)
-- Name: operations_schedule_property operations_schedule_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule_property
    ADD CONSTRAINT operations_schedule_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5920 (class 2606 OID 34140)
-- Name: operations_schedule operations_schedule_scheduled_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_schedule
    ADD CONSTRAINT operations_schedule_scheduled_status_id_fkey FOREIGN KEY (scheduled_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6721 (class 2606 OID 39702)
-- Name: personnel_actual personnel_actual_job_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual
    ADD CONSTRAINT personnel_actual_job_response_id_fkey FOREIGN KEY (job_response_id) REFERENCES common.job_response(id);


--
-- TOC entry 6720 (class 2606 OID 39707)
-- Name: personnel_actual personnel_actual_person_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual
    ADD CONSTRAINT personnel_actual_person_id_fkey FOREIGN KEY (person_id) REFERENCES common.person(id);


--
-- TOC entry 6719 (class 2606 OID 39712)
-- Name: personnel_actual personnel_actual_personnel_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual
    ADD CONSTRAINT personnel_actual_personnel_class_id_fkey FOREIGN KEY (personnel_class_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 6781 (class 2606 OID 40076)
-- Name: personnel_actual_property personnel_actual_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property
    ADD CONSTRAINT personnel_actual_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.personnel_actual_property(id);


--
-- TOC entry 6780 (class 2606 OID 40081)
-- Name: personnel_actual_property personnel_actual_property_personnel_actual_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property
    ADD CONSTRAINT personnel_actual_property_personnel_actual_id_fkey FOREIGN KEY (personnel_actual_id) REFERENCES common.personnel_actual(id);


--
-- TOC entry 6779 (class 2606 OID 40086)
-- Name: personnel_actual_property personnel_actual_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property
    ADD CONSTRAINT personnel_actual_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6775 (class 2606 OID 40832)
-- Name: personnel_actual_property personnel_actual_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property
    ADD CONSTRAINT personnel_actual_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6774 (class 2606 OID 40837)
-- Name: personnel_actual_property personnel_actual_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property
    ADD CONSTRAINT personnel_actual_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6776 (class 2606 OID 40827)
-- Name: personnel_actual_property personnel_actual_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property
    ADD CONSTRAINT personnel_actual_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6778 (class 2606 OID 40091)
-- Name: personnel_actual_property personnel_actual_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property
    ADD CONSTRAINT personnel_actual_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6777 (class 2606 OID 40096)
-- Name: personnel_actual_property personnel_actual_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual_property
    ADD CONSTRAINT personnel_actual_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6718 (class 2606 OID 39717)
-- Name: personnel_actual personnel_actual_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual
    ADD CONSTRAINT personnel_actual_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6717 (class 2606 OID 39722)
-- Name: personnel_actual personnel_actual_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual
    ADD CONSTRAINT personnel_actual_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6716 (class 2606 OID 39727)
-- Name: personnel_actual personnel_actual_segment_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual
    ADD CONSTRAINT personnel_actual_segment_requirement_id_fkey FOREIGN KEY (segment_requirement_id) REFERENCES common.segment_requirement(id);


--
-- TOC entry 6715 (class 2606 OID 39732)
-- Name: personnel_actual personnel_actual_segment_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_actual
    ADD CONSTRAINT personnel_actual_segment_response_id_fkey FOREIGN KEY (segment_response_id) REFERENCES common.segment_response(id);


--
-- TOC entry 6507 (class 2606 OID 38299)
-- Name: personnel_capability personnel_capability_capability_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_capability_type_id_fkey FOREIGN KEY (capability_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6506 (class 2606 OID 38304)
-- Name: personnel_capability personnel_capability_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6505 (class 2606 OID 38309)
-- Name: personnel_capability personnel_capability_common_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_common_capability_id_fkey FOREIGN KEY (operations_capability_id) REFERENCES common.operations_capability(id);


--
-- TOC entry 6504 (class 2606 OID 38314)
-- Name: personnel_capability personnel_capability_person_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_person_id_fkey FOREIGN KEY (person_id) REFERENCES common.person(id);


--
-- TOC entry 6503 (class 2606 OID 38319)
-- Name: personnel_capability personnel_capability_personnel_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_personnel_class_id_fkey FOREIGN KEY (personnel_class_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 6502 (class 2606 OID 38324)
-- Name: personnel_capability personnel_capability_process_segment_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_process_segment_capability_id_fkey FOREIGN KEY (process_segment_capability_id) REFERENCES common.process_segment_capability(id);


--
-- TOC entry 6632 (class 2606 OID 39109)
-- Name: personnel_capability_property personnel_capability_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property
    ADD CONSTRAINT personnel_capability_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.personnel_capability_property(id);


--
-- TOC entry 6631 (class 2606 OID 39114)
-- Name: personnel_capability_property personnel_capability_property_personnel_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property
    ADD CONSTRAINT personnel_capability_property_personnel_capability_id_fkey FOREIGN KEY (personnel_capability_id) REFERENCES common.personnel_capability(id);


--
-- TOC entry 6630 (class 2606 OID 39119)
-- Name: personnel_capability_property personnel_capability_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property
    ADD CONSTRAINT personnel_capability_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6626 (class 2606 OID 40904)
-- Name: personnel_capability_property personnel_capability_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property
    ADD CONSTRAINT personnel_capability_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6625 (class 2606 OID 40909)
-- Name: personnel_capability_property personnel_capability_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property
    ADD CONSTRAINT personnel_capability_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6627 (class 2606 OID 40899)
-- Name: personnel_capability_property personnel_capability_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property
    ADD CONSTRAINT personnel_capability_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6629 (class 2606 OID 39124)
-- Name: personnel_capability_property personnel_capability_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property
    ADD CONSTRAINT personnel_capability_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6628 (class 2606 OID 39129)
-- Name: personnel_capability_property personnel_capability_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_property
    ADD CONSTRAINT personnel_capability_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6501 (class 2606 OID 38329)
-- Name: personnel_capability personnel_capability_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6500 (class 2606 OID 38334)
-- Name: personnel_capability personnel_capability_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6145 (class 2606 OID 35780)
-- Name: personnel_capability_test_result personnel_capability_test_res_personnel_capability_test_sp_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_result
    ADD CONSTRAINT personnel_capability_test_res_personnel_capability_test_sp_fkey FOREIGN KEY (personnel_capability_test_specification_id) REFERENCES common.personnel_capability_test_specification(id);


--
-- TOC entry 6144 (class 2606 OID 35785)
-- Name: personnel_capability_test_result personnel_capability_test_result_result_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_result
    ADD CONSTRAINT personnel_capability_test_result_result_uom_id_fkey FOREIGN KEY (result_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6040 (class 2606 OID 35019)
-- Name: personnel_capability_test_specification personnel_capability_test_spec_personnel_class_property_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_specification
    ADD CONSTRAINT personnel_capability_test_spec_personnel_class_property_id_fkey FOREIGN KEY (personnel_class_property_id) REFERENCES common.personnel_class_property(id);


--
-- TOC entry 6043 (class 2606 OID 35004)
-- Name: personnel_capability_test_specification personnel_capability_test_specification_person_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_specification
    ADD CONSTRAINT personnel_capability_test_specification_person_id_fkey FOREIGN KEY (person_id) REFERENCES common.person(id);


--
-- TOC entry 6042 (class 2606 OID 35009)
-- Name: personnel_capability_test_specification personnel_capability_test_specification_person_property_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_specification
    ADD CONSTRAINT personnel_capability_test_specification_person_property_id_fkey FOREIGN KEY (person_property_id) REFERENCES common.person_property(id);


--
-- TOC entry 6041 (class 2606 OID 35014)
-- Name: personnel_capability_test_specification personnel_capability_test_specification_personnel_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability_test_specification
    ADD CONSTRAINT personnel_capability_test_specification_personnel_class_id_fkey FOREIGN KEY (personnel_class_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 6499 (class 2606 OID 38339)
-- Name: personnel_capability personnel_capability_work_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_work_capability_id_fkey FOREIGN KEY (work_capability_id) REFERENCES common.work_capability(id);


--
-- TOC entry 6498 (class 2606 OID 38344)
-- Name: personnel_capability personnel_capability_work_master_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_capability
    ADD CONSTRAINT personnel_capability_work_master_capability_id_fkey FOREIGN KEY (work_master_capability_id) REFERENCES common.work_master_capability(id);


--
-- TOC entry 6638 (class 2606 OID 39155)
-- Name: personnel_requirement personnel_requirement_job_order_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement
    ADD CONSTRAINT personnel_requirement_job_order_id_fkey FOREIGN KEY (job_order_id) REFERENCES common.job_order(id);


--
-- TOC entry 6637 (class 2606 OID 39160)
-- Name: personnel_requirement personnel_requirement_person_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement
    ADD CONSTRAINT personnel_requirement_person_id_fkey FOREIGN KEY (person_id) REFERENCES common.person(id);


--
-- TOC entry 6636 (class 2606 OID 39165)
-- Name: personnel_requirement personnel_requirement_personnel_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement
    ADD CONSTRAINT personnel_requirement_personnel_class_id_fkey FOREIGN KEY (personnel_class_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 6729 (class 2606 OID 39756)
-- Name: personnel_requirement_property personnel_requirement_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property
    ADD CONSTRAINT personnel_requirement_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.personnel_requirement_property(id);


--
-- TOC entry 6728 (class 2606 OID 39761)
-- Name: personnel_requirement_property personnel_requirement_property_personnel_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property
    ADD CONSTRAINT personnel_requirement_property_personnel_requirement_id_fkey FOREIGN KEY (personnel_requirement_id) REFERENCES common.personnel_requirement(id);


--
-- TOC entry 6727 (class 2606 OID 39766)
-- Name: personnel_requirement_property personnel_requirement_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property
    ADD CONSTRAINT personnel_requirement_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6723 (class 2606 OID 40976)
-- Name: personnel_requirement_property personnel_requirement_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property
    ADD CONSTRAINT personnel_requirement_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6722 (class 2606 OID 40981)
-- Name: personnel_requirement_property personnel_requirement_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property
    ADD CONSTRAINT personnel_requirement_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6724 (class 2606 OID 40971)
-- Name: personnel_requirement_property personnel_requirement_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property
    ADD CONSTRAINT personnel_requirement_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6726 (class 2606 OID 39771)
-- Name: personnel_requirement_property personnel_requirement_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property
    ADD CONSTRAINT personnel_requirement_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6725 (class 2606 OID 39776)
-- Name: personnel_requirement_property personnel_requirement_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement_property
    ADD CONSTRAINT personnel_requirement_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6635 (class 2606 OID 39170)
-- Name: personnel_requirement personnel_requirement_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement
    ADD CONSTRAINT personnel_requirement_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6634 (class 2606 OID 39175)
-- Name: personnel_requirement personnel_requirement_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement
    ADD CONSTRAINT personnel_requirement_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6633 (class 2606 OID 39180)
-- Name: personnel_requirement personnel_requirement_segment_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_requirement
    ADD CONSTRAINT personnel_requirement_segment_requirement_id_fkey FOREIGN KEY (segment_requirement_id) REFERENCES common.segment_requirement(id);


--
-- TOC entry 6735 (class 2606 OID 39802)
-- Name: physical_asset_actual physical_asset_actual_job_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual
    ADD CONSTRAINT physical_asset_actual_job_response_id_fkey FOREIGN KEY (job_response_id) REFERENCES common.job_response(id);


--
-- TOC entry 6734 (class 2606 OID 39807)
-- Name: physical_asset_actual physical_asset_actual_physical_asset_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual
    ADD CONSTRAINT physical_asset_actual_physical_asset_class_id_fkey FOREIGN KEY (physical_asset_class_id) REFERENCES common.physical_asset_class(id);


--
-- TOC entry 6733 (class 2606 OID 39812)
-- Name: physical_asset_actual physical_asset_actual_physical_asset_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual
    ADD CONSTRAINT physical_asset_actual_physical_asset_id_fkey FOREIGN KEY (physical_asset_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 6789 (class 2606 OID 40124)
-- Name: physical_asset_actual_property physical_asset_actual_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property
    ADD CONSTRAINT physical_asset_actual_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.physical_asset_actual_property(id);


--
-- TOC entry 6788 (class 2606 OID 40129)
-- Name: physical_asset_actual_property physical_asset_actual_property_physical_asset_actual_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property
    ADD CONSTRAINT physical_asset_actual_property_physical_asset_actual_id_fkey FOREIGN KEY (physical_asset_actual_id) REFERENCES common.physical_asset_actual(id);


--
-- TOC entry 6787 (class 2606 OID 40134)
-- Name: physical_asset_actual_property physical_asset_actual_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property
    ADD CONSTRAINT physical_asset_actual_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6783 (class 2606 OID 40850)
-- Name: physical_asset_actual_property physical_asset_actual_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property
    ADD CONSTRAINT physical_asset_actual_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6782 (class 2606 OID 40855)
-- Name: physical_asset_actual_property physical_asset_actual_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property
    ADD CONSTRAINT physical_asset_actual_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6784 (class 2606 OID 40845)
-- Name: physical_asset_actual_property physical_asset_actual_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property
    ADD CONSTRAINT physical_asset_actual_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6786 (class 2606 OID 40139)
-- Name: physical_asset_actual_property physical_asset_actual_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property
    ADD CONSTRAINT physical_asset_actual_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6785 (class 2606 OID 40144)
-- Name: physical_asset_actual_property physical_asset_actual_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual_property
    ADD CONSTRAINT physical_asset_actual_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6732 (class 2606 OID 39817)
-- Name: physical_asset_actual physical_asset_actual_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual
    ADD CONSTRAINT physical_asset_actual_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6731 (class 2606 OID 39822)
-- Name: physical_asset_actual physical_asset_actual_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual
    ADD CONSTRAINT physical_asset_actual_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6730 (class 2606 OID 39827)
-- Name: physical_asset_actual physical_asset_actual_segment_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_actual
    ADD CONSTRAINT physical_asset_actual_segment_response_id_fkey FOREIGN KEY (segment_response_id) REFERENCES common.segment_response(id);


--
-- TOC entry 6517 (class 2606 OID 38367)
-- Name: physical_asset_capability physical_asset_capability_capability_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_capability_type_id_fkey FOREIGN KEY (capability_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6516 (class 2606 OID 38372)
-- Name: physical_asset_capability physical_asset_capability_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6515 (class 2606 OID 38377)
-- Name: physical_asset_capability physical_asset_capability_common_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_common_capability_id_fkey FOREIGN KEY (operations_capability_id) REFERENCES common.operations_capability(id);


--
-- TOC entry 6514 (class 2606 OID 38382)
-- Name: physical_asset_capability physical_asset_capability_physical_asset_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_physical_asset_class_id_fkey FOREIGN KEY (physical_asset_class_id) REFERENCES common.physical_asset_class(id);


--
-- TOC entry 6513 (class 2606 OID 38387)
-- Name: physical_asset_capability physical_asset_capability_physical_asset_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_physical_asset_id_fkey FOREIGN KEY (physical_asset_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 6645 (class 2606 OID 39209)
-- Name: physical_asset_capability_property physical_asset_capability_pro_physical_asset_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property
    ADD CONSTRAINT physical_asset_capability_pro_physical_asset_capability_id_fkey FOREIGN KEY (physical_asset_capability_id) REFERENCES common.physical_asset_capability(id);


--
-- TOC entry 6512 (class 2606 OID 38392)
-- Name: physical_asset_capability physical_asset_capability_process_segment_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_process_segment_capability_id_fkey FOREIGN KEY (process_segment_capability_id) REFERENCES common.process_segment_capability(id);


--
-- TOC entry 6646 (class 2606 OID 39204)
-- Name: physical_asset_capability_property physical_asset_capability_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property
    ADD CONSTRAINT physical_asset_capability_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.physical_asset_capability_property(id);


--
-- TOC entry 6644 (class 2606 OID 39214)
-- Name: physical_asset_capability_property physical_asset_capability_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property
    ADD CONSTRAINT physical_asset_capability_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6640 (class 2606 OID 40922)
-- Name: physical_asset_capability_property physical_asset_capability_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property
    ADD CONSTRAINT physical_asset_capability_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6639 (class 2606 OID 40927)
-- Name: physical_asset_capability_property physical_asset_capability_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property
    ADD CONSTRAINT physical_asset_capability_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6641 (class 2606 OID 40917)
-- Name: physical_asset_capability_property physical_asset_capability_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property
    ADD CONSTRAINT physical_asset_capability_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6643 (class 2606 OID 39219)
-- Name: physical_asset_capability_property physical_asset_capability_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property
    ADD CONSTRAINT physical_asset_capability_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6642 (class 2606 OID 39224)
-- Name: physical_asset_capability_property physical_asset_capability_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_property
    ADD CONSTRAINT physical_asset_capability_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6511 (class 2606 OID 38397)
-- Name: physical_asset_capability physical_asset_capability_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6510 (class 2606 OID 38402)
-- Name: physical_asset_capability physical_asset_capability_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6394 (class 2606 OID 37548)
-- Name: physical_asset_capability_test_result physical_asset_capability_tes_physical_asset_capability_te_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_result
    ADD CONSTRAINT physical_asset_capability_tes_physical_asset_capability_te_fkey FOREIGN KEY (physical_asset_capability_test_specification_id) REFERENCES common.physical_asset_capability_test_spec(id);


--
-- TOC entry 6277 (class 2606 OID 36762)
-- Name: physical_asset_capability_test_spec physical_asset_capability_tes_physical_asset_class_propert_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_spec
    ADD CONSTRAINT physical_asset_capability_tes_physical_asset_class_propert_fkey FOREIGN KEY (physical_asset_class_property_id) REFERENCES common.physical_asset_class_property(id);


--
-- TOC entry 6275 (class 2606 OID 36772)
-- Name: physical_asset_capability_test_spec physical_asset_capability_test__physical_asset_property_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_spec
    ADD CONSTRAINT physical_asset_capability_test__physical_asset_property_id_fkey FOREIGN KEY (physical_asset_property_id) REFERENCES common.physical_asset_property(id);


--
-- TOC entry 6393 (class 2606 OID 37553)
-- Name: physical_asset_capability_test_result physical_asset_capability_test_result_result_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_result
    ADD CONSTRAINT physical_asset_capability_test_result_result_uom_id_fkey FOREIGN KEY (result_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6278 (class 2606 OID 36757)
-- Name: physical_asset_capability_test_spec physical_asset_capability_test_spe_physical_asset_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_spec
    ADD CONSTRAINT physical_asset_capability_test_spe_physical_asset_class_id_fkey FOREIGN KEY (physical_asset_class_id) REFERENCES common.physical_asset_class(id);


--
-- TOC entry 6276 (class 2606 OID 36767)
-- Name: physical_asset_capability_test_spec physical_asset_capability_test_spec_physical_asset_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability_test_spec
    ADD CONSTRAINT physical_asset_capability_test_spec_physical_asset_id_fkey FOREIGN KEY (physical_asset_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 6509 (class 2606 OID 38407)
-- Name: physical_asset_capability physical_asset_capability_work_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_work_capability_id_fkey FOREIGN KEY (work_capability_id) REFERENCES common.work_capability(id);


--
-- TOC entry 6508 (class 2606 OID 38412)
-- Name: physical_asset_capability physical_asset_capability_work_master_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_capability
    ADD CONSTRAINT physical_asset_capability_work_master_capability_id_fkey FOREIGN KEY (work_master_capability_id) REFERENCES common.work_master_capability(id);


--
-- TOC entry 6653 (class 2606 OID 39250)
-- Name: physical_asset_requirement physical_asset_requirement_equipment_level_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement
    ADD CONSTRAINT physical_asset_requirement_equipment_level_id_fkey FOREIGN KEY (equipment_level_id) REFERENCES common.lookup(id);


--
-- TOC entry 6652 (class 2606 OID 39255)
-- Name: physical_asset_requirement physical_asset_requirement_job_order_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement
    ADD CONSTRAINT physical_asset_requirement_job_order_id_fkey FOREIGN KEY (job_order_id) REFERENCES common.job_order(id);


--
-- TOC entry 6651 (class 2606 OID 39260)
-- Name: physical_asset_requirement physical_asset_requirement_physical_asset_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement
    ADD CONSTRAINT physical_asset_requirement_physical_asset_class_id_fkey FOREIGN KEY (physical_asset_class_id) REFERENCES common.physical_asset_class(id);


--
-- TOC entry 6650 (class 2606 OID 39265)
-- Name: physical_asset_requirement physical_asset_requirement_physical_asset_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement
    ADD CONSTRAINT physical_asset_requirement_physical_asset_id_fkey FOREIGN KEY (physical_asset_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 6742 (class 2606 OID 39856)
-- Name: physical_asset_requirement_property physical_asset_requirement_pr_physical_asset_requirement_i_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property
    ADD CONSTRAINT physical_asset_requirement_pr_physical_asset_requirement_i_fkey FOREIGN KEY (physical_asset_requirement_id) REFERENCES common.physical_asset_requirement(id);


--
-- TOC entry 6743 (class 2606 OID 39851)
-- Name: physical_asset_requirement_property physical_asset_requirement_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property
    ADD CONSTRAINT physical_asset_requirement_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.physical_asset_requirement_property(id);


--
-- TOC entry 6741 (class 2606 OID 39861)
-- Name: physical_asset_requirement_property physical_asset_requirement_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property
    ADD CONSTRAINT physical_asset_requirement_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6737 (class 2606 OID 40994)
-- Name: physical_asset_requirement_property physical_asset_requirement_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property
    ADD CONSTRAINT physical_asset_requirement_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6736 (class 2606 OID 40999)
-- Name: physical_asset_requirement_property physical_asset_requirement_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property
    ADD CONSTRAINT physical_asset_requirement_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6738 (class 2606 OID 40989)
-- Name: physical_asset_requirement_property physical_asset_requirement_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property
    ADD CONSTRAINT physical_asset_requirement_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6740 (class 2606 OID 39866)
-- Name: physical_asset_requirement_property physical_asset_requirement_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property
    ADD CONSTRAINT physical_asset_requirement_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6739 (class 2606 OID 39871)
-- Name: physical_asset_requirement_property physical_asset_requirement_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement_property
    ADD CONSTRAINT physical_asset_requirement_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6649 (class 2606 OID 39270)
-- Name: physical_asset_requirement physical_asset_requirement_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement
    ADD CONSTRAINT physical_asset_requirement_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6648 (class 2606 OID 39275)
-- Name: physical_asset_requirement physical_asset_requirement_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement
    ADD CONSTRAINT physical_asset_requirement_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6647 (class 2606 OID 39280)
-- Name: physical_asset_requirement physical_asset_requirement_segment_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_requirement
    ADD CONSTRAINT physical_asset_requirement_segment_requirement_id_fkey FOREIGN KEY (segment_requirement_id) REFERENCES common.segment_requirement(id);


--
-- TOC entry 6150 (class 2606 OID 35805)
-- Name: process_segment_capability process_segment_capability_capability_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_capability
    ADD CONSTRAINT process_segment_capability_capability_type_id_fkey FOREIGN KEY (capability_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6149 (class 2606 OID 35810)
-- Name: process_segment_capability process_segment_capability_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_capability
    ADD CONSTRAINT process_segment_capability_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6148 (class 2606 OID 35815)
-- Name: process_segment_capability process_segment_capability_common_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_capability
    ADD CONSTRAINT process_segment_capability_common_capability_id_fkey FOREIGN KEY (operations_capability_id) REFERENCES common.operations_capability(id);


--
-- TOC entry 6147 (class 2606 OID 35820)
-- Name: process_segment_capability process_segment_capability_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_capability
    ADD CONSTRAINT process_segment_capability_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.process_segment_capability(id);


--
-- TOC entry 6146 (class 2606 OID 35825)
-- Name: process_segment_capability process_segment_capability_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_capability
    ADD CONSTRAINT process_segment_capability_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6750 (class 2606 OID 39899)
-- Name: segment_data segment_data_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_data
    ADD CONSTRAINT segment_data_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.segment_data(id);


--
-- TOC entry 6749 (class 2606 OID 39904)
-- Name: segment_data segment_data_segment_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_data
    ADD CONSTRAINT segment_data_segment_response_id_fkey FOREIGN KEY (segment_response_id) REFERENCES common.segment_response(id);


--
-- TOC entry 6745 (class 2606 OID 41192)
-- Name: segment_data segment_data_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_data
    ADD CONSTRAINT segment_data_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6744 (class 2606 OID 41197)
-- Name: segment_data segment_data_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_data
    ADD CONSTRAINT segment_data_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6746 (class 2606 OID 41187)
-- Name: segment_data segment_data_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_data
    ADD CONSTRAINT segment_data_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6748 (class 2606 OID 39909)
-- Name: segment_data segment_data_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_data
    ADD CONSTRAINT segment_data_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6747 (class 2606 OID 39914)
-- Name: segment_data segment_data_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_data
    ADD CONSTRAINT segment_data_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6660 (class 2606 OID 39304)
-- Name: segment_parameter segment_parameter_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_parameter
    ADD CONSTRAINT segment_parameter_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.segment_parameter(id);


--
-- TOC entry 6659 (class 2606 OID 39309)
-- Name: segment_parameter segment_parameter_segment_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_parameter
    ADD CONSTRAINT segment_parameter_segment_requirement_id_fkey FOREIGN KEY (segment_requirement_id) REFERENCES common.segment_requirement(id);


--
-- TOC entry 6655 (class 2606 OID 41210)
-- Name: segment_parameter segment_parameter_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_parameter
    ADD CONSTRAINT segment_parameter_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6654 (class 2606 OID 41215)
-- Name: segment_parameter segment_parameter_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_parameter
    ADD CONSTRAINT segment_parameter_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6656 (class 2606 OID 41205)
-- Name: segment_parameter segment_parameter_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_parameter
    ADD CONSTRAINT segment_parameter_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6658 (class 2606 OID 39314)
-- Name: segment_parameter segment_parameter_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_parameter
    ADD CONSTRAINT segment_parameter_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6657 (class 2606 OID 39319)
-- Name: segment_parameter segment_parameter_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_parameter
    ADD CONSTRAINT segment_parameter_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6526 (class 2606 OID 38435)
-- Name: segment_requirement segment_requirement_duration_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_duration_uom_id_fkey FOREIGN KEY (duration_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6525 (class 2606 OID 38440)
-- Name: segment_requirement segment_requirement_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6524 (class 2606 OID 38445)
-- Name: segment_requirement segment_requirement_common_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_common_definition_id_fkey FOREIGN KEY (operations_definition_id) REFERENCES common.operations_definition(id);


--
-- TOC entry 6523 (class 2606 OID 38450)
-- Name: segment_requirement segment_requirement_common_request_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_common_request_id_fkey FOREIGN KEY (operations_request_id) REFERENCES common.operations_request(id);


--
-- TOC entry 6522 (class 2606 OID 38455)
-- Name: segment_requirement segment_requirement_common_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_common_segment_id_fkey FOREIGN KEY (operations_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6521 (class 2606 OID 38460)
-- Name: segment_requirement segment_requirement_common_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_common_type_id_fkey FOREIGN KEY (operations_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6520 (class 2606 OID 38465)
-- Name: segment_requirement segment_requirement_order_request_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_order_request_status_id_fkey FOREIGN KEY (order_request_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6519 (class 2606 OID 38470)
-- Name: segment_requirement segment_requirement_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.segment_requirement(id);


--
-- TOC entry 6518 (class 2606 OID 38475)
-- Name: segment_requirement segment_requirement_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_requirement
    ADD CONSTRAINT segment_requirement_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6669 (class 2606 OID 39343)
-- Name: segment_response segment_response_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6668 (class 2606 OID 39348)
-- Name: segment_response segment_response_common_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_common_definition_id_fkey FOREIGN KEY (operations_definition_id) REFERENCES common.operations_definition(id);


--
-- TOC entry 6667 (class 2606 OID 39353)
-- Name: segment_response segment_response_common_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_common_response_id_fkey FOREIGN KEY (operations_response_id) REFERENCES common.operations_response(id);


--
-- TOC entry 6666 (class 2606 OID 39358)
-- Name: segment_response segment_response_common_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_common_segment_id_fkey FOREIGN KEY (operations_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6665 (class 2606 OID 39363)
-- Name: segment_response segment_response_common_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_common_type_id_fkey FOREIGN KEY (operations_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6664 (class 2606 OID 39368)
-- Name: segment_response segment_response_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.segment_response(id);


--
-- TOC entry 6663 (class 2606 OID 39373)
-- Name: segment_response segment_response_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6662 (class 2606 OID 39378)
-- Name: segment_response segment_response_response_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_response_status_id_fkey FOREIGN KEY (response_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6661 (class 2606 OID 39383)
-- Name: segment_response segment_response_segment_requirement_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.segment_response
    ADD CONSTRAINT segment_response_segment_requirement_id_fkey FOREIGN KEY (segment_requirement_id) REFERENCES common.segment_requirement(id);


--
-- TOC entry 6046 (class 2606 OID 35039)
-- Name: work_alert_definition work_alert_definition_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition
    ADD CONSTRAINT work_alert_definition_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6045 (class 2606 OID 35044)
-- Name: work_alert_definition work_alert_definition_priority_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition
    ADD CONSTRAINT work_alert_definition_priority_id_fkey FOREIGN KEY (priority_id) REFERENCES common.lookup(id);


--
-- TOC entry 6161 (class 2606 OID 35885)
-- Name: work_alert_definition_property work_alert_definition_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition_property
    ADD CONSTRAINT work_alert_definition_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_alert_definition_property(id);


--
-- TOC entry 6156 (class 2606 OID 41246)
-- Name: work_alert_definition_property work_alert_definition_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition_property
    ADD CONSTRAINT work_alert_definition_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6155 (class 2606 OID 41251)
-- Name: work_alert_definition_property work_alert_definition_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition_property
    ADD CONSTRAINT work_alert_definition_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6157 (class 2606 OID 41241)
-- Name: work_alert_definition_property work_alert_definition_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition_property
    ADD CONSTRAINT work_alert_definition_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6160 (class 2606 OID 35890)
-- Name: work_alert_definition_property work_alert_definition_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition_property
    ADD CONSTRAINT work_alert_definition_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6159 (class 2606 OID 35895)
-- Name: work_alert_definition_property work_alert_definition_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition_property
    ADD CONSTRAINT work_alert_definition_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6158 (class 2606 OID 35900)
-- Name: work_alert_definition_property work_alert_definition_property_work_alert_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition_property
    ADD CONSTRAINT work_alert_definition_property_work_alert_definition_id_fkey FOREIGN KEY (work_alert_definition_id) REFERENCES common.work_alert_definition(id);


--
-- TOC entry 6044 (class 2606 OID 35049)
-- Name: work_alert_definition work_alert_definition_work_alert_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_definition
    ADD CONSTRAINT work_alert_definition_work_alert_category_id_fkey FOREIGN KEY (work_alert_category_id) REFERENCES common.lookup(id);


--
-- TOC entry 6154 (class 2606 OID 35847)
-- Name: work_alert work_alert_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert
    ADD CONSTRAINT work_alert_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6153 (class 2606 OID 35852)
-- Name: work_alert work_alert_priority_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert
    ADD CONSTRAINT work_alert_priority_id_fkey FOREIGN KEY (priority_id) REFERENCES common.lookup(id);


--
-- TOC entry 6286 (class 2606 OID 36794)
-- Name: work_alert_property work_alert_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property
    ADD CONSTRAINT work_alert_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_alert_property(id);


--
-- TOC entry 6280 (class 2606 OID 41228)
-- Name: work_alert_property work_alert_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property
    ADD CONSTRAINT work_alert_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6279 (class 2606 OID 41233)
-- Name: work_alert_property work_alert_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property
    ADD CONSTRAINT work_alert_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6281 (class 2606 OID 41223)
-- Name: work_alert_property work_alert_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property
    ADD CONSTRAINT work_alert_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6285 (class 2606 OID 36799)
-- Name: work_alert_property work_alert_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property
    ADD CONSTRAINT work_alert_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6284 (class 2606 OID 36804)
-- Name: work_alert_property work_alert_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property
    ADD CONSTRAINT work_alert_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6283 (class 2606 OID 36809)
-- Name: work_alert_property work_alert_property_work_alert_definition_property_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property
    ADD CONSTRAINT work_alert_property_work_alert_definition_property_id_fkey FOREIGN KEY (work_alert_definition_property_id) REFERENCES common.work_alert_definition_property(id);


--
-- TOC entry 6282 (class 2606 OID 36814)
-- Name: work_alert_property work_alert_property_work_alert_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert_property
    ADD CONSTRAINT work_alert_property_work_alert_id_fkey FOREIGN KEY (work_alert_id) REFERENCES common.work_alert(id);


--
-- TOC entry 6152 (class 2606 OID 35857)
-- Name: work_alert work_alert_work_alert_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert
    ADD CONSTRAINT work_alert_work_alert_category_id_fkey FOREIGN KEY (work_alert_category_id) REFERENCES common.lookup(id);


--
-- TOC entry 6151 (class 2606 OID 35862)
-- Name: work_alert work_alert_work_alert_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_alert
    ADD CONSTRAINT work_alert_work_alert_definition_id_fkey FOREIGN KEY (work_alert_definition_id) REFERENCES common.work_alert_definition(id);


--
-- TOC entry 6048 (class 2606 OID 35070)
-- Name: work_capability work_capability_capacity_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability
    ADD CONSTRAINT work_capability_capacity_type_id_fkey FOREIGN KEY (capacity_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6047 (class 2606 OID 35075)
-- Name: work_capability work_capability_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability
    ADD CONSTRAINT work_capability_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6168 (class 2606 OID 35925)
-- Name: work_capability_property work_capability_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability_property
    ADD CONSTRAINT work_capability_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_capability_property(id);


--
-- TOC entry 6163 (class 2606 OID 41264)
-- Name: work_capability_property work_capability_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability_property
    ADD CONSTRAINT work_capability_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6162 (class 2606 OID 41269)
-- Name: work_capability_property work_capability_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability_property
    ADD CONSTRAINT work_capability_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6164 (class 2606 OID 41259)
-- Name: work_capability_property work_capability_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability_property
    ADD CONSTRAINT work_capability_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6167 (class 2606 OID 35930)
-- Name: work_capability_property work_capability_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability_property
    ADD CONSTRAINT work_capability_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6166 (class 2606 OID 35935)
-- Name: work_capability_property work_capability_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability_property
    ADD CONSTRAINT work_capability_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6165 (class 2606 OID 35940)
-- Name: work_capability_property work_capability_property_work_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_capability_property
    ADD CONSTRAINT work_capability_property_work_capability_id_fkey FOREIGN KEY (work_capability_id) REFERENCES common.work_capability(id);


--
-- TOC entry 6398 (class 2606 OID 37573)
-- Name: work_master_capability work_master_capability_capacity_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability
    ADD CONSTRAINT work_master_capability_capacity_type_id_fkey FOREIGN KEY (capacity_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6397 (class 2606 OID 37578)
-- Name: work_master_capability work_master_capability_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability
    ADD CONSTRAINT work_master_capability_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6533 (class 2606 OID 38499)
-- Name: work_master_capability_property work_master_capability_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability_property
    ADD CONSTRAINT work_master_capability_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_master_capability_property(id);


--
-- TOC entry 6528 (class 2606 OID 41282)
-- Name: work_master_capability_property work_master_capability_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability_property
    ADD CONSTRAINT work_master_capability_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6527 (class 2606 OID 41287)
-- Name: work_master_capability_property work_master_capability_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability_property
    ADD CONSTRAINT work_master_capability_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6529 (class 2606 OID 41277)
-- Name: work_master_capability_property work_master_capability_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability_property
    ADD CONSTRAINT work_master_capability_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6532 (class 2606 OID 38504)
-- Name: work_master_capability_property work_master_capability_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability_property
    ADD CONSTRAINT work_master_capability_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6531 (class 2606 OID 38509)
-- Name: work_master_capability_property work_master_capability_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability_property
    ADD CONSTRAINT work_master_capability_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6530 (class 2606 OID 38514)
-- Name: work_master_capability_property work_master_capability_property_work_master_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability_property
    ADD CONSTRAINT work_master_capability_property_work_master_capability_id_fkey FOREIGN KEY (work_master_capability_id) REFERENCES common.work_master_capability(id);


--
-- TOC entry 6396 (class 2606 OID 37583)
-- Name: work_master_capability work_master_capability_work_capability_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability
    ADD CONSTRAINT work_master_capability_work_capability_id_fkey FOREIGN KEY (work_capability_id) REFERENCES common.work_capability(id);


--
-- TOC entry 6395 (class 2606 OID 37588)
-- Name: work_master_capability work_master_capability_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master_capability
    ADD CONSTRAINT work_master_capability_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


--
-- TOC entry 6172 (class 2606 OID 35964)
-- Name: work_performance work_performance_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance
    ADD CONSTRAINT work_performance_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6171 (class 2606 OID 35969)
-- Name: work_performance work_performance_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance
    ADD CONSTRAINT work_performance_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_performance(id);


--
-- TOC entry 6293 (class 2606 OID 36840)
-- Name: work_performance_property work_performance_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance_property
    ADD CONSTRAINT work_performance_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_performance_property(id);


--
-- TOC entry 6288 (class 2606 OID 41300)
-- Name: work_performance_property work_performance_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance_property
    ADD CONSTRAINT work_performance_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6287 (class 2606 OID 41305)
-- Name: work_performance_property work_performance_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance_property
    ADD CONSTRAINT work_performance_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6289 (class 2606 OID 41295)
-- Name: work_performance_property work_performance_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance_property
    ADD CONSTRAINT work_performance_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6292 (class 2606 OID 36845)
-- Name: work_performance_property work_performance_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance_property
    ADD CONSTRAINT work_performance_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6291 (class 2606 OID 36850)
-- Name: work_performance_property work_performance_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance_property
    ADD CONSTRAINT work_performance_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6290 (class 2606 OID 36855)
-- Name: work_performance_property work_performance_property_work_performance_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance_property
    ADD CONSTRAINT work_performance_property_work_performance_id_fkey FOREIGN KEY (work_performance_id) REFERENCES common.work_performance(id);


--
-- TOC entry 6170 (class 2606 OID 35974)
-- Name: work_performance work_performance_work_schedule_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance
    ADD CONSTRAINT work_performance_work_schedule_id_fkey FOREIGN KEY (work_schedule_id) REFERENCES common.work_schedule(id);


--
-- TOC entry 6169 (class 2606 OID 35979)
-- Name: work_performance work_performance_work_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_performance
    ADD CONSTRAINT work_performance_work_type_id_fkey FOREIGN KEY (work_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6177 (class 2606 OID 36001)
-- Name: work_request work_request_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request
    ADD CONSTRAINT work_request_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6176 (class 2606 OID 36006)
-- Name: work_request work_request_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request
    ADD CONSTRAINT work_request_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_request(id);


--
-- TOC entry 6175 (class 2606 OID 36011)
-- Name: work_request work_request_priority_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request
    ADD CONSTRAINT work_request_priority_id_fkey FOREIGN KEY (priority_id) REFERENCES common.lookup(id);


--
-- TOC entry 6300 (class 2606 OID 36881)
-- Name: work_request_property work_request_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request_property
    ADD CONSTRAINT work_request_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_request_property(id);


--
-- TOC entry 6295 (class 2606 OID 41318)
-- Name: work_request_property work_request_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request_property
    ADD CONSTRAINT work_request_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6294 (class 2606 OID 41323)
-- Name: work_request_property work_request_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request_property
    ADD CONSTRAINT work_request_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6296 (class 2606 OID 41313)
-- Name: work_request_property work_request_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request_property
    ADD CONSTRAINT work_request_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6299 (class 2606 OID 36886)
-- Name: work_request_property work_request_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request_property
    ADD CONSTRAINT work_request_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6298 (class 2606 OID 36891)
-- Name: work_request_property work_request_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request_property
    ADD CONSTRAINT work_request_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6297 (class 2606 OID 36896)
-- Name: work_request_property work_request_property_work_request_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request_property
    ADD CONSTRAINT work_request_property_work_request_id_fkey FOREIGN KEY (work_request_id) REFERENCES common.work_request(id);


--
-- TOC entry 6174 (class 2606 OID 36016)
-- Name: work_request work_request_work_schedule_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request
    ADD CONSTRAINT work_request_work_schedule_id_fkey FOREIGN KEY (work_schedule_id) REFERENCES common.work_schedule(id);


--
-- TOC entry 6173 (class 2606 OID 36021)
-- Name: work_request work_request_work_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_request
    ADD CONSTRAINT work_request_work_type_id_fkey FOREIGN KEY (work_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6306 (class 2606 OID 36920)
-- Name: work_response work_response_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response
    ADD CONSTRAINT work_response_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6305 (class 2606 OID 36925)
-- Name: work_response work_response_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response
    ADD CONSTRAINT work_response_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_response(id);


--
-- TOC entry 6405 (class 2606 OID 37611)
-- Name: work_response_property work_response_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response_property
    ADD CONSTRAINT work_response_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_response_property(id);


--
-- TOC entry 6400 (class 2606 OID 41336)
-- Name: work_response_property work_response_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response_property
    ADD CONSTRAINT work_response_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6399 (class 2606 OID 41341)
-- Name: work_response_property work_response_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response_property
    ADD CONSTRAINT work_response_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6401 (class 2606 OID 41331)
-- Name: work_response_property work_response_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response_property
    ADD CONSTRAINT work_response_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6404 (class 2606 OID 37616)
-- Name: work_response_property work_response_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response_property
    ADD CONSTRAINT work_response_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6403 (class 2606 OID 37621)
-- Name: work_response_property work_response_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response_property
    ADD CONSTRAINT work_response_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6402 (class 2606 OID 37626)
-- Name: work_response_property work_response_property_work_response_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response_property
    ADD CONSTRAINT work_response_property_work_response_id_fkey FOREIGN KEY (work_response_id) REFERENCES common.work_response(id);


--
-- TOC entry 6304 (class 2606 OID 36930)
-- Name: work_response work_response_response_status_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response
    ADD CONSTRAINT work_response_response_status_id_fkey FOREIGN KEY (response_status_id) REFERENCES common.lookup(id);


--
-- TOC entry 6303 (class 2606 OID 36935)
-- Name: work_response work_response_work_performance_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response
    ADD CONSTRAINT work_response_work_performance_id_fkey FOREIGN KEY (work_performance_id) REFERENCES common.work_performance(id);


--
-- TOC entry 6302 (class 2606 OID 36940)
-- Name: work_response work_response_work_request_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response
    ADD CONSTRAINT work_response_work_request_id_fkey FOREIGN KEY (work_request_id) REFERENCES common.work_request(id);


--
-- TOC entry 6301 (class 2606 OID 36945)
-- Name: work_response work_response_work_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_response
    ADD CONSTRAINT work_response_work_type_id_fkey FOREIGN KEY (work_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6051 (class 2606 OID 35096)
-- Name: work_schedule work_schedule_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule
    ADD CONSTRAINT work_schedule_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6050 (class 2606 OID 35101)
-- Name: work_schedule work_schedule_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule
    ADD CONSTRAINT work_schedule_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_schedule(id);


--
-- TOC entry 6184 (class 2606 OID 36045)
-- Name: work_schedule_property work_schedule_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule_property
    ADD CONSTRAINT work_schedule_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_schedule_property(id);


--
-- TOC entry 6179 (class 2606 OID 41354)
-- Name: work_schedule_property work_schedule_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule_property
    ADD CONSTRAINT work_schedule_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6178 (class 2606 OID 41359)
-- Name: work_schedule_property work_schedule_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule_property
    ADD CONSTRAINT work_schedule_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6180 (class 2606 OID 41349)
-- Name: work_schedule_property work_schedule_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule_property
    ADD CONSTRAINT work_schedule_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6183 (class 2606 OID 36050)
-- Name: work_schedule_property work_schedule_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule_property
    ADD CONSTRAINT work_schedule_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6182 (class 2606 OID 36055)
-- Name: work_schedule_property work_schedule_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule_property
    ADD CONSTRAINT work_schedule_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6181 (class 2606 OID 36060)
-- Name: work_schedule_property work_schedule_property_work_schedule_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule_property
    ADD CONSTRAINT work_schedule_property_work_schedule_id_fkey FOREIGN KEY (work_schedule_id) REFERENCES common.work_schedule(id);


--
-- TOC entry 6049 (class 2606 OID 35106)
-- Name: work_schedule work_schedule_work_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_schedule
    ADD CONSTRAINT work_schedule_work_type_id_fkey FOREIGN KEY (work_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6803 (class 2606 OID 41640)
-- Name: document document_document_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.document
    ADD CONSTRAINT document_document_category_id_fkey FOREIGN KEY (document_category_id) REFERENCES common.lookup(id);


--
-- TOC entry 6802 (class 2606 OID 41645)
-- Name: document document_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.document
    ADD CONSTRAINT document_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.document(id);


--
-- TOC entry 6804 (class 2606 OID 41635)
-- Name: document document_resource_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.document
    ADD CONSTRAINT document_resource_type_id_fkey FOREIGN KEY (resource_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6186 (class 2606 OID 36083)
-- Name: equipment_asset_mapping equipment_asset_mapping_equipment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_asset_mapping
    ADD CONSTRAINT equipment_asset_mapping_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES common.equipment(id);


--
-- TOC entry 6185 (class 2606 OID 36088)
-- Name: equipment_asset_mapping equipment_asset_mapping_physical_asset_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_asset_mapping
    ADD CONSTRAINT equipment_asset_mapping_physical_asset_id_fkey FOREIGN KEY (physical_asset_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 5925 (class 2606 OID 34187)
-- Name: equipment_class equipment_class_equipment_level_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class
    ADD CONSTRAINT equipment_class_equipment_level_id_fkey FOREIGN KEY (equipment_level_id) REFERENCES common.lookup(id);


--
-- TOC entry 6053 (class 2606 OID 35127)
-- Name: equipment_class_mapping equipment_class_mapping_equipment_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_mapping
    ADD CONSTRAINT equipment_class_mapping_equipment_class_id_fkey FOREIGN KEY (equipment_class_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6052 (class 2606 OID 35132)
-- Name: equipment_class_mapping equipment_class_mapping_equipment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_mapping
    ADD CONSTRAINT equipment_class_mapping_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES common.equipment(id);


--
-- TOC entry 5924 (class 2606 OID 34192)
-- Name: equipment_class equipment_class_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class
    ADD CONSTRAINT equipment_class_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6060 (class 2606 OID 35152)
-- Name: equipment_class_property equipment_class_property_equipment_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_property
    ADD CONSTRAINT equipment_class_property_equipment_class_id_fkey FOREIGN KEY (equipment_class_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6059 (class 2606 OID 35157)
-- Name: equipment_class_property equipment_class_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_property
    ADD CONSTRAINT equipment_class_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment_class_property(id);


--
-- TOC entry 6055 (class 2606 OID 40436)
-- Name: equipment_class_property equipment_class_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_property
    ADD CONSTRAINT equipment_class_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6054 (class 2606 OID 40441)
-- Name: equipment_class_property equipment_class_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_property
    ADD CONSTRAINT equipment_class_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6056 (class 2606 OID 40431)
-- Name: equipment_class_property equipment_class_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_property
    ADD CONSTRAINT equipment_class_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6058 (class 2606 OID 35162)
-- Name: equipment_class_property equipment_class_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_property
    ADD CONSTRAINT equipment_class_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6057 (class 2606 OID 35167)
-- Name: equipment_class_property equipment_class_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_class_property
    ADD CONSTRAINT equipment_class_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5923 (class 2606 OID 34160)
-- Name: equipment equipment_equipment_level_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment
    ADD CONSTRAINT equipment_equipment_level_id_fkey FOREIGN KEY (equipment_level_id) REFERENCES common.lookup(id);


--
-- TOC entry 5922 (class 2606 OID 34165)
-- Name: equipment equipment_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment
    ADD CONSTRAINT equipment_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment(id);


--
-- TOC entry 6067 (class 2606 OID 35193)
-- Name: equipment_property equipment_property_equipment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_property
    ADD CONSTRAINT equipment_property_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES common.equipment(id);


--
-- TOC entry 6066 (class 2606 OID 35198)
-- Name: equipment_property equipment_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_property
    ADD CONSTRAINT equipment_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment_property(id);


--
-- TOC entry 6062 (class 2606 OID 40364)
-- Name: equipment_property equipment_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_property
    ADD CONSTRAINT equipment_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6061 (class 2606 OID 40369)
-- Name: equipment_property equipment_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_property
    ADD CONSTRAINT equipment_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6063 (class 2606 OID 40359)
-- Name: equipment_property equipment_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_property
    ADD CONSTRAINT equipment_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6065 (class 2606 OID 35203)
-- Name: equipment_property equipment_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_property
    ADD CONSTRAINT equipment_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6064 (class 2606 OID 35208)
-- Name: equipment_property equipment_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_property
    ADD CONSTRAINT equipment_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6314 (class 2606 OID 36969)
-- Name: equipment_segment_specification_property equipment_segment_specificati_equipment_segment_specificat_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property
    ADD CONSTRAINT equipment_segment_specificati_equipment_segment_specificat_fkey FOREIGN KEY (equipment_segment_specification_id) REFERENCES common.equipment_segment_specification(id);


--
-- TOC entry 6191 (class 2606 OID 36106)
-- Name: equipment_segment_specification equipment_segment_specification_equipment_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification
    ADD CONSTRAINT equipment_segment_specification_equipment_class_id_fkey FOREIGN KEY (equipment_class_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6190 (class 2606 OID 36111)
-- Name: equipment_segment_specification equipment_segment_specification_equipment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification
    ADD CONSTRAINT equipment_segment_specification_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES common.equipment(id);


--
-- TOC entry 6189 (class 2606 OID 36116)
-- Name: equipment_segment_specification equipment_segment_specification_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification
    ADD CONSTRAINT equipment_segment_specification_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6313 (class 2606 OID 36974)
-- Name: equipment_segment_specification_property equipment_segment_specification_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property
    ADD CONSTRAINT equipment_segment_specification_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment_segment_specification_property(id);


--
-- TOC entry 6312 (class 2606 OID 36979)
-- Name: equipment_segment_specification_property equipment_segment_specification_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property
    ADD CONSTRAINT equipment_segment_specification_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6308 (class 2606 OID 40526)
-- Name: equipment_segment_specification_property equipment_segment_specification_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property
    ADD CONSTRAINT equipment_segment_specification_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6307 (class 2606 OID 40531)
-- Name: equipment_segment_specification_property equipment_segment_specification_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property
    ADD CONSTRAINT equipment_segment_specification_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6309 (class 2606 OID 40521)
-- Name: equipment_segment_specification_property equipment_segment_specification_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property
    ADD CONSTRAINT equipment_segment_specification_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6311 (class 2606 OID 36984)
-- Name: equipment_segment_specification_property equipment_segment_specification_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property
    ADD CONSTRAINT equipment_segment_specification_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6310 (class 2606 OID 36989)
-- Name: equipment_segment_specification_property equipment_segment_specification_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification_property
    ADD CONSTRAINT equipment_segment_specification_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6188 (class 2606 OID 36121)
-- Name: equipment_segment_specification equipment_segment_specification_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification
    ADD CONSTRAINT equipment_segment_specification_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6187 (class 2606 OID 36126)
-- Name: equipment_segment_specification equipment_segment_specification_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_segment_specification
    ADD CONSTRAINT equipment_segment_specification_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6800 (class 2606 OID 41609)
-- Name: equipment_template equipment_template_equipment_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_template
    ADD CONSTRAINT equipment_template_equipment_class_id_fkey FOREIGN KEY (equipment_class_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6799 (class 2606 OID 41614)
-- Name: equipment_template equipment_template_equipment_level_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_template
    ADD CONSTRAINT equipment_template_equipment_level_id_fkey FOREIGN KEY (equipment_level_id) REFERENCES common.lookup(id);


--
-- TOC entry 6801 (class 2606 OID 41604)
-- Name: equipment_template equipment_template_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_template
    ADD CONSTRAINT equipment_template_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment_template(id);


--
-- TOC entry 5910 (class 2606 OID 33838)
-- Name: event_class event_class_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class
    ADD CONSTRAINT event_class_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.event_class(id);


--
-- TOC entry 5932 (class 2606 OID 34216)
-- Name: event_class_property event_class_property_event_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class_property
    ADD CONSTRAINT event_class_property_event_class_id_fkey FOREIGN KEY (event_class_id) REFERENCES common.event_class(id);


--
-- TOC entry 5931 (class 2606 OID 34221)
-- Name: event_class_property event_class_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class_property
    ADD CONSTRAINT event_class_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.event_class_property(id);


--
-- TOC entry 5927 (class 2606 OID 40760)
-- Name: event_class_property event_class_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class_property
    ADD CONSTRAINT event_class_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 5926 (class 2606 OID 40765)
-- Name: event_class_property event_class_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class_property
    ADD CONSTRAINT event_class_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 5928 (class 2606 OID 40755)
-- Name: event_class_property event_class_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class_property
    ADD CONSTRAINT event_class_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 5930 (class 2606 OID 34226)
-- Name: event_class_property event_class_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class_property
    ADD CONSTRAINT event_class_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5929 (class 2606 OID 34231)
-- Name: event_class_property event_class_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_class_property
    ADD CONSTRAINT event_class_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5914 (class 2606 OID 34044)
-- Name: event_definition event_definition_event_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition
    ADD CONSTRAINT event_definition_event_class_id_fkey FOREIGN KEY (event_class_id) REFERENCES common.event_class(id);


--
-- TOC entry 5939 (class 2606 OID 34258)
-- Name: event_definition_property event_definition_property_event_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition_property
    ADD CONSTRAINT event_definition_property_event_definition_id_fkey FOREIGN KEY (event_definition_id) REFERENCES common.event_definition(id);


--
-- TOC entry 5938 (class 2606 OID 34263)
-- Name: event_definition_property event_definition_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition_property
    ADD CONSTRAINT event_definition_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.event_definition_property(id);


--
-- TOC entry 5934 (class 2606 OID 40778)
-- Name: event_definition_property event_definition_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition_property
    ADD CONSTRAINT event_definition_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 5933 (class 2606 OID 40783)
-- Name: event_definition_property event_definition_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition_property
    ADD CONSTRAINT event_definition_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 5935 (class 2606 OID 40773)
-- Name: event_definition_property event_definition_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition_property
    ADD CONSTRAINT event_definition_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 5937 (class 2606 OID 34268)
-- Name: event_definition_property event_definition_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition_property
    ADD CONSTRAINT event_definition_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5936 (class 2606 OID 34273)
-- Name: event_definition_property event_definition_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_definition_property
    ADD CONSTRAINT event_definition_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5942 (class 2606 OID 34297)
-- Name: event_object_relationship event_object_relationship_event_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship
    ADD CONSTRAINT event_object_relationship_event_definition_id_fkey FOREIGN KEY (event_definition_id) REFERENCES common.event_definition(id);


--
-- TOC entry 5941 (class 2606 OID 34302)
-- Name: event_object_relationship event_object_relationship_event_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship
    ADD CONSTRAINT event_object_relationship_event_type_id_fkey FOREIGN KEY (event_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5940 (class 2606 OID 34307)
-- Name: event_object_relationship event_object_relationship_object_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship
    ADD CONSTRAINT event_object_relationship_object_type_id_fkey FOREIGN KEY (object_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6074 (class 2606 OID 35235)
-- Name: event_object_relationship_property event_object_relationship_pro_event_object_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship_property
    ADD CONSTRAINT event_object_relationship_pro_event_object_relationship_id_fkey FOREIGN KEY (event_object_relationship_id) REFERENCES common.event_object_relationship(id);


--
-- TOC entry 6073 (class 2606 OID 35240)
-- Name: event_object_relationship_property event_object_relationship_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship_property
    ADD CONSTRAINT event_object_relationship_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.event_object_relationship_property(id);


--
-- TOC entry 6069 (class 2606 OID 40796)
-- Name: event_object_relationship_property event_object_relationship_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship_property
    ADD CONSTRAINT event_object_relationship_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6068 (class 2606 OID 40801)
-- Name: event_object_relationship_property event_object_relationship_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship_property
    ADD CONSTRAINT event_object_relationship_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6070 (class 2606 OID 40791)
-- Name: event_object_relationship_property event_object_relationship_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship_property
    ADD CONSTRAINT event_object_relationship_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6072 (class 2606 OID 35245)
-- Name: event_object_relationship_property event_object_relationship_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship_property
    ADD CONSTRAINT event_object_relationship_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6071 (class 2606 OID 35250)
-- Name: event_object_relationship_property event_object_relationship_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.event_object_relationship_property
    ADD CONSTRAINT event_object_relationship_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6321 (class 2606 OID 37017)
-- Name: from_resource_reference_property from_resource_reference_propert_from_resource_reference_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference_property
    ADD CONSTRAINT from_resource_reference_propert_from_resource_reference_id_fkey FOREIGN KEY (from_resource_reference_id) REFERENCES common.from_resource_reference(id);


--
-- TOC entry 6320 (class 2606 OID 37022)
-- Name: from_resource_reference_property from_resource_reference_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference_property
    ADD CONSTRAINT from_resource_reference_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.from_resource_reference_property(id);


--
-- TOC entry 6316 (class 2606 OID 40616)
-- Name: from_resource_reference_property from_resource_reference_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference_property
    ADD CONSTRAINT from_resource_reference_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6315 (class 2606 OID 40621)
-- Name: from_resource_reference_property from_resource_reference_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference_property
    ADD CONSTRAINT from_resource_reference_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6317 (class 2606 OID 40611)
-- Name: from_resource_reference_property from_resource_reference_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference_property
    ADD CONSTRAINT from_resource_reference_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6319 (class 2606 OID 37027)
-- Name: from_resource_reference_property from_resource_reference_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference_property
    ADD CONSTRAINT from_resource_reference_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6318 (class 2606 OID 37032)
-- Name: from_resource_reference_property from_resource_reference_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference_property
    ADD CONSTRAINT from_resource_reference_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6193 (class 2606 OID 36148)
-- Name: from_resource_reference from_resource_reference_resource_network_connection_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference
    ADD CONSTRAINT from_resource_reference_resource_network_connection_id_fkey FOREIGN KEY (resource_network_connection_id) REFERENCES common.resource_network_connection(id);


--
-- TOC entry 6192 (class 2606 OID 36153)
-- Name: from_resource_reference from_resource_reference_resource_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.from_resource_reference
    ADD CONSTRAINT from_resource_reference_resource_type_id_fkey FOREIGN KEY (resource_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5911 (class 2606 OID 33859)
-- Name: knowledge_category knowledge_category_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_category
    ADD CONSTRAINT knowledge_category_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.knowledge_category(id);


--
-- TOC entry 5949 (class 2606 OID 34331)
-- Name: knowledge_property knowledge_property_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_property
    ADD CONSTRAINT knowledge_property_category_id_fkey FOREIGN KEY (category_id) REFERENCES common.knowledge_category(id);


--
-- TOC entry 5948 (class 2606 OID 34336)
-- Name: knowledge_property knowledge_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_property
    ADD CONSTRAINT knowledge_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.knowledge_property(id);


--
-- TOC entry 5944 (class 2606 OID 40598)
-- Name: knowledge_property knowledge_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_property
    ADD CONSTRAINT knowledge_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 5943 (class 2606 OID 40603)
-- Name: knowledge_property knowledge_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_property
    ADD CONSTRAINT knowledge_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 5945 (class 2606 OID 40593)
-- Name: knowledge_property knowledge_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_property
    ADD CONSTRAINT knowledge_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 5947 (class 2606 OID 34341)
-- Name: knowledge_property knowledge_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_property
    ADD CONSTRAINT knowledge_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5946 (class 2606 OID 34346)
-- Name: knowledge_property knowledge_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.knowledge_property
    ADD CONSTRAINT knowledge_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5916 (class 2606 OID 34066)
-- Name: lookup lookup_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.lookup
    ADD CONSTRAINT lookup_category_id_fkey FOREIGN KEY (category_id) REFERENCES common.lookup_category(id);


--
-- TOC entry 5915 (class 2606 OID 34071)
-- Name: lookup lookup_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.lookup
    ADD CONSTRAINT lookup_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.lookup(id);


--
-- TOC entry 6797 (class 2606 OID 41578)
-- Name: material_assembly_definition material_assembly_definition_assembled_from_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_assembly_definition
    ADD CONSTRAINT material_assembly_definition_assembled_from_id_fkey FOREIGN KEY (assembled_from_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6795 (class 2606 OID 41588)
-- Name: material_assembly_definition material_assembly_definition_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_assembly_definition
    ADD CONSTRAINT material_assembly_definition_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 6796 (class 2606 OID 41583)
-- Name: material_assembly_definition material_assembly_definition_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_assembly_definition
    ADD CONSTRAINT material_assembly_definition_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6798 (class 2606 OID 41573)
-- Name: material_assembly_definition material_assembly_definition_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_assembly_definition
    ADD CONSTRAINT material_assembly_definition_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 5952 (class 2606 OID 34371)
-- Name: material_class material_class_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class
    ADD CONSTRAINT material_class_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 5951 (class 2606 OID 34376)
-- Name: material_class material_class_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class
    ADD CONSTRAINT material_class_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6076 (class 2606 OID 35273)
-- Name: material_class_mapping material_class_mapping_material_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_mapping
    ADD CONSTRAINT material_class_mapping_material_class_id_fkey FOREIGN KEY (material_class_id) REFERENCES common.material_class(id);


--
-- TOC entry 6075 (class 2606 OID 35278)
-- Name: material_class_mapping material_class_mapping_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_mapping
    ADD CONSTRAINT material_class_mapping_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 5950 (class 2606 OID 34381)
-- Name: material_class material_class_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class
    ADD CONSTRAINT material_class_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_class(id);


--
-- TOC entry 6083 (class 2606 OID 35298)
-- Name: material_class_property material_class_property_material_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_property
    ADD CONSTRAINT material_class_property_material_class_id_fkey FOREIGN KEY (material_class_id) REFERENCES common.material_class(id);


--
-- TOC entry 6082 (class 2606 OID 35303)
-- Name: material_class_property material_class_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_property
    ADD CONSTRAINT material_class_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_class_property(id);


--
-- TOC entry 6078 (class 2606 OID 40490)
-- Name: material_class_property material_class_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_property
    ADD CONSTRAINT material_class_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6077 (class 2606 OID 40495)
-- Name: material_class_property material_class_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_property
    ADD CONSTRAINT material_class_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6079 (class 2606 OID 40485)
-- Name: material_class_property material_class_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_property
    ADD CONSTRAINT material_class_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6081 (class 2606 OID 35308)
-- Name: material_class_property material_class_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_property
    ADD CONSTRAINT material_class_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6080 (class 2606 OID 35313)
-- Name: material_class_property material_class_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_class_property
    ADD CONSTRAINT material_class_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5955 (class 2606 OID 34402)
-- Name: material_definition material_definition_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition
    ADD CONSTRAINT material_definition_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 5954 (class 2606 OID 34407)
-- Name: material_definition material_definition_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition
    ADD CONSTRAINT material_definition_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5953 (class 2606 OID 34412)
-- Name: material_definition material_definition_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition
    ADD CONSTRAINT material_definition_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6090 (class 2606 OID 35339)
-- Name: material_definition_property material_definition_property_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition_property
    ADD CONSTRAINT material_definition_property_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6089 (class 2606 OID 35344)
-- Name: material_definition_property material_definition_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition_property
    ADD CONSTRAINT material_definition_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_definition_property(id);


--
-- TOC entry 6085 (class 2606 OID 40418)
-- Name: material_definition_property material_definition_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition_property
    ADD CONSTRAINT material_definition_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6084 (class 2606 OID 40423)
-- Name: material_definition_property material_definition_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition_property
    ADD CONSTRAINT material_definition_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6086 (class 2606 OID 40413)
-- Name: material_definition_property material_definition_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition_property
    ADD CONSTRAINT material_definition_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6088 (class 2606 OID 35349)
-- Name: material_definition_property material_definition_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition_property
    ADD CONSTRAINT material_definition_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6087 (class 2606 OID 35354)
-- Name: material_definition_property material_definition_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_definition_property
    ADD CONSTRAINT material_definition_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6414 (class 2606 OID 37652)
-- Name: material_segment_specification_property material_segment_specificatio_material_segment_specificati_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specificatio_material_segment_specificati_fkey FOREIGN KEY (material_segment_specification_id) REFERENCES common.material_segment_specification(id);


--
-- TOC entry 6413 (class 2606 OID 37657)
-- Name: material_segment_specification_property material_segment_specificatio_common_material_bill_ite_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specificatio_common_material_bill_ite_fkey FOREIGN KEY (operations_material_bill_item_id) REFERENCES common.operations_material_bill_item(id);


--
-- TOC entry 6201 (class 2606 OID 36173)
-- Name: material_segment_specification material_segment_specification_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification
    ADD CONSTRAINT material_segment_specification_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 6200 (class 2606 OID 36178)
-- Name: material_segment_specification material_segment_specification_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification
    ADD CONSTRAINT material_segment_specification_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6199 (class 2606 OID 36183)
-- Name: material_segment_specification material_segment_specification_material_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification
    ADD CONSTRAINT material_segment_specification_material_class_id_fkey FOREIGN KEY (material_class_id) REFERENCES common.material_class(id);


--
-- TOC entry 6198 (class 2606 OID 36188)
-- Name: material_segment_specification material_segment_specification_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification
    ADD CONSTRAINT material_segment_specification_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6197 (class 2606 OID 36193)
-- Name: material_segment_specification material_segment_specification_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification
    ADD CONSTRAINT material_segment_specification_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_segment_specification(id);


--
-- TOC entry 6196 (class 2606 OID 36198)
-- Name: material_segment_specification material_segment_specification_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification
    ADD CONSTRAINT material_segment_specification_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6412 (class 2606 OID 37662)
-- Name: material_segment_specification_property material_segment_specification_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specification_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_segment_specification_property(id);


--
-- TOC entry 6411 (class 2606 OID 37667)
-- Name: material_segment_specification_property material_segment_specification_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specification_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6407 (class 2606 OID 40580)
-- Name: material_segment_specification_property material_segment_specification_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specification_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6406 (class 2606 OID 40585)
-- Name: material_segment_specification_property material_segment_specification_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specification_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6408 (class 2606 OID 40575)
-- Name: material_segment_specification_property material_segment_specification_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specification_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6410 (class 2606 OID 37672)
-- Name: material_segment_specification_property material_segment_specification_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specification_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6409 (class 2606 OID 37677)
-- Name: material_segment_specification_property material_segment_specification_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification_property
    ADD CONSTRAINT material_segment_specification_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6195 (class 2606 OID 36203)
-- Name: material_segment_specification material_segment_specification_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification
    ADD CONSTRAINT material_segment_specification_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6194 (class 2606 OID 36208)
-- Name: material_segment_specification material_segment_specification_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_segment_specification
    ADD CONSTRAINT material_segment_specification_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 5962 (class 2606 OID 34435)
-- Name: person_property person_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person_property
    ADD CONSTRAINT person_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.person_property(id);


--
-- TOC entry 5961 (class 2606 OID 34440)
-- Name: person_property person_property_person_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person_property
    ADD CONSTRAINT person_property_person_id_fkey FOREIGN KEY (person_id) REFERENCES common.person(id);


--
-- TOC entry 5957 (class 2606 OID 40382)
-- Name: person_property person_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person_property
    ADD CONSTRAINT person_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 5956 (class 2606 OID 40387)
-- Name: person_property person_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person_property
    ADD CONSTRAINT person_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 5958 (class 2606 OID 40377)
-- Name: person_property person_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person_property
    ADD CONSTRAINT person_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 5960 (class 2606 OID 34445)
-- Name: person_property person_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person_property
    ADD CONSTRAINT person_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5959 (class 2606 OID 34450)
-- Name: person_property person_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.person_property
    ADD CONSTRAINT person_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5918 (class 2606 OID 34092)
-- Name: personnel_class_mapping personnel_class_mapping_person_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_mapping
    ADD CONSTRAINT personnel_class_mapping_person_id_fkey FOREIGN KEY (person_id) REFERENCES common.person(id);


--
-- TOC entry 5917 (class 2606 OID 34097)
-- Name: personnel_class_mapping personnel_class_mapping_personnel_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_mapping
    ADD CONSTRAINT personnel_class_mapping_personnel_class_id_fkey FOREIGN KEY (personnel_class_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 5912 (class 2606 OID 33911)
-- Name: personnel_class personnel_class_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class
    ADD CONSTRAINT personnel_class_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 5969 (class 2606 OID 34476)
-- Name: personnel_class_property personnel_class_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_property
    ADD CONSTRAINT personnel_class_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.personnel_class_property(id);


--
-- TOC entry 5968 (class 2606 OID 34481)
-- Name: personnel_class_property personnel_class_property_personnel_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_property
    ADD CONSTRAINT personnel_class_property_personnel_class_id_fkey FOREIGN KEY (personnel_class_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 5964 (class 2606 OID 40454)
-- Name: personnel_class_property personnel_class_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_property
    ADD CONSTRAINT personnel_class_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 5963 (class 2606 OID 40459)
-- Name: personnel_class_property personnel_class_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_property
    ADD CONSTRAINT personnel_class_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 5965 (class 2606 OID 40449)
-- Name: personnel_class_property personnel_class_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_property
    ADD CONSTRAINT personnel_class_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 5967 (class 2606 OID 34486)
-- Name: personnel_class_property personnel_class_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_property
    ADD CONSTRAINT personnel_class_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5966 (class 2606 OID 34491)
-- Name: personnel_class_property personnel_class_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_class_property
    ADD CONSTRAINT personnel_class_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6328 (class 2606 OID 37063)
-- Name: personnel_segment_specification_property personnel_segment_specificati_personnel_segment_specificat_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property
    ADD CONSTRAINT personnel_segment_specificati_personnel_segment_specificat_fkey FOREIGN KEY (personnel_segment_specification_id) REFERENCES common.personnel_segment_specification(id);


--
-- TOC entry 6206 (class 2606 OID 36231)
-- Name: personnel_segment_specification personnel_segment_specification_person_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification
    ADD CONSTRAINT personnel_segment_specification_person_id_fkey FOREIGN KEY (person_id) REFERENCES common.person(id);


--
-- TOC entry 6205 (class 2606 OID 36236)
-- Name: personnel_segment_specification personnel_segment_specification_personnel_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification
    ADD CONSTRAINT personnel_segment_specification_personnel_class_id_fkey FOREIGN KEY (personnel_class_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 6204 (class 2606 OID 36241)
-- Name: personnel_segment_specification personnel_segment_specification_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification
    ADD CONSTRAINT personnel_segment_specification_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6329 (class 2606 OID 37058)
-- Name: personnel_segment_specification_property personnel_segment_specification_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property
    ADD CONSTRAINT personnel_segment_specification_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.personnel_segment_specification_property(id);


--
-- TOC entry 6327 (class 2606 OID 37068)
-- Name: personnel_segment_specification_property personnel_segment_specification_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property
    ADD CONSTRAINT personnel_segment_specification_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6323 (class 2606 OID 40544)
-- Name: personnel_segment_specification_property personnel_segment_specification_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property
    ADD CONSTRAINT personnel_segment_specification_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6322 (class 2606 OID 40549)
-- Name: personnel_segment_specification_property personnel_segment_specification_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property
    ADD CONSTRAINT personnel_segment_specification_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6324 (class 2606 OID 40539)
-- Name: personnel_segment_specification_property personnel_segment_specification_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property
    ADD CONSTRAINT personnel_segment_specification_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6326 (class 2606 OID 37073)
-- Name: personnel_segment_specification_property personnel_segment_specification_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property
    ADD CONSTRAINT personnel_segment_specification_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6325 (class 2606 OID 37078)
-- Name: personnel_segment_specification_property personnel_segment_specification_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification_property
    ADD CONSTRAINT personnel_segment_specification_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6203 (class 2606 OID 36246)
-- Name: personnel_segment_specification personnel_segment_specification_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification
    ADD CONSTRAINT personnel_segment_specification_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6202 (class 2606 OID 36251)
-- Name: personnel_segment_specification personnel_segment_specification_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_segment_specification
    ADD CONSTRAINT personnel_segment_specification_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6208 (class 2606 OID 36272)
-- Name: physical_asset_class_mapping physical_asset_class_mapping_physical_asset_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_mapping
    ADD CONSTRAINT physical_asset_class_mapping_physical_asset_class_id_fkey FOREIGN KEY (physical_asset_class_id) REFERENCES common.physical_asset_class(id);


--
-- TOC entry 6207 (class 2606 OID 36277)
-- Name: physical_asset_class_mapping physical_asset_class_mapping_physical_asset_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_mapping
    ADD CONSTRAINT physical_asset_class_mapping_physical_asset_id_fkey FOREIGN KEY (physical_asset_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 5913 (class 2606 OID 33933)
-- Name: physical_asset_class physical_asset_class_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class
    ADD CONSTRAINT physical_asset_class_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.physical_asset_class(id);


--
-- TOC entry 5976 (class 2606 OID 34517)
-- Name: physical_asset_class_property physical_asset_class_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_property
    ADD CONSTRAINT physical_asset_class_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.physical_asset_class_property(id);


--
-- TOC entry 5975 (class 2606 OID 34522)
-- Name: physical_asset_class_property physical_asset_class_property_physical_asset_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_property
    ADD CONSTRAINT physical_asset_class_property_physical_asset_class_id_fkey FOREIGN KEY (physical_asset_class_id) REFERENCES common.physical_asset_class(id);


--
-- TOC entry 5971 (class 2606 OID 40472)
-- Name: physical_asset_class_property physical_asset_class_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_property
    ADD CONSTRAINT physical_asset_class_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 5970 (class 2606 OID 40477)
-- Name: physical_asset_class_property physical_asset_class_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_property
    ADD CONSTRAINT physical_asset_class_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 5972 (class 2606 OID 40467)
-- Name: physical_asset_class_property physical_asset_class_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_property
    ADD CONSTRAINT physical_asset_class_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 5974 (class 2606 OID 34527)
-- Name: physical_asset_class_property physical_asset_class_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_property
    ADD CONSTRAINT physical_asset_class_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5973 (class 2606 OID 34532)
-- Name: physical_asset_class_property physical_asset_class_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_class_property
    ADD CONSTRAINT physical_asset_class_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6092 (class 2606 OID 35378)
-- Name: physical_asset physical_asset_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset
    ADD CONSTRAINT physical_asset_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 6091 (class 2606 OID 35383)
-- Name: physical_asset physical_asset_physical_location_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset
    ADD CONSTRAINT physical_asset_physical_location_id_fkey FOREIGN KEY (physical_location_id) REFERENCES common.equipment(id);


--
-- TOC entry 6215 (class 2606 OID 36297)
-- Name: physical_asset_property physical_asset_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_property
    ADD CONSTRAINT physical_asset_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.physical_asset_property(id);


--
-- TOC entry 6214 (class 2606 OID 36302)
-- Name: physical_asset_property physical_asset_property_physical_asset_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_property
    ADD CONSTRAINT physical_asset_property_physical_asset_id_fkey FOREIGN KEY (physical_asset_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 6210 (class 2606 OID 40400)
-- Name: physical_asset_property physical_asset_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_property
    ADD CONSTRAINT physical_asset_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6209 (class 2606 OID 40405)
-- Name: physical_asset_property physical_asset_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_property
    ADD CONSTRAINT physical_asset_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6211 (class 2606 OID 40395)
-- Name: physical_asset_property physical_asset_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_property
    ADD CONSTRAINT physical_asset_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6213 (class 2606 OID 36307)
-- Name: physical_asset_property physical_asset_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_property
    ADD CONSTRAINT physical_asset_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6212 (class 2606 OID 36312)
-- Name: physical_asset_property physical_asset_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_property
    ADD CONSTRAINT physical_asset_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6336 (class 2606 OID 37111)
-- Name: physical_asset_segment_specification_property physical_asset_segment_specif_physical_asset_segment_speci_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property
    ADD CONSTRAINT physical_asset_segment_specif_physical_asset_segment_speci_fkey FOREIGN KEY (physical_asset_segment_specification_id) REFERENCES common.physical_asset_segment_specification(id);


--
-- TOC entry 6220 (class 2606 OID 36336)
-- Name: physical_asset_segment_specification physical_asset_segment_specificati_physical_asset_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification
    ADD CONSTRAINT physical_asset_segment_specificati_physical_asset_class_id_fkey FOREIGN KEY (physical_asset_class_id) REFERENCES common.physical_asset_class(id);


--
-- TOC entry 6219 (class 2606 OID 36341)
-- Name: physical_asset_segment_specification physical_asset_segment_specification_physical_asset_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification
    ADD CONSTRAINT physical_asset_segment_specification_physical_asset_id_fkey FOREIGN KEY (physical_asset_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 6218 (class 2606 OID 36346)
-- Name: physical_asset_segment_specification physical_asset_segment_specification_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification
    ADD CONSTRAINT physical_asset_segment_specification_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6335 (class 2606 OID 37116)
-- Name: physical_asset_segment_specification_property physical_asset_segment_specification_prope_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property
    ADD CONSTRAINT physical_asset_segment_specification_prope_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6332 (class 2606 OID 40557)
-- Name: physical_asset_segment_specification_property physical_asset_segment_specification_prope_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property
    ADD CONSTRAINT physical_asset_segment_specification_prope_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6330 (class 2606 OID 40567)
-- Name: physical_asset_segment_specification_property physical_asset_segment_specification_proper_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property
    ADD CONSTRAINT physical_asset_segment_specification_proper_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6334 (class 2606 OID 37121)
-- Name: physical_asset_segment_specification_property physical_asset_segment_specification_propert_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property
    ADD CONSTRAINT physical_asset_segment_specification_propert_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6337 (class 2606 OID 37106)
-- Name: physical_asset_segment_specification_property physical_asset_segment_specification_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property
    ADD CONSTRAINT physical_asset_segment_specification_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.physical_asset_segment_specification_property(id);


--
-- TOC entry 6331 (class 2606 OID 40562)
-- Name: physical_asset_segment_specification_property physical_asset_segment_specification_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property
    ADD CONSTRAINT physical_asset_segment_specification_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6333 (class 2606 OID 37126)
-- Name: physical_asset_segment_specification_property physical_asset_segment_specification_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification_property
    ADD CONSTRAINT physical_asset_segment_specification_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6217 (class 2606 OID 36351)
-- Name: physical_asset_segment_specification physical_asset_segment_specification_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification
    ADD CONSTRAINT physical_asset_segment_specification_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6216 (class 2606 OID 36356)
-- Name: physical_asset_segment_specification physical_asset_segment_specification_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_segment_specification
    ADD CONSTRAINT physical_asset_segment_specification_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6225 (class 2606 OID 36378)
-- Name: process_segment_dependency process_segment_dependency_dependant_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_dependency
    ADD CONSTRAINT process_segment_dependency_dependant_segment_id_fkey FOREIGN KEY (dependant_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6224 (class 2606 OID 36383)
-- Name: process_segment_dependency process_segment_dependency_dependency_factor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_dependency
    ADD CONSTRAINT process_segment_dependency_dependency_factor_id_fkey FOREIGN KEY (dependency_factor_id) REFERENCES common.lookup(id);


--
-- TOC entry 6223 (class 2606 OID 36388)
-- Name: process_segment_dependency process_segment_dependency_dependency_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_dependency
    ADD CONSTRAINT process_segment_dependency_dependency_type_id_fkey FOREIGN KEY (dependency_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6222 (class 2606 OID 36393)
-- Name: process_segment_dependency process_segment_dependency_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_dependency
    ADD CONSTRAINT process_segment_dependency_segment_id_fkey FOREIGN KEY (segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6221 (class 2606 OID 36398)
-- Name: process_segment_dependency process_segment_dependency_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_dependency
    ADD CONSTRAINT process_segment_dependency_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6096 (class 2606 OID 35404)
-- Name: process_segment process_segment_duration_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment
    ADD CONSTRAINT process_segment_duration_uom_id_fkey FOREIGN KEY (duration_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6095 (class 2606 OID 35409)
-- Name: process_segment process_segment_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment
    ADD CONSTRAINT process_segment_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6094 (class 2606 OID 35414)
-- Name: process_segment process_segment_common_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment
    ADD CONSTRAINT process_segment_common_type_id_fkey FOREIGN KEY (operations_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6232 (class 2606 OID 36422)
-- Name: process_segment_parameter process_segment_parameter_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_parameter
    ADD CONSTRAINT process_segment_parameter_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.process_segment_parameter(id);


--
-- TOC entry 6231 (class 2606 OID 36427)
-- Name: process_segment_parameter process_segment_parameter_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_parameter
    ADD CONSTRAINT process_segment_parameter_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6227 (class 2606 OID 40508)
-- Name: process_segment_parameter process_segment_parameter_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_parameter
    ADD CONSTRAINT process_segment_parameter_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6226 (class 2606 OID 40513)
-- Name: process_segment_parameter process_segment_parameter_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_parameter
    ADD CONSTRAINT process_segment_parameter_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6228 (class 2606 OID 40503)
-- Name: process_segment_parameter process_segment_parameter_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_parameter
    ADD CONSTRAINT process_segment_parameter_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6230 (class 2606 OID 36432)
-- Name: process_segment_parameter process_segment_parameter_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_parameter
    ADD CONSTRAINT process_segment_parameter_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6229 (class 2606 OID 36437)
-- Name: process_segment_parameter process_segment_parameter_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment_parameter
    ADD CONSTRAINT process_segment_parameter_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6093 (class 2606 OID 35419)
-- Name: process_segment process_segment_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.process_segment
    ADD CONSTRAINT process_segment_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6238 (class 2606 OID 36473)
-- Name: resource_network_connection_property resource_network_connection__resource_network_connection__fkey1; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property
    ADD CONSTRAINT resource_network_connection__resource_network_connection__fkey1 FOREIGN KEY (resource_network_connection_type_property_id) REFERENCES common.resource_network_connection_type_property(id);


--
-- TOC entry 6239 (class 2606 OID 36468)
-- Name: resource_network_connection_property resource_network_connection_p_resource_network_connection__fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property
    ADD CONSTRAINT resource_network_connection_p_resource_network_connection__fkey FOREIGN KEY (resource_network_connection_id) REFERENCES common.resource_network_connection(id);


--
-- TOC entry 6240 (class 2606 OID 36463)
-- Name: resource_network_connection_property resource_network_connection_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property
    ADD CONSTRAINT resource_network_connection_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.resource_network_connection_property(id);


--
-- TOC entry 6234 (class 2606 OID 40652)
-- Name: resource_network_connection_property resource_network_connection_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property
    ADD CONSTRAINT resource_network_connection_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6233 (class 2606 OID 40657)
-- Name: resource_network_connection_property resource_network_connection_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property
    ADD CONSTRAINT resource_network_connection_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6235 (class 2606 OID 40647)
-- Name: resource_network_connection_property resource_network_connection_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property
    ADD CONSTRAINT resource_network_connection_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6237 (class 2606 OID 36478)
-- Name: resource_network_connection_property resource_network_connection_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property
    ADD CONSTRAINT resource_network_connection_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6236 (class 2606 OID 36483)
-- Name: resource_network_connection_property resource_network_connection_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_property
    ADD CONSTRAINT resource_network_connection_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6098 (class 2606 OID 35441)
-- Name: resource_network_connection resource_network_connection_resource_network_connection_ty_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection
    ADD CONSTRAINT resource_network_connection_resource_network_connection_ty_fkey FOREIGN KEY (resource_network_connection_type_id) REFERENCES common.resource_network_connection_type(id);


--
-- TOC entry 6097 (class 2606 OID 35446)
-- Name: resource_network_connection resource_network_connection_resource_relationship_network__fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection
    ADD CONSTRAINT resource_network_connection_resource_relationship_network__fkey FOREIGN KEY (resource_relationship_network_id) REFERENCES common.resource_relationship_network(id);


--
-- TOC entry 5982 (class 2606 OID 34563)
-- Name: resource_network_connection_type_property resource_network_connection_t_resource_network_connection__fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type_property
    ADD CONSTRAINT resource_network_connection_t_resource_network_connection__fkey FOREIGN KEY (resource_network_connection_type_id) REFERENCES common.resource_network_connection_type(id);


--
-- TOC entry 5983 (class 2606 OID 34558)
-- Name: resource_network_connection_type_property resource_network_connection_type_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type_property
    ADD CONSTRAINT resource_network_connection_type_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.resource_network_connection_type_property(id);


--
-- TOC entry 5978 (class 2606 OID 40670)
-- Name: resource_network_connection_type_property resource_network_connection_type_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type_property
    ADD CONSTRAINT resource_network_connection_type_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 5977 (class 2606 OID 40675)
-- Name: resource_network_connection_type_property resource_network_connection_type_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type_property
    ADD CONSTRAINT resource_network_connection_type_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 5979 (class 2606 OID 40665)
-- Name: resource_network_connection_type_property resource_network_connection_type_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type_property
    ADD CONSTRAINT resource_network_connection_type_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 5981 (class 2606 OID 34568)
-- Name: resource_network_connection_type_property resource_network_connection_type_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type_property
    ADD CONSTRAINT resource_network_connection_type_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5980 (class 2606 OID 34573)
-- Name: resource_network_connection_type_property resource_network_connection_type_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_network_connection_type_property
    ADD CONSTRAINT resource_network_connection_type_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5985 (class 2606 OID 34597)
-- Name: resource_relationship_network resource_relationship_network_relationship_form_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_relationship_network
    ADD CONSTRAINT resource_relationship_network_relationship_form_id_fkey FOREIGN KEY (relationship_form_id) REFERENCES common.lookup(id);


--
-- TOC entry 5984 (class 2606 OID 34602)
-- Name: resource_relationship_network resource_relationship_network_relationship_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.resource_relationship_network
    ADD CONSTRAINT resource_relationship_network_relationship_type_id_fkey FOREIGN KEY (relationship_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6344 (class 2606 OID 37154)
-- Name: to_resource_reference_property to_resource_reference_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference_property
    ADD CONSTRAINT to_resource_reference_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.to_resource_reference_property(id);


--
-- TOC entry 6339 (class 2606 OID 40634)
-- Name: to_resource_reference_property to_resource_reference_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference_property
    ADD CONSTRAINT to_resource_reference_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6338 (class 2606 OID 40639)
-- Name: to_resource_reference_property to_resource_reference_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference_property
    ADD CONSTRAINT to_resource_reference_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6343 (class 2606 OID 37159)
-- Name: to_resource_reference_property to_resource_reference_property_to_resource_reference_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference_property
    ADD CONSTRAINT to_resource_reference_property_to_resource_reference_id_fkey FOREIGN KEY (to_resource_reference_id) REFERENCES common.to_resource_reference(id);


--
-- TOC entry 6340 (class 2606 OID 40629)
-- Name: to_resource_reference_property to_resource_reference_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference_property
    ADD CONSTRAINT to_resource_reference_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6342 (class 2606 OID 37164)
-- Name: to_resource_reference_property to_resource_reference_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference_property
    ADD CONSTRAINT to_resource_reference_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6341 (class 2606 OID 37169)
-- Name: to_resource_reference_property to_resource_reference_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference_property
    ADD CONSTRAINT to_resource_reference_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6242 (class 2606 OID 36507)
-- Name: to_resource_reference to_resource_reference_resource_network_connection_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference
    ADD CONSTRAINT to_resource_reference_resource_network_connection_id_fkey FOREIGN KEY (resource_network_connection_id) REFERENCES common.resource_network_connection(id);


--
-- TOC entry 6241 (class 2606 OID 36512)
-- Name: to_resource_reference to_resource_reference_resource_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.to_resource_reference
    ADD CONSTRAINT to_resource_reference_resource_type_id_fkey FOREIGN KEY (resource_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5919 (class 2606 OID 34115)
-- Name: uom uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.uom
    ADD CONSTRAINT uom_category_id_fkey FOREIGN KEY (category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6102 (class 2606 OID 35483)
-- Name: work_calendar_definition_entry_property work_calendar_definition_entr_work_calendar_definition_ent_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry_property
    ADD CONSTRAINT work_calendar_definition_entr_work_calendar_definition_ent_fkey FOREIGN KEY (work_calendar_definition_entry_id) REFERENCES common.work_calendar_definition_entry(id);


--
-- TOC entry 5988 (class 2606 OID 34622)
-- Name: work_calendar_definition_entry work_calendar_definition_entry_entry_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry
    ADD CONSTRAINT work_calendar_definition_entry_entry_type_id_fkey FOREIGN KEY (entry_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5987 (class 2606 OID 34627)
-- Name: work_calendar_definition_entry work_calendar_definition_entry_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry
    ADD CONSTRAINT work_calendar_definition_entry_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_calendar_definition_entry(id);


--
-- TOC entry 6105 (class 2606 OID 35468)
-- Name: work_calendar_definition_entry_property work_calendar_definition_entry_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry_property
    ADD CONSTRAINT work_calendar_definition_entry_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_calendar_definition_entry_property(id);


--
-- TOC entry 6100 (class 2606 OID 40706)
-- Name: work_calendar_definition_entry_property work_calendar_definition_entry_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry_property
    ADD CONSTRAINT work_calendar_definition_entry_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6099 (class 2606 OID 40711)
-- Name: work_calendar_definition_entry_property work_calendar_definition_entry_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry_property
    ADD CONSTRAINT work_calendar_definition_entry_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6101 (class 2606 OID 40701)
-- Name: work_calendar_definition_entry_property work_calendar_definition_entry_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry_property
    ADD CONSTRAINT work_calendar_definition_entry_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6104 (class 2606 OID 35473)
-- Name: work_calendar_definition_entry_property work_calendar_definition_entry_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry_property
    ADD CONSTRAINT work_calendar_definition_entry_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6103 (class 2606 OID 35478)
-- Name: work_calendar_definition_entry_property work_calendar_definition_entry_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry_property
    ADD CONSTRAINT work_calendar_definition_entry_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5986 (class 2606 OID 34632)
-- Name: work_calendar_definition_entry work_calendar_definition_entry_work_calendar_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_entry
    ADD CONSTRAINT work_calendar_definition_entry_work_calendar_definition_id_fkey FOREIGN KEY (work_calendar_definition_id) REFERENCES common.work_calendar_definition(id);


--
-- TOC entry 5992 (class 2606 OID 34670)
-- Name: work_calendar_definition_property work_calendar_definition_prope_work_calendar_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_property
    ADD CONSTRAINT work_calendar_definition_prope_work_calendar_definition_id_fkey FOREIGN KEY (work_calendar_definition_id) REFERENCES common.work_calendar_definition(id);


--
-- TOC entry 5995 (class 2606 OID 34655)
-- Name: work_calendar_definition_property work_calendar_definition_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_property
    ADD CONSTRAINT work_calendar_definition_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_calendar_definition_property(id);


--
-- TOC entry 5990 (class 2606 OID 40724)
-- Name: work_calendar_definition_property work_calendar_definition_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_property
    ADD CONSTRAINT work_calendar_definition_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 5989 (class 2606 OID 40729)
-- Name: work_calendar_definition_property work_calendar_definition_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_property
    ADD CONSTRAINT work_calendar_definition_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 5991 (class 2606 OID 40719)
-- Name: work_calendar_definition_property work_calendar_definition_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_property
    ADD CONSTRAINT work_calendar_definition_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 5994 (class 2606 OID 34660)
-- Name: work_calendar_definition_property work_calendar_definition_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_property
    ADD CONSTRAINT work_calendar_definition_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 5993 (class 2606 OID 34665)
-- Name: work_calendar_definition_property work_calendar_definition_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_definition_property
    ADD CONSTRAINT work_calendar_definition_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6109 (class 2606 OID 35507)
-- Name: work_calendar_entry work_calendar_entry_entry_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry
    ADD CONSTRAINT work_calendar_entry_entry_type_id_fkey FOREIGN KEY (entry_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6108 (class 2606 OID 35512)
-- Name: work_calendar_entry work_calendar_entry_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry
    ADD CONSTRAINT work_calendar_entry_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_calendar_entry(id);


--
-- TOC entry 6250 (class 2606 OID 36534)
-- Name: work_calendar_entry_property work_calendar_entry_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property
    ADD CONSTRAINT work_calendar_entry_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_calendar_entry_property(id);


--
-- TOC entry 6244 (class 2606 OID 40742)
-- Name: work_calendar_entry_property work_calendar_entry_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property
    ADD CONSTRAINT work_calendar_entry_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6243 (class 2606 OID 40747)
-- Name: work_calendar_entry_property work_calendar_entry_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property
    ADD CONSTRAINT work_calendar_entry_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6245 (class 2606 OID 40737)
-- Name: work_calendar_entry_property work_calendar_entry_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property
    ADD CONSTRAINT work_calendar_entry_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6249 (class 2606 OID 36539)
-- Name: work_calendar_entry_property work_calendar_entry_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property
    ADD CONSTRAINT work_calendar_entry_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6248 (class 2606 OID 36544)
-- Name: work_calendar_entry_property work_calendar_entry_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property
    ADD CONSTRAINT work_calendar_entry_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6247 (class 2606 OID 36549)
-- Name: work_calendar_entry_property work_calendar_entry_property_work_calendar_definition_entr_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property
    ADD CONSTRAINT work_calendar_entry_property_work_calendar_definition_entr_fkey FOREIGN KEY (work_calendar_definition_entry_property_id) REFERENCES common.work_calendar_definition_entry_property(id);


--
-- TOC entry 6246 (class 2606 OID 36554)
-- Name: work_calendar_entry_property work_calendar_entry_property_work_calendar_entry_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry_property
    ADD CONSTRAINT work_calendar_entry_property_work_calendar_entry_id_fkey FOREIGN KEY (work_calendar_entry_id) REFERENCES common.work_calendar_entry(id);


--
-- TOC entry 6107 (class 2606 OID 35517)
-- Name: work_calendar_entry work_calendar_entry_work_calendar_definition_entry_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry
    ADD CONSTRAINT work_calendar_entry_work_calendar_definition_entry_id_fkey FOREIGN KEY (work_calendar_definition_entry_id) REFERENCES common.work_calendar_definition_entry(id);


--
-- TOC entry 6106 (class 2606 OID 35522)
-- Name: work_calendar_entry work_calendar_entry_work_calendar_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_entry
    ADD CONSTRAINT work_calendar_entry_work_calendar_id_fkey FOREIGN KEY (work_calendar_id) REFERENCES common.work_calendar(id);


--
-- TOC entry 6002 (class 2606 OID 34696)
-- Name: work_calendar_property work_calendar_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_property
    ADD CONSTRAINT work_calendar_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.work_calendar_property(id);


--
-- TOC entry 5997 (class 2606 OID 40688)
-- Name: work_calendar_property work_calendar_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_property
    ADD CONSTRAINT work_calendar_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 5996 (class 2606 OID 40693)
-- Name: work_calendar_property work_calendar_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_property
    ADD CONSTRAINT work_calendar_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 5998 (class 2606 OID 40683)
-- Name: work_calendar_property work_calendar_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_property
    ADD CONSTRAINT work_calendar_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6001 (class 2606 OID 34701)
-- Name: work_calendar_property work_calendar_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_property
    ADD CONSTRAINT work_calendar_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6000 (class 2606 OID 34706)
-- Name: work_calendar_property work_calendar_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_property
    ADD CONSTRAINT work_calendar_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 5999 (class 2606 OID 34711)
-- Name: work_calendar_property work_calendar_property_work_calendar_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_calendar_property
    ADD CONSTRAINT work_calendar_property_work_calendar_id_fkey FOREIGN KEY (work_calendar_id) REFERENCES common.work_calendar(id);


--
-- TOC entry 6790 (class 2606 OID 40170)
-- Name: privilege privilege_app_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_privilege
    ADD CONSTRAINT privilege_app_id_fkey FOREIGN KEY (app_id) REFERENCES common.lookup(id);


--
-- TOC entry 6792 (class 2606 OID 40189)
-- Name: role_privileges role_privileges_privilege_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_role_privileges
    ADD CONSTRAINT role_privileges_privilege_id_fkey FOREIGN KEY (privilege_id) REFERENCES common.au_privilege(id);


--
-- TOC entry 6791 (class 2606 OID 40194)
-- Name: role_privileges role_privileges_role_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_role_privileges
    ADD CONSTRAINT role_privileges_role_id_fkey FOREIGN KEY (role_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 6794 (class 2606 OID 40211)
-- Name: user_restricted_privileges user_restricted_privileges_privilege_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_user_restricted_privileges
    ADD CONSTRAINT user_restricted_privileges_privilege_id_fkey FOREIGN KEY (privilege_id) REFERENCES common.au_privilege(id);


--
-- TOC entry 6793 (class 2606 OID 40216)
-- Name: user_restricted_privileges user_restricted_privileges_user_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.au_user_restricted_privileges
    ADD CONSTRAINT user_restricted_privileges_user_id_fkey FOREIGN KEY (user_id) REFERENCES common.person(id);


--
-- TOC entry 6420 (class 2606 OID 37703)
-- Name: equipment_specification equipment_specification_equipment_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification
    ADD CONSTRAINT equipment_specification_equipment_class_id_fkey FOREIGN KEY (equipment_class_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6419 (class 2606 OID 37708)
-- Name: equipment_specification equipment_specification_equipment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification
    ADD CONSTRAINT equipment_specification_equipment_id_fkey FOREIGN KEY (equipment_id) REFERENCES common.equipment(id);


--
-- TOC entry 6418 (class 2606 OID 37713)
-- Name: equipment_specification equipment_specification_operation_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification
    ADD CONSTRAINT equipment_specification_operation_segment_id_fkey FOREIGN KEY (operations_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6541 (class 2606 OID 38540)
-- Name: equipment_specification_property equipment_specification_propert_equipment_specification_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property
    ADD CONSTRAINT equipment_specification_propert_equipment_specification_id_fkey FOREIGN KEY (equipment_specification_id) REFERENCES common.equipment_specification(id);


--
-- TOC entry 6540 (class 2606 OID 38545)
-- Name: equipment_specification_property equipment_specification_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property
    ADD CONSTRAINT equipment_specification_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.equipment_specification_property(id);


--
-- TOC entry 6539 (class 2606 OID 38550)
-- Name: equipment_specification_property equipment_specification_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property
    ADD CONSTRAINT equipment_specification_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6535 (class 2606 OID 41372)
-- Name: equipment_specification_property equipment_specification_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property
    ADD CONSTRAINT equipment_specification_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6534 (class 2606 OID 41377)
-- Name: equipment_specification_property equipment_specification_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property
    ADD CONSTRAINT equipment_specification_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6536 (class 2606 OID 41367)
-- Name: equipment_specification_property equipment_specification_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property
    ADD CONSTRAINT equipment_specification_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6538 (class 2606 OID 38555)
-- Name: equipment_specification_property equipment_specification_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property
    ADD CONSTRAINT equipment_specification_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6537 (class 2606 OID 38560)
-- Name: equipment_specification_property equipment_specification_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification_property
    ADD CONSTRAINT equipment_specification_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6417 (class 2606 OID 37718)
-- Name: equipment_specification equipment_specification_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification
    ADD CONSTRAINT equipment_specification_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6416 (class 2606 OID 37723)
-- Name: equipment_specification equipment_specification_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification
    ADD CONSTRAINT equipment_specification_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6415 (class 2606 OID 37728)
-- Name: equipment_specification equipment_specification_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.equipment_specification
    ADD CONSTRAINT equipment_specification_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


--
-- TOC entry 6430 (class 2606 OID 37750)
-- Name: material_specification material_specification_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 6429 (class 2606 OID 37755)
-- Name: material_specification material_specification_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6428 (class 2606 OID 37760)
-- Name: material_specification material_specification_material_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_material_class_id_fkey FOREIGN KEY (material_class_id) REFERENCES common.material_class(id);


--
-- TOC entry 6427 (class 2606 OID 37765)
-- Name: material_specification material_specification_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6426 (class 2606 OID 37770)
-- Name: material_specification material_specification_operation_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_operation_segment_id_fkey FOREIGN KEY (operations_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6425 (class 2606 OID 37775)
-- Name: material_specification material_specification_common_material_bill_item_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_common_material_bill_item_id_fkey FOREIGN KEY (operations_material_bill_item_id) REFERENCES common.operations_material_bill_item(id);


--
-- TOC entry 6424 (class 2606 OID 37780)
-- Name: material_specification material_specification_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_specification(id);


--
-- TOC entry 6549 (class 2606 OID 38588)
-- Name: material_specification_property material_specification_property_material_specification_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property
    ADD CONSTRAINT material_specification_property_material_specification_id_fkey FOREIGN KEY (material_specification_id) REFERENCES common.material_specification(id);


--
-- TOC entry 6548 (class 2606 OID 38593)
-- Name: material_specification_property material_specification_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property
    ADD CONSTRAINT material_specification_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.material_specification_property(id);


--
-- TOC entry 6547 (class 2606 OID 38598)
-- Name: material_specification_property material_specification_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property
    ADD CONSTRAINT material_specification_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6543 (class 2606 OID 41426)
-- Name: material_specification_property material_specification_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property
    ADD CONSTRAINT material_specification_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6542 (class 2606 OID 41431)
-- Name: material_specification_property material_specification_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property
    ADD CONSTRAINT material_specification_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6544 (class 2606 OID 41421)
-- Name: material_specification_property material_specification_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property
    ADD CONSTRAINT material_specification_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6546 (class 2606 OID 38603)
-- Name: material_specification_property material_specification_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property
    ADD CONSTRAINT material_specification_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6545 (class 2606 OID 38608)
-- Name: material_specification_property material_specification_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification_property
    ADD CONSTRAINT material_specification_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6423 (class 2606 OID 37785)
-- Name: material_specification material_specification_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6422 (class 2606 OID 37790)
-- Name: material_specification material_specification_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6421 (class 2606 OID 37795)
-- Name: material_specification material_specification_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.material_specification
    ADD CONSTRAINT material_specification_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


--
-- TOC entry 6111 (class 2606 OID 35543)
-- Name: operations_definition operations_definition_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition
    ADD CONSTRAINT operations_definition_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6110 (class 2606 OID 35548)
-- Name: operations_definition operations_definition_common_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition
    ADD CONSTRAINT operations_definition_common_type_id_fkey FOREIGN KEY (operations_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6257 (class 2606 OID 36580)
-- Name: operations_definition_property operations_definition_property_common_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition_property
    ADD CONSTRAINT operations_definition_property_common_definition_id_fkey FOREIGN KEY (operations_definition_id) REFERENCES common.operations_definition(id);


--
-- TOC entry 6256 (class 2606 OID 36585)
-- Name: operations_definition_property operations_definition_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition_property
    ADD CONSTRAINT operations_definition_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_definition_property(id);


--
-- TOC entry 6252 (class 2606 OID 41444)
-- Name: operations_definition_property operations_definition_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition_property
    ADD CONSTRAINT operations_definition_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6251 (class 2606 OID 41449)
-- Name: operations_definition_property operations_definition_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition_property
    ADD CONSTRAINT operations_definition_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6253 (class 2606 OID 41439)
-- Name: operations_definition_property operations_definition_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition_property
    ADD CONSTRAINT operations_definition_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6255 (class 2606 OID 36590)
-- Name: operations_definition_property operations_definition_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition_property
    ADD CONSTRAINT operations_definition_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6254 (class 2606 OID 36595)
-- Name: operations_definition_property operations_definition_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_definition_property
    ADD CONSTRAINT operations_definition_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6352 (class 2606 OID 37193)
-- Name: operations_material_bill_item operations_material_bill_item_assembly_relationship_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item
    ADD CONSTRAINT operations_material_bill_item_assembly_relationship_id_fkey FOREIGN KEY (assembly_relationship_id) REFERENCES common.lookup(id);


--
-- TOC entry 6351 (class 2606 OID 37198)
-- Name: operations_material_bill_item operations_material_bill_item_assembly_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item
    ADD CONSTRAINT operations_material_bill_item_assembly_type_id_fkey FOREIGN KEY (assembly_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6350 (class 2606 OID 37203)
-- Name: operations_material_bill_item operations_material_bill_item_material_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item
    ADD CONSTRAINT operations_material_bill_item_material_class_id_fkey FOREIGN KEY (material_class_id) REFERENCES common.material_class(id);


--
-- TOC entry 6349 (class 2606 OID 37208)
-- Name: operations_material_bill_item operations_material_bill_item_material_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item
    ADD CONSTRAINT operations_material_bill_item_material_definition_id_fkey FOREIGN KEY (material_definition_id) REFERENCES common.material_definition(id);


--
-- TOC entry 6348 (class 2606 OID 37213)
-- Name: operations_material_bill_item operations_material_bill_item_common_material_bill_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item
    ADD CONSTRAINT operations_material_bill_item_common_material_bill_id_fkey FOREIGN KEY (operations_material_bill_id) REFERENCES common.operations_material_bill(id);


--
-- TOC entry 6347 (class 2606 OID 37218)
-- Name: operations_material_bill_item operations_material_bill_item_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item
    ADD CONSTRAINT operations_material_bill_item_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_material_bill_item(id);


--
-- TOC entry 6346 (class 2606 OID 37223)
-- Name: operations_material_bill_item operations_material_bill_item_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item
    ADD CONSTRAINT operations_material_bill_item_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6345 (class 2606 OID 37228)
-- Name: operations_material_bill_item operations_material_bill_item_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill_item
    ADD CONSTRAINT operations_material_bill_item_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6258 (class 2606 OID 36619)
-- Name: operations_material_bill operations_material_bill_common_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_material_bill
    ADD CONSTRAINT operations_material_bill_common_definition_id_fkey FOREIGN KEY (operations_definition_id) REFERENCES common.operations_definition(id);


--
-- TOC entry 6357 (class 2606 OID 37251)
-- Name: operations_segment_dependency operations_segment_dependency_dependant_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_dependency
    ADD CONSTRAINT operations_segment_dependency_dependant_segment_id_fkey FOREIGN KEY (dependant_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6356 (class 2606 OID 37256)
-- Name: operations_segment_dependency operations_segment_dependency_dependency_factor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_dependency
    ADD CONSTRAINT operations_segment_dependency_dependency_factor_id_fkey FOREIGN KEY (dependency_factor_id) REFERENCES common.lookup(id);


--
-- TOC entry 6355 (class 2606 OID 37261)
-- Name: operations_segment_dependency operations_segment_dependency_dependency_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_dependency
    ADD CONSTRAINT operations_segment_dependency_dependency_type_id_fkey FOREIGN KEY (dependency_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6354 (class 2606 OID 37266)
-- Name: operations_segment_dependency operations_segment_dependency_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_dependency
    ADD CONSTRAINT operations_segment_dependency_segment_id_fkey FOREIGN KEY (segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6353 (class 2606 OID 37271)
-- Name: operations_segment_dependency operations_segment_dependency_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_dependency
    ADD CONSTRAINT operations_segment_dependency_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6264 (class 2606 OID 36639)
-- Name: operations_segment operations_segment_duration_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment
    ADD CONSTRAINT operations_segment_duration_uom_id_fkey FOREIGN KEY (duration_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6263 (class 2606 OID 36644)
-- Name: operations_segment operations_segment_hierarchy_scope_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment
    ADD CONSTRAINT operations_segment_hierarchy_scope_id_fkey FOREIGN KEY (hierarchy_scope_id) REFERENCES common.equipment(id);


--
-- TOC entry 6359 (class 2606 OID 37292)
-- Name: operations_segment_mapping operations_segment_mapping_common_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_mapping
    ADD CONSTRAINT operations_segment_mapping_common_segment_id_fkey FOREIGN KEY (operations_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6358 (class 2606 OID 37297)
-- Name: operations_segment_mapping operations_segment_mapping_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_mapping
    ADD CONSTRAINT operations_segment_mapping_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6262 (class 2606 OID 36649)
-- Name: operations_segment operations_segment_common_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment
    ADD CONSTRAINT operations_segment_common_definition_id_fkey FOREIGN KEY (operations_definition_id) REFERENCES common.operations_definition(id);


--
-- TOC entry 6261 (class 2606 OID 36654)
-- Name: operations_segment operations_segment_common_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment
    ADD CONSTRAINT operations_segment_common_type_id_fkey FOREIGN KEY (operations_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6366 (class 2606 OID 37317)
-- Name: operations_segment_parameter_specification operations_segment_parameter_specific_operation_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_parameter_specification
    ADD CONSTRAINT operations_segment_parameter_specific_operation_segment_id_fkey FOREIGN KEY (operations_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6365 (class 2606 OID 37322)
-- Name: operations_segment_parameter_specification operations_segment_parameter_specification_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_parameter_specification
    ADD CONSTRAINT operations_segment_parameter_specification_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_segment_parameter_specification(id);


--
-- TOC entry 6361 (class 2606 OID 41462)
-- Name: operations_segment_parameter_specification operations_segment_parameter_specification_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_parameter_specification
    ADD CONSTRAINT operations_segment_parameter_specification_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6360 (class 2606 OID 41467)
-- Name: operations_segment_parameter_specification operations_segment_parameter_specification_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_parameter_specification
    ADD CONSTRAINT operations_segment_parameter_specification_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6362 (class 2606 OID 41457)
-- Name: operations_segment_parameter_specification operations_segment_parameter_specification_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_parameter_specification
    ADD CONSTRAINT operations_segment_parameter_specification_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6364 (class 2606 OID 37327)
-- Name: operations_segment_parameter_specification operations_segment_parameter_specification_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_parameter_specification
    ADD CONSTRAINT operations_segment_parameter_specification_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6363 (class 2606 OID 37332)
-- Name: operations_segment_parameter_specification operations_segment_parameter_specification_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment_parameter_specification
    ADD CONSTRAINT operations_segment_parameter_specification_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6260 (class 2606 OID 36659)
-- Name: operations_segment operations_segment_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment
    ADD CONSTRAINT operations_segment_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6259 (class 2606 OID 36664)
-- Name: operations_segment operations_segment_process_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.operations_segment
    ADD CONSTRAINT operations_segment_process_segment_id_fkey FOREIGN KEY (process_segment_id) REFERENCES common.process_segment(id);


--
-- TOC entry 6436 (class 2606 OID 37818)
-- Name: personnel_specification personnel_specification_operation_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification
    ADD CONSTRAINT personnel_specification_operation_segment_id_fkey FOREIGN KEY (operations_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6435 (class 2606 OID 37823)
-- Name: personnel_specification personnel_specification_person_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification
    ADD CONSTRAINT personnel_specification_person_id_fkey FOREIGN KEY (person_id) REFERENCES common.person(id);


--
-- TOC entry 6434 (class 2606 OID 37828)
-- Name: personnel_specification personnel_specification_personnel_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification
    ADD CONSTRAINT personnel_specification_personnel_class_id_fkey FOREIGN KEY (personnel_class_id) REFERENCES common.personnel_class(id);


--
-- TOC entry 6556 (class 2606 OID 38641)
-- Name: personnel_specification_property personnel_specification_propert_personnel_specification_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property
    ADD CONSTRAINT personnel_specification_propert_personnel_specification_id_fkey FOREIGN KEY (personnel_specification_id) REFERENCES common.personnel_specification(id);


--
-- TOC entry 6557 (class 2606 OID 38636)
-- Name: personnel_specification_property personnel_specification_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property
    ADD CONSTRAINT personnel_specification_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.personnel_specification_property(id);


--
-- TOC entry 6555 (class 2606 OID 38646)
-- Name: personnel_specification_property personnel_specification_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property
    ADD CONSTRAINT personnel_specification_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6551 (class 2606 OID 41390)
-- Name: personnel_specification_property personnel_specification_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property
    ADD CONSTRAINT personnel_specification_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6550 (class 2606 OID 41395)
-- Name: personnel_specification_property personnel_specification_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property
    ADD CONSTRAINT personnel_specification_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6552 (class 2606 OID 41385)
-- Name: personnel_specification_property personnel_specification_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property
    ADD CONSTRAINT personnel_specification_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6554 (class 2606 OID 38651)
-- Name: personnel_specification_property personnel_specification_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property
    ADD CONSTRAINT personnel_specification_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6553 (class 2606 OID 38656)
-- Name: personnel_specification_property personnel_specification_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification_property
    ADD CONSTRAINT personnel_specification_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6433 (class 2606 OID 37833)
-- Name: personnel_specification personnel_specification_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification
    ADD CONSTRAINT personnel_specification_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6432 (class 2606 OID 37838)
-- Name: personnel_specification personnel_specification_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification
    ADD CONSTRAINT personnel_specification_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6431 (class 2606 OID 37843)
-- Name: personnel_specification personnel_specification_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.personnel_specification
    ADD CONSTRAINT personnel_specification_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


--
-- TOC entry 6564 (class 2606 OID 38688)
-- Name: physical_asset_specification_property physical_asset_specification__physical_asset_specification_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property
    ADD CONSTRAINT physical_asset_specification__physical_asset_specification_fkey FOREIGN KEY (physical_asset_specification_id) REFERENCES common.physical_asset_specification(id);


--
-- TOC entry 6442 (class 2606 OID 37865)
-- Name: physical_asset_specification physical_asset_specification_operation_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification
    ADD CONSTRAINT physical_asset_specification_operation_segment_id_fkey FOREIGN KEY (operations_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6441 (class 2606 OID 37870)
-- Name: physical_asset_specification physical_asset_specification_physical_asset_class_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification
    ADD CONSTRAINT physical_asset_specification_physical_asset_class_id_fkey FOREIGN KEY (physical_asset_class_id) REFERENCES common.physical_asset_class(id);


--
-- TOC entry 6440 (class 2606 OID 37875)
-- Name: physical_asset_specification physical_asset_specification_physical_asset_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification
    ADD CONSTRAINT physical_asset_specification_physical_asset_id_fkey FOREIGN KEY (physical_asset_id) REFERENCES common.physical_asset(id);


--
-- TOC entry 6565 (class 2606 OID 38683)
-- Name: physical_asset_specification_property physical_asset_specification_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property
    ADD CONSTRAINT physical_asset_specification_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.physical_asset_specification_property(id);


--
-- TOC entry 6563 (class 2606 OID 38693)
-- Name: physical_asset_specification_property physical_asset_specification_property_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property
    ADD CONSTRAINT physical_asset_specification_property_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6559 (class 2606 OID 41408)
-- Name: physical_asset_specification_property physical_asset_specification_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property
    ADD CONSTRAINT physical_asset_specification_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6558 (class 2606 OID 41413)
-- Name: physical_asset_specification_property physical_asset_specification_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property
    ADD CONSTRAINT physical_asset_specification_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6560 (class 2606 OID 41403)
-- Name: physical_asset_specification_property physical_asset_specification_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property
    ADD CONSTRAINT physical_asset_specification_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6562 (class 2606 OID 38698)
-- Name: physical_asset_specification_property physical_asset_specification_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property
    ADD CONSTRAINT physical_asset_specification_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6561 (class 2606 OID 38703)
-- Name: physical_asset_specification_property physical_asset_specification_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification_property
    ADD CONSTRAINT physical_asset_specification_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6439 (class 2606 OID 37880)
-- Name: physical_asset_specification physical_asset_specification_quantity_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification
    ADD CONSTRAINT physical_asset_specification_quantity_uom_id_fkey FOREIGN KEY (quantity_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6438 (class 2606 OID 37885)
-- Name: physical_asset_specification physical_asset_specification_resource_use_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification
    ADD CONSTRAINT physical_asset_specification_resource_use_id_fkey FOREIGN KEY (resource_use_id) REFERENCES common.lookup(id);


--
-- TOC entry 6437 (class 2606 OID 37890)
-- Name: physical_asset_specification physical_asset_specification_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.physical_asset_specification
    ADD CONSTRAINT physical_asset_specification_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


--
-- TOC entry 6371 (class 2606 OID 37356)
-- Name: work_master work_master_duration_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master
    ADD CONSTRAINT work_master_duration_uom_id_fkey FOREIGN KEY (duration_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6370 (class 2606 OID 37361)
-- Name: work_master work_master_common_definition_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master
    ADD CONSTRAINT work_master_common_definition_id_fkey FOREIGN KEY (operations_definition_id) REFERENCES common.operations_definition(id);


--
-- TOC entry 6369 (class 2606 OID 37366)
-- Name: work_master work_master_common_segment_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master
    ADD CONSTRAINT work_master_common_segment_id_fkey FOREIGN KEY (operations_segment_id) REFERENCES common.operations_segment(id);


--
-- TOC entry 6368 (class 2606 OID 37371)
-- Name: work_master work_master_work_master_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master
    ADD CONSTRAINT work_master_work_master_type_id_fkey FOREIGN KEY (work_master_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6367 (class 2606 OID 37376)
-- Name: work_master work_master_work_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.work_master
    ADD CONSTRAINT work_master_work_type_id_fkey FOREIGN KEY (work_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6671 (class 2606 OID 39415)
-- Name: workflow_specification_connection workflow_specification_conne_workflow_specification_conne_fkey1; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection
    ADD CONSTRAINT workflow_specification_conne_workflow_specification_conne_fkey1 FOREIGN KEY (workflow_specification_connection_type_id) REFERENCES common.workflow_specification_connection_type(id);


--
-- TOC entry 6754 (class 2606 OID 39955)
-- Name: workflow_specification_connection_property workflow_specification_conne_workflow_specification_conne_fkey2; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_property
    ADD CONSTRAINT workflow_specification_conne_workflow_specification_conne_fkey2 FOREIGN KEY (workflow_specification_connection_id) REFERENCES common.workflow_specification_connection(id);


--
-- TOC entry 6673 (class 2606 OID 39405)
-- Name: workflow_specification_connection workflow_specification_connec_from_workflow_specification__fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection
    ADD CONSTRAINT workflow_specification_connec_from_workflow_specification__fkey FOREIGN KEY (from_workflow_specification_node_id) REFERENCES common.workflow_specification_node(id);


--
-- TOC entry 6672 (class 2606 OID 39410)
-- Name: workflow_specification_connection workflow_specification_connec_to_workflow_specification_no_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection
    ADD CONSTRAINT workflow_specification_connec_to_workflow_specification_no_fkey FOREIGN KEY (to_workflow_specification_node_id) REFERENCES common.workflow_specification_node(id);


--
-- TOC entry 6006 (class 2606 OID 34752)
-- Name: workflow_specification_connection_type_property workflow_specification_connec_workflow_specification_conne_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type_property
    ADD CONSTRAINT workflow_specification_connec_workflow_specification_conne_fkey FOREIGN KEY (workflow_specification_connection_type_id) REFERENCES common.workflow_specification_connection_type(id);


--
-- TOC entry 6670 (class 2606 OID 39420)
-- Name: workflow_specification_connection workflow_specification_connectio_workflow_specification_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection
    ADD CONSTRAINT workflow_specification_connectio_workflow_specification_id_fkey FOREIGN KEY (workflow_specification_id) REFERENCES common.workflow_specification(id);


--
-- TOC entry 6757 (class 2606 OID 39940)
-- Name: workflow_specification_connection_property workflow_specification_connection_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_property
    ADD CONSTRAINT workflow_specification_connection_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.workflow_specification_connection_property(id);


--
-- TOC entry 6752 (class 2606 OID 41498)
-- Name: workflow_specification_connection_property workflow_specification_connection_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_property
    ADD CONSTRAINT workflow_specification_connection_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6751 (class 2606 OID 41503)
-- Name: workflow_specification_connection_property workflow_specification_connection_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_property
    ADD CONSTRAINT workflow_specification_connection_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6753 (class 2606 OID 41493)
-- Name: workflow_specification_connection_property workflow_specification_connection_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_property
    ADD CONSTRAINT workflow_specification_connection_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6756 (class 2606 OID 39945)
-- Name: workflow_specification_connection_property workflow_specification_connection_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_property
    ADD CONSTRAINT workflow_specification_connection_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6755 (class 2606 OID 39950)
-- Name: workflow_specification_connection_property workflow_specification_connection_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_property
    ADD CONSTRAINT workflow_specification_connection_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6005 (class 2606 OID 41511)
-- Name: workflow_specification_connection_type_property workflow_specification_connection_type_pro_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type_property
    ADD CONSTRAINT workflow_specification_connection_type_pro_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6003 (class 2606 OID 41521)
-- Name: workflow_specification_connection_type_property workflow_specification_connection_type_prop_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type_property
    ADD CONSTRAINT workflow_specification_connection_type_prop_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6008 (class 2606 OID 34742)
-- Name: workflow_specification_connection_type_property workflow_specification_connection_type_prope_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type_property
    ADD CONSTRAINT workflow_specification_connection_type_prope_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6007 (class 2606 OID 34747)
-- Name: workflow_specification_connection_type_property workflow_specification_connection_type_proper_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type_property
    ADD CONSTRAINT workflow_specification_connection_type_proper_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6009 (class 2606 OID 34737)
-- Name: workflow_specification_connection_type_property workflow_specification_connection_type_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type_property
    ADD CONSTRAINT workflow_specification_connection_type_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.workflow_specification_connection_type_property(id);


--
-- TOC entry 6004 (class 2606 OID 41516)
-- Name: workflow_specification_connection_type_property workflow_specification_connection_type_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_connection_type_property
    ADD CONSTRAINT workflow_specification_connection_type_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6569 (class 2606 OID 38729)
-- Name: workflow_specification_node workflow_specification_node_child_workflow_specification_i_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node
    ADD CONSTRAINT workflow_specification_node_child_workflow_specification_i_fkey FOREIGN KEY (child_workflow_specification_id) REFERENCES common.workflow_specification(id);


--
-- TOC entry 6677 (class 2606 OID 39457)
-- Name: workflow_specification_node_property workflow_specification_node_p_workflow_specification_node__fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_property
    ADD CONSTRAINT workflow_specification_node_p_workflow_specification_node__fkey FOREIGN KEY (workflow_specification_node_id) REFERENCES common.workflow_specification_node(id);


--
-- TOC entry 6680 (class 2606 OID 39442)
-- Name: workflow_specification_node_property workflow_specification_node_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_property
    ADD CONSTRAINT workflow_specification_node_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.workflow_specification_node_property(id);


--
-- TOC entry 6675 (class 2606 OID 41534)
-- Name: workflow_specification_node_property workflow_specification_node_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_property
    ADD CONSTRAINT workflow_specification_node_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6674 (class 2606 OID 41539)
-- Name: workflow_specification_node_property workflow_specification_node_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_property
    ADD CONSTRAINT workflow_specification_node_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6676 (class 2606 OID 41529)
-- Name: workflow_specification_node_property workflow_specification_node_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_property
    ADD CONSTRAINT workflow_specification_node_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6679 (class 2606 OID 39447)
-- Name: workflow_specification_node_property workflow_specification_node_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_property
    ADD CONSTRAINT workflow_specification_node_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6678 (class 2606 OID 39452)
-- Name: workflow_specification_node_property workflow_specification_node_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_property
    ADD CONSTRAINT workflow_specification_node_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6013 (class 2606 OID 34793)
-- Name: workflow_specification_node_type_property workflow_specification_node_t_workflow_specification_node__fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type_property
    ADD CONSTRAINT workflow_specification_node_t_workflow_specification_node__fkey FOREIGN KEY (workflow_specification_node_type_id) REFERENCES common.workflow_specification_node_type(id);


--
-- TOC entry 6016 (class 2606 OID 34778)
-- Name: workflow_specification_node_type_property workflow_specification_node_type_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type_property
    ADD CONSTRAINT workflow_specification_node_type_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.workflow_specification_node_type_property(id);


--
-- TOC entry 6011 (class 2606 OID 41552)
-- Name: workflow_specification_node_type_property workflow_specification_node_type_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type_property
    ADD CONSTRAINT workflow_specification_node_type_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6010 (class 2606 OID 41557)
-- Name: workflow_specification_node_type_property workflow_specification_node_type_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type_property
    ADD CONSTRAINT workflow_specification_node_type_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6012 (class 2606 OID 41547)
-- Name: workflow_specification_node_type_property workflow_specification_node_type_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type_property
    ADD CONSTRAINT workflow_specification_node_type_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6015 (class 2606 OID 34783)
-- Name: workflow_specification_node_type_property workflow_specification_node_type_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type_property
    ADD CONSTRAINT workflow_specification_node_type_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6014 (class 2606 OID 34788)
-- Name: workflow_specification_node_type_property workflow_specification_node_type_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node_type_property
    ADD CONSTRAINT workflow_specification_node_type_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6568 (class 2606 OID 38734)
-- Name: workflow_specification_node workflow_specification_node_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node
    ADD CONSTRAINT workflow_specification_node_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


--
-- TOC entry 6567 (class 2606 OID 38739)
-- Name: workflow_specification_node workflow_specification_node_workflow_specification_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node
    ADD CONSTRAINT workflow_specification_node_workflow_specification_id_fkey FOREIGN KEY (workflow_specification_id) REFERENCES common.workflow_specification(id);


--
-- TOC entry 6566 (class 2606 OID 38744)
-- Name: workflow_specification_node workflow_specification_node_workflow_specification_node_ty_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_node
    ADD CONSTRAINT workflow_specification_node_workflow_specification_node_ty_fkey FOREIGN KEY (workflow_specification_node_type_id) REFERENCES common.workflow_specification_node_type(id);


--
-- TOC entry 6576 (class 2606 OID 38766)
-- Name: workflow_specification_property workflow_specification_property_parent_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_property
    ADD CONSTRAINT workflow_specification_property_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES common.workflow_specification_property(id);


--
-- TOC entry 6571 (class 2606 OID 41480)
-- Name: workflow_specification_property workflow_specification_property_sensor_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_property
    ADD CONSTRAINT workflow_specification_property_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES common.equipment(id);


--
-- TOC entry 6570 (class 2606 OID 41485)
-- Name: workflow_specification_property workflow_specification_property_sensor_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_property
    ADD CONSTRAINT workflow_specification_property_sensor_type_id_fkey FOREIGN KEY (sensor_type_id) REFERENCES common.equipment_class(id);


--
-- TOC entry 6572 (class 2606 OID 41475)
-- Name: workflow_specification_property workflow_specification_property_uom_category_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_property
    ADD CONSTRAINT workflow_specification_property_uom_category_id_fkey FOREIGN KEY (uom_category_id) REFERENCES common.uom_category(id);


--
-- TOC entry 6575 (class 2606 OID 38771)
-- Name: workflow_specification_property workflow_specification_property_value_type_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_property
    ADD CONSTRAINT workflow_specification_property_value_type_id_fkey FOREIGN KEY (value_type_id) REFERENCES common.lookup(id);


--
-- TOC entry 6574 (class 2606 OID 38776)
-- Name: workflow_specification_property workflow_specification_property_value_uom_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_property
    ADD CONSTRAINT workflow_specification_property_value_uom_id_fkey FOREIGN KEY (value_uom_id) REFERENCES common.uom(id);


--
-- TOC entry 6573 (class 2606 OID 38781)
-- Name: workflow_specification_property workflow_specification_property_workflow_specification_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification_property
    ADD CONSTRAINT workflow_specification_property_workflow_specification_id_fkey FOREIGN KEY (workflow_specification_id) REFERENCES common.workflow_specification(id);


--
-- TOC entry 6443 (class 2606 OID 37912)
-- Name: workflow_specification workflow_specification_work_master_id_fkey; Type: FK CONSTRAINT; Schema: common; Owner: -
--

ALTER TABLE ONLY common.workflow_specification
    ADD CONSTRAINT workflow_specification_work_master_id_fkey FOREIGN KEY (work_master_id) REFERENCES common.work_master(id);


-- Completed on 2019-09-13 11:57:00 PKT

--
-- PostgreSQL database dump complete
--
