--
-- TOC entry 6932 (class 0 OID 33869)
-- Dependencies: 206
-- Data for Name: lookup_category; Type: TABLE DATA; Schema: common; Owner: apps
--

INSERT INTO common.lookup_category VALUES (1, false, 'Enterprise', 'equipment_level', 'Equpiment Level', 0);
INSERT INTO common.lookup_category VALUES (2, false, 'Production Operation', 'operations_type', 'Operations Type', 0);
INSERT INTO common.lookup_category VALUES (3, false, 'Order Request Status', 'order_request_status', 'Order Request Status', 0);
INSERT INTO common.lookup_category VALUES (4, false, 'Material Use', 'material_use', 'Material Use', 0);
INSERT INTO common.lookup_category VALUES (5, false, 'Priority', 'priority', 'Priority', 0);
INSERT INTO common.lookup_category VALUES (6, false, 'Response Status', 'response_status', 'Response Status', 0);
INSERT INTO common.lookup_category VALUES (7, false, 'Scheduled Status', 'scheduled_status', 'Scheduled Status', 0);
INSERT INTO common.lookup_category VALUES (8, false, 'Material Status', 'material_status', 'Material Status', 0);
INSERT INTO common.lookup_category VALUES (9, false, 'Capability Type', 'capability_type', 'Capability Type', 0);
INSERT INTO common.lookup_category VALUES (10, false, 'Job Cammand', 'job_command', 'Job Cammand', 0);
INSERT INTO common.lookup_category VALUES (11, false, 'Dependency Type', 'dependency_type', 'Dependency Type', 0);
INSERT INTO common.lookup_category VALUES (12, false, 'Entry Type', 'entry_type', 'Entry Type', 0);
INSERT INTO common.lookup_category VALUES (13, false, 'Value Type', 'value_type', 'Value Type', 0);
INSERT INTO common.lookup_category VALUES (14, false, 'Work Definition Type', 'work_definition_type', 'Work Definition Type', 0);
INSERT INTO common.lookup_category VALUES (15, false, 'Work Type', 'work_type', 'Work Type', 0);
INSERT INTO common.lookup_category VALUES (16, false, 'Assembly Relationship', 'assembly_relationship', 'Assembly Relationship', 0);
INSERT INTO common.lookup_category VALUES (17, false, 'Assembly Type', 'assembly_type', 'Assembly Type', 0);
INSERT INTO common.lookup_category VALUES (18, false, 'Capacity Type', 'capacity_type', 'Capacity Type', 0);
INSERT INTO common.lookup_category VALUES (19, false, 'Work Alert Category Type', 'work_alert_category_id', 'Work Alert Category Type', 0);
INSERT INTO common.lookup_category VALUES (20, false, 'Dependency Factor', 'dependency_factor', 'Dependency Factor', 0);
INSERT INTO common.lookup_category VALUES (21, false, 'Product Type BlueScope Specific Category', 'product_type', 'Product Type', 0);
INSERT INTO common.lookup_category VALUES (22, false, 'Color Coat Bottom BlueScope Specific Category', 'color_coat_bottom', 'Color Coat Bottom', 0);
INSERT INTO common.lookup_category VALUES (23, false, 'Color Coat Top BlueScope Specific Category', 'color_coat_top', 'Color Coat Top', 0);
INSERT INTO common.lookup_category VALUES (24, false, 'Scrap Code', 'scrap_code', 'Scrap Code', 0);
INSERT INTO common.lookup_category VALUES (25, false, 'Resource Type', 'resource_type', 'Resource Type', 0);
INSERT INTO common.lookup_category VALUES (26, false, 'Object Type', 'object_type', 'Object Type', 0);
INSERT INTO common.lookup_category VALUES (27, false, 'Event Type', 'event_type', 'Event Type', 0);
INSERT INTO common.lookup_category VALUES (28, false, 'Defines applications lookups', 'application', 'Application', 0);
INSERT INTO common.lookup_category VALUES (29, false, 'Document Category', 'document_category', 'Document Category', 0);


--
-- TOC entry 6954 (class 0 OID 34053)
-- Dependencies: 228
-- Data for Name: lookup; Type: TABLE DATA; Schema: common; Owner: apps
--

INSERT INTO common.lookup VALUES (10, false, NULL, 'production', 'Production Operation', 0, 2, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (11, false, NULL, 'inventory', 'Inventory Operations', 0, 2, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (12, false, NULL, 'maintenance', 'Maintenance Operation', 0, 2, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (13, false, NULL, 'quality', 'Quality Operations', 0, 2, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (14, false, NULL, 'mixed', 'Mixed Operations', 0, 2, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (15, false, NULL, 'forecasted', 'Forecasted', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (16, false, NULL, 'released', 'Released', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (17, false, NULL, 'cancelled', 'Cancelled', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (18, false, NULL, 'ready', 'Ready', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (19, false, NULL, 'running', 'Running', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (20, false, NULL, 'completed', 'Completed', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (21, false, NULL, 'aborted', 'Aborted', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (22, false, NULL, 'held', 'Held', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (23, false, NULL, 'paused', 'Paused', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (24, false, NULL, 'closed', 'Closed', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (25, false, NULL, 'scheduled', 'Scheduled', 0, 3, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (26, false, NULL, 'consumed', 'Consumed', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (27, false, NULL, 'produced', 'Produced', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (28, false, NULL, 'consumable', 'Consumable', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (29, false, NULL, 'by_product_produced', 'By Product', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (30, false, NULL, 'co_product_produced', 'Co Product', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (31, false, NULL, 'replaced_asset', 'Replaced Asset', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (32, false, NULL, 'replacement_asset', 'Replacement Asset', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (33, false, NULL, 'sample', 'Sample', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (34, false, NULL, 'returned_sample', 'Returned Sample', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (35, false, NULL, 'carrier', 'Carrier', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (36, false, NULL, 'returned_carrier', 'Returned Carrier', 0, 4, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (37, false, NULL, 'running', 'Running', 0, 6, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (38, false, NULL, 'completed', 'Completed', 0, 6, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (39, false, NULL, 'aborted', 'Aborted', 0, 6, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (40, false, NULL, 'held', 'Held', 0, 6, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (41, false, NULL, 'paused', 'Paused', 0, 6, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (42, false, NULL, 'scheduled', 'Scheduled', 0, 7, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (43, false, NULL, 'used', 'Used', 0, 9, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (44, false, NULL, 'unused', 'Unused', 0, 9, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (45, false, NULL, 'total', 'Total', 0, 9, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (46, false, NULL, 'committed', 'Committed', 0, 9, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (47, false, NULL, 'available', 'Available', 0, 9, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (48, false, NULL, 'unattainable', 'Unattainable', 0, 9, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (49, false, NULL, 'start', 'Start', 0, 10, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (50, false, NULL, 'stop', 'Stop', 0, 10, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (51, false, NULL, 'hold', 'Hold', 0, 10, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (52, false, NULL, 'restart', 'Restart', 0, 10, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (53, false, NULL, 'abort', 'Abort', 0, 10, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (54, false, NULL, 'reset', 'Reset', 0, 10, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (55, false, NULL, 'pause', 'Pause', 0, 10, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (56, false, NULL, 'resume', 'Resume', 0, 10, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (57, false, NULL, 'not_follow', 'Not Follow', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (58, false, NULL, 'possible_parallel', 'Possible Parallel', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (59, false, NULL, 'not_in_parallerl', 'Not In Parallel', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (60, false, NULL, 'at_start', 'At Start', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (61, false, NULL, 'after_start', 'After Start', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (62, false, NULL, 'after_end', 'After End', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (63, false, NULL, 'no_later_after_start', 'No Later After Start', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (64, false, NULL, 'no_earlier_after_start', 'No Earlier After Start', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (65, false, NULL, 'no_later_after_end', 'No Later After End', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (66, false, NULL, 'no_earlier_after_end', 'No Earlier After End', 0, 11, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (67, false, NULL, 'range', 'Range', 0, 13, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (68, false, NULL, 'discrete', 'Discrete', 0, 13, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (69, false, NULL, 'premivitive', 'Premitive', 0, 13, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (70, false, NULL, 'work_master', 'Work Master', 0, 14, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (71, false, NULL, 'work_directive', 'Work Directive', 0, 14, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (72, false, NULL, 'production', 'Production Operation', 0, 15, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (73, false, NULL, 'inventory', 'Inventory Operations', 0, 15, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (74, false, NULL, 'maintenance', 'Maintenance Operation', 0, 15, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (75, false, NULL, 'quality', 'Quality Operations', 0, 15, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (76, false, NULL, 'mixed', 'Mixed Operations', 0, 15, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (77, false, NULL, 'permanent', 'Permanent', 0, 16, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (78, false, NULL, 'transient', 'Transient', 0, 16, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (79, false, NULL, 'physical', 'Physical', 0, 17, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (80, false, NULL, 'logical', 'Logical', 0, 17, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (81, false, NULL, 'used', 'Used', 0, 18, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (82, false, NULL, 'unused', 'Unused', 0, 18, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (83, false, NULL, 'total', 'Total', 0, 18, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (84, false, NULL, 'committed', 'Committed', 0, 18, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (85, false, NULL, 'available', 'Available', 0, 18, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (86, false, NULL, 'unattainable', 'Unattainable', 0, 18, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (87, false, NULL, 'time', 'Time', 0, 20, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (88, false, NULL, 'boolean', 'Boolean', 0, 20, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (89, false, NULL, 'field', 'Field', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (90, false, NULL, 'selected', 'Selected', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (91, false, NULL, 'por1', 'POR1', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (92, false, NULL, 'por2', 'POR2', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (93, false, NULL, 'child', 'Child', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (94, false, NULL, 'high', 'High', 0, 5, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (95, false, NULL, 'dispositioned', 'Dispositioned', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (96, false, NULL, '01', 'ZINCALUME AZ150', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (97, false, NULL, '02', 'Gemilang AZ100', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (98, false, NULL, '03', 'Gemilang SS AZ100', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (99, false, NULL, '04', 'ABADI  AZ100', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (100, false, NULL, '05', 'INTERIOR AZ050', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (101, false, NULL, '06', 'PERISAI  AZ070', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (102, false, NULL, '07', 'TANGGUH AZ100', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (103, false, NULL, '08', 'KEMILAU  AZ070', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (104, false, NULL, '09', 'KEMILAU AZ050', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (105, false, NULL, '10', 'KIRANA AZ100', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (106, false, NULL, '11', 'KEMILAU SS AZ070', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (107, false, NULL, '12', 'ABADI AZ100 FEED', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (108, false, NULL, '13', 'GALVALUME(R)', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (109, false, NULL, '14', 'B-Zacs Painted GSS-POM', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (110, false, NULL, '15', 'B-Zacs Painted GEMILANG', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (111, false, NULL, '16', 'B-Zacs ABADI AZ100', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (112, false, NULL, '17', 'B-Zacs TANGGUH AZ100', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (113, false, NULL, '18', 'RS RW150', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (114, false, NULL, '19', 'POM AZ150', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (115, false, NULL, '20', 'ZINCALUME ULTRA AZ200', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (116, false, NULL, '21', 'BlueScope Zacs Cool Coated', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (117, false, NULL, '22', 'BLUESCOPE ZACS COOL', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (118, false, NULL, '23', 'BlueScope Zacs  Lite', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (119, false, NULL, '24', 'BLUESCOPE ZACS PAINTFEED AZ070', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (120, false, NULL, '25', 'BlueScope Zacs Cool Roof', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (121, false, NULL, '26', 'BlueScope Zacs Cool Truss', 0, 21, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (122, false, NULL, '50', 'Clear Coat', 0, 22, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (123, false, NULL, '51', 'Backer Abu Semeru NIR(Grey)', 0, 22, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (124, false, NULL, '52', 'Backer Abu Krakatau NIR (Grey)', 0, 22, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (125, false, NULL, '53', 'Paint DuraPrime 2020', 0, 22, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (126, false, NULL, '54', 'Backer Shadow Grey', 0, 22, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (127, false, NULL, '55', 'Resin clear', 0, 22, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (128, false, NULL, '56', 'Resin tinted blue', 0, 22, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (129, false, NULL, '57', 'Chemical coat', 0, 22, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (130, false, NULL, '25', 'Paint Merah Carita NIR (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (131, false, NULL, '26', 'Paint Merah Merapi [RED]', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (132, false, NULL, '27', 'Paint Biru Bromo [Blue]', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (133, false, NULL, '28', 'Paint Hijau Borneo [Green]', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (134, false, NULL, '29', 'Paint Coklat Jati NIR (Brown)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (135, false, NULL, '30', 'Paint Merah Barito NIR (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (136, false, NULL, '31', 'Paint Biru Kapuas NIR (BLUE)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (137, false, NULL, '32', 'Paint Merah Bawean NIR (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (138, false, NULL, '33', 'Paint Merah Anyer NIR (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (139, false, NULL, '34', 'Paint Merah Batavia NIR (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (140, false, NULL, '35', 'Paint Biru Anggi NIR (Blue)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (141, false, NULL, '36', 'Paint Biru Rinjani NIR (Blue)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (142, false, NULL, '37', 'Paint Merah PEMO NIR (Red)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (143, false, NULL, '38', 'Paint Merah Singkawang NIR (Red)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (144, false, NULL, '39', 'Paint Coklat Bengkalis NIR (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (145, false, NULL, '40', 'Paint Biru Papandayan NIR(Blue)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (146, false, NULL, '41', 'Paint Coklat Meranti NIR (Brown)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (147, false, NULL, '42', 'Paint Mung Kung Red (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (148, false, NULL, '43', 'Paint Merah Musi NIR(Red)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (149, false, NULL, '44', 'Paint Merah Banjar NIR (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (150, false, NULL, '45', 'Paint Merah Mataram NIR (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (151, false, NULL, '46', 'Paint Biru Bogowonto', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (152, false, NULL, '47', 'Paint DuraPrime 2020', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (153, false, NULL, '48', 'Clear Coat', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (154, false, NULL, '49', 'Paint Putih Patanu', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (155, false, NULL, '50', 'Backer Shadow Grey', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (156, false, NULL, '51', 'Pint Merah Sindoro (RED)', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (157, false, NULL, '52', 'Hijau Cilebes', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (158, false, NULL, '53', 'Merah Praba Polyester', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (159, false, NULL, '54', 'Biru Sagara Polyester', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (160, false, NULL, '55', 'Hijau Gurda Polyester', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (161, false, NULL, '56', 'PAINT MERAH GAHARU', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (162, false, NULL, '57', 'Paint Coklat Kilimanjaro', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (163, false, NULL, '58', 'Paint Merah Fuji', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (164, false, NULL, '59', 'Paint Biru Everest', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (165, false, NULL, '60', 'Paint Hijau Alpen', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (166, false, NULL, '61', 'Resin clear', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (167, false, NULL, '62', 'Resin tinted blue', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (168, false, NULL, '63', 'Chemical coat', 0, 23, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (169, false, NULL, 'p1a', 'Black Rust Pup Coil', 0, 24, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (170, false, NULL, 'cpor2', 'CPOR2', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (171, false, NULL, 'p1b', 'Black Rust Sheets', 0, 24, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (172, false, NULL, 'p1c', 'Crane Handling', 0, 24, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (173, false, NULL, 'personnel_class', 'Personnel Class', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (174, false, NULL, 'cpor1', 'CPOR1', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (175, false, NULL, 'person', 'Person', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (176, false, NULL, 'hold', 'Hold', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (177, false, NULL, 'threaded', 'Threaded', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (178, false, NULL, 'current', 'Current', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (179, false, NULL, 'assign', 'Assign', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (180, false, NULL, 'dispositioned', 'Dispositioned', 0, 8, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (181, false, NULL, 'equipment_class', 'Equipment Class', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (182, false, NULL, 'physical_asset_class', 'Physical Asset Class', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (183, false, NULL, 'physical_asset', 'Physical Asset', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (184, false, NULL, 'material_class', 'Material Class', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (185, false, NULL, 'material_definition', 'Material Definition', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (186, false, NULL, 'material_lot', 'Material Lot', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (187, false, NULL, 'material_sublot', 'Material Sublot', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (188, false, NULL, 'work_master', 'Work Master', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (189, false, NULL, 'process_segment', 'Process Segment', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (190, false, NULL, 'operations_definition', 'Operations Definition', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (191, false, NULL, 'operations_segment', 'Operations Segment', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (192, false, NULL, 'equipment', 'Equipment', 0, 25, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (1, false, NULL, 'enterprise', 'Enterprise', 0, 1, false, false, '10', NULL);
INSERT INTO common.lookup VALUES (2, false, NULL, 'site', 'Site', 0, 1, false, false, '20', NULL);
INSERT INTO common.lookup VALUES (3, false, NULL, 'area', 'Area', 0, 1, false, false, '30', NULL);
INSERT INTO common.lookup VALUES (4, false, NULL, 'work_center', 'Work Center', 0, 1, false, false, '40', NULL);
INSERT INTO common.lookup VALUES (193, false, NULL, 'equipment_unit', 'Equipment Unit', 0, 1, false, false, '100', NULL);
INSERT INTO common.lookup VALUES (195, false, NULL, 'component', 'Component', 0, 1, false, false, '120', NULL);
INSERT INTO common.lookup VALUES (196, false, NULL, 'sensor', 'Sensor', 0, 1, false, false, '130', NULL);
INSERT INTO common.lookup VALUES (197, false, 'Manufacturing Operations Management', 'mom', 'MOM', 0, 28, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (198, false, 'Self Service AI', 'ssai', 'SSAI', 0, 28, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (199, false, 'Asset and Process Performance Management', 'appm', 'APPM', 0, 28, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (5, false, NULL, 'work_unit', 'Work Unit', 0, 1, false, false, '50', NULL);
INSERT INTO common.lookup VALUES (194, false, NULL, 'sub_unit', 'Sub Unit', 0, 1, false, false, '110', NULL);
INSERT INTO common.lookup VALUES (201, false, NULL, 'part', 'Part', 0, 1, false, false, '125', NULL);
INSERT INTO common.lookup VALUES (202, false, 'Design Documents', 'design_document', 'Design Document', 0, 29, false, false, NULL, NULL);
INSERT INTO common.lookup VALUES (203, false, 'Specification Sheets', 'specification_sheets', 'specification_sheets', 0, 29, false, false, NULL, 202);
INSERT INTO common.lookup VALUES (204, false, 'Process and Instrumentation Diagram', 'pid', 'P&ID', 0, 29, false, false, NULL, 202);
INSERT INTO common.lookup VALUES (205, false, 'Process Flow Diagram', 'pfd', 'PFD', 0, 29, false, false, NULL, 202);


--
-- TOC entry 6962 (class 0 OID 34149)
-- Dependencies: 236
-- Data for Name: equipment; Type: TABLE DATA; Schema: common; Owner: apps
--

INSERT INTO common.equipment VALUES (1, false, NULL, 'enterprise', 'Enterprise', 0, 1, NULL);


--
-- TOC entry 6998 (class 0 OID 34806)
-- Dependencies: 272
-- Data for Name: job_list; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7014 (class 0 OID 35085)
-- Dependencies: 288
-- Data for Name: work_schedule; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7064 (class 0 OID 35990)
-- Dependencies: 338
-- Data for Name: work_request; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 6942 (class 0 OID 33958)
-- Dependencies: 216
-- Data for Name: uom_category; Type: TABLE DATA; Schema: common; Owner: apps
--

INSERT INTO common.uom_category VALUES (10, false, NULL, 'length', 'Length', 0);
INSERT INTO common.uom_category VALUES (11, false, NULL, 'mass_flow_rate', 'Mass flow rate', 0);
INSERT INTO common.uom_category VALUES (12, false, NULL, 'power', 'Power', 0);
INSERT INTO common.uom_category VALUES (13, false, NULL, 'pressure', 'Pressure', 0);
INSERT INTO common.uom_category VALUES (14, false, NULL, 'speed_linear', 'Speed-Linear', 0);
INSERT INTO common.uom_category VALUES (15, false, NULL, 'speed_rotational', 'Speed-Rotational', 0);
INSERT INTO common.uom_category VALUES (16, false, NULL, 'temperature', 'Temperature', 0);
INSERT INTO common.uom_category VALUES (17, false, NULL, 'torque', 'Torque', 0);
INSERT INTO common.uom_category VALUES (18, false, NULL, 'volumetric_flow_Rate', 'Volumetric Flow Rate', 0);
INSERT INTO common.uom_category VALUES (19, false, NULL, 'current', 'Current', 0);
INSERT INTO common.uom_category VALUES (20, false, NULL, 'voltage', 'Voltage', 0);
INSERT INTO common.uom_category VALUES (21, false, NULL, 'count', 'Count', 0);
INSERT INTO common.uom_category VALUES (22, false, NULL, 'density', 'Density', 0);
INSERT INTO common.uom_category VALUES (23, false, NULL, 'composition', 'Composition', 0);
INSERT INTO common.uom_category VALUES (24, false, NULL, 'volume', 'Volume', 0);
INSERT INTO common.uom_category VALUES (25, false, NULL, 'area', 'Area', 0);
INSERT INTO common.uom_category VALUES (26, false, NULL, 'mass', 'Mass', 0);
INSERT INTO common.uom_category VALUES (27, false, NULL, 'acceleration', 'Acceleration', 0);
INSERT INTO common.uom_category VALUES (28, false, NULL, 'electrical_resistance', 'Electrical Resistance', 0);
INSERT INTO common.uom_category VALUES (29, false, NULL, 'force', 'Force', 0);
INSERT INTO common.uom_category VALUES (30, false, NULL, 'energy', 'Energy', 0);
INSERT INTO common.uom_category VALUES (31, false, NULL, 'angle', 'Angle', 0);
INSERT INTO common.uom_category VALUES (32, false, NULL, 'undefined', 'undefined', 0);
INSERT INTO common.uom_category VALUES (33, false, NULL, 'energy_per_quantiy', 'energy per quantiy', 0);
INSERT INTO common.uom_category VALUES (34, false, NULL, 'viscosity', 'viscosity', 0);
INSERT INTO common.uom_category VALUES (35, false, NULL, 'entropy_and_specific_heat', 'entropy and specific heat', 0);
INSERT INTO common.uom_category VALUES (36, false, NULL, 'molecular_weight', 'molecular weight', 0);


--
-- TOC entry 6958 (class 0 OID 34104)
-- Dependencies: 232
-- Data for Name: uom; Type: TABLE DATA; Schema: common; Owner: apps
--

INSERT INTO common.uom VALUES (1, false, NULL, 'milliinches', 'Milli Inches', 0, 'mils', 10);
INSERT INTO common.uom VALUES (2, false, NULL, 'inches', 'Inches', 0, 'In', 10);
INSERT INTO common.uom VALUES (3, false, NULL, 'feet', 'feet', 0, 'ft', 10);
INSERT INTO common.uom VALUES (4, false, NULL, 'millimeters', 'Millimeters', 0, 'mm', 10);
INSERT INTO common.uom VALUES (5, false, NULL, 'centimeters', 'Centimeters', 0, 'cm', 10);
INSERT INTO common.uom VALUES (6, false, NULL, 'meters', 'Meters', 0, 'm', 10);
INSERT INTO common.uom VALUES (7, false, NULL, 'pound_per_second', 'pound per second', 0, 'lbm/sec', 11);
INSERT INTO common.uom VALUES (8, false, NULL, 'pound_per_minute', 'pound per minute', 0, 'lbm/min', 11);
INSERT INTO common.uom VALUES (9, false, NULL, 'pound_per_hour', 'pound per hour', 0, 'lbm/hr', 11);
INSERT INTO common.uom VALUES (10, false, NULL, 'pound_per_day', 'pound per day', 0, 'lbm/day', 11);
INSERT INTO common.uom VALUES (11, false, NULL, 'kilopounds_per_sec', 'kilopounds per sec', 0, 'klbm/sec', 11);
INSERT INTO common.uom VALUES (12, false, NULL, 'kilopound_per_minute', 'kilopound per minute', 0, 'klbm/min', 11);
INSERT INTO common.uom VALUES (13, false, NULL, 'kilopound_per_hour', 'kilopound per hour', 0, 'klbm/hr', 11);
INSERT INTO common.uom VALUES (14, false, NULL, 'kilopound_per_day', 'kilopound per day', 0, 'klbm/day', 11);
INSERT INTO common.uom VALUES (15, false, NULL, 'megapound_per_day', 'megapound per day', 0, 'Mlbm/day', 11);
INSERT INTO common.uom VALUES (16, false, NULL, 'megapound_per_hour', 'megapound per hour', 0, 'Mlbm/hr', 11);
INSERT INTO common.uom VALUES (17, false, NULL, 'megapound_per_minute', 'megapound per minute', 0, 'Mlbm/min', 11);
INSERT INTO common.uom VALUES (18, false, NULL, 'megapound_per_second', 'megapound per second', 0, 'Mlbm/sec', 11);
INSERT INTO common.uom VALUES (19, false, NULL, 'kilograms_per_day', 'kilograms per day', 0, 'kg/day', 11);
INSERT INTO common.uom VALUES (20, false, NULL, 'kilograms_per_hour', 'kilograms per hour', 0, 'kg/hr', 11);
INSERT INTO common.uom VALUES (21, false, NULL, 'kilograms_per_minute', 'kilograms per minute', 0, 'kg/min', 11);
INSERT INTO common.uom VALUES (22, false, NULL, 'kilograms_per_sec', 'kilograms per sec', 0, 'kg/sec', 11);
INSERT INTO common.uom VALUES (23, false, NULL, 'metric_ton_per_second', 'metric ton per second', 0, 'Metric Ton/sec', 11);
INSERT INTO common.uom VALUES (24, false, NULL, 'metric_ton_per_hour', 'metric ton per hour', 0, 'Metric Ton/hr', 11);
INSERT INTO common.uom VALUES (25, false, NULL, 'metric_ton_per_day', 'metric ton per day', 0, 'Metric Ton/day', 11);
INSERT INTO common.uom VALUES (26, false, NULL, 'metric_ton_per_minute', 'metric ton per minute', 0, 'Metric Ton/min', 11);
INSERT INTO common.uom VALUES (27, false, NULL, 'horsepower', 'horsepower', 0, 'hp', 12);
INSERT INTO common.uom VALUES (28, false, NULL, 'british_thermal_units_per_hour', 'british thermal units per hour', 0, 'btu/hour', 12);
INSERT INTO common.uom VALUES (29, false, NULL, 'millions_of_british_thermal_units_per_hpur', 'millions of british thermal units per hour', 0, 'Mbtu/hour', 12);
INSERT INTO common.uom VALUES (30, false, NULL, 'watts', 'Watts', 0, 'W', 12);
INSERT INTO common.uom VALUES (31, false, NULL, 'kilowatts', 'Kilowatts', 0, 'kW', 12);
INSERT INTO common.uom VALUES (32, false, NULL, 'megawatts', 'Megawatts', 0, 'MW', 12);
INSERT INTO common.uom VALUES (33, false, NULL, 'newton_meters_per_second', 'Newton-meters per second', 0, 'N-m/s', 12);
INSERT INTO common.uom VALUES (34, false, NULL, 'atmospheres', 'Atmospheres', 0, 'Atm', 13);
INSERT INTO common.uom VALUES (35, false, NULL, 'pounds_per_square_inch_absolute', 'Pounds per square inch (absolute)', 0, 'psia', 13);
INSERT INTO common.uom VALUES (36, false, NULL, 'pounds_per_square_inch_gauge', 'Pounds per square inch (gauge)', 0, 'psig', 13);
INSERT INTO common.uom VALUES (37, false, NULL, 'bar_absolute', 'Bar (absolute)', 0, 'bar a', 13);
INSERT INTO common.uom VALUES (38, false, NULL, 'bar_gauge', 'Bar (gauge)', 0, 'bar g', 13);
INSERT INTO common.uom VALUES (39, false, NULL, 'millibars', 'Millibars', 0, 'mBar', 13);
INSERT INTO common.uom VALUES (40, false, NULL, 'millimeters_of_water', 'Millimeters of water', 0, 'mm.H2O', 13);
INSERT INTO common.uom VALUES (41, false, NULL, 'inches_of_water', 'Inches of water', 0, 'in.H2O', 13);
INSERT INTO common.uom VALUES (42, false, NULL, 'feet_of_water', 'Feet of water', 0, 'ft.H2O', 13);
INSERT INTO common.uom VALUES (43, false, NULL, 'millimeters_of_mercury', 'Millimeters of mercury', 0, 'mm.Hg', 13);
INSERT INTO common.uom VALUES (44, false, NULL, 'inches_of_mercury', 'Inches of mercury', 0, 'in.Hg', 13);
INSERT INTO common.uom VALUES (45, false, NULL, 'torr', 'Torr', 0, 'Torr', 13);
INSERT INTO common.uom VALUES (46, false, NULL, 'micropascal', 'Micropascal', 0, 'µPa', 13);
INSERT INTO common.uom VALUES (47, false, NULL, 'millipascal', 'Millipascal', 0, 'mPa', 13);
INSERT INTO common.uom VALUES (48, false, NULL, 'centipascal', 'Centipascal', 0, 'cPa', 13);
INSERT INTO common.uom VALUES (49, false, NULL, 'decipascal', 'Decipascal', 0, 'dPa', 13);
INSERT INTO common.uom VALUES (50, false, NULL, 'pascal', 'Pascal', 0, 'Pa', 13);
INSERT INTO common.uom VALUES (51, false, NULL, 'dekapascal', 'Dekapascal', 0, 'daPa', 13);
INSERT INTO common.uom VALUES (52, false, NULL, 'hectopascal', 'Hectopascal', 0, 'hPa', 13);
INSERT INTO common.uom VALUES (53, false, NULL, 'kilopascal', 'Kilopascal', 0, 'kPa', 13);
INSERT INTO common.uom VALUES (54, false, NULL, 'megapascal', 'Megapascal', 0, 'MPa', 13);
INSERT INTO common.uom VALUES (55, false, NULL, 'inches_per_second', 'Inches per second', 0, 'in/sec', 14);
INSERT INTO common.uom VALUES (56, false, NULL, 'feet_per_minute', 'Feet per minute', 0, 'ft/min', 14);
INSERT INTO common.uom VALUES (57, false, NULL, 'meters_per_minute', 'Meters per minute', 0, 'm/min', 14);
INSERT INTO common.uom VALUES (58, false, NULL, 'cycles_per_second', 'Cycles per second', 0, 'cycles/sec', 15);
INSERT INTO common.uom VALUES (59, false, NULL, 'cycles_per_minute', 'Cycles per minute', 0, 'cycles/min', 15);
INSERT INTO common.uom VALUES (60, false, NULL, 'orders', 'Orders', 0, 'X', 15);
INSERT INTO common.uom VALUES (61, false, NULL, 'revolutions_per_minute', 'Revolutions per minute', 0, 'rpm', 15);
INSERT INTO common.uom VALUES (62, false, NULL, 'fahrenheit', 'Fahrenheit', 0, 'Fahrenheit', 16);
INSERT INTO common.uom VALUES (63, false, NULL, 'rankine', 'Rankine', 0, 'Rankine', 16);
INSERT INTO common.uom VALUES (64, false, NULL, 'celcius', 'Celcius', 0, 'Celcius', 16);
INSERT INTO common.uom VALUES (65, false, NULL, 'kelvin', 'Kelvin', 0, 'Kelvin', 16);
INSERT INTO common.uom VALUES (66, false, NULL, 'foot_pound_force', 'Foot pound force', 0, 'ft-lbF', 17);
INSERT INTO common.uom VALUES (67, false, NULL, 'newton_meters', 'Newton-meters', 0, 'N-m', 17);
INSERT INTO common.uom VALUES (68, false, NULL, 'barrels_per_day', 'Barrels of oil per day', 0, 'bbl/day', 18);
INSERT INTO common.uom VALUES (69, false, NULL, 'barrels_per_hour', 'Barrels of oil per hour', 0, 'bbl/hr', 18);
INSERT INTO common.uom VALUES (70, false, NULL, 'barrels_per_minute', 'Barrels of oil per minute', 0, 'bbl/min', 18);
INSERT INTO common.uom VALUES (71, false, NULL, 'barrels_per_second', 'Barrels of oil per second', 0, 'bbl/sec', 18);
INSERT INTO common.uom VALUES (72, false, NULL, 'cubic_centimeters_per_day', 'Cubic centimeters per day', 0, 'cm3/day', 18);
INSERT INTO common.uom VALUES (73, false, NULL, 'cubic_centimeters_per_hour', 'Cubic centimeters per hour', 0, 'cm3/hr', 18);
INSERT INTO common.uom VALUES (74, false, NULL, 'cubic_centimeters_per_minute', 'Cubic centimeters per minute', 0, 'cm3/min', 18);
INSERT INTO common.uom VALUES (75, false, NULL, 'cubic_centimeters_per_second', 'Cubic centimeters per second', 0, 'cm3/sec', 18);
INSERT INTO common.uom VALUES (76, false, NULL, 'cubic_feet_per_day', 'Cubic feet per day', 0, 'ft3/day', 18);
INSERT INTO common.uom VALUES (77, false, NULL, 'cubic_feet_per_hour', 'Cubic feet per hour', 0, 'ft3/hr', 18);
INSERT INTO common.uom VALUES (78, false, NULL, 'cubic_feet_per_minute', 'Cubic feet per minute', 0, 'ft3/min', 18);
INSERT INTO common.uom VALUES (79, false, NULL, 'cubic_feet_per_second', 'Cubic feet per second', 0, 'ft3/sec', 18);
INSERT INTO common.uom VALUES (80, false, NULL, 'gallons_per_day', 'Gallons per day', 0, 'ga/day', 18);
INSERT INTO common.uom VALUES (81, false, NULL, 'gallons_per_hour', 'Gallons per hour', 0, 'gal/hr', 18);
INSERT INTO common.uom VALUES (82, false, NULL, 'gallons_per_minute', 'Gallons per minute', 0, 'gal/min', 18);
INSERT INTO common.uom VALUES (83, false, NULL, 'gallons_per_second', 'Gallons per second', 0, 'gal/sec', 18);
INSERT INTO common.uom VALUES (84, false, NULL, 'cubic_inches_per_day', 'Cubic inches per day', 0, 'in3/day', 18);
INSERT INTO common.uom VALUES (85, false, NULL, 'cubic_inches_per_hour', 'Cubic inches per hour', 0, 'in3/hr', 18);
INSERT INTO common.uom VALUES (86, false, NULL, 'cubic_inches_per_minute', 'Cubic inches per minute', 0, 'in3/min', 18);
INSERT INTO common.uom VALUES (87, false, NULL, 'cubic_inches_per_second', 'Cubic inches per second', 0, 'in3/sec', 18);
INSERT INTO common.uom VALUES (88, false, NULL, 'thousands_of_barrels_per_day', 'Thousands of barrels of oil per day', 0, 'kbbl/day', 18);
INSERT INTO common.uom VALUES (89, false, NULL, 'thousands_of_barrels_per_hour', 'Thousands of barrels of oil per hour', 0, 'kbbl/hr', 18);
INSERT INTO common.uom VALUES (90, false, NULL, 'thousands_of_barrels_per_minute', 'Thousands of barrels of oil per minute', 0, 'kbbl/min', 18);
INSERT INTO common.uom VALUES (91, false, NULL, 'thousands_of_barrels_per_second', 'Thousands of barrels of oil per second', 0, 'kbbl/sec', 18);
INSERT INTO common.uom VALUES (92, false, NULL, 'thousands_of_cubic_feet_per_day', 'Thousands of cubic feet per day', 0, 'kft3/day', 18);
INSERT INTO common.uom VALUES (93, false, NULL, 'thousands_of_cubic_feet_per_hour', 'Thousands of cubic feet per hour', 0, 'kft3/hr', 18);
INSERT INTO common.uom VALUES (94, false, NULL, 'thousands_of_cubic_feet_per_minute', 'Thousands of Cubic feet per minute', 0, 'kft3/min', 18);
INSERT INTO common.uom VALUES (95, false, NULL, 'thousands_of_cubic_feet_per_second', 'Thousands of cubic feet per second', 0, 'kft3/sec', 18);
INSERT INTO common.uom VALUES (96, false, NULL, 'liters_per_day', 'Liters per day', 0, 'liter/day', 18);
INSERT INTO common.uom VALUES (97, false, NULL, 'liters_per_hour', 'Liters per hour', 0, 'liter/hr', 18);
INSERT INTO common.uom VALUES (98, false, NULL, 'liters_per_minute', 'Liters per minute', 0, 'liter/min', 18);
INSERT INTO common.uom VALUES (99, false, NULL, 'liters_per_second', 'Liters per second', 0, 'liter/sec', 18);
INSERT INTO common.uom VALUES (100, false, NULL, 'cubic_meters_per_day', 'Cubic meters per day', 0, 'm3/day', 18);
INSERT INTO common.uom VALUES (101, false, NULL, 'cubic_meters_per_hour', 'Cubic meters per hour', 0, 'm3/hr', 18);
INSERT INTO common.uom VALUES (102, false, NULL, 'cubic_meters_per_minute', 'Cubic meters per minute', 0, 'm3/min', 18);
INSERT INTO common.uom VALUES (103, false, NULL, 'cubic_meters_per_second', 'Cubic meters per second', 0, 'm3/sec', 18);
INSERT INTO common.uom VALUES (104, false, NULL, 'millions_of_barrels_per_day', 'Millions of barrels of oil per day', 0, 'Mbbl/day', 18);
INSERT INTO common.uom VALUES (105, false, NULL, 'millions_of_barrels_per_hour', 'Millions of barrels of oil per hour', 0, 'Mbbl/hr', 18);
INSERT INTO common.uom VALUES (106, false, NULL, 'millions_of_barrels_per_minute', 'Millions of barrels of oil per minute', 0, 'Mbbl/min', 18);
INSERT INTO common.uom VALUES (107, false, NULL, 'millions_of_barrels_per_second', 'Millions of barrels of oil per second', 0, 'Mbbl/sec', 18);
INSERT INTO common.uom VALUES (108, false, NULL, 'millions_of_cubic_feet_per_day', 'Millions of cubic feet per day', 0, 'Mft3/day', 18);
INSERT INTO common.uom VALUES (109, false, NULL, 'millions_of_cubic_feet_per_hour', 'Millions of cubic feet per hour', 0, 'Mft3/hr', 18);
INSERT INTO common.uom VALUES (110, false, NULL, 'millions_of_cubic_feet_per_minute', 'Millions of cubic feet per minute', 0, 'Mft3/min', 18);
INSERT INTO common.uom VALUES (111, false, NULL, 'millions_of_cubic_feet_per_second', 'Millions of cubic feet per second', 0, 'Mft3/sec', 18);
INSERT INTO common.uom VALUES (112, false, NULL, 'ampere', 'Ampere', 0, 'A', 19);
INSERT INTO common.uom VALUES (113, false, NULL, 'milliampere', 'Milli Ampere', 0, 'mA', 19);
INSERT INTO common.uom VALUES (114, false, NULL, 'kiloampere', 'Kilo Ampere', 0, 'MA', 19);
INSERT INTO common.uom VALUES (115, false, NULL, 'volts', 'Volts', 0, 'V', 20);
INSERT INTO common.uom VALUES (116, false, NULL, 'kilovolts', 'KiloVolts', 0, 'kV', 20);
INSERT INTO common.uom VALUES (117, false, NULL, 'each', 'Each', 0, '', 21);
INSERT INTO common.uom VALUES (118, false, NULL, 'kilogram_per_meter_cube', 'Kilogram per meter cube', 0, 'kg/m3', 22);
INSERT INTO common.uom VALUES (119, false, NULL, 'gram_per_centimeter_cube', 'Gram Per Centimeter Cube', 0, 'g/cm3', 22);
INSERT INTO common.uom VALUES (120, false, NULL, 'percentage_by_mass', 'Percentage by Mass', 0, '%m', 23);
INSERT INTO common.uom VALUES (121, false, NULL, 'percentage_by_volume', 'Percentage by Volume', 0, '% v', 23);
INSERT INTO common.uom VALUES (122, false, NULL, 'meter_cube', 'Meter Cube', 0, 'm3', 24);
INSERT INTO common.uom VALUES (123, false, NULL, 'litre', 'Litre', 0, 'l', 24);
INSERT INTO common.uom VALUES (124, false, NULL, 'meter_square', 'Meter Square', 0, 'm2', 25);
INSERT INTO common.uom VALUES (125, false, NULL, 'centimeter_square', 'Centimeter', 0, 'cm2', 25);
INSERT INTO common.uom VALUES (126, false, NULL, 'kilogram', 'kilogram', 0, 'kg', 26);
INSERT INTO common.uom VALUES (127, false, NULL, 'ton', 'Ton', 0, 'ton', 26);
INSERT INTO common.uom VALUES (128, false, NULL, 'meters_per_second_squared', 'meters per second squared', 0, 'm/s2', 27);
INSERT INTO common.uom VALUES (129, false, NULL, 'g', 'G''s', 0, 'g', 27);
INSERT INTO common.uom VALUES (130, false, NULL, 'ohm', 'Ohm', 0, 'Ω', 28);
INSERT INTO common.uom VALUES (131, false, NULL, 'newton', 'Newton', 0, 'N', 29);
INSERT INTO common.uom VALUES (132, false, NULL, 'joules', 'Joules', 0, 'J', 30);
INSERT INTO common.uom VALUES (133, false, NULL, 'calories', 'Calories', 0, 'cal', 30);
INSERT INTO common.uom VALUES (134, false, NULL, 'kilojoules', 'kilo Joules', 0, 'kJ', 30);
INSERT INTO common.uom VALUES (135, false, NULL, 'radian', 'radian', 0, 'rad', 31);
INSERT INTO common.uom VALUES (136, false, NULL, 'degree', 'degree', 0, 'o', 31);
INSERT INTO common.uom VALUES (137, false, NULL, 'undefined', 'undefined', 0, '', 32);
INSERT INTO common.uom VALUES (138, false, NULL, 'kilojoule_per_kg', 'Kilojoule per kg', 0, 'kJ/kg', 33);
INSERT INTO common.uom VALUES (139, false, NULL, 'kilojoule_per_mol', 'Kilojoule per mol', 0, 'kJ/mol', 33);
INSERT INTO common.uom VALUES (140, false, NULL, 'pascal_second', 'Pascal second', 0, 'Pa.s', 34);
INSERT INTO common.uom VALUES (141, false, NULL, 'kilojoule_per_kg_celcius', 'Kilojoule per kg celcius', 0, 'kJ/kgoC', 35);
INSERT INTO common.uom VALUES (142, false, NULL, 'kilojoule_per_mol_celcius', 'Kilojoule per mol celcius', 0, 'kJ/kgmoloC', 35);
INSERT INTO common.uom VALUES (143, false, NULL, 'gram_per_mol', 'Gram per mol', 0, 'g/kmol', 36);
INSERT INTO common.uom VALUES (144, false, NULL, 'kilogram_per_kilomole', 'Kilogram per kilomole', 0, 'kg/kmol', 36);


--
-- TOC entry 7032 (class 0 OID 35393)
-- Dependencies: 306
-- Data for Name: process_segment; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7040 (class 0 OID 35532)
-- Dependencies: 314
-- Data for Name: operations_definition; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7098 (class 0 OID 36628)
-- Dependencies: 372
-- Data for Name: operations_segment; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7132 (class 0 OID 37345)
-- Dependencies: 406
-- Data for Name: work_master; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7134 (class 0 OID 37385)
-- Dependencies: 408
-- Data for Name: job_order; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7062 (class 0 OID 35953)
-- Dependencies: 336
-- Data for Name: work_performance; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7112 (class 0 OID 36909)
-- Dependencies: 386
-- Data for Name: work_response; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7162 (class 0 OID 38030)
-- Dependencies: 436
-- Data for Name: job_response; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 6960 (class 0 OID 34124)
-- Dependencies: 234
-- Data for Name: operations_schedule; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7004 (class 0 OID 34916)
-- Dependencies: 278
-- Data for Name: operations_performance; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7138 (class 0 OID 37486)
-- Dependencies: 412
-- Data for Name: operations_request; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7170 (class 0 OID 38242)
-- Dependencies: 444
-- Data for Name: operations_response; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7176 (class 0 OID 38424)
-- Dependencies: 450
-- Data for Name: segment_requirement; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7214 (class 0 OID 39332)
-- Dependencies: 488
-- Data for Name: segment_response; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 6964 (class 0 OID 34175)
-- Dependencies: 238
-- Data for Name: equipment_class; Type: TABLE DATA; Schema: common; Owner: apps
--

INSERT INTO common.equipment_class VALUES (1, false, NULL, 'base_equipment_class', 'Base Equipment Class', 0, NULL, false, NULL);
INSERT INTO common.equipment_class VALUES (2, false, NULL, 'site_hierarchy', 'Site Hierarchy', 0, 1, false, 1);
INSERT INTO common.equipment_class VALUES (3, false, NULL, 'equipment', 'Equipment', 0, 193, false, 1);
INSERT INTO common.equipment_class VALUES (4, false, NULL, 'sensor', 'Sensor', 0, 196, false, 1);
INSERT INTO common.equipment_class VALUES (5, false, 'This refers to the class composition concept.', 'template', 'Template', 0, NULL, false, 1);
INSERT INTO common.equipment_class VALUES (6, false, NULL, 'enterprise', 'Enterprise', 0, 1, true, 2);
INSERT INTO common.equipment_class VALUES (7, false, NULL, 'site', 'Site', 0, 2, true, 2);
INSERT INTO common.equipment_class VALUES (8, false, NULL, 'factory', 'Factory', 0, 3, true, 2);
INSERT INTO common.equipment_class VALUES (9, false, NULL, 'area', 'Area', 0, 3, true, 2);
INSERT INTO common.equipment_class VALUES (10, false, NULL, 'production_line', 'Production Line', 0, 4, true, 2);
INSERT INTO common.equipment_class VALUES (11, false, NULL, 'work_center', 'Work Center', 0, 4, true, 2);


--
-- TOC entry 7220 (class 0 OID 39470)
-- Dependencies: 494
-- Data for Name: equipment_actual; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7240 (class 0 OID 39968)
-- Dependencies: 514
-- Data for Name: equipment_actual_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7002 (class 0 OID 34884)
-- Dependencies: 276
-- Data for Name: operations_capability; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7054 (class 0 OID 35794)
-- Dependencies: 328
-- Data for Name: process_segment_capability; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7012 (class 0 OID 35059)
-- Dependencies: 286
-- Data for Name: work_capability; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7142 (class 0 OID 37562)
-- Dependencies: 416
-- Data for Name: work_master_capability; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7158 (class 0 OID 37921)
-- Dependencies: 432
-- Data for Name: equipment_capability; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7192 (class 0 OID 38794)
-- Dependencies: 466
-- Data for Name: equipment_capability_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7018 (class 0 OID 35139)
-- Dependencies: 292
-- Data for Name: equipment_class_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7020 (class 0 OID 35180)
-- Dependencies: 294
-- Data for Name: equipment_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7042 (class 0 OID 35558)
-- Dependencies: 316
-- Data for Name: equipment_capability_test_specification; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7100 (class 0 OID 36675)
-- Dependencies: 374
-- Data for Name: equipment_capability_test_result; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7194 (class 0 OID 38841)
-- Dependencies: 468
-- Data for Name: equipment_requirement; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7222 (class 0 OID 39517)
-- Dependencies: 496
-- Data for Name: equipment_requirement_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7044 (class 0 OID 35593)
-- Dependencies: 318
-- Data for Name: job_list_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7160 (class 0 OID 37989)
-- Dependencies: 434
-- Data for Name: job_order_parameter; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7196 (class 0 OID 38888)
-- Dependencies: 470
-- Data for Name: job_response_data; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 6976 (class 0 OID 34391)
-- Dependencies: 250
-- Data for Name: material_definition; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7000 (class 0 OID 34832)
-- Dependencies: 274
-- Data for Name: material_lot; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7046 (class 0 OID 35634)
-- Dependencies: 320
-- Data for Name: material_sub_lot; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 6974 (class 0 OID 34359)
-- Dependencies: 248
-- Data for Name: material_class; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7224 (class 0 OID 39565)
-- Dependencies: 498
-- Data for Name: material_actual; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7242 (class 0 OID 40016)
-- Dependencies: 516
-- Data for Name: material_actual_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7164 (class 0 OID 38082)
-- Dependencies: 438
-- Data for Name: material_capability; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7198 (class 0 OID 38929)
-- Dependencies: 472
-- Data for Name: material_capability_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7102 (class 0 OID 36700)
-- Dependencies: 376
-- Data for Name: material_lot_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7026 (class 0 OID 35285)
-- Dependencies: 300
-- Data for Name: material_class_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7028 (class 0 OID 35326)
-- Dependencies: 302
-- Data for Name: material_definition_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7136 (class 0 OID 37436)
-- Dependencies: 410
-- Data for Name: material_capability_test_specification; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7166 (class 0 OID 38176)
-- Dependencies: 440
-- Data for Name: material_capability_test_result; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7200 (class 0 OID 38977)
-- Dependencies: 474
-- Data for Name: material_requirement; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7226 (class 0 OID 39643)
-- Dependencies: 500
-- Data for Name: material_requirement_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7048 (class 0 OID 35687)
-- Dependencies: 322
-- Data for Name: operations_capability_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7050 (class 0 OID 35728)
-- Dependencies: 324
-- Data for Name: operations_performance_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7168 (class 0 OID 38201)
-- Dependencies: 442
-- Data for Name: operations_request_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7202 (class 0 OID 39055)
-- Dependencies: 476
-- Data for Name: operations_response_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7006 (class 0 OID 34952)
-- Dependencies: 280
-- Data for Name: operations_schedule_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 6934 (class 0 OID 33884)
-- Dependencies: 208
-- Data for Name: person; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6936 (class 0 OID 33899)
-- Dependencies: 210
-- Data for Name: personnel_class; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7228 (class 0 OID 39691)
-- Dependencies: 502
-- Data for Name: personnel_actual; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7244 (class 0 OID 40063)
-- Dependencies: 518
-- Data for Name: personnel_actual_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7172 (class 0 OID 38288)
-- Dependencies: 446
-- Data for Name: personnel_capability; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7204 (class 0 OID 39096)
-- Dependencies: 478
-- Data for Name: personnel_capability_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 6978 (class 0 OID 34422)
-- Dependencies: 252
-- Data for Name: person_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6980 (class 0 OID 34463)
-- Dependencies: 254
-- Data for Name: personnel_class_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7008 (class 0 OID 34993)
-- Dependencies: 282
-- Data for Name: personnel_capability_test_specification; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7052 (class 0 OID 35769)
-- Dependencies: 326
-- Data for Name: personnel_capability_test_result; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7206 (class 0 OID 39144)
-- Dependencies: 480
-- Data for Name: personnel_requirement; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7230 (class 0 OID 39743)
-- Dependencies: 504
-- Data for Name: personnel_requirement_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7030 (class 0 OID 35367)
-- Dependencies: 304
-- Data for Name: physical_asset; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6938 (class 0 OID 33921)
-- Dependencies: 212
-- Data for Name: physical_asset_class; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7232 (class 0 OID 39791)
-- Dependencies: 506
-- Data for Name: physical_asset_actual; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7246 (class 0 OID 40111)
-- Dependencies: 520
-- Data for Name: physical_asset_actual_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7174 (class 0 OID 38356)
-- Dependencies: 448
-- Data for Name: physical_asset_capability; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7208 (class 0 OID 39191)
-- Dependencies: 482
-- Data for Name: physical_asset_capability_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 6982 (class 0 OID 34504)
-- Dependencies: 256
-- Data for Name: physical_asset_class_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7080 (class 0 OID 36284)
-- Dependencies: 354
-- Data for Name: physical_asset_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7104 (class 0 OID 36746)
-- Dependencies: 378
-- Data for Name: physical_asset_capability_test_spec; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7140 (class 0 OID 37537)
-- Dependencies: 414
-- Data for Name: physical_asset_capability_test_result; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7210 (class 0 OID 39239)
-- Dependencies: 484
-- Data for Name: physical_asset_requirement; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7234 (class 0 OID 39838)
-- Dependencies: 508
-- Data for Name: physical_asset_requirement_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7236 (class 0 OID 39886)
-- Dependencies: 510
-- Data for Name: segment_data; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7212 (class 0 OID 39291)
-- Dependencies: 486
-- Data for Name: segment_parameter; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7010 (class 0 OID 35028)
-- Dependencies: 284
-- Data for Name: work_alert_definition; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7056 (class 0 OID 35836)
-- Dependencies: 330
-- Data for Name: work_alert; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7058 (class 0 OID 35872)
-- Dependencies: 332
-- Data for Name: work_alert_definition_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7106 (class 0 OID 36781)
-- Dependencies: 380
-- Data for Name: work_alert_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7060 (class 0 OID 35912)
-- Dependencies: 334
-- Data for Name: work_capability_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7178 (class 0 OID 38486)
-- Dependencies: 452
-- Data for Name: work_master_capability_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7108 (class 0 OID 36827)
-- Dependencies: 382
-- Data for Name: work_performance_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7110 (class 0 OID 36868)
-- Dependencies: 384
-- Data for Name: work_request_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7144 (class 0 OID 37598)
-- Dependencies: 418
-- Data for Name: work_response_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7066 (class 0 OID 36032)
-- Dependencies: 340
-- Data for Name: work_schedule_property; Type: TABLE DATA; Schema: applications; Owner: apps
--



--
-- TOC entry 7258 (class 0 OID 41624)
-- Dependencies: 532
-- Data for Name: document; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7068 (class 0 OID 36073)
-- Dependencies: 342
-- Data for Name: equipment_asset_mapping; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7016 (class 0 OID 35117)
-- Dependencies: 290
-- Data for Name: equipment_class_mapping; Type: TABLE DATA; Schema: common; Owner: apps
--

INSERT INTO common.equipment_class_mapping VALUES (1, false, NULL, 6, 1);


--
-- TOC entry 7070 (class 0 OID 36095)
-- Dependencies: 344
-- Data for Name: equipment_segment_specification; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7114 (class 0 OID 36956)
-- Dependencies: 388
-- Data for Name: equipment_segment_specification_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7256 (class 0 OID 41595)
-- Dependencies: 530
-- Data for Name: equipment_template; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6928 (class 0 OID 33827)
-- Dependencies: 202
-- Data for Name: event_class; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6966 (class 0 OID 34202)
-- Dependencies: 240
-- Data for Name: event_class_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6952 (class 0 OID 34033)
-- Dependencies: 226
-- Data for Name: event_definition; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6968 (class 0 OID 34244)
-- Dependencies: 242
-- Data for Name: event_definition_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6970 (class 0 OID 34286)
-- Dependencies: 244
-- Data for Name: event_object_relationship; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7022 (class 0 OID 35221)
-- Dependencies: 296
-- Data for Name: event_object_relationship_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6940 (class 0 OID 33943)
-- Dependencies: 214
-- Data for Name: resource_network_connection_type; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6986 (class 0 OID 34586)
-- Dependencies: 260
-- Data for Name: resource_relationship_network; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7034 (class 0 OID 35430)
-- Dependencies: 308
-- Data for Name: resource_network_connection; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7072 (class 0 OID 36137)
-- Dependencies: 346
-- Data for Name: from_resource_reference; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7116 (class 0 OID 37004)
-- Dependencies: 390
-- Data for Name: from_resource_reference_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6930 (class 0 OID 33848)
-- Dependencies: 204
-- Data for Name: knowledge_category; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6972 (class 0 OID 34316)
-- Dependencies: 246
-- Data for Name: knowledge_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7254 (class 0 OID 41567)
-- Dependencies: 528
-- Data for Name: material_assembly_definition; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7024 (class 0 OID 35263)
-- Dependencies: 298
-- Data for Name: material_class_mapping; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7074 (class 0 OID 36162)
-- Dependencies: 348
-- Data for Name: material_segment_specification; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7096 (class 0 OID 36608)
-- Dependencies: 370
-- Data for Name: operations_material_bill; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7124 (class 0 OID 37182)
-- Dependencies: 398
-- Data for Name: operations_material_bill_item; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7146 (class 0 OID 37639)
-- Dependencies: 420
-- Data for Name: material_segment_specification_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6956 (class 0 OID 34082)
-- Dependencies: 230
-- Data for Name: personnel_class_mapping; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7076 (class 0 OID 36220)
-- Dependencies: 350
-- Data for Name: personnel_segment_specification; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7118 (class 0 OID 37045)
-- Dependencies: 392
-- Data for Name: personnel_segment_specification_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7078 (class 0 OID 36262)
-- Dependencies: 352
-- Data for Name: physical_asset_class_mapping; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7082 (class 0 OID 36325)
-- Dependencies: 356
-- Data for Name: physical_asset_segment_specification; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7120 (class 0 OID 37093)
-- Dependencies: 394
-- Data for Name: physical_asset_segment_specification_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7084 (class 0 OID 36367)
-- Dependencies: 358
-- Data for Name: process_segment_dependency; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7086 (class 0 OID 36409)
-- Dependencies: 360
-- Data for Name: process_segment_parameter; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6984 (class 0 OID 34545)
-- Dependencies: 258
-- Data for Name: resource_network_connection_type_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7088 (class 0 OID 36450)
-- Dependencies: 362
-- Data for Name: resource_network_connection_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7090 (class 0 OID 36496)
-- Dependencies: 364
-- Data for Name: to_resource_reference; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7122 (class 0 OID 37141)
-- Dependencies: 396
-- Data for Name: to_resource_reference_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6944 (class 0 OID 33973)
-- Dependencies: 218
-- Data for Name: work_calendar; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6946 (class 0 OID 33988)
-- Dependencies: 220
-- Data for Name: work_calendar_definition; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6988 (class 0 OID 34611)
-- Dependencies: 262
-- Data for Name: work_calendar_definition_entry; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7036 (class 0 OID 35455)
-- Dependencies: 310
-- Data for Name: work_calendar_definition_entry_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6990 (class 0 OID 34642)
-- Dependencies: 264
-- Data for Name: work_calendar_definition_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7038 (class 0 OID 35496)
-- Dependencies: 312
-- Data for Name: work_calendar_entry; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7092 (class 0 OID 36521)
-- Dependencies: 366
-- Data for Name: work_calendar_entry_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 6992 (class 0 OID 34683)
-- Dependencies: 266
-- Data for Name: work_calendar_property; Type: TABLE DATA; Schema: common; Owner: apps
--



--
-- TOC entry 7248 (class 0 OID 40159)
-- Dependencies: 522
-- Data for Name: privilege; Type: TABLE DATA; Schema: guardian; Owner: apps
--



--
-- TOC entry 7250 (class 0 OID 40179)
-- Dependencies: 524
-- Data for Name: role_privileges; Type: TABLE DATA; Schema: guardian; Owner: apps
--



--
-- TOC entry 7252 (class 0 OID 40201)
-- Dependencies: 526
-- Data for Name: user_restricted_privileges; Type: TABLE DATA; Schema: guardian; Owner: apps
--



--
-- TOC entry 7148 (class 0 OID 37692)
-- Dependencies: 422
-- Data for Name: equipment_specification; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7180 (class 0 OID 38527)
-- Dependencies: 454
-- Data for Name: equipment_specification_property; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7150 (class 0 OID 37739)
-- Dependencies: 424
-- Data for Name: material_specification; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7182 (class 0 OID 38575)
-- Dependencies: 456
-- Data for Name: material_specification_property; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7094 (class 0 OID 36567)
-- Dependencies: 368
-- Data for Name: operations_definition_property; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7126 (class 0 OID 37240)
-- Dependencies: 400
-- Data for Name: operations_segment_dependency; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7128 (class 0 OID 37282)
-- Dependencies: 402
-- Data for Name: operations_segment_mapping; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7130 (class 0 OID 37304)
-- Dependencies: 404
-- Data for Name: operations_segment_parameter_specification; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7152 (class 0 OID 37807)
-- Dependencies: 426
-- Data for Name: personnel_specification; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7184 (class 0 OID 38623)
-- Dependencies: 458
-- Data for Name: personnel_specification_property; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7154 (class 0 OID 37854)
-- Dependencies: 428
-- Data for Name: physical_asset_specification; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7186 (class 0 OID 38670)
-- Dependencies: 460
-- Data for Name: physical_asset_specification_property; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7156 (class 0 OID 37901)
-- Dependencies: 430
-- Data for Name: workflow_specification; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 6948 (class 0 OID 34003)
-- Dependencies: 222
-- Data for Name: workflow_specification_connection_type; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 6950 (class 0 OID 34018)
-- Dependencies: 224
-- Data for Name: workflow_specification_node_type; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7188 (class 0 OID 38718)
-- Dependencies: 462
-- Data for Name: workflow_specification_node; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7216 (class 0 OID 39394)
-- Dependencies: 490
-- Data for Name: workflow_specification_connection; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7238 (class 0 OID 39927)
-- Dependencies: 512
-- Data for Name: workflow_specification_connection_property; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 6994 (class 0 OID 34724)
-- Dependencies: 268
-- Data for Name: workflow_specification_connection_type_property; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7218 (class 0 OID 39429)
-- Dependencies: 492
-- Data for Name: workflow_specification_node_property; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 6996 (class 0 OID 34765)
-- Dependencies: 270
-- Data for Name: workflow_specification_node_type_property; Type: TABLE DATA; Schema: operations; Owner: apps
--



--
-- TOC entry 7190 (class 0 OID 38753)
-- Dependencies: 464
-- Data for Name: workflow_specification_property; Type: TABLE DATA; Schema: operations; Owner: apps
--


-- TOC entry 7264 (class 0 OID 0)
-- Dependencies: 493
-- Name: equipment_actual_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_actual_id_seq', 1, false);


--
-- TOC entry 7265 (class 0 OID 0)
-- Dependencies: 513
-- Name: equipment_actual_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_actual_property_id_seq', 1, false);


--
-- TOC entry 7266 (class 0 OID 0)
-- Dependencies: 431
-- Name: equipment_capability_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_capability_id_seq', 1, false);


--
-- TOC entry 7267 (class 0 OID 0)
-- Dependencies: 465
-- Name: equipment_capability_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_capability_property_id_seq', 1, false);


--
-- TOC entry 7268 (class 0 OID 0)
-- Dependencies: 373
-- Name: equipment_capability_test_result_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_capability_test_result_id_seq', 1, false);


--
-- TOC entry 7269 (class 0 OID 0)
-- Dependencies: 315
-- Name: equipment_capability_test_specification_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_capability_test_specification_id_seq', 1, false);


--
-- TOC entry 7270 (class 0 OID 0)
-- Dependencies: 467
-- Name: equipment_requirement_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_requirement_id_seq', 1, false);


--
-- TOC entry 7271 (class 0 OID 0)
-- Dependencies: 495
-- Name: equipment_requirement_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_requirement_property_id_seq', 1, false);


--
-- TOC entry 7272 (class 0 OID 0)
-- Dependencies: 271
-- Name: job_list_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.job_list_id_seq', 1, false);


--
-- TOC entry 7273 (class 0 OID 0)
-- Dependencies: 317
-- Name: job_list_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.job_list_property_id_seq', 1, false);


--
-- TOC entry 7274 (class 0 OID 0)
-- Dependencies: 407
-- Name: job_order_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.job_order_id_seq', 1, false);


--
-- TOC entry 7275 (class 0 OID 0)
-- Dependencies: 433
-- Name: job_order_parameter_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.job_order_parameter_id_seq', 1, false);


--
-- TOC entry 7276 (class 0 OID 0)
-- Dependencies: 469
-- Name: job_response_data_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.job_response_data_id_seq', 1, false);


--
-- TOC entry 7277 (class 0 OID 0)
-- Dependencies: 435
-- Name: job_response_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.job_response_id_seq', 1, false);


--
-- TOC entry 7278 (class 0 OID 0)
-- Dependencies: 497
-- Name: material_actual_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_actual_id_seq', 1, false);


--
-- TOC entry 7279 (class 0 OID 0)
-- Dependencies: 515
-- Name: material_actual_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_actual_property_id_seq', 1, false);


--
-- TOC entry 7280 (class 0 OID 0)
-- Dependencies: 437
-- Name: material_capability_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_capability_id_seq', 1, false);


--
-- TOC entry 7281 (class 0 OID 0)
-- Dependencies: 471
-- Name: material_capability_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_capability_property_id_seq', 1, false);


--
-- TOC entry 7282 (class 0 OID 0)
-- Dependencies: 439
-- Name: material_capability_test_result_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_capability_test_result_id_seq', 1, false);


--
-- TOC entry 7283 (class 0 OID 0)
-- Dependencies: 409
-- Name: material_capability_test_specification_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_capability_test_specification_id_seq', 1, false);


--
-- TOC entry 7284 (class 0 OID 0)
-- Dependencies: 273
-- Name: material_lot_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_lot_id_seq', 1, false);


--
-- TOC entry 7285 (class 0 OID 0)
-- Dependencies: 375
-- Name: material_lot_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_lot_property_id_seq', 1, false);


--
-- TOC entry 7286 (class 0 OID 0)
-- Dependencies: 473
-- Name: material_requirement_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_requirement_id_seq', 1, false);


--
-- TOC entry 7287 (class 0 OID 0)
-- Dependencies: 499
-- Name: material_requirement_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_requirement_property_id_seq', 1, false);


--
-- TOC entry 7288 (class 0 OID 0)
-- Dependencies: 319
-- Name: material_sub_lot_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.material_sub_lot_id_seq', 1, false);


--
-- TOC entry 7289 (class 0 OID 0)
-- Dependencies: 275
-- Name: operations_capability_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_capability_id_seq', 1, false);


--
-- TOC entry 7290 (class 0 OID 0)
-- Dependencies: 321
-- Name: operations_capability_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_capability_property_id_seq', 1, false);


--
-- TOC entry 7291 (class 0 OID 0)
-- Dependencies: 277
-- Name: operations_performance_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_performance_id_seq', 1, false);


--
-- TOC entry 7292 (class 0 OID 0)
-- Dependencies: 323
-- Name: operations_performance_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_performance_property_id_seq', 1, false);


--
-- TOC entry 7293 (class 0 OID 0)
-- Dependencies: 411
-- Name: operations_request_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_request_id_seq', 1, false);


--
-- TOC entry 7294 (class 0 OID 0)
-- Dependencies: 441
-- Name: operations_request_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_request_property_id_seq', 1, false);


--
-- TOC entry 7295 (class 0 OID 0)
-- Dependencies: 443
-- Name: operations_response_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_response_id_seq', 1, false);


--
-- TOC entry 7296 (class 0 OID 0)
-- Dependencies: 475
-- Name: operations_response_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_response_property_id_seq', 1, false);


--
-- TOC entry 7297 (class 0 OID 0)
-- Dependencies: 233
-- Name: operations_schedule_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_schedule_id_seq', 1, false);


--
-- TOC entry 7298 (class 0 OID 0)
-- Dependencies: 279
-- Name: operations_schedule_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.operations_schedule_property_id_seq', 1, false);


--
-- TOC entry 7299 (class 0 OID 0)
-- Dependencies: 501
-- Name: personnel_actual_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_actual_id_seq', 1, false);


--
-- TOC entry 7300 (class 0 OID 0)
-- Dependencies: 517
-- Name: personnel_actual_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_actual_property_id_seq', 1, false);


--
-- TOC entry 7301 (class 0 OID 0)
-- Dependencies: 445
-- Name: personnel_capability_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_capability_id_seq', 1, false);


--
-- TOC entry 7302 (class 0 OID 0)
-- Dependencies: 477
-- Name: personnel_capability_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_capability_property_id_seq', 1, false);


--
-- TOC entry 7303 (class 0 OID 0)
-- Dependencies: 325
-- Name: personnel_capability_test_result_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_capability_test_result_id_seq', 1, false);


--
-- TOC entry 7304 (class 0 OID 0)
-- Dependencies: 281
-- Name: personnel_capability_test_specification_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_capability_test_specification_id_seq', 1, false);


--
-- TOC entry 7305 (class 0 OID 0)
-- Dependencies: 479
-- Name: personnel_requirement_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_requirement_id_seq', 1, false);


--
-- TOC entry 7306 (class 0 OID 0)
-- Dependencies: 503
-- Name: personnel_requirement_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_requirement_property_id_seq', 1, false);


--
-- TOC entry 7307 (class 0 OID 0)
-- Dependencies: 505
-- Name: physical_asset_actual_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_actual_id_seq', 1, false);


--
-- TOC entry 7308 (class 0 OID 0)
-- Dependencies: 519
-- Name: physical_asset_actual_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_actual_property_id_seq', 1, false);


--
-- TOC entry 7309 (class 0 OID 0)
-- Dependencies: 447
-- Name: physical_asset_capability_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_capability_id_seq', 1, false);


--
-- TOC entry 7310 (class 0 OID 0)
-- Dependencies: 481
-- Name: physical_asset_capability_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_capability_property_id_seq', 1, false);


--
-- TOC entry 7311 (class 0 OID 0)
-- Dependencies: 413
-- Name: physical_asset_capability_test_result_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_capability_test_result_id_seq', 1, false);


--
-- TOC entry 7312 (class 0 OID 0)
-- Dependencies: 377
-- Name: physical_asset_capability_test_spec_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_capability_test_spec_id_seq', 1, false);


--
-- TOC entry 7313 (class 0 OID 0)
-- Dependencies: 483
-- Name: physical_asset_requirement_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_requirement_id_seq', 1, false);


--
-- TOC entry 7314 (class 0 OID 0)
-- Dependencies: 507
-- Name: physical_asset_requirement_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_requirement_property_id_seq', 1, false);


--
-- TOC entry 7315 (class 0 OID 0)
-- Dependencies: 327
-- Name: process_segment_capability_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.process_segment_capability_id_seq', 1, false);


--
-- TOC entry 7316 (class 0 OID 0)
-- Dependencies: 509
-- Name: segment_data_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.segment_data_id_seq', 1, false);


--
-- TOC entry 7317 (class 0 OID 0)
-- Dependencies: 485
-- Name: segment_parameter_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.segment_parameter_id_seq', 1, false);


--
-- TOC entry 7318 (class 0 OID 0)
-- Dependencies: 449
-- Name: segment_requirement_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.segment_requirement_id_seq', 1, false);


--
-- TOC entry 7319 (class 0 OID 0)
-- Dependencies: 487
-- Name: segment_response_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.segment_response_id_seq', 1, false);


--
-- TOC entry 7320 (class 0 OID 0)
-- Dependencies: 283
-- Name: work_alert_definition_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_alert_definition_id_seq', 1, false);


--
-- TOC entry 7321 (class 0 OID 0)
-- Dependencies: 331
-- Name: work_alert_definition_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_alert_definition_property_id_seq', 1, false);


--
-- TOC entry 7322 (class 0 OID 0)
-- Dependencies: 329
-- Name: work_alert_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_alert_id_seq', 1, false);


--
-- TOC entry 7323 (class 0 OID 0)
-- Dependencies: 379
-- Name: work_alert_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_alert_property_id_seq', 1, false);


--
-- TOC entry 7324 (class 0 OID 0)
-- Dependencies: 285
-- Name: work_capability_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_capability_id_seq', 1, false);


--
-- TOC entry 7325 (class 0 OID 0)
-- Dependencies: 333
-- Name: work_capability_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_capability_property_id_seq', 1, false);


--
-- TOC entry 7326 (class 0 OID 0)
-- Dependencies: 415
-- Name: work_master_capability_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_master_capability_id_seq', 1, false);


--
-- TOC entry 7327 (class 0 OID 0)
-- Dependencies: 451
-- Name: work_master_capability_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_master_capability_property_id_seq', 1, false);


--
-- TOC entry 7328 (class 0 OID 0)
-- Dependencies: 335
-- Name: work_performance_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_performance_id_seq', 1, false);


--
-- TOC entry 7329 (class 0 OID 0)
-- Dependencies: 381
-- Name: work_performance_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_performance_property_id_seq', 1, false);


--
-- TOC entry 7330 (class 0 OID 0)
-- Dependencies: 337
-- Name: work_request_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_request_id_seq', 1, false);


--
-- TOC entry 7331 (class 0 OID 0)
-- Dependencies: 383
-- Name: work_request_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_request_property_id_seq', 1, false);


--
-- TOC entry 7332 (class 0 OID 0)
-- Dependencies: 385
-- Name: work_response_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_response_id_seq', 1, false);


--
-- TOC entry 7333 (class 0 OID 0)
-- Dependencies: 417
-- Name: work_response_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_response_property_id_seq', 1, false);


--
-- TOC entry 7334 (class 0 OID 0)
-- Dependencies: 287
-- Name: work_schedule_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_schedule_id_seq', 1, false);


--
-- TOC entry 7335 (class 0 OID 0)
-- Dependencies: 339
-- Name: work_schedule_property_id_seq; Type: SEQUENCE SET; Schema: applications; Owner: apps
--

SELECT pg_catalog.setval('common.work_schedule_property_id_seq', 1, false);


--
-- TOC entry 7336 (class 0 OID 0)
-- Dependencies: 531
-- Name: document_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.document_id_seq', 1, false);


--
-- TOC entry 7337 (class 0 OID 0)
-- Dependencies: 341
-- Name: equipment_asset_mapping_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_asset_mapping_id_seq', 1, false);


--
-- TOC entry 7338 (class 0 OID 0)
-- Dependencies: 237
-- Name: equipment_class_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_class_id_seq', 11, true);


--
-- TOC entry 7339 (class 0 OID 0)
-- Dependencies: 289
-- Name: equipment_class_mapping_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_class_mapping_id_seq', 1, true);


--
-- TOC entry 7340 (class 0 OID 0)
-- Dependencies: 291
-- Name: equipment_class_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_class_property_id_seq', 1, false);


--
-- TOC entry 7341 (class 0 OID 0)
-- Dependencies: 235
-- Name: equipment_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_id_seq', 1, true);


--
-- TOC entry 7342 (class 0 OID 0)
-- Dependencies: 293
-- Name: equipment_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_property_id_seq', 1, false);


--
-- TOC entry 7343 (class 0 OID 0)
-- Dependencies: 343
-- Name: equipment_segment_specification_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_segment_specification_id_seq', 1, false);


--
-- TOC entry 7344 (class 0 OID 0)
-- Dependencies: 387
-- Name: equipment_segment_specification_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_segment_specification_property_id_seq', 1, false);


--
-- TOC entry 7345 (class 0 OID 0)
-- Dependencies: 529
-- Name: equipment_template_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_template_id_seq', 1, false);


--
-- TOC entry 7346 (class 0 OID 0)
-- Dependencies: 201
-- Name: event_class_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.event_class_id_seq', 1, false);


--
-- TOC entry 7347 (class 0 OID 0)
-- Dependencies: 239
-- Name: event_class_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.event_class_property_id_seq', 1, false);


--
-- TOC entry 7348 (class 0 OID 0)
-- Dependencies: 225
-- Name: event_definition_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.event_definition_id_seq', 1, false);


--
-- TOC entry 7349 (class 0 OID 0)
-- Dependencies: 241
-- Name: event_definition_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.event_definition_property_id_seq', 1, false);


--
-- TOC entry 7350 (class 0 OID 0)
-- Dependencies: 243
-- Name: event_object_relationship_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.event_object_relationship_id_seq', 1, false);


--
-- TOC entry 7351 (class 0 OID 0)
-- Dependencies: 295
-- Name: event_object_relationship_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.event_object_relationship_property_id_seq', 1, false);


--
-- TOC entry 7352 (class 0 OID 0)
-- Dependencies: 345
-- Name: from_resource_reference_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.from_resource_reference_id_seq', 1, false);


--
-- TOC entry 7353 (class 0 OID 0)
-- Dependencies: 389
-- Name: from_resource_reference_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.from_resource_reference_property_id_seq', 1, false);


--
-- TOC entry 7354 (class 0 OID 0)
-- Dependencies: 203
-- Name: knowledge_category_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.knowledge_category_id_seq', 1, false);


--
-- TOC entry 7355 (class 0 OID 0)
-- Dependencies: 245
-- Name: knowledge_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.knowledge_property_id_seq', 1, false);


--
-- TOC entry 7356 (class 0 OID 0)
-- Dependencies: 205
-- Name: lookup_category_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.lookup_category_id_seq', 29, true);


--
-- TOC entry 7357 (class 0 OID 0)
-- Dependencies: 227
-- Name: lookup_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.lookup_id_seq', 205, true);


--
-- TOC entry 7358 (class 0 OID 0)
-- Dependencies: 527
-- Name: material_assembly_definition_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.material_assembly_definition_id_seq', 1, false);


--
-- TOC entry 7359 (class 0 OID 0)
-- Dependencies: 247
-- Name: material_class_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.material_class_id_seq', 1, false);


--
-- TOC entry 7360 (class 0 OID 0)
-- Dependencies: 297
-- Name: material_class_mapping_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.material_class_mapping_id_seq', 1, false);


--
-- TOC entry 7361 (class 0 OID 0)
-- Dependencies: 299
-- Name: material_class_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.material_class_property_id_seq', 1, false);


--
-- TOC entry 7362 (class 0 OID 0)
-- Dependencies: 249
-- Name: material_definition_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.material_definition_id_seq', 1, false);


--
-- TOC entry 7363 (class 0 OID 0)
-- Dependencies: 301
-- Name: material_definition_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.material_definition_property_id_seq', 1, false);


--
-- TOC entry 7364 (class 0 OID 0)
-- Dependencies: 347
-- Name: material_segment_specification_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.material_segment_specification_id_seq', 1, false);


--
-- TOC entry 7365 (class 0 OID 0)
-- Dependencies: 419
-- Name: material_segment_specification_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.material_segment_specification_property_id_seq', 1, false);


--
-- TOC entry 7366 (class 0 OID 0)
-- Dependencies: 207
-- Name: person_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.person_id_seq', 1, false);


--
-- TOC entry 7367 (class 0 OID 0)
-- Dependencies: 251
-- Name: person_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.person_property_id_seq', 1, false);


--
-- TOC entry 7368 (class 0 OID 0)
-- Dependencies: 209
-- Name: personnel_class_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_class_id_seq', 1, false);


--
-- TOC entry 7369 (class 0 OID 0)
-- Dependencies: 229
-- Name: personnel_class_mapping_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_class_mapping_id_seq', 1, false);


--
-- TOC entry 7370 (class 0 OID 0)
-- Dependencies: 253
-- Name: personnel_class_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_class_property_id_seq', 1, false);


--
-- TOC entry 7371 (class 0 OID 0)
-- Dependencies: 349
-- Name: personnel_segment_specification_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_segment_specification_id_seq', 1, false);


--
-- TOC entry 7372 (class 0 OID 0)
-- Dependencies: 391
-- Name: personnel_segment_specification_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_segment_specification_property_id_seq', 1, false);


--
-- TOC entry 7373 (class 0 OID 0)
-- Dependencies: 211
-- Name: physical_asset_class_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_class_id_seq', 1, false);


--
-- TOC entry 7374 (class 0 OID 0)
-- Dependencies: 351
-- Name: physical_asset_class_mapping_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_class_mapping_id_seq', 1, false);


--
-- TOC entry 7375 (class 0 OID 0)
-- Dependencies: 255
-- Name: physical_asset_class_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_class_property_id_seq', 1, false);


--
-- TOC entry 7376 (class 0 OID 0)
-- Dependencies: 303
-- Name: physical_asset_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_id_seq', 1, false);


--
-- TOC entry 7377 (class 0 OID 0)
-- Dependencies: 353
-- Name: physical_asset_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_property_id_seq', 1, false);


--
-- TOC entry 7378 (class 0 OID 0)
-- Dependencies: 355
-- Name: physical_asset_segment_specification_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_segment_specification_id_seq', 1, false);


--
-- TOC entry 7379 (class 0 OID 0)
-- Dependencies: 393
-- Name: physical_asset_segment_specification_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_segment_specification_property_id_seq', 1, false);


--
-- TOC entry 7380 (class 0 OID 0)
-- Dependencies: 357
-- Name: process_segment_dependency_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.process_segment_dependency_id_seq', 1, false);


--
-- TOC entry 7381 (class 0 OID 0)
-- Dependencies: 305
-- Name: process_segment_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.process_segment_id_seq', 1, false);


--
-- TOC entry 7382 (class 0 OID 0)
-- Dependencies: 359
-- Name: process_segment_parameter_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.process_segment_parameter_id_seq', 1, false);


--
-- TOC entry 7383 (class 0 OID 0)
-- Dependencies: 307
-- Name: resource_network_connection_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.resource_network_connection_id_seq', 1, false);


--
-- TOC entry 7384 (class 0 OID 0)
-- Dependencies: 361
-- Name: resource_network_connection_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.resource_network_connection_property_id_seq', 1, false);


--
-- TOC entry 7385 (class 0 OID 0)
-- Dependencies: 213
-- Name: resource_network_connection_type_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.resource_network_connection_type_id_seq', 1, false);


--
-- TOC entry 7386 (class 0 OID 0)
-- Dependencies: 257
-- Name: resource_network_connection_type_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.resource_network_connection_type_property_id_seq', 1, false);


--
-- TOC entry 7387 (class 0 OID 0)
-- Dependencies: 259
-- Name: resource_relationship_network_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.resource_relationship_network_id_seq', 1, false);


--
-- TOC entry 7388 (class 0 OID 0)
-- Dependencies: 363
-- Name: to_resource_reference_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.to_resource_reference_id_seq', 1, false);


--
-- TOC entry 7389 (class 0 OID 0)
-- Dependencies: 395
-- Name: to_resource_reference_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.to_resource_reference_property_id_seq', 1, false);


--
-- TOC entry 7390 (class 0 OID 0)
-- Dependencies: 215
-- Name: uom_category_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.uom_category_id_seq', 37, false);


--
-- TOC entry 7391 (class 0 OID 0)
-- Dependencies: 231
-- Name: uom_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.uom_id_seq', 145, false);


--
-- TOC entry 7392 (class 0 OID 0)
-- Dependencies: 261
-- Name: work_calendar_definition_entry_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.work_calendar_definition_entry_id_seq', 1, false);


--
-- TOC entry 7393 (class 0 OID 0)
-- Dependencies: 309
-- Name: work_calendar_definition_entry_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.work_calendar_definition_entry_property_id_seq', 1, false);


--
-- TOC entry 7394 (class 0 OID 0)
-- Dependencies: 219
-- Name: work_calendar_definition_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.work_calendar_definition_id_seq', 1, false);


--
-- TOC entry 7395 (class 0 OID 0)
-- Dependencies: 263
-- Name: work_calendar_definition_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.work_calendar_definition_property_id_seq', 1, false);


--
-- TOC entry 7396 (class 0 OID 0)
-- Dependencies: 311
-- Name: work_calendar_entry_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.work_calendar_entry_id_seq', 1, false);


--
-- TOC entry 7397 (class 0 OID 0)
-- Dependencies: 365
-- Name: work_calendar_entry_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.work_calendar_entry_property_id_seq', 1, false);


--
-- TOC entry 7398 (class 0 OID 0)
-- Dependencies: 217
-- Name: work_calendar_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.work_calendar_id_seq', 1, false);


--
-- TOC entry 7399 (class 0 OID 0)
-- Dependencies: 265
-- Name: work_calendar_property_id_seq; Type: SEQUENCE SET; Schema: common; Owner: apps
--

SELECT pg_catalog.setval('common.work_calendar_property_id_seq', 1, false);


--
-- TOC entry 7400 (class 0 OID 0)
-- Dependencies: 521
-- Name: privilege_id_seq; Type: SEQUENCE SET; Schema: guardian; Owner: apps
--

SELECT pg_catalog.setval('common.au_privilege_id_seq', 1, false);


--
-- TOC entry 7401 (class 0 OID 0)
-- Dependencies: 523
-- Name: role_privileges_id_seq; Type: SEQUENCE SET; Schema: guardian; Owner: apps
--

SELECT pg_catalog.setval('common.au_role_privileges_id_seq', 1, false);


--
-- TOC entry 7402 (class 0 OID 0)
-- Dependencies: 525
-- Name: user_restricted_privileges_id_seq; Type: SEQUENCE SET; Schema: guardian; Owner: apps
--

SELECT pg_catalog.setval('common.au_user_restricted_privileges_id_seq', 1, false);


--
-- TOC entry 7403 (class 0 OID 0)
-- Dependencies: 421
-- Name: equipment_specification_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_specification_id_seq', 1, false);


--
-- TOC entry 7404 (class 0 OID 0)
-- Dependencies: 453
-- Name: equipment_specification_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.equipment_specification_property_id_seq', 1, false);


--
-- TOC entry 7405 (class 0 OID 0)
-- Dependencies: 423
-- Name: material_specification_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.material_specification_id_seq', 1, false);


--
-- TOC entry 7406 (class 0 OID 0)
-- Dependencies: 455
-- Name: material_specification_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.material_specification_property_id_seq', 1, false);


--
-- TOC entry 7407 (class 0 OID 0)
-- Dependencies: 313
-- Name: operations_definition_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.operations_definition_id_seq', 1, false);


--
-- TOC entry 7408 (class 0 OID 0)
-- Dependencies: 367
-- Name: operations_definition_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.operations_definition_property_id_seq', 1, false);


--
-- TOC entry 7409 (class 0 OID 0)
-- Dependencies: 369
-- Name: operations_material_bill_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.operations_material_bill_id_seq', 1, false);


--
-- TOC entry 7410 (class 0 OID 0)
-- Dependencies: 397
-- Name: operations_material_bill_item_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.operations_material_bill_item_id_seq', 1, false);


--
-- TOC entry 7411 (class 0 OID 0)
-- Dependencies: 399
-- Name: operations_segment_dependency_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.operations_segment_dependency_id_seq', 1, false);


--
-- TOC entry 7412 (class 0 OID 0)
-- Dependencies: 371
-- Name: operations_segment_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.operations_segment_id_seq', 1, false);


--
-- TOC entry 7413 (class 0 OID 0)
-- Dependencies: 401
-- Name: operations_segment_mapping_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.operations_segment_mapping_id_seq', 1, false);


--
-- TOC entry 7414 (class 0 OID 0)
-- Dependencies: 403
-- Name: operations_segment_parameter_specification_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.operations_segment_parameter_specification_id_seq', 1, false);


--
-- TOC entry 7415 (class 0 OID 0)
-- Dependencies: 425
-- Name: personnel_specification_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_specification_id_seq', 1, false);


--
-- TOC entry 7416 (class 0 OID 0)
-- Dependencies: 457
-- Name: personnel_specification_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.personnel_specification_property_id_seq', 1, false);


--
-- TOC entry 7417 (class 0 OID 0)
-- Dependencies: 427
-- Name: physical_asset_specification_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_specification_id_seq', 1, false);


--
-- TOC entry 7418 (class 0 OID 0)
-- Dependencies: 459
-- Name: physical_asset_specification_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.physical_asset_specification_property_id_seq', 1, false);


--
-- TOC entry 7419 (class 0 OID 0)
-- Dependencies: 405
-- Name: work_master_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.work_master_id_seq', 1, false);


--
-- TOC entry 7420 (class 0 OID 0)
-- Dependencies: 489
-- Name: workflow_specification_connection_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_connection_id_seq', 1, false);


--
-- TOC entry 7421 (class 0 OID 0)
-- Dependencies: 511
-- Name: workflow_specification_connection_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_connection_property_id_seq', 1, false);


--
-- TOC entry 7422 (class 0 OID 0)
-- Dependencies: 221
-- Name: workflow_specification_connection_type_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_connection_type_id_seq', 1, false);


--
-- TOC entry 7423 (class 0 OID 0)
-- Dependencies: 267
-- Name: workflow_specification_connection_type_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_connection_type_property_id_seq', 1, false);


--
-- TOC entry 7424 (class 0 OID 0)
-- Dependencies: 429
-- Name: workflow_specification_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_id_seq', 1, false);


--
-- TOC entry 7425 (class 0 OID 0)
-- Dependencies: 461
-- Name: workflow_specification_node_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_node_id_seq', 1, false);


--
-- TOC entry 7426 (class 0 OID 0)
-- Dependencies: 491
-- Name: workflow_specification_node_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_node_property_id_seq', 1, false);


--
-- TOC entry 7427 (class 0 OID 0)
-- Dependencies: 223
-- Name: workflow_specification_node_type_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_node_type_id_seq', 1, false);


--
-- TOC entry 7428 (class 0 OID 0)
-- Dependencies: 269
-- Name: workflow_specification_node_type_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_node_type_property_id_seq', 1, false);


--
-- TOC entry 7429 (class 0 OID 0)
-- Dependencies: 463
-- Name: workflow_specification_property_id_seq; Type: SEQUENCE SET; Schema: operations; Owner: apps
--

SELECT pg_catalog.setval('common.workflow_specification_property_id_seq', 1, false);


-- Completed on 2019-09-16 15:24:26 PKT

--
-- PostgreSQL database dump complete
--
