# How to run:
1. Consult [`README.md`](./app/README.md) and run the main app according to its steps
2. Consult [docker-compose](./app/docker-compose.yml) and create appropriate volumes/paths and override configs in `all_configs` folder to your local
    - Add postgres connection credentials
    - Specify paths and volumes depending on OS (current paths are specified for Mac OS). *NOTE*: paths for grafana/prometheus may differ on different OSs
3. Extract the content of `all_configs` folder to `~`
4. `docker-compse up -d`
5. Ideally this happy-path should lead to all the services up and running. Go to [`localhost:3000`](http://localhost:3000) and see Grafana Backend ready. Default credentials: `admin`/`admin`.
# Detailed description: 
## Prometheus
Prometheus configs are collected in [`all_configs/prometheus`](all_configs/prometheus) folder. In this config we specify
job names and the urls prometheus will access each `n` seconds specified in `scrape_interval` to get the metrics 
provided by extractors.

These metrics include: 
- metrics from PostgreSQL (provided by PostgreSQL extractor)
- metrics from NodeJS app (provided by `prometheus-gc-stats`/`prom-client` extractors)
- default system metrics collected by Prometheus itself

### Grafana
Grafana will visualize the data provided by external datasources. First of all, we need to
create `config.ini` file and make grafana boot with this config. The most important part of this config is
this part:
```
[paths]
# Path to where grafana can store temp files, sessions, and the sqlite3 db (if that is used)
;data = /var/lib/grafana

# Temporary files in `data` directory older than given duration will be removed
;temp_data_lifetime = 24h

# Directory where grafana can store logs
;logs = /var/log/grafana

# Directory where grafana will automatically scan and look for plugins
;plugins = /var/lib/grafana/plugins

# folder that contains provisioning config files that grafana will apply on startup and while running.
;provisioning = conf/provisioning
provisioning = /etc/grafana/provisioning
```
Here we specify provisioning and plugins paths, from which grafana will try to get datasources, pre-built dashboards, plugins, etc.

The next step to make grafana work properly is to pass `datasources`. Our datasources are PostgreSQL and Prometheus, so we create
[datasource.yaml](all_configs/provisioning/datasources/datasource.yaml) and specify urls and connect credentails to be used in dashboards.

When the datasources are successfully connected we can add dasboards to visualize the data from these datasources. To do it we
need to do the following steps:
1. For each dashboard we want to use we create its `yaml` file in `provisioning/dashboards/[dashboard_name].yaml` and specify different environments,
acces rights, organizationId, etc. And, **the most important step**, we specify path to visualization config (e.g. which charts to show,
how to query data, how exactly to place charts on the page, etc.)
2. Add appropriate `json` files descrbing how to visualize the data
